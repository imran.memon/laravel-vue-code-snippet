@extends('layouts.header')
@section('content')

<!-- Main section-->
<section  id="sectionManager"class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
           <!-- Start New Manager Type  --->
         <form action="{{route('accountInsertPostStatusSettings')}}" method="post">
         @csrf
            <div class="content-heading">
               <div class="heading">{{__('Account')}} / {{__('Status')}} / {{__('New Status')}} </div>
               <div class="ml-auto">
                      <button class="btn btn-success btn-lg  " type="submit">{{__('Save')}}</button>
                       <button class="btn btn-danger btn-lg back-btn" type="button" onclick="goBack()">{{__('Back')}}</button>
               </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="card card m-4 b-01">
                         <div class="card-body">
                         <div class="row">
                          <div class="col-sm-12">
                               <div class="form-group">
                                 <label class="col-form-label">{{__('Status name')}} </label>
                                 <input class="form-control @error('status_name') is-invalid @enderror borderLeft" value="{{old('status_name')}}" type="text" name="status_name" required>
                                 @error('status_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-group">
                              <label for="Entity" class="col-form-label">{{__('Entity')}} </label> 
                                <select class="form-control  @error('entity') is-invalid @enderror borderLeft"  id="entity" name="entity">
                                  <option value="Session">{{__('Session')}}</option>
                                </select>
                                @error('entity')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="form-group">
                                <label class="col-form-label">{{__('Text color')}} </label> 
                                <select class="form-control  @error('color_code') is-invalid @enderror borderLeft"  id="color_code" name="color_code">
                                  @foreach($color as $colorCode)
                                  <option value="{{$colorCode['color_name']}}" class="font-weight-bold" style="color:{{$colorCode['color_name']}};">{{$colorCode['name']}}</option>
                                  @endforeach
                                </select>
                                @error('color_code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                          </div>

                      </div>
                     </div>
                  </div>
                </div>

                    
            </div>
            </form>
              <!-- End New Manager Type  --->
         </div>
</section>
@endsection