@extends('app_user.app_user_header')

@section('app_user_content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="main_section new-session-section scheduler_screen" style="height:100% !important;width:auto !important;">
    <div class="loader-container">
        <div class="loader loader-join"></div>
    </div>
    <div class="join_session final_class ">
        <div class="header">
            <div class="header-inner">
                <div class="back-arrow">
                    <span class="backScreen"><img src="{{asset('/img/back-arrow.svg')}}" alt=""></span>
                </div>
                <div class="title">
                    {{__("Edit session")}}
                </div>
                <div class="right"></div>
            </div>
        </div>
        <form action="{{route('changeBookOperatorMeeting',@$meeting_id)}}" method="post">
            @csrf
            <div class="form-container">
                @if ($errors->has('meetingDate'))
                    <p><span class="text-danger">{{ $errors->first('meetingDate') }}</span></p>
                @endif
                
                @if ($errors->has('tSlot'))
                    <p><span class="text-danger">{{ $errors->first('tSlot') }}</span></p>
                @endif
                <div class="select-date-section">
                    <div class="title">
                        {{__("Select a date")}}
                    </div>
                    <div class="calendar-new-session">
                       
                    </div>
                </div>
                <div class="select-time-section">
                    <div class="title">
                        {{__("Select a time")}}
                    </div>
                    <div class="time-select timeSlot">
                        <div class="btn-group-toggle" data-toggle="buttons">
                            
                          </div>
                    </div>
                </div>
                {{-- <div class="note-section">
                    <div class="form-group">
                        <label for="my-textarea">Add some notes for the session</label>
                        <textarea id="my-textarea" class="form-control" name="" rows="3"></textarea>
                    </div>
                </div> --}}
                <div class="footer">
                    <div class="footer-inner">
                        <input type="submit" class="submit-button" name="login_submit" value="{{__('Change Booking Session')}}">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <input type="hidden" name="schedule_id" class="schedule_id" value="{{@$schedule_id}}">
</section>
@endsection
@section('app_user_script')
    <script>
        var FinalDataResult = {};
        var datePicker,dataDates,scheduleDates=[];
        $(document).ready(function(){
            $(".backScreen").click(function(){
                window.history.back();
            });
            $(document).on("change",".time-select-input",function(){
                $(".timer-lable").removeClass("active");
                $(this).find(".timer-lable").addClass("active");
                $(".note-section").removeClass("d-none");
                var schedulerId = $(this).attr('data-schedulerid');
                $("#scheduleId").val(schedulerId);
            });
            var schedule_id = $(".schedule_id").val();
            //Date Click Event Start
                datePicker = $('.calendar-new-session').datepicker({dateFormat: 'dd/mm/y',
                    beforeShowDay: function(date){
                            console.log(date);
                        }})
                    .on('changeDate', function(e) {
                    var html = '';
                    var selectOperatorId = $(".operatorList").val();
                    var seldate=e.date; 
                    var curr_date = seldate.getDate();
                    var curr_month = seldate.getMonth()+1;
                    var curr_year = seldate.getFullYear();
                    var compareDate = curr_year +'-'+ curr_month +'-'+ curr_date;
                    
                    if(compareDate in FinalDataResult){
                        var scheduleId =  FinalDataResult['scheduleId'];
                        var date =  FinalDataResult['date'];
                        html += '<input type="hidden" name="meetingDate" id="meetingDate" value="'+compareDate+'">';  
                        html += '<input type="hidden" name="scheduleId" id="scheduleId" value="'+schedule_id+'">';
                           
                        $.each(FinalDataResult[compareDate], function(index, value) {
                            html += '<div class="btn-group-toggle" data-toggle="buttons">';
                                html += '<label class="btn btn-secondary timer-lable">';
                                    html += '<input class="time-select-input" type="radio" name="tSlot" id="option1" autocomplete="off" value="'+index+'" data-schedulerid="'+FinalDataResult[compareDate+"_"]+'">'+ value;
                                html += '</label>';
                            html += '</div>';
                           
                        });
                    }
                    $(".timeSlot").html(html);

                    if (curr_date.toString().length == 1) {
                        curr_date = "0" + curr_date;
                    }
                    if (curr_month.toString().length == 1) {
                        curr_month = "0" + curr_month;
                    }
                    var finalDate = curr_year +'-'+ curr_month +'-'+ curr_date;
                    var data = { date:finalDate, operatorId:selectOperatorId};
                    $('html, body').animate({
                        scrollTop: $(".select-time-section").offset().top
                    }, 2000);
                });
            //Date Click Event End

            ajaxCall();

            //Function Ajax Call Starts
                function ajaxCall(){
                    var route = '{{ route("bookingChangeDateAjax") }}';
                    var data = { schedule_id : schedule_id };
                    var dataDates = [];
                    var scheduleDates = [];
                    $(".loader-container").addClass("show-loader");
                    $.ajax({
                        url: route,
                        type: "GET",
                        data:  data,
                        dataType: 'json',
                        success: function(resp){
                            $.each(resp, function(index, value) {
                                var startDate = value.start;
                                var date = new Date(startDate); 
                                var day = date.getDate();
                                var month = date.getMonth();
                                var year = date. getFullYear();
                                dataDates.push(new Date(year, month, day));
                                scheduleDates.push(value.date);
                                var newMonth = month+1;
                                var dateId = year+'-'+newMonth+'-'+day;
                                FinalDataResult[dateId] = value.meetings;
                                FinalDataResult[dateId+"_"] = value.id;
                                FinalDataResult['scheduleId'] = value.id;
                                FinalDataResult['date'] = dateId;
                                FinalDataResult['operatorId'] = value.operatorId;
                                
                            });
                            $(".loader-container").removeClass("show-loader");
                            $( ".calendar-new-session" ).datepicker( "destroy" );
                            $('.calendar-new-session').datepicker({
                                beforeShowDay: function(d){
                                    var curr_date = d.getDate();
                                    var curr_month = d.getMonth() + 1; //Months are zero based
                                    curr_month = (curr_month > 9 ) ? curr_month : "0"+curr_month;

                                    curr_date = (curr_date > 9 ) ? curr_date : "0"+curr_date;

                                    var curr_year = d.getFullYear();
                                    var formattedDate = curr_year + "-" + curr_month + "-" + curr_date;
                                    console.log(scheduleDates);
                                    console.log(formattedDate);
                                    if ($.inArray(formattedDate, scheduleDates) != -1)
                                    {
                                        console.log("MAtch"+formattedDate);
                                        return {
                                            classes: 'can_meeting'
                                        };
                                    }else {
                                        return {
                                            enabled:false
                                        }
                                    }
                                    //console.log(date);
                                }
                            });
                        }
                    });
                }
            //Function Ajax call Ends
        });
    </script>
    
@endsection