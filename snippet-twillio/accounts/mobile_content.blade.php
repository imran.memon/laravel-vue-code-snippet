@extends('layouts.header')
@section('content')
   <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
   <section id="sectionManager" class="section-container">
      <div class="content-wrapper">
         <div class="content-heading">
            <div class="heading">{{__('Account')}}/{{__('Mobile Contents')}}</div>
         </div>
         
         <div role="tabpanel">
            {{-- <ul class="nav nav-tabs nav-fill pt-3 pr-3 pl-3 bg-white" role="tablist">
               <li class="nav-item " role="presentation"> 
                    <a class="nav-link bb0 appScreenText {{ !empty(auth()->user()->account_id) ? 'active' : ''}}" href="#mobile_content" aria-controls="mobile_content" role="tab" data-toggle="tab" aria-selected="false">
                        {{__('Mobile Content')}}
                    </a>
               </li>
            </ul> --}}
            <div class="tab-content p-0">
             
               <!-- Mobile Contect Starts -->
               <div class="tab-pane  active" id="mobile_content" role="tabpanel">
                        @csrf
                        <div class="row">
                           <div class="col-sm-5">
                              <div class="card card-transparent p-3">
                                    <div class="card-body p-0">
                                          <ul class="list-group">
                                                @foreach($getMobileData as $getMobileDataVal)
                                                    <li class="dd-item dd3-item " data-id="41">
                                                        <div class="dd3-content">{{__($getMobileDataVal->module_name)}}
                                                           <div class="form-group float-right csrf">
                                                              @csrf
                                                              <button class="btn btn-info p-0 btn_p editMobileContent" data-id="{{$getMobileDataVal->id}}"><i class="fa fa-edit"></i>
                                                              </button>
                                                           </div>
                                                        </div>
                                                    </li>
                                                @endforeach 
                                          </ul>
                                      </div>
                              </div>
                              
                           </div>
                            <div class="col-sm-7 editMobileConDiv">
                                <form id="MobileContectForm" action={{route('AccountUpdateMobileContent')}} method="post" data-parsley-validate="" > 
                                 @csrf
                                 <div class="card p-3 mr-3 my-3"> 
                                    <div class="card-header">
                                       <div class="row">
                                          <div class="col-8">
                                             <h4 class="head_text_template"></h4>
                                          </div>
                                          <input type="hidden" class="emailId" name="id" />
                                          <div class="col-4 text-right">
                                             <button class="btn btn-success" type="submit">{{__('Save')}}</button>
                                             <button class="btn btn-danger back-btn" type="button" >{{__('Close')}}</button>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_english" class="mobileContentHtmlBody_english">
                                                      <input type="hidden" name="mobileContentId" class="mobileContentId">
                                                      <label class="col-form-label">{{__('Content Text in  English')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_english" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_hebrew" class="mobileContentHtmlBody_hebrew">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text In Hebrew')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_hebrew"  ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_arabic" class="mobileContentHtmlBody_arabic">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in Arabic')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_arabic"></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_chinese" class="mobileContentHtmlBody_chinese">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in Chinese')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_chinese" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div><div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_czech" class="mobileContentHtmlBody_czech">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in Czech')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_czech" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_french" class="mobileContentHtmlBody_french">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in French')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_french" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div><div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_german" class="mobileContentHtmlBody_german">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in German')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_german" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div><div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_hindi" class="mobileContentHtmlBody_hindi">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in Hindi')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_hindi" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_italian" class="mobileContentHtmlBody_italian">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text In Italian')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_italian" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_japanese" class="mobileContentHtmlBody_japanese">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in Japanese')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_japanese" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_portuguese" class="mobileContentHtmlBody_portuguese">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in Portuguese')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_portuguese" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_russian" class="mobileContentHtmlBody_russian">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in Russian')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_russian" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-body ">
                                       <div class="templateText">
                                          <div class="row">
                                                <div class="col-12">
                                                   <div class="form-group">
                                                      <input type="hidden" name="mobileContentHtmlBody_spanish" class="mobileContentHtmlBody_spanish">
                                                      <!-- <input type="hidden" name="mobileContentId" class="mobileContentId"> -->
                                                      <label class="col-form-label">{{__('Content Text in Spanish')}}</label>
                                                        @if ($errors->has('name'))
                                                               <span class="text-danger">{{ $errors->first('name') }}</span>
                                                        @endif
                                                        <div class="mobileContHtml">
                                                            <textarea id="mobileContent_spanish" ></textarea>
                                                        </div>
                                                   </div>                                
                                                </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                            </div>
                        </div>
                  </div>
               <!-- Mobile Contect Ends -->
            </div>
         </div>
      </div>
      
   </section>
   @if (Session::has('general'))
      <script type="text/javascript">
         $(document).ready(function(){
            $(".{{ session('general') }}").trigger("click");
         });
            
      </script>
   @endif
   <script src="{{asset('js/select2.min.js')}}"></script>
   <script type="text/javascript">
      $(document).ready(function(){
        $(".approved_file_extensions").select2({
          tags: true,
        });
         
      });

      $('.nav-tabs a').on('shown.bs.tab', function(event){
         if($(event.target).hasClass('genBtn')){
            $('.saveBtn').attr('form','updateGeneralSettings');
            $('.saveBtn').attr('type','submit');
            $('.saveBtn').show();
         } else if($(event.target).hasClass('apiBtn')){
            $('.saveBtn').attr('form','updateApiSettings');
            $('.saveBtn').attr('type','submit');
            $('.saveBtn').show();
         } else {
            $('.saveBtn').attr('form','');
            $('.saveBtn').attr('type','button');
            $('.saveBtn').hide();
         }
      });
   </script>
   <script type="text/javascript">
        var editor_english = CKEDITOR.replace('mobileContent_english', {});
        var editor_hebrew = CKEDITOR.replace('mobileContent_hebrew', {contentsLangDirection: 'rtl'});
        var editor_arabic = CKEDITOR.replace('mobileContent_arabic',  { contentsLangDirection: 'rtl'});
        var editor_chinese = CKEDITOR.replace('mobileContent_chinese', {});
        var editor_czech = CKEDITOR.replace('mobileContent_czech', {});
        var editor_french = CKEDITOR.replace('mobileContent_french', {});
        var editor_german = CKEDITOR.replace('mobileContent_german', {language: 'de'});
        var editor_hindi = CKEDITOR.replace('mobileContent_hindi', {language: 'hi'});
        var editor_italian = CKEDITOR.replace('mobileContent_italian', {});
        var editor_japanese = CKEDITOR.replace('mobileContent_japanese', {});
        var editor_portuguese = CKEDITOR.replace('mobileContent_portuguese', {});
        var editor_russian = CKEDITOR.replace('mobileContent_russian', {});
        var editor_spanish = CKEDITOR.replace('mobileContent_spanish', {});
        CKEDITOR.instances['mobileContent_english'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_english").val(editor_english.getData());
        });
        CKEDITOR.instances['mobileContent_hebrew'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_hebrew").val(editor_hebrew.getData());
        });
        CKEDITOR.instances['mobileContent_arabic'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_arabic").val(editor_arabic.getData());
        });
        CKEDITOR.instances['mobileContent_chinese'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_chinese").val(editor_chinese.getData());
        });
        CKEDITOR.instances['mobileContent_czech'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_czech").val(editor_czech.getData());
        });
        CKEDITOR.instances['mobileContent_french'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_french").val(editor_french.getData());
        });
        CKEDITOR.instances['mobileContent_german'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_german").val(editor_german.getData());
        });
        CKEDITOR.instances['mobileContent_hindi'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_hindi").val(editor_hindi.getData());
        });
        CKEDITOR.instances['mobileContent_italian'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_italian").val(editor_italian.getData());
        });
        CKEDITOR.instances['mobileContent_japanese'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_japanese").val(editor_japanese.getData());
        });
        CKEDITOR.instances['mobileContent_portuguese'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_portuguese").val(editor_portuguese.getData());
        });
        CKEDITOR.instances['mobileContent_russian'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_russian").val(editor_russian.getData());
        });
        CKEDITOR.instances['mobileContent_spanish'].on( 'change', function( evt ) {
          $(".mobileContentHtmlBody_spanish").val(editor_spanish.getData());
        });
        $('.editMobileConDiv').hide();
        $('.back-btn').on('click', function(){
          $('.editMobileConDiv').hide();
        });
        $('.editMobileContent').on('click', function(){
                $("#MobileContectForm").attr('action', '{{route("AccountUpdateMobileContent")}}');
                $('.editMobileConDiv').show();
                $('.head_text_message').text("Edit Message");
                var id = $(this).data('id');
                var token = $(this).parent('.csrf').find("input[name=_token]").val();
                data = new FormData();
                data.append('id', id);
                data.append('_token',token);
                $.ajax({
                   url: "{{ route('accountGetMobileContent') }}",
                   type: "POST",
                   data:  data,
                   dataType: 'json',
                   processData: false,
                   contentType: false,
                   success: function(resp){
                      CKEDITOR.instances['mobileContent_english'].setData(resp.content_text_english);
                      CKEDITOR.instances['mobileContent_hebrew'].setData(resp.content_text_hebrew);
                      CKEDITOR.instances['mobileContent_arabic'].setData(resp.content_text_arabic);
                      CKEDITOR.instances['mobileContent_chinese'].setData(resp.content_text_chinese);
                      CKEDITOR.instances['mobileContent_czech'].setData(resp.content_text_czech);
                      CKEDITOR.instances['mobileContent_french'].setData(resp.content_text_french);
                      CKEDITOR.instances['mobileContent_german'].setData(resp.content_text_german);
                      CKEDITOR.instances['mobileContent_hindi'].setData(resp.content_text_hindi);
                      CKEDITOR.instances['mobileContent_italian'].setData(resp.content_text_italian);
                      CKEDITOR.instances['mobileContent_japanese'].setData(resp.content_text_japanese);
                      CKEDITOR.instances['mobileContent_portuguese'].setData(resp.content_text_portuguese);
                      CKEDITOR.instances['mobileContent_russian'].setData(resp.content_text_russian);
                      CKEDITOR.instances['mobileContent_spanish'].setData(resp.content_text_spanish);
                      $('.mobileContentId').val(id);
                   }
               });
           });
   </script>

@endsection