@extends('app_user.app_user_header')

@section('app_user_content')
<style type="text/css">html, body {
  height: 100% !important;
}</style>
<section class="main_section new-session-section scheduler_screen" style="height:100% !important;width:auto !important;">
    <div class="join_session final_class">
        <div class="header">
            <div class="header-inner">
                <div class="back-arrow">
                    <span class="backScreen"><img src="../img/back-arrow.svg" alt=""></span>
                </div>
                <div class="title">
                    {{__("Login")}}
                </div>
                <div class="right"></div>
            </div>
        </div>
        <div class="form-container">
            <div class="col-sm-12 text-center   ">
              <h5>{!!(@$page_content->value)!!}</h5 >
            </div>
            <form id="appUserLoginForm" action="{{route('accountSessionLoginScreen',@$account_data->url_name)}}" method="post" style="min-height: auto !important">
              @csrf
                <div class="form-group">
                  <label for="name">{{__("Name")}}</label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror borderLeft" id="name" placeholder="{{__('Enter name')}}" name="name" required value="{{old('name')}}">
                  @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                  @endif

                </div>
                <div class="form-group forboth">
                  <div class="country">
                       <label class="col-form-label">{{__('Country Code')}} </label>
                        <select id="countryCode" name="country_code" class="form-control" required>
                      @foreach($countryCodes as $co)
                      <option value="{{"+".$co->country_code}}" {{$co->country_name_en=="Israel"?"selected":" "}}>{{"+".$co->country_code}}</option>
                      @endforeach
                        </select>
                  </div>
                  <div>
                        <label for="phone">{{__("Phone")}}</label>
                        <input type="text" class="form-control" id="phone" placeholder="{{__('Enter phone')}}" name="phone" required value="{{old('phone')}}">
                           @if ($errors->has('phone'))
                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                  </div>
                </div>

                <div class="form-group">
                  
                </div>
                <div class="form-group">
                  <label for="Email">{{__("Email")}}</label>
                  <input type="email" class="form-control" id="email" placeholder="{{__('Enter email')}}" name="email" required value="{{old('email')}}">
                  @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <div class="footer">
                    <div class="footer-inner">
                        <input type="submit" class="submit-button" name="login_submit" value="{{__('Login')}}">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('app_user_script')
    <script type="text/javascript">
        $(document).ready(function(){
            $(".backScreen").click(function(){
                window.history.back();
            });
            $("#appUserLoginForm").validate({
               errorClass:'text-danger custom-error'
            });
            jQuery.extend(jQuery.validator.messages, {
              required: "",
              email: ""
            });
        });

    </script>
@endsection
