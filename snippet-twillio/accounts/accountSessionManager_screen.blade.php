@extends('app_user.app_user_header')

@section('app_user_content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="main_section new-session-section scheduler_screen">
    <div class="loader-container">
        <div class="loader loader-join"></div>
    </div>
    <div class="join_session final_class ">
        <div class="header">
            <div class="header-inner">
                <div class="back-arrow">
                    <span class="backScreen"><img src="../img/back-arrow.svg" alt=""></span>
                </div>
                <div class="title">
                    {{__("New Session")}}
                </div>
                <div class="right"></div>
            </div>
        </div>
        <form id="bookingMeeting" action="{{route('bookOperatorMeeting')}}" method="post" >
            @csrf
            <div class="form-container">
                <div class="form-group">
                    <label for="name">{{__("Filter operator by type")}}</label>
                    <select class="form-control operatorTypeList" >
                        <option data-class="operator_show_all" value="">{{__("All operator type")}}</option>
                        @foreach(@$types as $type)
                            <option data-class="operator_show_{{$type->id}}" value="{{$type->id}}" >{{$type->title}}</option>
                        @endforeach
                    </select>
                   
                   
                </div>

                <div class="form-group">
                    <input type="hidden" name="accountId" class="accountId" value="{{@$accountData->id}}">
                    <label for="name">{{__("Select a representative")}}</label>
                    <select class="form-control operatorList" required>
                        <option value="">{{__("Select Operator")}}</option>
                        @foreach(@$managers as $manager)
                            <option class="operator_show_all operator_show_{{$manager['op_type']}}" value="{{$manager['id']}}" >{{$manager['first_name']}} {{$manager['last_name']}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('custom_error'))
                        <p><span class="text-danger custom-error">{{ $errors->first('custom_error') }}</span></p>
                    @endif
                   
                </div>
                <div class="select-date-section  d-none">
                    <div class="title">
                        {{__("Select a date")}}
                    </div>
                    <div class="calendar-new-session">
                       
                    </div>
                </div>
                <div class="select-time-section d-none">
                    <div class="title">
                        {{__("Select a time")}}
                    </div>
                    <div class="time-select timeSlot">
                        <div class="btn-group-toggle" data-toggle="buttons">
                            
                          </div>
                    </div>
                </div>
                <div class="note-section d-none">
                    <div class="form-group">
                        <label for="my-textarea">{{__("Add some notes for the session")}}</label>
                        <textarea id="my-textarea" class="form-control" name="meeting_notes" rows="3"></textarea>
                    </div>
                </div>
                <div class="footer">
                    <div class="footer-inner">
                        <input type="submit" class="submit-button" name="login_submit" value="{{__('BOOK A SESSION')}}">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection
@section('app_user_script')
    <script>
        var FinalDataResult = {};
        var datePicker,dataDates,scheduleDates=[];
        $(document).ready(function(){
            $(".operatorTypeList").on("change",function(){
                className=  $('option:selected', this).attr('data-class');
                console.log(className);
                $('.operatorList').val("").trigger("change");
                $(".operator_show_all").css("display",'none');
                $("."+className).css("display","block");
                $(".operator_show_all").prop("disabled",true);
                $("."+className).prop("disabled",false);
                
            });
            $("#bookingMeeting").validate({
                errorClass:'text-danger custom-error'
            });
            $(document).on("change",".time-select-input",function(){
                $(".timer-lable").removeClass("active");
                $(this).find(".timer-lable").addClass("active");
                $(".note-section").removeClass("d-none");
                var schedulerId = $(this).attr('data-schedulerid');
                $("#scheduleId").val(schedulerId);
                $('html, body').animate({
                    scrollTop: $("#my-textarea").offset().top
                }, 2000);
            });
            $(".backScreen").click(function(){
                window.history.back();
            });
            var accountId = $('.accountId').val();
            //Date Click Event Start
            datePicker = $('.calendar-new-session').datepicker({dateFormat: 'dd/mm/y',beforeShowDay: function(date){
                    console.log(date);
                }})
                .on('changeDate', function(e) {
                var html = '';
                var selectOperatorId = $(".operatorList").val();
                $(".select-time-section").removeClass("d-none");
                var seldate=e.date;
                var curr_date = seldate.getDate();
                var curr_month = seldate.getMonth()+1;
                var curr_year = seldate.getFullYear();
                var compareDate = curr_year +'-'+ curr_month +'-'+ curr_date;
                console.log(compareDate);
                console.log(FinalDataResult);
                    if(compareDate in FinalDataResult){
                        var scheduleId =  FinalDataResult['scheduleId'];
                        var date =  compareDate;
                        var operatorId =  FinalDataResult['operatorId'];
                        html += '<input type="hidden" name="meetingDate" id="meetingDate" required   value="'+date+'">';  
                        html += '<input type="hidden" name="scheduleId" id="scheduleId" value="'+scheduleId+'">';
                        html += '<input type="hidden" name="operatorId" id="operatorId" value="'+operatorId+'">'; 
                        $.each(FinalDataResult[compareDate], function(index, value) {
                            html += '<div class="btn-group-toggle" data-toggle="buttons">';
                                html += '<label class="btn btn-secondary timer-lable ">';
                                    html += '<input required class="time-select-input" type="radio" name="tSlot" id="" autocomplete="off" value="'+index+'" data-schedulerid="'+FinalDataResult[compareDate+"_"]+'">'+ value;
                                html += '</label>';
                            html += '</div>';
                            
                        });
                    }
                    $(".timeSlot").html(html);

                    if (curr_date.toString().length == 1) {
                        curr_date = "0" + curr_date;
                    }
                    if (curr_month.toString().length == 1) {
                        curr_month = "0" + curr_month;
                    }
                    
                    var finalDate = curr_year +'-'+ curr_month +'-'+ curr_date;
                    var data = { date:finalDate ,accountId:accountId, operatorId:selectOperatorId};
                    //window.scrollTo(0,$(".select-time-section").offset().top);
                    // $('html, body').animate({
                    //     scrollTop: $(".select-time-section").offset().top
                    // }, 2000);
                    // setTimeout(function(){
                     $('html, body').animate({
                       scrollTop: $(".select-time-section").offset().top
                     }, 2000)
                      // window.scrollTo(0, 400);
                    // },1000);
                });
            //Date Click Event End

            //DropDown Change Event Starts
                $('.operatorList').change(function(){
                    var operatorId = $(this).val();
                    $(".timeSlot").html("");
                    if(operatorId != '') {
                        $(".custom-error").addClass("d-none");
                        $(".select-date-section ").removeClass("d-none");
                        $(".select-time-section ").removeClass("d-none");
                    } else {
                        $(".select-date-section ").addClass("d-none");
                        $(".select-time-section ").addClass("d-none");
                    }
                    FinalDataResult = {};
                    ajaxCall(operatorId,accountId);
                });
            //Dropdown Change Event Ends

            //Function Ajax Call Starts
                function ajaxCall(operatorId , accountId){
                    var route = '{{ route("getOperatorMeetingList") }}';
                    var data = { accountId : accountId ,operatorId:operatorId};
                    var dataDates = [];
                    var scheduleDates = [];
                    $(".loader-container").addClass("show-loader");
                    $.ajax({
                        url: route,
                        type: "GET",
                        data:  data,
                        dataType: 'json',
                        success: function(resp){
                            console.log(resp);
                            
                            $.each(resp, function(index, value) {
                                var startDate = value.start;
                                var date = new Date(startDate); 
                                var day = date.getDate();
                                var month = date.getMonth();
                                var year = date. getFullYear();
                                dataDates.push(new Date(year, month, day));
                                scheduleDates.push(value.date);
                                console.log(dataDates);
                                var newMonth = month+1;
                                var dateId = year+'-'+newMonth+'-'+day;
                                console.log(value.operatorId + "<>" + operatorId);
                               // if(operatorId == value.operatorId){
                                    FinalDataResult[dateId] = value.meetings;
                                    FinalDataResult[dateId+"_"] = value.id;
                                    FinalDataResult['scheduleId'] = value.id;
                                    FinalDataResult['date'] = dateId;
                                    FinalDataResult['operatorId'] = value.operatorId;
                               // }
                            });
                            
                            $(".loader-container").removeClass("show-loader");
                            //console.log( FinalDataResult);
                            $( ".calendar-new-session" ).datepicker( "destroy" );
                            
                            $('.calendar-new-session').datepicker({
                                beforeShowDay: function(d){
                                    var curr_date = d.getDate();
                                    var curr_month = d.getMonth() + 1; //Months are zero based
                                    curr_month = (curr_month > 9 ) ? curr_month : "0"+curr_month;

                                    curr_date = (curr_date > 9 ) ? curr_date : "0"+curr_date;

                                    var curr_year = d.getFullYear();
                                    var formattedDate = curr_year + "-" + curr_month + "-" + curr_date;
                                     console.log(scheduleDates);
                                     console.log(formattedDate);
                                    if ($.inArray(formattedDate, scheduleDates) != -1)
                                    {
                                       // console.log("MAtch"+formattedDate);
                                        return {
                                            classes: 'can_meeting'
                                        };
                                    } else {
                                        return {
                                            enabled:false
                                        }
                                    }
                                    //console.log(date);
                                }
                            });
                            
                        }
                    });
                    
                }
            //Function Ajax call Ends
        });
    </script>
    
@endsection