@extends('app_user.app_user_header')

@section('app_user_content')
<style type="text/css">html, body {
  height: 100% !important;
}</style>
<section class="main_section new-session-section scheduler_screen" style="height:100% !important;width:auto !important;">
    <div class="join_session final_class">
        <div class="header">
            <div class="header-inner">
                <div class="back-arrow">
                    <span class="backScreen"><img src="{{asset('/img/back-arrow.svg')}}" alt=""></span>
                </div>
                <div class="title">
                    {{__("Verification Code")}}
                </div>
                <div class="right"></div>
            </div>
        </div>
        <div class="form-container">
            <div class="col-sm-12 text-center   ">
                <h5>{!!(@$page_content->value)!!}</h5 >
              </div>
            <form action="{{route('accountSessionOTPScreen',['accountUrl' => @$accountUrl,'userId' => @$userId])}}" method="post" style="min-height: auto !important">
              @csrf 
                <div class="form-group">
                  <label for="name">{{__("Enter verification code")}}</label>
                  <input type="text" class="form-control" id="varificationCode" placeholder="{{__('Enter code')}}" name="varificationCode">
                  @if($errors->any())
                    <span class="text-danger">{{$errors->first()}}</span>
                    @endif
                </div>
                <div style="text-align: center;">
                    <a href="{{route('accountSessionSendOtpEmail',['accountUrl' => @$accountUrl,'userId' => @$userId])}}">{{__("Did not get the email?")}}</a>
                </div>
                <div class="footer">
                    <div class="footer-inner">
                        <input type="submit" class="submit-button" name="login_submit" value="{{__('Next')}}">
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</section>
@endsection
@section('app_user_script')
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".backScreen").click(function(){
                window.history.back();
            });
        });
    </script>
@endsection


