@extends('app_user.app_user_header')

@section('app_user_content')

<section class="main_section bg_color scheduler_screen">
    
<div class="join_session final_class success-meeting">
    <div class="loader-container">
        <div class="loader loader-join"></div>
    </div>
        <a href="#" class="logo custom-col">
            @if(isset($account_data->logo) && $account_data->logo != '')
                <img class="img-fluid px-3" src="{{url('uploads/'.$account_data->logo)}}"></a>
            @else
                <img class="img-fluid px-3" src="{{url((new \App\Libraries\GeneralSetting)->getSettingInfo()['app_logo'])}}"></a>
            @endif
        <div class="join_Session_text custom-col ">
            {!!(@$page_content->value)!!}
        </div>
        <style type="text/css" media="screen">
            body, html, .main_section{
                height: 100vh;
            }
            .main_section{
                padding: 0;
            }
        </style>
        
    </div>
</section>
@endsection

@section('app_user_script')
@endsection