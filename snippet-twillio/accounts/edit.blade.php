@extends('layouts.header')
@section('content')
 <!-- Main section-->
<!---Side Bar End-->
      <!-- Main section-->
      <section  id="sectionManager" class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
          <form  id="editAccount" name="editAccount" action="{{route('accounts.update',$account->id)}}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="content-heading">
               <div class="heading">{{__('Accounts')}}/{{__('Edit Account')}}</div>
               <div class="ml-auto">
                    <button class="btn btn-success btn-lg  " type="submit">{{__('Save')}}</button>
                    <button class="btn btn-danger btn-lg back-btn" type="button" onclick="goBack()">{{__('Back')}}</button>
               </div>
            </div>
            <div class="card" role="tabpanel">
                @if (Session::has('status') && session('status') == 200 )
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{session('msg')}}
                </div>
            @endif
            @if (Session::has('status') && session('status') == 404 )
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{session('msg')}}
                </div>
            @endif
               <div class="tab-content p-0 bg-white">
                    <div class="tab-pane active" id="home" role="tabpanel">
                        <div class="row p-4">
                           <div class="col-sm-6">
                            <!-- START card-->
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Name')}} <sup class="text-danger">*</sup></label>
                                                    <input class="form-control @error('name') is-invalid @enderror borderLeft"  name="name" type="text"  value="{{$account->name}}" required>
                                                    @if ($errors->has('name'))
                                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>                                              
                                            </div>
                                            
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Email')}} <sup class="text-danger">*</sup></label>
                                                    <input class="form-control @error('email') is-invalid @enderror borderLeft"  name="email" type="email" value="{{$account->email}}" required>
                                                    @if ($errors->has('email'))
                                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>                                              
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="checkbox c-checkbox">
                                                        <label class="pl-0">
                                                            <input name="require_rec" type="checkbox" value="1" {{$account->require_rec  == 1 ? 'checked' : ''}}>
                                                            <span class="fa fa-check"></span> 
                                                            {{__('Allow Recording')}}
                                                        </label>
                                                    </div>
                                                </div> 
                                                <div class="form-group">
                                                    <div class="checkbox c-checkbox">
                                                        <label class="pl-0">
                                                            <input name="require_loc" type="checkbox" value="1" {{$account->require_loc == 1? 'checked' : ''}}>
                                                            <span class="fa fa-check"></span> 
                                                            {{__('Allow Location')}}
                                                        </label>
                                                    </div>
                                                </div>                                            
                                            </div>
                                            @if(!isset($statusChange) || $statusChange) 
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="col-form-label ">{{__('Status')}}</label>
                                                    <div class="c-radio">
                                                        <label>
                                                            <input type="radio" name="status" value="1" {{($account->status == 1) ? 'checked' : ''}}>
                                                            <span class="fa fa-circle"></span> Active
                                                        </label>
                                                    </div>
                                                    <div class="c-radio">
                                                        <label>
                                                            <input type="radio" name="status" value="0" {{($account->status == 0) ? 'checked' : ''}}>
                                                            <span class="fa fa-circle"></span> Inactive
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-sm-12 ">
                                                <div class="form-group">
                                                    <label class="col-form-label">{{__('Logo')}} <sup class="text-danger">*</sup></label>
                                                    <input class="form-control dropify @error('logo') is-invalid @enderror borderLeft"  type="file" name="logo" id="input-file-now" data-allowed-file-extensions="jpg png jpeg" data-default-file="{{url('/').'/uploads/'.$account->logo}}" >
                                                    
                                                    @if ($errors->has('logo'))
                                                        <span class="text-danger">
                                                            {{ $errors->first('logo') }}
                                                        </span>
                                                    @endif
                                                </div>                                            
                                            </div>
                                        </div>                                           
                                    </div>                                           
                               </div><!-- END card-->
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
              </form>
         </div>
      </section>
      
@endsection

@section("script") 
      <script >
          $(document).ready(function(){
                $("#editAccount").validate();
          });
      </script>
@endsection