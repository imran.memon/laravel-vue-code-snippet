<div class="content-wrapper">
   <form  id="editSchedule" name="editSchedule" action="{{route('AccountUpdateSchedule',$account_id)}}" method="post" enctype="multipart/form-data">
      @csrf
      <input class="form-control account_id"  name="account_id" type="hidden" value="{{ @$account_id }}">
      <div class="row p-4">
         <div class="col-sm-12">
            <div class="card">
               <div class="card-body">
                  <div class="row">
                     <!-- Schedule Link Starts -->
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="col-form-label">{{__('Schedule link')}}<sup class="text-danger"></sup></label>
                              <input class="form-control copyUrlInput"  name="schedule_link" type="text" value="{{ @$account_url_name }}"readonly>
                              
                              <button class="btn copyUrlBtn btn-md  pull-right" type="submit" style="color:#fff;">{{__('Copy URL')}}</button>
                           </div> 
                        </div>
                     <!-- Schedule Links Ends -->

                     <!-- Meeting Reminder Starts -->
                        <div class="col-sm-12">
                        <div class="form-group">
                           <div class="checkbox c-checkbox">
                              <label class="pl-0">
                                 <input name="meeting_reminder" type="checkbox" value="1" 
                                  {{@$getAccountSchedule->meeting_reminder  == 1 ? 'checked' : ''}}>
                                 <span class="fa fa-check"></span> 
                                 {{__('Send meeting reminders to operator')}}
                              </label>
                           </div>
                           
                        </div>
                        </div>
                     <!-- Meeting Reminder Ends -->

                     <!-- First Meeting Reminder Starts -->
                        <div class="col-sm-12">
                           <div class="form-group schedule_form_group">
                              <div class="checkbox c-checkbox schedule_checkbox">
                                 <label class="pl-0">
                                    <input name="first_meeting_reminder" type="checkbox" value="1" {{@$getAccountSchedule->first_meeting_reminder  == 1 ? 'checked' : ''}} class="remainder_chk"    data-related-field="first_meeting_value,first_meeting_value_type">
                                    <span class="fa fa-check"></span> 
                                    {{__('First meeting reminder')}}
                                 </label>
                              </div>
                              <input class="form-control schedule_input" name="first_meeting_value" type="number" min="0"  value="{{@$getAccountSchedule->first_meeting_value}}">
                              <select class="login_lang_dropdown_list form-control  schedule_select" name="first_meeting_value_type">
                                 <option value="1" @if(@$getAccountSchedule->first_meeting_value_type == 1) selected @endif>{{__("Days")}}</option>
                                 <option value="2" @if(@$getAccountSchedule->first_meeting_value_type == 2) selected @endif>{{__("Hours")}}</option>
                                 <option value="3" @if(@$getAccountSchedule->first_meeting_value_type == 3) selected @endif>{{__("Minutes")}}</option>
                              </select>
                           </div>
                        </div>
                     <!-- First Meeting Reminder Ends -->

                     <!-- Second Meeting Reminder Starts -->
                        <div class="col-sm-12">
                           <div class="form-group schedule_form_group">
                              <div class="checkbox c-checkbox schedule_checkbox">
                                 <label class="pl-0">
                                    <input name="second_meeting_reminder" type="checkbox" value="1"  {{@$getAccountSchedule->second_meeting_reminder  == 1 ? 'checked' : ''}} class="remainder_chk"    data-related-field="second_meeting_value,second_meeting_value_type">
                                    <span class="fa fa-check"></span> 
                                    {{__('Second meeting reminder')}}
                                 </label>
                              </div>
                              <input class="form-control schedule_input" name="second_meeting_value" type="number" min="0"  value="{{@$getAccountSchedule->second_meeting_value}}">
                              <select class="login_lang_dropdown_list form-control  schedule_select"  name="second_meeting_value_type">
                                 <option value="1" @if(@$getAccountSchedule->second_meeting_value_type == 1) selected @endif>{{__("Days")}}</option>
                                 <option value="2" @if(@$getAccountSchedule->second_meeting_value_type == 2) selected @endif>{{__("Hours")}}</option>
                                 <option value="3" @if(@$getAccountSchedule->second_meeting_value_type == 3) selected @endif>{{__("Minutes")}}</option>
                              </select>
                           </div>
                        </div>
                     <!-- Second Meeting Reminder Ends -->

                     <!-- Third Meeting Reminder Starts -->
                        <div class="col-sm-12">
                           <div class="form-group schedule_form_group">
                              <div class="checkbox c-checkbox schedule_checkbox">
                                 <label class="pl-0">
                                    <input name="third_meeting_reminder" type="checkbox" value="1"  {{@$getAccountSchedule->third_meeting_reminder  == 1 ? 'checked' : ''}} 
                                    class="remainder_chk"    data-related-field="third_meeting_value,third_meeting_value_type"
                                    >
                                    <span class="fa fa-check"></span> 
                                    {{__('Third meeting reminder')}}
                                 </label>
                              </div>
                              <input class="form-control schedule_input"  name="third_meeting_value" type="number" min="0"  value="{{@$getAccountSchedule->third_meeting_value}}">
                              <select class="login_lang_dropdown_list form-control  schedule_select"  name="third_meeting_value_type">
                                 <option value="1" @if(@$getAccountSchedule->third_meeting_value_type == 1) selected @endif>{{__("Days")}}</option>
                                 <option value="2" @if(@$getAccountSchedule->third_meeting_value_type == 2) selected @endif>{{("Hours")}}</option>
                                 <option value="3" @if(@$getAccountSchedule->third_meeting_value_type == 3) selected @endif>{{__("Minutes")}}</option>
                              </select>
                           </div>
                        </div>
                     <!-- Third Meeting Reminder Ends -->

                     <div class="col-sm-12">
                        <div class="form-group">
                           <label class="col-form-label">{{__('Maximum Waiting time for user in a minutes')}} <sup class="text-danger"></sup></label>
                           <input class="form-control max_wait_input"  name="max_wait_time" type="number" min="0"  value="{{@$getAccountSchedule->max_wait_time}}">
                           @if ($errors->has('schedule_link'))
                              <span class="text-danger">{{ $errors->first('schedule_link') }}</span>
                           @endif
                        </div> 
                     </div>
                     @if(@$accountData->allow_email == 1)
                        <div class="col-sm-12">
                           <div class="form-group">
                              <div class="checkbox c-checkbox">
                                 <label class="pl-0">
                                    <input name="message_via_email" type="checkbox" value="1"  {{@$getAccountSchedule->message_via_email  == 1 ? 'checked' : ''}}>
                                    <span class="fa fa-check"></span>
                                    {{__('Send meeting message via email')}}
                                 </label>
                              </div>
                           </div>
                        </div>
                     @endif
                     @if(@$accountData->allow_sms == 1)
                     <div class="col-sm-12">
                        <div class="form-group">
                           <div class="checkbox c-checkbox">
                              <label class="pl-0">
                                 <input name="message_via_sms" type="checkbox" value="1"  {{@$getAccountSchedule->message_via_sms  == 1 ? 'checked' : ''}}>
                                 <span class="fa fa-check"></span> 
                                 {{__('Send meeting message via SMS')}}
                              </label>
                           </div>
                        </div>
                     </div>
                     @endif
                     <div class="col-sm-12">
                           <button class="btn btn-success btn-lg  pull-right" type="submit">{{__('Save')}}</button>
                        </div>                                        
                  </div>                                           
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
<script >
        $(document).ready(function(){

            $('.copyUrlBtn ').on("click", function(e){
               e.preventDefault();
               value = $(this).parent().find('.copyUrlInput').val();
               var $temp = $("<input>");
               $("body").append($temp);
               $temp.val(value).select();
               document.execCommand("copy");
               $temp.remove();
            });
            $(".remainder_chk").each(function(index,element) {
               var fields =  $(element).attr('data-related-field');
               fields = fields.split(",");
               if($(this).prop('checked')) {
                  $("input[name='"+fields[0]+"']").removeClass("hide");
                  $("select[name='"+fields[1]+"']").removeClass("hide");
               } else{
                  $("input[name='"+fields[0]+"']").addClass("hide");
                  $("select[name='"+fields[1]+"']").addClass("hide");
               }

            })
            $(".remainder_chk").on("change",function(){
               var fields =  $(this).attr('data-related-field');
               fields = fields.split(",");
               if($(this).prop('checked')) {
                  $("input[name='"+fields[0]+"']").removeClass("hide");
                  $("select[name='"+fields[1]+"']").removeClass("hide");
               } else{
                  $("input[name='"+fields[0]+"']").addClass("hide");
                  $("select[name='"+fields[1]+"']").addClass("hide");
               }

            })
        });
    </script>