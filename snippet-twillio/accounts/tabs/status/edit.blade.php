<form id="statusEditForm" action="{{route('accountEditStatusSettings',$statusData['id'])}}" method="post">
    @csrf
       <div class="row">
           <div class="col-sm-12">
               <div class="card card">
                    <div class="card-body">
                    <div class="row">
                     <div class="col-sm-12">
                          <div class="form-group">
                            <label class="col-form-label">{{__('Status name')}} </label>
                            <input class="form-control @error('status_name') is-invalid @enderror borderLeft" value="{{$statusData['status_name']}}" type="text" name="status_name" required>
                            @error('status_name')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                         </div>
                         <div class="form-group">
                         <label for="Entity" class="col-form-label">{{__('Entity')}} </label> 
                           <select class="form-control  @error('entity') is-invalid @enderror borderLeft"  id="entity" name="entity">
                             <option value="Session" {{$statusData['entity'] == 'Session' ? 'selected' : ''}}>{{__('Session')}}</option>
                           </select>
                           @error('entity')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                           </div>
                           <div class="form-group">
                           <label class="col-form-label">{{__('Text color')}} </label> 
                           <select class="form-control  @error('color_code') is-invalid @enderror borderLeft"  id="color_code" name="color_code">
                             @foreach($color as $colorCode)
                               @if($statusData['color_code'] == $colorCode['color_name'])
                                 <option value="{{$colorCode['color_name']}}" class="font-weight-bold" style="color:{{$colorCode['color_name']}};" selected>{{$colorCode['name']}}</option>
                               @else
                                 <option value="{{$colorCode['color_name']}}" class="font-weight-bold" style="color:{{$colorCode['color_name']}};">{{$colorCode['name']}}</option>
                               @endif
                             @endforeach
                           </select>
                           @error('color_code')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                         </div>
                     </div>

                 </div>
                </div>
                
             </div>
             <div class="ml-auto pull-right">
              <button class="btn btn-success btn-lg  " type="submit">{{__('Update')}}</button>
              <button class="btn btn-danger btn-lg back-btn" type="button" data-dismiss="modal">{{__('Close')}}</button>
            </div>
           </div>
           
               
       </div>
       </form>
    