<div class="row">
   <div class="col-sm-5">
      <div class="card card-transparent p-3">
         <div class="card-body p-0">
            <ul class="list-group">
               @foreach($getMobileData as $getMobileDataVal)
                  <li class="dd-item dd3-item " data-id="41">
                     <div class="dd3-content">{{__($getMobileDataVal->module_name)}}
                        <div class="form-group float-right csrf">
                           @csrf
                           <button class="btn btn-info p-0 btn_p editMobileContent" data-id="{{$getMobileDataVal->id}}"><i class="fa fa-edit"></i>
                           </button>
                        </div>
                     </div>
                  </li>
               @endforeach 
            </ul>
         </div>
      </div>
      <div class="card card-transparent p-3">
         <div class="card-body p-0">
            @if(!empty($getScheduleData) && !empty($account_data->require_sch)) 
               <p><strong>{{__("Schedule Management Content")}}</strong></p>
            
          <ul class="list-group">
                @foreach($getScheduleData as $getScheduleDataVal)
                    <li class="dd-item dd3-item " data-id="41">
                        <div class="dd3-content">{{__($getScheduleDataVal->module_name)}}
                           <div class="form-group float-right csrf">
                              @csrf
                              <button class="btn btn-info p-0 btn_p editMobileContent" data-id="{{$getScheduleDataVal->id}}"><i class="fa fa-edit"></i>
                              </button>
                           </div>
                        </div>
                    </li>
                @endforeach 
          </ul>
          @endif
      </div>
   </div>
       
    </div>
     <div class="col-sm-7 editMobileConDiv">
         <form id="MobileContectForm" action={{route('AccountUpdateMobileContent')}} method="post" data-parsley-validate="" > 
          @csrf
          <div class="card p-3 mr-3 my-3"> 
             <div class="card-header">
                <div class="row">
                   <div class="col-8">
                      <h4 class="head_text_template"></h4>
                   </div>
                   <input type="hidden" class="emailId" name="id" />
                   <div class="col-4 text-right">
                      <button class="btn btn-success" type="submit">{{__('Save')}}</button>
                      <button class="btn btn-danger back-btn" type="button" >{{__('Close')}}</button>
                   </div>
                </div>
             </div>
             <input type="hidden" name="mobileContentId" class="mobileContentId">
             @foreach($languages  as $lang) 
             <div class="card-body ">
                <div class="templateText">
                   <div class="row">
                       

                           <div class="col-12">
                              <div class="form-group">
                              <input type="hidden" name="mobileContentHtmlBody_{{$lang->slug}}" class="mobileContentHtmlBody_{{$lang->slug}}">
                                 
                              <label class="col-form-label">{{__('Content Text in ')}}{{__($lang->name)}}</label>
                                    @if ($errors->has('name'))
                                          <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                    <div class="mobileContHtml">
                                       <textarea id="mobileContent_{{$lang->slug}}" ></textarea>
                                    </div>
                              </div>                                
                           </div>
                        
                   </div>
                </div>
             </div>
             @endforeach
      
          </div>
       </form>
     </div>
 </div>
      @foreach($languages as $lang)
         <script type="text/javascript">
            
            var editor_{{$lang->slug}} = CKEDITOR.replace('mobileContent_{{$lang->slug}}', {contentsLangDirection: '{{$lang->direction}}'});
            CKEDITOR.instances['mobileContent_{{$lang->slug}}'].on( 'change', function( evt ) {
               $(".mobileContentHtmlBody_{{$lang->slug}}").val(editor_{{$lang->slug}}.getData());
            });
         </script>
        @endforeach
        <script type="text/javascript">
        
        $('.editMobileConDiv').hide();
        $('.back-btn').on('click', function(){
          $('.editMobileConDiv').hide();
        });
       // CKEDITOR.on('instanceReady', function() {
        $('.editMobileContent').on('click', function(event){
                event.preventDefault();
                $("#MobileContectForm").attr('action', '{{route("AccountUpdateMobileContent")}}');
                $('.editMobileConDiv').show();
                $('.head_text_message').text("Edit Message");
                var id = $(this).data('id');
                var token = $(this).parent('.csrf').find("input[name=_token]").val();
                data = new FormData();
                data.append('id', id);
                data.append('_token',token);
                $.ajax({
                   url: "{{ route('accountGetMobileContent') }}",
                   type: "POST",
                   data:  data,
                   dataType: 'json',
                   processData: false,
                   contentType: false,
                   success: function(resp){
                     $.each(resp, function( index, value ) {
                        console.log(value !=  null);
                        if(value !=  null && value != '')
                           CKEDITOR.instances[index].setData(value);
                        else 
                           CKEDITOR.instances[index].setData("");
                     });

                     //  var englishText = resp.content_text_english != null ? resp.content_text_english : '';
                     //  var hebreText = resp.content_text_hebrew != null ? resp.content_text_hebrew : ''; 
                     //  var arabicText = resp.content_text_arabic != null ? resp.content_text_arabic : '';
                     //  var chinesText = resp.content_text_chinese != null ? resp.content_text_chinese : ''; 
                     //  var czechText = resp.content_text_czech != null ? resp.content_text_czech : '';
                     //  var frenchText = resp.content_text_french != null ? resp.content_text_french : '';
                     //  var germanText = resp.content_text_german != null ? resp.content_text_german : ''; 
                     //  var hindiText = resp.content_text_hindi != null ? resp.content_text_hindi : ''; 
                     //  var italianText = resp.content_text_italian != null ? resp.content_text_italian : ''; 
                     //  var japaneseText = resp.content_text_japanese != null ? resp.content_text_japanese : ''; 
                     //  var portugueseText = resp.content_text_portuguese != null ? resp.content_text_portuguese : ''; 
                     //  var russionText = resp.content_text_russian != null ? resp.content_text_russian : '';
                     //  var spanishText = resp.content_text_spanish != null ? resp.content_text_spanish : '';
                      
                     //  CKEDITOR.instances['mobileContent_english'].setData(englishText);
                     //  CKEDITOR.instances['mobileContent_hebrew'].setData(hebreText);
                     //  CKEDITOR.instances['mobileContent_arabic'].setData(arabicText);
                     //  CKEDITOR.instances['mobileContent_chinese'].setData(chinesText);
                     //  CKEDITOR.instances['mobileContent_czech'].setData(czechText);
                     //  CKEDITOR.instances['mobileContent_french'].setData(frenchText);
                     //  CKEDITOR.instances['mobileContent_german'].setData(germanText);
                     //  CKEDITOR.instances['mobileContent_hindi'].setData(hindiText);
                     //  CKEDITOR.instances['mobileContent_italian'].setData(italianText);
                     //  CKEDITOR.instances['mobileContent_japanese'].setData(japaneseText);
                     //  CKEDITOR.instances['mobileContent_portuguese'].setData(portugueseText);
                     //  CKEDITOR.instances['mobileContent_russian'].setData(russionText);
                     //  CKEDITOR.instances['mobileContent_spanish'].setData(spanishText);
                      $('.mobileContentId').val(id);
                   }
               });
           });
      //  });
           $("#MobileContectForm").on("submit",function(event){
                event.preventDefault();
                var url =  $("#MobileContectForm").attr('action');
                data = $(this).serialize();
                $.ajax({
                   url: url,
                   type: "POST",
                   data:  data,
                   success: function(resp){
                     $('.editMobileConDiv').hide();
                     swal(resp.msg); 
                   },
                   error: function(error) {
                     swal(__("Something went wrong."));
                   }
               });

           });
   </script>