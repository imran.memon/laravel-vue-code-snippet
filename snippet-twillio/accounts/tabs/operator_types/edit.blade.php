<form id="operatorTypeEditForm" action="{{route('operator-types.update',$id)}}" method="post">
  @csrf @method("PUT")
  <div class="row">
          <div class="col-sm-12">
              <div class="form-group">
                  <label class="col-form-label">{{__('Title')}} </label>
                  <input class="form-control @error('title') is-invalid @enderror borderLeft" value="{{$type->title}}" type="text" name="title" required />
                  @error('title')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
          </div>
          <div class="col-sm-12">
            <div class="ml-auto pull-right">
                <button class="btn btn-success btn-lg" type="submit">{{__('Update')}}</button>
            </div>
          </div>
  </div>
</form>
