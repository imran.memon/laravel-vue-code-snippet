<div class="card m-3">         
    <div class="ml-auto m-3">
        <a href="javascript:void(0)" data-action="{{route('accountInsertStatusSettings')}}" class="newStatus"><button class="btn btn-info btn-lg" type="button">{{__('New')}}</button></a>
     </div>                             
    <div class="card-body p-0">
       <!-- Datatable Start-->
       
       <div class="table-responsive">
       <input type="hidden" id="accountId" value="{{$account_id}}">
       <table class="table table-striped my-4 w-100" id="ajxTable">
          <thead>
             <tr>
                <th data-priority="1">{{__('Id')}}</th>
                <th>{{__('Status name')}}</th>
                <th>{{__('Entity')}}</th>
                <th>{{__('Text color')}}</th>
                <th style="width:160px"></th>
             </tr>
          </thead>
          
       </table>
       </div>
         <!-- Datatable Start-->
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#ajxTable").DataTable({
            serverSide: true,
            ajax: "{{$page_url}}",
            columns: [
                 { name: 'id', data:'id' },
                 { name: 'status_name',data:'statusName' },
                 { name: 'entity',data:'entity'},
                 { name: 'color_code',data:'color_code'},
                 {name:'edit_delete', data:'edit_delete',orderable: false },
            ],
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'stateSave': true,
            'order': [[1,'asc']],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
             "infoCallback": function( settings, start, end, max, total, pre ) {
              return "{{__('Showing')}} "+start +" {{__('to')}} "+ end+ " {{__('of')}} " +total+ " {{__('entries')}}";
            },
            processing: true,
            oLanguage: {
               sEmptyTable: "{{__('Empty Data Dable')}}",
               sZeroRecords: "{{__('No records')}}",
                sLengthMenu: '_MENU_ {{__("records per page")}}',
                sSearch: "{{__('Search')}}",
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: "{{__('No records available')}}",
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                },
                sProcessing: "{{__('Loading...')}}",
            }
        });
    });
</script>