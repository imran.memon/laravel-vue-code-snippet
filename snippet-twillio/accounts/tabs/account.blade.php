<div class="content-wrapper">
<form  id="editAccount" name="editAccount" action="{{route('accounts.update',$account->id)}}" method="post" enctype="multipart/form-data">
    @method('PUT')
    @csrf
<div class="row p-4">
    <div class="col-sm-6">
     <!-- START card-->
         <div class="card">
             <div class="card-body">
                 <div class="row">
                     <div class="col-sm-12">
                         <div class="form-group">
                             <label class="col-form-label">{{__('Name')}} <sup class="text-danger">*</sup></label>
                             <input class="form-control @error('name') is-invalid @enderror borderLeft"  name="name" type="text"  value="{{$account->name}}" required>
                             @if ($errors->has('name'))
                                 <span class="text-danger">{{ $errors->first('name') }}</span>
                             @endif
                         </div>                                              
                     </div>
                     
                     <div class="col-sm-12">
                         <div class="form-group">
                             <label class="col-form-label">{{__('Email')}} <sup class="text-danger">*</sup></label>
                             <input class="form-control @error('email') is-invalid @enderror borderLeft"  name="email" type="email" value="{{$account->email}}" required>
                             @if ($errors->has('email'))
                                 <span class="text-danger">{{ $errors->first('email') }}</span>
                             @endif
                         </div>                                              
                     </div>
                     <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Default Language')}} <sup class="text-danger">*</sup></label>
                            <select class="login_lang_dropdown_list form-control  borderLeft" required name="language">
                                @foreach($languages as $lang)
                                   <option {{$account->language == $lang->slug ? 'selected' : ''}}  value="{{$lang->slug}}">{{__($lang->name)}}</option>
                                @endforeach 
                             </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Account Type')}} <sup class="text-danger">*</sup></label>
                            <select class="account_type form-control  borderLeft" required name="account_type">
                              <option  value="health_care">{{__('Health Care')}}</option>
                              <option  value="insurance">{{__('Insurance' )}}</option>
                              <option  value="hls">{{__('HLS')}}</option>
                            </select>
                        </div>
                    </div>
                     {{-- <div class="col-sm-12">
                         <div class="form-group">
                             <label class="checkbox-inline"><input name="require_rec" type="checkbox" value="1" {{$account->require_rec  == 1 ? 'checked' : ''}}>{{__('Allow Recording')}}</label>
                         </div> 
                         <div class="form-group">
                             <label class="checkbox-inline"><input name="require_loc" type="checkbox" value="1" {{$account->require_loc == 1? 'checked' : ''}}>{{__('Allow Location')}}</label>
                         </div>                                            
                     </div> --}}
                     @if(!isset($statusChange) || $statusChange) 
                     <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-form-label ">{{__('Status')}}</label>
                            <div class="c-radio">
                                <label>
                                    <input type="radio" name="status" value="1" {{($account->status == 1) ? 'checked' : ''}}>
                                    <span class="fa fa-circle"></span>{{__("Active")}}
                                </label>
                            </div>
                            <div class="c-radio">
                                <label>
                                    <input type="radio" name="status" value="0" {{($account->status == 0) ? 'checked' : ''}}>
                                    <span class="fa fa-circle"></span>{{__("Inactive")}}
                                </label>
                            </div>
                        </div>
                    </div>
                     @endif
                     <div class="col-sm-12 ">
                         <div class="form-group">
                             <label class="col-form-label">{{__('Logo')}} <sup class="text-danger">*</sup></label>
                             <input class="form-control dropify @error('logo') is-invalid @enderror borderLeft"  type="file" name="logo" id="input-file-now" data-allowed-file-extensions="jpg png jpeg" data-default-file="{{url('/').'/uploads/'.$account->logo}}" >
                             
                             @if ($errors->has('logo'))
                                 <span class="text-danger">
                                     {{ $errors->first('logo') }}
                                 </span>
                             @endif
                         </div>                                            
                     </div>
                     <div class="col-sm-12">
                        <button class="btn btn-success btn-lg  pull-right" type="submit">{{__('Save')}}</button>
                     </div>
                 </div>                                           
             </div>                                           
        </div><!-- END card-->
          </div>
       </div>
 </div>
</form>
</div>
<script >
    $(document).ready(function(){
          $("#editAccount").validate({
                errorClass:'text-danger'
          });
          $('.dropify').dropify();
          
    });
</script>