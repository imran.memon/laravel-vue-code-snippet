<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Call;
use App\Order;

class Operator extends Authenticatable
{
    use SoftDeletes;
    use Notifiable , HasApiTokens;
    protected $table = 'operators';
    protected $softDelete = true;
    protected $guarded = [

    ];
    protected $dates = ['deleted_at'];

    public function getCity(){
        return $this->hasOne('App\City','id','city');
    }

}
