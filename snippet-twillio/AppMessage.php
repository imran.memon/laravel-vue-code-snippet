<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AppMessage extends Model
{
    use SoftDeletes;

    protected $table = 'app_message';
    protected $softDelete = true;
    protected $guarded = [
        
    ];
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   

    
    
    
}
