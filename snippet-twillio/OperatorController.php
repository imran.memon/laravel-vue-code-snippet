<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\VideoCall;
use TwilioRestClient;
use TwilioJwtAccessToken;
use TwilioJwtGrantsVideoGrant;
use Illuminate\Routing\UrlGenerator;
use DB;
use Auth;
use Storage;
use Hash;
use App\Account;
use File;
use Twilio\Rest\Client as Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use App\AppUser;
use App\Operator;
use Carbon\Carbon;
use App\StatusSettings;
use App\Managers;
use Redirect;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OperatorExport;
use App\Exports\OperatorSessionExport;
use App\ParticipantDetails;
use App\API;
use App\VideoAttachments;
use App\ShareSessionData;
use Validator;
use Session;
use DataTables;

class OperatorController extends Controller
{
	protected $sid;
	protected $token;
	protected $key;
	protected $secret;

	public function __construct(UrlGenerator $url){
        $this->url = $url;
        $keys = $this->getThirdPartyDetails();

        if($keys && !empty($keys)){
            $this->sid    = $keys->twilio_account_sid;
            $this->token  = $keys->twilio_account_token;
            $this->key    = $keys->twilio_video_api_key;
            $this->secret = $keys->twilio_video_api_secret;
        }
        
		// $this->sid    = config('services.twilio.sid');
		// $this->token  = config('services.twilio.token');
		// $this->key    = config('services.twilio.key');
		// $this->secret = config('services.twilio.secret');
	}

    public function getThirdPartyDetails(){
        $keys = API::get()->first();
        return $keys;
    }

    public function deleteSession($id){
    	VideoCall::where('id',$id)->delete();
    	
    	return redirect()->back()->with('success', 'Session Deleted Successfully');
    }
    public function send_share_seesion(Request $request){
        try{
        $this->middleware('auth');
        
            $rules = array(
                'phone' => 'required',
                'name'         => 'required',
                'email' => 'required|email',
                'emailSubject' => 'required',
                'emailMessage' => 'required'
            );
        //  $general_settings = AppGeneralSetting::first();
            $validator      = Validator::make($request->all(),$rules);
        /*  $randomRoomName = 'room_'.str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT)."_".$general_settings->emergency_number;*/

            if($validator->fails()){
                \Session::flash('sharesessionopen', '');
                $errors = $validator->messages();     
                return Redirect::back()->withErrors($errors)->withInput($request->input());
            }
            
           
           // $url = 
          
            $fourdigitrandom = rand(1000,9999);
           //echo "<pre>";print_r($request->phone);exit;
           //$fourdigitrandom = '1234';
            $ShareSessionData = new ShareSessionData();
            $ShareSessionData->name = $request->name;
            $ShareSessionData->email = $request->email;
            // $ShareSessionData->phone_number = $request->phone;
            $ShareSessionData->phone_number = $request->country_code.$request->phone;  
            $ShareSessionData->subject = $request->emailSubject;
            $ShareSessionData->description = $request->emailMessage;
            $ShareSessionData->verification_code = $fourdigitrandom;
            $ShareSessionData->session_id = $request->share_session_id;
            $ShareSessionData->flag = '1';
            //echo $ShareSessionData->phone_number;exit;
            
            $ShareSessionData->save();
            $normalCode = $fourdigitrandom.'_'.$request->share_session_id.'_'.$ShareSessionData->id;
                $code = base64_encode($normalCode);
                /*echo "<pre>";print_r($fourdigitrandom);
                echo $code;
                echo "<pre>";print_r(base64_decode($code));
            exit;*/
             // $url = $this->url->to('/').'/smsotp/'.$code;
            $url = $this->url->to('/').'/shareSession/'.$code;
            
                
                $string = "Hello ".$request->name." \n";
                $string .= $request->emailMessage.", \n";
                $string .= "Please click on below link for view session information \n";
                $string .= $url;
                $string .= "\n This is your otp for above url :".$fourdigitrandom;
                
                //$string = str_replace("full_name",$request->name,$string);
                //$url    = url('/join_session/customer/'.$createdRoom->sid);
                //$string =str_replace("URL",$url,$string);

                $account_sid = "AC17445fd074a7db09c686f7aac185a37e";
                $auth_token = "fcb833ee7e007e43bb5046f3d1972e21";

                $client_new = new Client($account_sid,$auth_token);
                //$twilio_number = $request->country_code=="+972"?"+972528903950":"+13475545217";

                if($request->country_code == "+972"){
                    $twilio_number = "+972528903950";
                }else if($request->country_code == "+1"){
                    $twilio_number = "+18334730912";
                }else{
                    $twilio_number = "+13475545217";
                }    
                $client_new->messages->create($request->country_code.$request->phone,
                    array(
                        'from' => $twilio_number,
                        'body' => $string
                    )
                ); 
            $user['link'] = $url;
            $user['description'] = $request->emailMessage;
            $user['otp'] = $fourdigitrandom;
            $user['email'] = $request->email;
            $user['name'] = $request->name;
            $user['subject'] = $request->emailSubject;
            Mail::send('operators.email_share_session', ['user' => $user], function ($m) use ($user) {
                   // $m->from('noreply@jastok.com', 'Remote Doctor'); //company Email

                    $m->to($user['email'], $user['name'])->subject($user['subject']); // receiver Email
                });   
                
            \Session::flash('success', __('Share session detail successfully send in your email and phone please check it.'));
           // echo $url;exit;
                return Redirect::back();
               // echo $url;exit;
           // $returnUrl = 'edit/session/'.$request->share_session_id;
        //   return redirect(url('edit/session/'.$request->share_session_id));
       // return redirect()->route("'edit/session/'.$request->share_session_id.");
        
    }catch(\Exception $e) {
           //echo "<pre>";print_r($e->getCode());exit;
            if($e->getCode() == 21211){
                \Session::flash('error', __('Phone Number is not valid, Please enter valid Phone Number ...'));
                return Redirect::back()->withInput($request->input());
            }else{
                \Session::flash('error', __('Oppps! something went wrong while share session, please try again ...'));
                return Redirect::back()->withInput($request->input());
            }
        }
    }
    public function smsotp($id){
        $data = array();
        $data['id'] = $id;
            return view('operators.smsotp',$data);
    }
    public function verifictionOtp(Request $request){
         $this->middleware('auth');
        
            $rules = array(
                'otp' => 'required|numeric'
            );
        //  $general_settings = AppGeneralSetting::first();
            $validator      = Validator::make($request->all(),$rules);
        /*  $randomRoomName = 'room_'.str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT)."_".$general_settings->emergency_number;*/

            if($validator->fails()){
                $errors = $validator->messages();     
                return Redirect::back()->withErrors($errors)->withInput($request->input());
            }
        
            $status = ShareSessionData::where('id',$request->id)->where('session_id',$request->sessionId)->where('flag','1')->first();
              // echo "<pre>";print_r($status);exit;
            if(isset($status)){
                if($status->verification_code == $request->otp){
                     $status->flag = 0;
                     $status->save();
                    // setcookie("sharesessionaccess", '1', time()+3600);
                     $normalCode = $status->verification_code.'_'.$request->sessionId.'_'.$request->id;
                    $code = base64_encode($normalCode);
                     $url = $this->url->to('/').'/shareSession/'.$code;
                     return redirect(url($url));
                }else{
                    //echo "123";exit;
                    //$errors = $validator->messages();
                    \Session::flash('message', __('Invalid sms code'));
                return Redirect::back()->withInput($request->input());
                }   
            }else{
                $errors = $validator->messages();     
                return Redirect::back()->withErrors($errors)->withInput($request->input());
            }
    }
    
    public function getLatestSessionId($id){
        $videoCall = VideoCall::find($id);

        if(!is_null($videoCall->latitude) && !is_null($videoCall->longitude)){
            $operatorId = $videoCall->operator_id;
            
            $result = VideoCall::where('operator_id',$operatorId)
                        ->where('latitude',$videoCall->latitude)
                        ->where('longitude',$videoCall->longitude)
                        ->orderBy('id','desc')
                        ->first();
            
            if($result){
                $id = $result->id;
            }
        }

        return redirect('edit/session/'.$id);
    }

    public function editSession($id){
        if(Auth::user()->type == 'operator'){
            $allowSessionIds = VideoCall::where('operator_id',Auth::user()->id)->pluck('id')->toArray();
            
            if(!in_array($id, $allowSessionIds)){
                \Session::flash('permission', __('Oppps! You don\'t have permission to access this session'));
                return redirect()->route('dashboardListing');
            }
        }

        $session   = VideoCall::find($id);
        $getAllUser = VideoCall::where('room_sid',$session->room_sid)->pluck('app_user_id')->toArray();
//        $videoPath = (!is_null($session->file))?$this->getStorageURL($session->file):null; 
        $videoPath = (!is_null($session->file))?'/videocall/'.$session->file:null; 
        $appUser   = AppUser::whereIn('id',$getAllUser)->orderBy('id','asc')->get()->toArray();
        $status    = StatusSettings::find($session->status_id);
        $client    = new Client($this->sid, $this->token);
        $details   = Managers::find($session->operator_id);
        /*if($session->allow_recording == 1){
            // For Video Start
            if(is_null($session->composition_sid)){
                $audio = $video = array();
                $parDetail = ParticipantDetails::where('room_sid',$session->room_sid)
                            ->pluck('participant_id','type')->toArray();
                         

                if(!empty($parDetail['operator']) && !empty($parDetail['customer'])){
                foreach($parDetail as $key => $value){
                    $recordings = $client->video->v1->recordings
                                        ->read(["groupingSid" => [$value]],
                                            20
                                        );

                    foreach ($recordings as $record) {
                        if($record->type == 'audio'){
                            $audio[$key] = $record->sid; 
                        }else if($record->type == 'video'){
                            $video[$key] = $record->sid; 
                        }
                    }
                }
            }
     
                if(count($audio) == 2 && count($video) == 2 && !empty($audio['operator']) && !empty($audio['customer']) && !empty($video['customer']) && !empty($video['customer']) ){
                    $composition = $client->video->compositions->create($session->room_sid, [
                        'audioSources' => array($audio['customer'],$audio['operator']),
                        'videoLayout' =>  array(
                                            'main' => array (
                                              'z_pos' => 1,
                                              'video_sources' => array($video['customer'])
                                            ),
                                            'pip' => array(
                                              'z_pos' => 2,
                                              'width' => 240,
                                              'height' => 180,
                                              'video_sources' => array($video['operator'])
                                            )
                                          ),
                        'statusCallback' => 'http://my.server.org/callbacks',
                        'resolution' => '640x480',
                        'format' => 'mp4'
                    ]);

                    $session->composition_sid    = $composition->sid;
                    $session->composition_status = $composition->status;
                    $session->save();
                }
            }
                
        }
        if(is_null($session->file)){
            $uri = "https://video.twilio.com/v1/Compositions/".$session->composition_sid."/Media/?Ttl=3600";
            $response = $client->request("GET", $uri);
            $mediaLocation = $response->getContent();

            if(count($mediaLocation) == 1 && isset($mediaLocation['redirect_to'])){
                $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.mp4';
                $redirect = $mediaLocation['redirect_to'];

                $path = public_path('videocall');
                if(!File::isDirectory($path)){
                    File::makeDirectory($path, 0777, true, true);
                }
                chmod($path, 0777);

                file_put_contents(public_path('/videocall/'.$fileName), fopen($redirect, 'r'));

                $videoPath                   = '/videocall/'.$fileName;
                $session->file               = $fileName;
                $session->composition_status = 'Completed';
                $session->save();
            }
        }*/
        //get s3 video link 
        $videoError = 0;
        if(!empty($session->file)){
            $videoPath =   $this->getStorageURL($session->file);
        }else{
            $videoError = 1;
        }
        //get files
        $getFiles = VideoAttachments::where('room_sid',$session['room_sid'])->select('uploader_type','file','file_size','is_snap','thumbnail')->get()->toArray();
        $filesList = array();
        if(count($getFiles)){
            foreach($getFiles as $key => $val){
                if($val['uploader_type'] == 'operator'){
                    $filesList['operator'][] = $val;
                }else{
                    $filesList['customer'][] = $val;
                }
            }
        }
        $getAccountDetail = Account::find($session->account_id);
        $data['page_title'] = 'Edit Session';
        $participants = ParticipantDetails::where('room_sid',$session->room_sid)->get(); 
        return view('videocall.edit_session',$data)
            ->with('sessionId',$id)
            ->with('session',$session)
            ->with('appUser',$appUser)
            ->with('status',$status)
            ->with('videoPath',$videoPath)
            ->with('filesList',$filesList)
            ->with('details',$details)
            ->with('participants',$participants)
            ->with('videoError',$videoError);
    }

    /**
     * Get Storage URL from S3 Bucket
     */
    public function getStorageURL($filename)
	{
		$disk = Storage::disk('s3');
		if ($disk->exists($filename)) {
			$client = $disk->getDriver()->getAdapter()->getClient();
			$bucket = \Config::get('filesystems.disks.s3.bucket');

			$command = $client->getCommand('GetObject', [
				'Bucket' => $bucket,
				'Key' => $filename
			]);

			$request = $client->createPresignedRequest($command, '+120 minutes');
			return (string) $request->getUri();
		}
		else 
			return '';
    }

public function shareSession($combineencode){
    $combine = base64_decode($combineencode);
        /*if(Auth::user()->type == 'operator'){
            $allowSessionIds = VideoCall::where('operator_id',Auth::user()->id)->pluck('id')->toArray();
            
            if(!in_array($id, $allowSessionIds)){
                \Session::flash('permission', "Oppps! You don't have permission to access this session");
                return redirect()->route('dashboardListing');
            }
        }*/
        
        $idsArray = explode('_',$combine);
        $id = $idsArray['1'];
       
        $dataId = $idsArray['2'];
        $ShareSessionData = ShareSessionData::where('id',$dataId)->first(); 
        if( $ShareSessionData->flag != 0){
            \Session::flash('permission', __('Oppps! You don\'t have permission to access this page'));
                $fourdigitrandom = rand(1000,9999);
                $normalCode = $fourdigitrandom.'_'.$idsArray['1'].'_'.$idsArray['2'];
                            $code = base64_encode($normalCode);
                $url = $this->url->to('/').'/smsotp/'.$code;
                return redirect(url($url));
               // return redirect()->route($url);
        }
        
        $ShareSessionData->flag = 1;
        $ShareSessionData->save();
        
        
        
        $session   = VideoCall::find($id);
        $getAllUser = VideoCall::where('room_sid',$session->room_sid)->pluck('app_user_id')->toArray();
        
        $videoPath = (!is_null($session->file)) ? $this->getStorageURL($session->file) :null; 
        
        $appUser   = AppUser::find($session->app_user_id);
        if(!empty($appUser)) {
            if (($key = array_search($session->app_user_id, $getAllUser)) !== false) {
                unset($getAllUser[$key]);
            }
        }
        $appUserOther   = AppUser::whereIn('id',$getAllUser)->orderBy('id','asc')->get()->toArray(); 

        
        $status    = StatusSettings::find($session->status_id);
        $client    = new Client($this->sid, $this->token);
        $details   = Managers::find($session->operator_id);
        if($session->allow_recording == 1){
            // For Video Start
            if(is_null($session->composition_sid)){
                $audio = $video = array();
                $parDetail = ParticipantDetails::where('room_sid',$session->room_sid)
                            ->pluck('participant_id','type')->toArray();
                         

                if(!empty($parDetail['operator']) && !empty($parDetail['customer'])){
                foreach($parDetail as $key => $value){
                    $recordings = $client->video->v1->recordings
                                        ->read(["groupingSid" => [$value]],
                                            20
                                        );

                    foreach ($recordings as $record) {
                        if($record->type == 'audio'){
                            $audio[$key] = $record->sid; 
                        }else if($record->type == 'video'){
                            $video[$key] = $record->sid; 
                        }
                    }
                }
            }
     
                if(count($audio) == 2 && count($video) == 2 && !empty($audio['operator']) && !empty($audio['customer']) && !empty($video['customer']) && !empty($video['customer']) ){
                    DB::beginTransaction();
                    try{
                        $composition = $client->video->compositions->create($session->room_sid, [
                            'audioSources' => array($audio['customer'],$audio['operator']),
                            'videoLayout' =>  array(
                                                'main' => array (
                                                  'z_pos' => 1,
                                                  'video_sources' => array($video['customer'])
                                                ),
                                                'pip' => array(
                                                  'z_pos' => 2,
                                                  // 'x_pos' => 1000,
                                                  // 'y_pos' => 30,
                                                  'width' => 240,
                                                  'height' => 180,
                                                  'video_sources' => array($video['operator'])
                                                )
                                              ),
                            'statusCallback' => 'http://my.server.org/callbacks',
                            'resolution' => '640x480',
                            // 'resolution' => '1280x720',
                            'format' => 'mp4'
                        ]);

                        $session->composition_sid    = $composition->sid;
                        $session->composition_status = $composition->status;
                        $session->save();
                        DB::commit();
                    }catch (Exception $e) {
                        DB::rollback();
                        $this->LogIncidents(1, 'Jastok Video Log Testing0'.$id, $e->getMessage());
                    }
                }

                // $composition = $client->video->compositions->create($session->room_sid, [
                //     'audioSources' => '*',
                //     'videoLayout' =>  array(
                //                         'grid' => array (
                //                           'video_sources' => array('*')
                //                         )
                //                       ),
                //     'statusCallback' => 'http://my.server.org/callbacks',
                //     'resolution' => '640x480',
                //     'format' => 'mp4'
                // ]);

                // $session->composition_sid    = $composition->sid;
                // $session->composition_status = $composition->status;
                // $session->save();
            }
                
            if(is_null($session->file)){
                $uri = "https://video.twilio.com/v1/Compositions/".$session->composition_sid."/Media/?Ttl=3600";
                $response = $client->request("GET", $uri);
                $mediaLocation = $response->getContent();
                    
                if(count($mediaLocation) == 1 && isset($mediaLocation['redirect_to'])){
                    $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.mp4';
                    $redirect = $mediaLocation['redirect_to'];

                    $path = public_path('videocall');
                    if(!File::isDirectory($path)){
                        File::makeDirectory($path, 0777, true, true);
                    }

                    file_put_contents(public_path('/videocall/'.$fileName), fopen($redirect, 'r'));

                    $videoPath                   = '/videocall/'.$fileName;
                    $session->file               = $fileName;
                    $session->composition_status = 'Completed';
                    $session->save();
                }
            }
            // For Video End 
        }
        //get s3 video link 
//        $videoName  = (!is_null($session->file))?$session->file:null;
//        $videoPath = Storage::disk('s3')->url($session->file);
        //get files
        $getFiles = VideoAttachments::where('room_sid',$session['room_sid'])->select('uploader_type','file','file_size')->get()->toArray();
        $filesList = array();
        if(count($getFiles)){
            foreach($getFiles as $key => $val){
                if($val['uploader_type'] == 'operator'){
                    $filesList['operator'][] = $val;
                }else{
                    $filesList['customer'][] = $val;
                }
            }
        }
//        echo "<pre>";print_r($filesList);exit;
        $data['page_title'] = 'Edit Session';
         $participants = ParticipantDetails::where('room_sid',$session['room_sid'])->get(); 
        return view('videocall.edit_session_share',$data)
            ->with('sessionId',$id)
            ->with('session',$session)
            ->with('appUser',$appUser)
            ->with('status',$status)
            ->with('videoPath',$videoPath)
            ->with('filesList',$filesList)
            ->with('details',$details)
            ->with('participants',$participants)
            ->with('appUserOther',$appUserOther);
    }
       public function getShareSessionById($id){
        $callDetail = VideoCall::find($id);
        $appUser    = AppUser::find($callDetail->app_user_id);
        $details    = Managers::find($callDetail->operator_id);
        
        $appUser->number = $appUser->number;

        return \Response::json(array('callDetail'=>$callDetail,'appUser'=>$appUser,'details' => $details));
    }
    public function getSessionById($id){
        $callDetail = VideoCall::find($id);
        $appUser    = AppUser::find($callDetail->app_user_id);
        $details    = Managers::find($callDetail->operator_id);
    	
        $appUser->number = $appUser->number;

    	return \Response::json(array('callDetail'=>$callDetail,'appUser'=>$appUser,'details' => $details));
    }
    public function saveOperatorComment(Request $request){
        try {
            $roomSID         = $request->input('roomSID');
            $status          = $request->input('status');
            $operatorComment = $request->input('operatorComment');
            $userName        = $request->input('userName');
            
            $operator                   = VideoCall::where('room_sid',$roomSID)->first();
            $operator->operator_comment = nl2br($operatorComment); 
            $operator->status_id        = $status; 
            $operator->save();
            if(!empty($operator->app_user_id )){
                $app_user = AppUser::find($operator->app_user_id);
                $app_user->name=$userName;
                $app_user->save();
            }
            
            
            return \Response::json(array('status'=>true,'message'=>'Comment Saved Successfully'));
        } catch (\Exception $e) {
            return \Response::json(array('status'=>false,'message'=>'Comment Not Saved Successfully'));
        }
    }

    public function operatorsExport()
    {
        if(Auth::user()->role == "manager")
        {
            $getOperators = Managers::where('role','operator')->where('created_by',Auth::user()->id)->get();
            foreach ($getOperators as $key => $value) {
                $data['session'][$key] = VideoCall::where('operator_id',$value->id)->count();
            }
        }
        if(Auth::user()->role == "Top Manager")
        {
            $getOperators = Managers::with('getVideoCall')->where('role','operator')->get();
            foreach ($getOperators as $key => $value) {
                $data['session'][$key] = VideoCall::where('operator_id',$value->id)->count();
            }
        }
        $data['operator'] = $getOperators;
        return Excel::download(new OperatorExport($data), Now().'_Operator.xlsx');
    }

    public function operatorsSessionExport($id)
    {
        $data['appUserSession'] = VideoCall::leftjoin('app_user','video_call.app_user_id','app_user.id')
            ->leftjoin('status_settings','video_call.status_id','status_settings.id')
            ->leftjoin('managers','video_call.operator_id','managers.id')
            ->where('managers.id',$id) 
            ->orderBy('video_call.id','desc')
            ->select('video_call.created_at as videoDate','app_user.name',
                'video_call.address','status_settings.status_name','app_user.number',
                DB::raw("CONCAT(managers.first_name,' ',managers.last_name) as operator_name"),
                'video_call.duration','video_call.id as session_id
                ')->get();
        return Excel::download(new OperatorSessionExport($data), Now().'_SessionList.xlsx');
    }

    public function index(Request $request)
    {
        
        if(request()->ajax()) {
            $model = Managers::where('role','operator'); 
            if(strcasecmp(Auth::user()->role,"manager") == 0)   
            {
                // $data['operators'] = Managers::where('role','operator')->where('parent_manager_id',Auth::user()->id)->get();
                $model = $model->where('parent_manager_id',Auth::user()->id); 

                // foreach ($data['operators'] as $key => $value) {
                //     $data['session'][$key] = VideoCall::where('operator_id',$value->id)->count();
                //     $data['operators'][$key]["total_hours"] = VideoCall::where('operator_id',$value->id)->sum('duration');
                //     $data['operators'][$key]['account_name'] = (new \App\Helpers\CommonHelper)->getAccountNameByParent($value->parent_manager_id);
                // }
            }
            // else  if(Auth::user()->role == "Top Manager")
            // {
            //     $data['operators'] = Managers::where('role','operator')->get();
                
            //     foreach ($data['operators'] as $key => $value) {
            //         $data['session'][$key] = VideoCall::where('operator_id',$value->id)->count();
            //         // $data['operators'][$key]["total_hours"] = VideoCall::where('operator_id',$value->id)->sum('duration');
            //         // $data['operators'][$key]['account_name'] = (new \App\Helpers\CommonHelper)->getAccountNameByParent($value->parent_manager_id);
            //     }
            // }
            return DataTables::eloquent($model)
                ->addColumn('total_session',function(Managers $operator) {
                    if(!empty($operator->getVideoCall)) {
                        return $operator->getVideoCall->count();
                    }
                    return 0;

                })->addColumn('name',function(Managers $operator){
                    return ucfirst($operator->first_name) . " ". ucfirst($operator->last_name); 
                })->addColumn('op_type_val',function(Managers $operator){
                    if(empty($operator->op_type_ref)) {
                        return "";
                    }
                    return ucfirst($operator->op_type_ref->title);
                })->addColumn('total_duration',function(Managers $operator) {
                    if(!empty($operator->getVideoCall)) {
                        try {
                            $totalDuration =  $operator->getVideoCall->whereNotNull('duration')->sum('duration');
                            return cnvStoH($totalDuration);
                        } catch(\Exception $e) {
                        
                        }
                    } 
                    return "00:00";
                })->addColumn('edit_delete',function(Managers $operator){
                    $deleteUrl  = route('deleteOperators',$operator->id); 
                    $editUrl = route('editOperators',$operator->id);
                    return '<button class="btn-icon btn btn-danger mr-2  deleteOnElement" data-route="'.$deleteUrl.'" type="button"><i class="fa fa-trash"></i></button><a href="'.$editUrl.'" class="btn-icon btn btn-info"><i class="fa fa-edit"></i></a>';
                })
                ->addColumn('account_name',function(Managers $operator) {
                     return (new \App\Helpers\CommonHelper)->getAccountNameByParent($operator->parent_manager_id);
                })->addColumn('online',function(Managers $operator) {
                    $color = $operator->online == 1 ?  'rounded-circle bg-success' : 'rounded-circle bg-danger';
                    return '<div class="'.$color.'" style="width:1% !important; padding: 0.7rem !important;"></div>';
                })->addColumn('last_login_text',function($operator){
                    return (new \App\Helpers\CommonHelper)->formatDate($operator->last_enter);
                })->addColumn('created_at_text',function($operator){
                    return (new \App\Helpers\CommonHelper)->formatDate($operator->created_at);
                })->addColumn('updated_at_text',function($operator){
                    return (new \App\Helpers\CommonHelper)->formatDate($operator->updated_at);
                })->escapeColumns([])->make(true);

        }
        
        if(strcasecmp(Auth::user()->role,"manager") != 0 && Auth::user()->role != "Top Manager")
        {
            return route('unauthorized');
        }
        
        $data['page_title']='Operators';
        $data['link']='Operators_Listing';
        $data['menu']='Operators_Listing';
        $data['subMenu']='Operators_Listing';
        
        // $data['subMenu']='Dictionary';
        return view('operators.operators',$data);
    }

    public function addOperators(Request $request)
    {   
        $types = [];
        $roleClass = "";
        if(Auth::user()->role == "Top Manager")
        {
            $data['manager'] = Managers::where('role','manager')->get();
            $roleClass = "top_manager_class";
        } else {
            $accountId = auth()->user()->account_id; 
            if(!empty($accountId)) {
                $types = \App\OperatorType::where('account_id',$accountId)->get();
            }
        }
        if($request->isMethod('post')){
            $request->validate([
                'first_name' => 'required|min:3',
                'last_name' => 'required|min:2',
                'phone' => 'required|max:15|min:10|unique:managers',
                'email' => 'required|email|unique:managers',
                'password' => 'required|min:8',
                'confirm_password' => 'required|same:password',
                'op_type' =>'required',
                //'image' => 'required'
            ],
            [
                'password.regex'=>'Your password must be more than 8 characters long.',
                'confirm_password.same' => 'Confirm Password  should match the Password'
            ]);
            
            if(Auth::user()->role == "Top Manager")
            {
                $request->validate(['manager' => 'required',]);
                $parent_manager = $request->manager;
            }
            else
            {
                $parent_manager = Auth::user()->id;
            }
                
            $managerData = collect($request->all())->merge(['type_id'=>2,'type'=>'operator','role' => 'operator','created_at'=> Carbon::now(),'created_by'=>Auth::user()->id,'parent_manager_id'=>$parent_manager])->forget(['_token','confirm_password','manager'])->put('password',Hash::make($request->password))->toArray();
            if ($request->hasFile('image')) {
                $image  = $request->file('image');
                $fileName   = time() . '.' . $image->getClientOriginalExtension();
                Storage::disk('public')->put($fileName,File::get($image));
                $managerData['image']=$fileName;
            }
            else
            {
                $managerData['image']="";
            }
           
            $manager = new Managers();
            $manager->fill($managerData);
           
            if($manager->save())
            {
                \Session::flash('message', __('Successfully Created Operator'));
                \Session::flash('class', 'success');
                $user['name'] = $request->first_name;
                $user['email'] = $request->email;
                $user['password'] = $request->password;

                Mail::send('operators.email', ['user' => $user], function ($m) use ($user) {
                        //$m->from('noreply@jastok.com', 'Remote Doctor'); //company Email

                        $m->to($user['email'], $user['name'])->subject('Email Subject!'); // receiver Email
                    });   
                return redirect()->route('operatorsListing');
            }
            else{
                \Session::flash('message', __('Unsuccessfully Created Operator'));
                \Session::flash('class', 'danger');
                return redirect()->route('operatorsListing');
            }

        }
        else
        {
            $data['types'] = $types;
            $data['roleClass'] = $roleClass;
            $data['page_title']='Add Operators';
            $data['link']='Operators_Listing';
            $data['menu']='Operators_Listing';
            // $data['subMenu']='Dictionary';
            return view('operators.addOperators',$data);
        }
    }

    public function deleteOperators($id)
    {
        DB::beginTransaction();
        try {
            Managers::where("id",$id)->update(["deleted_by"=> Auth::user()->id]);
                $deleteOperators=Managers::destroy($id);
            // $deleteData = StatusSettings::where('id',$id)->delete();
            DB::commit();
            \Session::flash('message', __('Successfully Deleted Operator'));
            \Session::flash('class', 'success');   
            return redirect()->route('operatorsListing');
        } catch (\Exception $e) {
            //dd($e);
            DB::rollback();
            \Session::flash('message', __('Unsuccessfully Deleted Operator'));
            \Session::flash('class', 'danger');
            return redirect()->route('operatorsListing');   
            // something went wrong
        }
    }

    public function editOperators(Request $request,$id = null)
    {
        
        if(request()->ajax()) {
            $model = VideoCall::where('operator_id',$id);
           
            return DataTables::eloquent($model)->addColumn('edit_delete',function(VideoCall $session) {
                $deleteUrl  = url('delete/session/'.$session->id); 
                $editUrl = url('edit/session/'.$session->id);
                return '<button class="btn-icon btn btn-danger mr-2  deleteOnElement" data-route="'.$deleteUrl.'" type="button"><i class="fa fa-trash"></i></button><a href="'.$editUrl.'" class="btn-icon btn btn-info"><i class="fa fa-edit"></i></a>';
            })->addColumn('operator',function(VideoCall $session){
                if(isset($session->operator) && !empty($session->operator)) {
                    return $session->operator->first_name . " ". $session->operator->last_name;
                }
                return "";
            })->addColumn('user',function(VideoCall $session){
                if(isset($session->user) && !empty($session->user)) {
                    return $session->user->name;
                } 
                return "";
            })->addColumn('user_phone',function(VideoCall $session){
                if(isset($session->user) && !empty($session->user)) {
                    return $session->user->number;
                } 
                return "";
            })->addColumn('status',function(VideoCall $session){
                if(isset($session->status) && !empty($session->status)) {
                    return '<span style="color:'.$session->status->color_code.'">'.$session->status->status_name.'</span>';
                } 
                return "";
            })->addColumn('duration',function(VideoCall $session){
                if(isset($session->duration) && !empty($session->duration) ) {
                    return (new \App\Helpers\CommonHelper)->displayDuration(intval($session->duration));
                } 
                return "00:00";
            })->addColumn('created_at_text',function($session){
                return (new \App\Helpers\CommonHelper)->formatDate($session->created_at);
            })->escapeColumns([])->make(true);
        }

        if(Auth::user()->type == 'operator'){
            $allowSessionIds = Managers::where('id',Auth::user()->id)->pluck('id')->toArray();
            
            if(!in_array($id, $allowSessionIds)){
                \Session::flash('permission', __('Oppps! You don\'t have permission to access this Page'));
                return redirect()->route('dashboardListing');
            }
        }
        $types = [];
        $roleClass = "";
            
        if(Auth::user()->role == "Top Manager")
        {
            $data['manager'] = Managers::where('role','manager')->get();
            $roleClass = "top_manager_class";
        }
        if($request->isMethod('post')){
            $manager =Managers::find($id);
                $request->validate([
            
                    'first_name' => 'required|min:3',
                    'last_name' => 'required|min:2',
                    'phone' => 'required|max:15|min:10|unique:managers,phone,'.$id,
                    'email' => 'required|email|unique:managers,email,'.$id,
                   
                    
                ]);
                if(Auth::user()->role == "Top Manager")
                {
                    $request->validate(['manager' => 'required',]);
                    $parent_manager = $request->manager;
                }
                else
                {
                    $parent_manager = Auth::user()->id;
                }

                if(Auth::user()->role == "operator")
                {
                    $managerData = collect($request->all())->merge(['updated_at'=> Carbon::now(),'updated_by'=>Auth::user()->id])->forget(['_token','password','confirm_password','manager','created_by'])->toArray();
                }
                else
                {
                    $managerData = collect($request->all())->merge(['parent_manager_id'=>$parent_manager,'updated_at'=> Carbon::now(),'updated_by'=>Auth::user()->id])->forget(['_token','password','confirm_password','manager','created_by'])->toArray();
                }
                if($request->password != '')
                {   
                    $request->validate([
                
                        'password' => 'required|min:8',
                        'confirm_password' => 'required|same:password',
                    ],
                    [
                        'password.regex'=>'Your password must be more than 8 characters long.',
                        'confirm_password.same' => 'Confirm Password  should match the Password'
                    ]);
                    
                    $managerData['password'] = Hash::make($request->password);
                    $request->validate([
                
                        'password' => 'required|min:8',
                        'confirm_password' => 'required|same:password',
                    ]);
                }

                if($request->confirm_password != '')
                {   
                    $request->validate([
                
                        'password' => 'required|min:8',
                        'confirm_password' => 'required|same:password',
                    ],
                    [
                        'password.regex'=>'Your password must be more than 8 characters long.',
                        'confirm_password.same' => 'Confirm Password  should match the Password'
                    ]);
                }
                
                if($request->hasFile('image')) {
                    
                    $image  = $request->file('image');
                    $fileName   = time() . '.' . $image->getClientOriginalExtension();
                    Storage::disk('public')->put($fileName,File::get($image));
                    $managerData['image']=$fileName;
                }
            $manager->fill($managerData);        
            if($manager->save())
            {
                \Session::flash('message', __('Successfully Updated Operator'));
                \Session::flash('class', 'success');
                if(Auth::user()->role == "operator")
                {
                    return redirect()->route('dashboardListing');
                }
                else{
                    return redirect()->route('operatorsListing');
                }    
                
            }
            else{
            
                DB::rollback();
                \Session::flash('message', __('Unsuccessfully Updated Operator'));
                \Session::flash('class', 'danger');
                if(Auth::user()->role == "operator")
                {
                    return redirect()->route('dashboardListing');
                }
                else{
                    return redirect()->route('operatorsListing');
                }
            }
        }
        else
        {
            if(Auth::user()->role == "operator")
            {
                $data['operators']= Managers::where('id',Auth::user()->id)->first();
            }
            else
            {
                
                // $data['appUserSession'] = VideoCall::leftjoin('app_user','video_call.app_user_id','app_user.id')
                // ->leftjoin('status_settings','video_call.status_id','status_settings.id')
                // ->leftjoin('managers','video_call.operator_id','managers.id')
                // ->where('managers.id',$id) 
                // ->orderBy('video_call.id','desc')
                // ->select('video_call.created_at as videoDate','app_user.name',
                //     'video_call.address','status_settings.status_name','status_settings.color_code','app_user.number',
                //     DB::raw("CONCAT(managers.first_name,' ',managers.last_name) as operator_name"),
                //     'video_call.duration','video_call.id as session_id')->get();
                $data['operators']= Managers::where('id',$id)->first();
            }
            $manager = Managers::where('id',$data['operators']->parent_manager_id)->first();
            $types = \App\OperatorType::where('account_id',$manager->account_id)->get();
            $data['page_url'] = route('editOperators',$id);
            $data['page_title']='Operators';
            $data['link']='Operators_Listing';
            $data['menu']='Operators_Listing';
            $data['types']= $types;
            $data['roleClass'] = $roleClass;
            // $data['subMenu']='Dictionary';
            return view('operators.editOperators',$data);
        }
        
    }

}
