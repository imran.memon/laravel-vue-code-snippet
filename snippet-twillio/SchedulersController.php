<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Managers;
use Validator;
use App\Scheduler;
use Carbon\Carbon;
use Carbon\CarbonTimeZone; 

class SchedulersController extends Controller
{
    private $viewPath; 
    private $viewData;
    private $events;
    private $meetingEvents;
    private $sameTimeEvent;
    /**
    * set default setting 
    */    
    public function __construct()
    {
        $this->viewPath = "scheduler.";
    }

    /** 
     * index show calender
     */
    public function index()
    {
        $authUserType = auth()->user()->type_id;
        $this->viewData['page_title'] = "Calender";
        $schedulers = Scheduler::get();
        $this->viewData['currentDate'] = (new \App\Helpers\CommonHelper)->formatDateDatabase(Carbon::now());
        $days = [
            Carbon::SUNDAY,
            Carbon::MONDAY,
            Carbon::TUESDAY,
            Carbon::WEDNESDAY, 
            Carbon::THURSDAY, 
            Carbon::FRIDAY, 
            Carbon::SATURDAY
        ];
       

        $this->events = [];
        // $filtered = $schedulers->filter(function ($schedule, $key) use ($days){
        //     $startDate =  (new \App\Helpers\CommonHelper)->formatDateDatabase($schedule->from_date." ".$schedule->start_time); 
        //     $takeSecond =  (new \App\Helpers\CommonHelper)->formatDateDatabase($schedule->from_date." ".$schedule->end_time);
        //     $cdate =Carbon::parse($startDate);
        //     $takeSecond =Carbon::parse($takeSecond);
        //     $diffMin  = $cdate->diffInMinutes($takeSecond);
        //     if($diffMin < 5 && $diffMin < $schedule->time_slot  ) return false;
        //     $numberOfSlot = intval($diffMin/$schedule->time_slot);
            
        //     //for($i = 1; $i<=$numberOfSlot; $i++) {

        //         $this->events[] = (Object) array(
        //             "title" => $schedule->operator->first_name . " ".$schedule->operator->last_name,
        //             "start" => $cdate->format('Y-m-d')." ".$schedule->start_time,
        //             "end" => $cdate->format('Y-m-d')." ".$schedule->end_time,
        //             "constraint" => 'availableForMeeting',
        //             "is_schedule" => true,
        //             "color" => '#ccc',
        //             "id" => $schedule->id
        //         );
        //        // $cdate = $cdate->addMinutes($schedule->time_slot);
        //    // }
            
        //     if(empty($schedule->is_end)) {
        //         $currentDate = Carbon::createFromFormat("Y-m-d H:i:s",$startDate);
        //         $endDate = $currentDate->addYear()->format('Y-m-d H:i:s');
        //     }
        //     //dd($endDate);
        //     $endDate = Carbon::parse($endDate);
        //     if($schedule->is_repeat == 1) {
                
        //         $weekDays = explode(",",$schedule->repeat_on_weeks);
        //         foreach($weekDays as $week) {
        //            $startDate = Carbon::parse($startDate)->next($days[$week]);
        //            for($date = $startDate; $date->lte($endDate); $date->addWeek($schedule->repeat_count)) {
                        
        //                 $this->events[] = (Object) array(
        //                     "title" => $schedule->operator->first_name . " ".$schedule->operator->last_name,
        //                     "start" => $date->format('Y-m-d')." ".$schedule->start_time,
        //                     "end" => $date->format('Y-m-d')." ".$schedule->end_time,
        //                     "constraint" => 'availableForMeeting',
        //                     "is_schedule" => true,
        //                     "color" => '#ccc'
        //                 );
        //             }
                    
        //         }
        //     } elseif($schedule->is_repeat == 2) {
        //         if(empty($schedule->repeat_on_months)) {
        //             return false;
        //         }
        //         $month = explode("_",$schedule->repeat_on_months);
        //         if(count($month) == 2) {
        //             $dy = strtolower($month[1]);
        //             if($month[0]  == 1) {
        //                 $f = 'first'; 
        //             } else if($month[0] == 2) {
        //                 $f = "second";
        //             } else if($month[0] == 3) {
        //                 $f = "third";
        //             } else if($month[0] == 4) {
        //                 $f = "fourth";
        //             } else if($month[0] == 5) {
        //                 $f = "fifth";
        //             }

                   
        //             $startDate = Carbon::parse($startDate)->startOfMonth()->addMonths(1);//->next($daysByDay[$dy]);
        //           //  dd( $startDate);
        //            // dd($endDate);
        //            // $startDate = 
        //             for($date = $startDate; $date->lte($endDate); $date->addMonths($schedule->repeat_count)) {
        //                 $dynamicDate = Carbon::parse( "{$f} {$dy} of {$date->format('F')}");
        //                 $this->events[] = (Object) array(
        //                     "title" => $schedule->operator->first_name . " ".$schedule->operator->last_name,
        //                     "start" => $dynamicDate->format('Y-m-d')." ".$schedule->start_time,
        //                     "end" => $dynamicDate->format('Y-m-d')." ".$schedule->end_time,
        //                     "constraint" => 'availableForMeeting',
        //                     "is_schedule" => true,
        //                     "color" => '#ccc'
        //                 );
        //             }
        //         } else {
        //             $startDate = Carbon::parse($startDate)->startOfMonth()->addMonths(1);//->nex
        //             for($date = $startDate; $date->lte($endDate); $date->addMonths($schedule->repeat_count)) {
        //                 $dynamicDate = $date->firstOfMonth()->addDays($month[0]-1);
        //                 $this->events[] = (Object) array(
        //                     "title" => $schedule->operator->first_name . " ".$schedule->operator->last_name,
        //                     "start" => $dynamicDate->format('Y-m-d')." ".$schedule->start_time,
        //                     "end" => $dynamicDate->format('Y-m-d')." ".$schedule->end_time,
        //                     "constraint" => 'availableForMeeting',
        //                     "is_schedule" => true,
        //                     "color" => '#ccc'
        //                 );
        //             }
        //         }
        //      //   dd($this->events);
        //     }

        //     return true;
        // });
        $accounts = []; 
        $operators = [];
        if(strcasecmp(auth()->user()->role,"Top Manager") == 0) {
            $accounts = Account::where('require_sch',1)->where('status',1)->orderBy('name','asc')->get();    
        } elseif(strcasecmp(auth()->user()->role,"operator") == 0) {
            $account_id =  (new \App\Helpers\CommonHelper)->getAccountData();
            
        } else {
            $operators = Managers::where('role','operator')->where('parent_manager_id',auth()->user()->id)->get();
        }
        $this->viewData['link'] ="Schedule_Management";
        $this->viewData['accounts'] = $accounts; 
        $this->viewData['operators'] = $operators;
        $this->viewData['events'] = $this->events;
        $this->viewData['authUserType'] = $authUserType;
        return view('scheduler/calender',$this->viewData);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->viewData['login_as'] = 'manager';
        $accounts = []; 
        $operators = [];
        if(strcasecmp(auth()->user()->role,"Top Manager") == 0) {
            $this->viewData['login_as'] = 'top_manager'; 
            $accounts = Account::where('require_sch',1)->where('status',1)->orderBy('name','asc')->get();    
        } elseif(strcasecmp(auth()->user()->role,"operator") == 0) {
            $this->viewData['login_as'] = "operator"; 
            $account_id =  (new \App\Helpers\CommonHelper)->getAccountData();
            //$accounts = Account::find($account_id);
            
        } else {
            $operators = Managers::where('role','operator')->where('parent_manager_id',auth()->user()->id)->get();
        }
        $this->viewData['accounts'] = $accounts; 
        $this->viewData['operators'] = $operators;
        $this->viewData['link'] ="Schedule_Management";
        $this->viewData['page_title'] = __("Create a schedule");
        $this->viewData['initTime'] = 5;
        return view($this->viewPath."schedule.create",$this->viewData);
    }

    /**
     * store schedule for operator
     */
    public function store(Request $request)
    {
        $requestData = $request->except('_token');
       // dd($requestData );
        if(auth()->user()->type_id == 3 ) {
            $requestData['account_id'] = auth()->user()->account_id;
        }
        $repeat_on_months = 0;
        $validation = Validator::make($requestData,[
            'from_date' => 'required|date',
            'operator_id' => 'required',
            'account_id' => 'required', 
            'start_time'=> 'required', 
            'end_time' => 'required',
            'time_slot' => 'required'
        ]);

        if($validation->fails()) {
            if($request->ajax()){
                return response()->json(['msg'=>__($validation->errors()->first())],401);
            }
        }
        $requestData['added_by'] = auth()->user()->id;
        if($requestData['is_repeat'] == 1) {
           $custom_repeat = (array) json_decode($requestData['custom_repeat']); 
           if($custom_repeat['is_repeat'] == 1) {
            $weeks = [];
                foreach($custom_repeat as $key=>$repeat) {
                        if(strpos($key,"repeat_on_weeks") !== FALSE) {
                            $weeks[] = $repeat;
                            unset($custom_repeat[$key]);
                        }
                }
                if(!empty($weeks)) {
                    $custom_repeat['repeat_on_weeks'] = implode(",",$weeks);
                }
                
            } 
            if($custom_repeat['is_end'] == 1) {
                if(!empty($custom_repeat['end_date'])) {
                    $cTempdateObj = (new \App\Helpers\CommonHelper)->cnvTimetoUserTimezone($custom_repeat['end_date'],"Y-m-d");
                    $custom_repeat['when_end'] = (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($cTempdateObj,1);
                }
                unset($custom_repeat['end_date']);
            }

            if($custom_repeat['is_end'] == 2) {
                $custom_repeat['when_end'] = $custom_repeat['occurrences'];
                unset($custom_repeat['occurrences']);
            }
            if($custom_repeat['is_repeat'] == 2) {
                $repeat_on_months = explode("_",$custom_repeat['repeat_on_months']);
                $repeat_on_months = count($repeat_on_months);
                foreach($custom_repeat as $key=>$repeat) {
                    if(strpos($key,"repeat_on_weeks") !== FALSE) {
                        unset($custom_repeat[$key]);
                    }
                }
            }
           $requestData = array_merge($requestData,$custom_repeat);
           
        }
        if(isset($requestData['custom_repeat'])) {
            unset($requestData['custom_repeat']);
        }
        $tempDate = $requestData['from_date']." ".$requestData['start_time']; 
        $tempdateObj = (new \App\Helpers\CommonHelper)->cnvTimetoUserTimezone($tempDate,"m/d/Y H:i");
        $endTime = $requestData['from_date']." ".$requestData['end_time']; 

        $tempEndDateObj = (new \App\Helpers\CommonHelper)->cnvTimetoUserTimezone($endTime,"m/d/Y H:i");
       
        $requestData['from_date'] = (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,1);
        $requestData['start_time'] =  (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,2);
        
        $requestData['end_time'] =  (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempEndDateObj,2);
        if($this->isAlreadySchedule($requestData)) {
            return  response()->json(['msg'=>__("The schedule already exists at this time.")],404); 
        }
        if($repeat_on_months == 1) {
            $requestData['repeat_on_months'] =  (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,9);
        }
    //    dd($startTimeNotDate->diffInMinutes($endTimeNotDate)
        if(Scheduler::create($requestData)) {
            return response()->json(['msg'=>__("The schedule successfully added."),'redirect'=>route('schedulerListing')], 200);
        }
        return response()->json(['msg'=>__("Something went wrong")],404);   
    }

    /** 
     * scheduler edit 
     */
    public function edit($id) 
    {
        $this->viewData['login_as'] = 'manager';
        $accounts = []; 
        $operators = [];

        $scheduler = Scheduler::findOrFail($id);
        
        $managers = Managers::where('role','manager')->where('account_id',$scheduler->account_id)->pluck('id')->toArray();
        $operators = Managers::select('id','first_name','last_name')->where('role','operator')->whereIn('parent_manager_id',$managers)->get();
        
        if(strcasecmp(auth()->user()->role,"Top Manager") == 0) {
            $this->viewData['login_as'] = 'top_manager'; 
            $accounts = Account::where('require_sch',1)->where('status',1)->orderBy('name','asc')->get();    
        } elseif(strcasecmp(auth()->user()->role,"operator") == 0) {
            $this->viewData['login_as'] = "operator"; 
            $account_id =  (new \App\Helpers\CommonHelper)->getAccountData();
            //$accounts = Account::find($account_id);
            
        } else {
            $operators = Managers::where('role','operator')->where('parent_manager_id',auth()->user()->id)->get();
        }
        if($scheduler->is_repeat == 2) {
            $t = explode("_",$scheduler->repeat_on_months);
            if(count($t) == 1) {
                $dateObj1 = Carbon::parse($scheduler->from_date." ".$scheduler->start_time);
                $scheduler->repeat_on_months = (new \App\Helpers\CommonHelper)->formatDateDatabase($dateObj1,9); 
            }
        }
        $custom_repeat = array(
            'repeat_on_weeks'=> $scheduler->repeat_on_weeks, 
            'is_repeat' => $scheduler->is_repeat, 
            'repeat_count' => $scheduler->repeat_count, 
            'repeat_on_months' => $scheduler->repeat_on_months,
            'is_end' => $scheduler->is_end, 
            'when_end' => ($scheduler->is_end == 1) ? (new \App\Helpers\CommonHelper)->formatDateDatabase($scheduler->when_end,1) :  $scheduler->when_end,
        );
        $dateObj = Carbon::parse($scheduler->from_date." ".$scheduler->start_time);
        $scheduler->from_date =(new \App\Helpers\CommonHelper)->formatDateSchedule($dateObj,1);
        $startObj = Carbon::parse($scheduler->start_time);
        $endObj =  Carbon::parse($scheduler->end_time);
        $scheduler->start_time = (new \App\Helpers\CommonHelper)->formatDateDatabase($startObj,2); 
        $scheduler->end_time = (new \App\Helpers\CommonHelper)->formatDateDatabase($endObj,2);
      //  dd($scheduler->from_date);
        $this->viewData['custom_repeat'] = json_encode($custom_repeat);
        $this->viewData['accounts'] = $accounts; 
        $this->viewData['scheduler'] = $scheduler;
        $this->viewData['operators'] = $operators;
        $this->viewData['link'] ="Schedule_Management";
        $this->viewData['menu'] ="Schedule_Management";
        //$this->viewData 
        $this->viewData['page_title'] = __("Edit a schedule");
        $this->viewData['initTime'] = 5;
        return view($this->viewPath."schedule.edit",$this->viewData);
    }

    /** 
     * update scheduler data
     */
    public function update(Request $request,$id) 
    {
        $requestData = $request->except('_token','_method');
       // dd($requestData );
       $repeat_on_months = 0;
        if(auth()->user()->type_id == 3 ) {
            $requestData['account_id'] = auth()->user()->account_id;
        }
        $validation = Validator::make($requestData,[
            'from_date' => 'required|date',
            'operator_id' => 'required',
            'account_id' => 'required', 
            'start_time'=> 'required', 
            'end_time' => 'required',
            'time_slot' => 'required'
        ]);

        if($validation->fails()) {
            if($request->ajax()){
                return response()->json(['msg'=>__("validation error")],401);
            }
        }

        if($requestData['is_repeat'] == 1) {
            $custom_repeat = (array) json_decode($requestData['custom_repeat']); 
            if($custom_repeat['is_repeat'] == 1) {
                $weeks = [];
                    foreach($custom_repeat as $key=>$repeat) {
                            if(strpos($key,"repeat_on_weeks") !== FALSE) {
                                $weeks[] = $repeat;
                                unset($custom_repeat[$key]);
                            }
                    }
                    if(!empty($weeks)) {
                        $custom_repeat['repeat_on_weeks'] = implode(",",$weeks);
                    }
                } 
                if($custom_repeat['is_end'] == 1 && isset($custom_repeat['end_date'])) {
                    if(!empty($custom_repeat['end_date'])) {
                        $cTempdateObj = (new \App\Helpers\CommonHelper)->cnvTimetoUserTimezone($custom_repeat['end_date'],"Y-m-d");
                        $custom_repeat['when_end'] = (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($cTempdateObj,1);
                        
                    }
                    unset($custom_repeat['end_date']);
                }

                if($custom_repeat['is_end'] == 2 && isset($custom_repeat['occurrences'])) {
                    $custom_repeat['when_end'] = $custom_repeat['occurrences'];
                    unset($custom_repeat['occurrences']);
                }
                if($custom_repeat['is_repeat'] == 2) {
                    $repeat_on_months = explode("_",$custom_repeat['repeat_on_months']);
                    $repeat_on_months = count($repeat_on_months);
                
                    foreach($custom_repeat as $key=>$repeat) {
                        if(strpos($key,"repeat_on_weeks") !== FALSE) {
                            unset($custom_repeat[$key]);
                        }
                    }
                }
                
            $requestData = array_merge($requestData,$custom_repeat);
            
        }
        if(isset($requestData['custom_repeat'])) {
            unset($requestData['custom_repeat']);
        }
        $tempDate = $requestData['from_date']." ".$requestData['start_time']; 
        $tempdateObj = (new \App\Helpers\CommonHelper)->cnvTimetoUserTimezone($tempDate,"m/d/Y H:i");

        $endTime = $requestData['from_date']." ".$requestData['end_time']; 

        $tempEndDateObj = (new \App\Helpers\CommonHelper)->cnvTimetoUserTimezone($endTime,"m/d/Y H:i");
        $requestData['from_date'] = (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,1);
        $requestData['start_time'] =  (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,2);
        $requestData['end_time'] =  (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempEndDateObj,2);
        if($repeat_on_months == 1) {
            $requestData['repeat_on_months'] =  (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,9);
        }
        if(Scheduler::where('id',$id)->update($requestData)) {
            return response()->json(['msg'=>__("The schedule successfully updated."),'redirect'=>route('schedulerListing')], 200);
        }

        return response()->json(['msg'=>__("Something went wrong")],404);   
    }

    /**
     * destroy scheduler
     */
    public function destroy($id) {
        $scheduler = Scheduler::findOrFail($id); 
        $scheduler->delete();
        \Session::flash('success', __('A schedule successfully deleted.'));
        return redirect()->route('schedulerListing');
    }
    /** 
     * get operator data by account id
     */
    public function getOperators(Request $request)
    {
        if(!isset($request->accountId) || empty($request->accountId)) {
            return  response()->json(['msg'=>__("invalid param")],404);
        }
        $managers = Managers::where('role','manager')->where('account_id',$request->accountId)->pluck('id')->toArray();
        $operators = Managers::select('id','first_name','last_name')->where('role','operator')->whereIn('parent_manager_id',$managers)->get();
        $htmlContent  = "<option value=''>".__('Select Operator')."</option>";
        foreach($operators as $operator) {
            $htmlContent .= "<option value='".$operator->id."'>".$operator->first_name. " ".$operator->last_name."</option>";
        }
        return response()->json(['html'=>$htmlContent], 200);
    }

    /**
     * get scheduler time 
     */
    public function getTime($schedule,$startTime)
    {
        $takeStart = $takeEnd = null;
        $startTimeNotDate = Carbon::parse($schedule->start_time); 
        $startTimeNotDate = (new \App\Helpers\CommonHelper)->formatDateDatabaseObj($startTimeNotDate);
        $endTimeNotDate = Carbon::parse($schedule->end_time);
        $endTimeNotDate = (new \App\Helpers\CommonHelper)->formatDateDatabaseObj($endTimeNotDate);
        $minutes = (new \App\Helpers\CommonHelper)->convertTimeToMinute($endTimeNotDate->format("H:i"));
       // $minutes = $startTimeNotDate->diffInMinutes($endTimeNotDate);
      //  $startTime->startOfDay();
        //$minutes = (new \App\Helpers\CommonHelper)->convertTimeToMinute($startTimeNotDate->format("H:i"));
        $mTime = (new \App\Helpers\CommonHelper)->convertTimeToMinute($startTimeNotDate->format("H:i"));
       
        //dd($mTime,$minutes,$minutes-$mTime);
        //dd($startTime->toImmutable());
        $takeStart = $startTime->copy();
        $takeEnd = $takeStart->copy();
        $takeEnd->startOfDay();
    //     if($schedule->id == 2) {
    //          print_r($takeStart."<>");
    //          print_r($takeEnd);
    //          dd($minutes,$mTime,$startTime);
    //    }
     //   dd($minutes,$mTime,$startTime);
      // $takeStart->addMinutes($mTime);
    //sdd($takeEnd,$takeStart);
        $minutes  = (new \App\Helpers\CommonHelper)->getEndTime($schedule->start_time,$schedule->end_time);
        $takeEnd->addMinutes($minutes);
        
        //dd($takeStart,$takeEnd);
        $tempArr =  [
            "start"=>$takeStart,
            "end" => $takeEnd
        ];
        $ts = $takeStart->copy();
        $te = $takeEnd->copy();
        $tempArrRef =  [
            "start"=>(new \App\Helpers\CommonHelper)->formatDateDatabaseObj($ts),
            "end" => (new \App\Helpers\CommonHelper)->formatDateDatabaseObj($te)
        ];

        $this->createScheduleObj($schedule,$tempArrRef);
        return  $tempArr;
    }

    /**
     * add schedule obj 
     */
    public function createScheduleObj($schedule,$times) 
    {
        $userTimeZone = $times['start'];
        $userTimeZoneEnd = $times['end'];
         if($schedule->id == 2) {
       //   print_r($times);
        }
        $this->sameTimeEvent[$userTimeZone->format("Y-m-d H:i")."-".$userTimeZoneEnd->format("Y-m-d H:i")][] = [
            'op'=> $schedule->operator->id, 
            'op_name' => $schedule->operator->first_name . " ".$schedule->operator->last_name,
            'index' => count( $this->events)
        ];
        $this->events[] = (Object) array(
            "title" => $schedule->operator->first_name . " ".$schedule->operator->last_name,
            "name" =>$schedule->operator->first_name . " ".$schedule->operator->last_name,
            "start" => $userTimeZone->format("Y-m-d H:i:s"),
            "end" => $userTimeZoneEnd->format("Y-m-d H:i:s"),
            "constraint" => 'availableForMeeting',
            "is_schedule" => true,
            "color" => '#ccc',
            "meeting_url"=> route("meeting.create",["scheduler_id"=>$schedule->id,"operator_id"=>$schedule->operator_id,"date"=>$userTimeZone->format("Y-m-d")]),
            'obj_type' => auth()->user()->type_id,
            "id" => $schedule->id
        );
    }

    /**
     * fetch list of events
     */
    public function getSchedules(Request $request){
        $accountId = 0; 
        $operatorId = 0; 
        $whereCond = [];
        $this->sameTimeEvent = [];
        if(isset($request->accountId) && !empty($request->accountId)) {
            $accountId = $request->accountId;
        }
        if(isset($request->operatorId) && !empty($request->operatorId)) {
            $operatorId = $request->operatorId;
        }
        if(auth()->user()->type_id == 3) {
            $accountId  = auth()->user()->account_id;
            if(empty($operatorId)) {
                $operatorId =  Managers::where('role','operator')->where('parent_manager_id',auth()->user()->id)->pluck('id')->toArray();
            }
        }

        if(auth()->user()->role == 'operator') {
            $accountData  =  (new \App\Helpers\CommonHelper)->getAccountData();
            if(!empty($accountData)) {
                $accountId = $accountData->id;
            }
            $operatorId = auth()->user()->id;
        }

        if(empty($accountId)) {
            return [];
        }
        $this->getMeetings($request,$accountId,$operatorId);
        $schedulers = Scheduler::where('account_id',$accountId);
        if(!empty($operatorId)) {
            if(is_array($operatorId)) {
                $schedulers = $schedulers->whereIn("operator_id",$operatorId);
            } else {
                $schedulers = $schedulers->where("operator_id",$operatorId); 
            }
        }

        $schedulers = $schedulers->get();
        $this->viewData['currentDate'] = (new \App\Helpers\CommonHelper)->formatDateDatabase(Carbon::now());
        
        $days = [
            Carbon::SUNDAY,
            Carbon::MONDAY,
            Carbon::TUESDAY,
            Carbon::WEDNESDAY, 
            Carbon::THURSDAY, 
            Carbon::FRIDAY, 
            Carbon::SATURDAY
        ];
        
        $this->events = [];
        $filtered = $schedulers->filter(function ($schedule, $key) use ($days,$request){
            $numberOfTime = -1;
            $totalEvents = 1;
            $startDate = Carbon::parse($schedule->from_date." ".$schedule->start_time);
            $testMs = Carbon::parse($schedule->from_date." ".$schedule->start_time);
            $testMs =  (new \App\Helpers\CommonHelper)->formatDateDatabase($testMs); 
            
            $testMs1 = Carbon::parse($schedule->from_date." ".$schedule->start_time);
            $minutes = (new \App\Helpers\CommonHelper)->convertTimeToMinute($schedule->end_time);
            $mTime = (new \App\Helpers\CommonHelper)->convertTimeToMinute($schedule->start_time);
            $takeSecond = $testMs1->addMinutes($minutes-$mTime);
            $cdate = $mainDate  = $startDate;
            $diffMin  = $cdate->diffInMinutes($takeSecond);
            if($diffMin < 5 && $diffMin < $schedule->time_slot  ) return false;
            $numberOfSlot = intval($diffMin/$schedule->time_slot);
            $times = $this->getTime($schedule,$startDate);
            $startDate = $times['start'];
            $userTimeZoneEnd = $times['end'];
                
            if(empty($schedule->is_end)) {
                $endDate = Carbon::parse($request->end);
                $endDate = (new \App\Helpers\CommonHelper)->formatDateDatabaseObj($endDate);
            } elseif($schedule->is_end == 1) {
                $endDate = $schedule->when_end;
                $tempDate = Carbon::parse($endDate);
                $tempDate1 = Carbon::parse($request->end);
                if(!$tempDate->gt($tempDate1))  {
                    $endDate = $schedule->when_end;
                } else {
                    $endDate = $request->end;
                }
            } elseif($schedule->is_end == 2 ) {
                $currentDate = Carbon::createFromFormat("Y-m-d H:i:s",$startDate);
                $endDate = $currentDate->addYear(2)->format('Y-m-d H:i:s');
                $numberOfTime = $schedule->when_end ;
                if($numberOfTime ==  1 || empty($numberOfTime)) {
                    return true;
                }
            }
           
            $endDate = Carbon::parse($endDate);
            $endDate->endOfDay();
            if($schedule->is_repeat == 1) {
                $weekDays = explode(",",$schedule->repeat_on_weeks);
                if(!empty($weekDays)){
                    $startDate = Carbon::parse($startDate);
                    for($date = $startDate; $date->lte($endDate);   $date->addWeek($schedule->repeat_count)) {
                        foreach($weekDays as $week) {
                            if($week == '') continue;
                            $tt = $date->weekday($week);
                            if($tt->lte($testMs) || $tt->gte($endDate)) {
                                continue;
                            }
                            $times = $this->getTime($schedule,$tt);
                            $userTimeZone = $times['start'];
                            $userTimeZoneEnd = $times['end'];
                            $totalEvents++;
                            if($totalEvents == $numberOfTime ) {
                                return true;
                            }
                        }
                    }
                }
            }elseif($schedule->is_repeat == 2) {
                if(empty($schedule->repeat_on_months)) {
                    return false;
                }
                $month = explode("_",$schedule->repeat_on_months);
                if(count($month) == 2) {
                    $dy = strtolower($month[1]);
                    if($month[0]  == 1) {
                        $f = 'first'; 
                    } else if($month[0] == 2) {
                        $f = "second";
                    } else if($month[0] == 3) {
                        $f = "third";
                    } else if($month[0] == 4) {
                        $f = "fourth";
                    } else if($month[0] == 5) {
                        $f = "fifth";
                    }
                    $startDate = Carbon::parse($startDate)->startOfMonth();
                    for($date = $startDate; $date->lte($endDate); $date->addMonths($schedule->repeat_count)) {
                        $dynamicDate = Carbon::parse( "{$f} {$dy} of {$date->format('F')} ".$schedule->start_time );
                        if($mainDate->diffInDays($dynamicDate) == 0 || $dynamicDate->gte($endDate) ) {
                            continue;
                        }

                        $times = $this->getTime($schedule,$dynamicDate);
                        
                        $totalEvents++;
                        if($totalEvents == $numberOfTime  ) {
                            return true;
                        }
                    }
                }else{
                    $startDate = Carbon::parse($startDate)->startOfMonth()->addMonths(1);//->nex
                    for($date = $startDate; $date->lte($endDate); $date->addMonths($schedule->repeat_count)) {
                        $dynamicDate = $date->firstOfMonth()->addDays($month[0]-1);
                        $dynamicDate = Carbon::parse($dynamicDate->format("Y-m-d")." ".$schedule->start_time);
                        $times = $this->getTime($schedule,$dynamicDate);
                        $totalEvents++;
                        if($totalEvents == $numberOfTime ) {
                            return true;
                        }
                    }
                }
            }
            return true;
        });

        foreach($this->sameTimeEvent as $dateTime => $tempEvent) {
            if(count($tempEvent) >= 2) {
                $tempRow = [];
                foreach($tempEvent as $index=>$subEvent) {
                    if($index != 0) {
                        $tempT =$this->events[$tempEvent[0]['index']];
                        array_push($tempRow, $this->events[$tempEvent[$index]['index']]);
                        unset($this->events[$subEvent['index']]);
                        if($index+1 == count($tempEvent)) {
                            $tempT =$this->events[$tempEvent[0]['index']];
                            $tempT->more = json_encode($tempRow);
                            $this->events[$tempEvent[0]['index']] = $tempT;
                        }
                    }else{
                        array_push($tempRow, $this->events[$tempEvent[$index]['index']]);
                        $this->events[$tempEvent[0]['index']]->title = count($tempEvent)." ".__("Operators");
                    }
                }
            }
        }
        return array_merge($this->meetingEvents,$this->events);
    }

    /**
     * get list of meeting on given date 
     */
    public function getMeetings($request,$accountId,$operatorId = 0) 
    {
        $meetings = \App\Meeting::where('status',"!=",4)->whereHas('schedule',function($q) use($accountId){
            $q->where('account_id',$accountId); 
        });
        if(!empty($operatorId)) {
            if(is_array($operatorId)) {
                $meetings->whereIn('operator_id',$operatorId);
            } else {
                $meetings->where('operator_id',$operatorId);
            }
        }
        if(!empty($accountId)) {
     //       $meetings->where('account_id',$accountId);
        }
        $this->meetingEvents = [];
        $startDate = $request->start; 
        $endDate = $request->end;
        if(!empty($startDate)) {
            $startDate = (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($startDate,1);
        }

        if(!empty($endDate)) {
            $endDate = (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($endDate,1);
        }

        if(!empty($startDate)) {
            $meetings->where("meeting_date", ">=",$startDate);
        }

        if(!empty($endDate)) {
            $meetings->where("meeting_date", "<=",$endDate);
        }
        $meetings = $meetings->get();
        $meetings->filter(function($meeting){
            $meetingStartTime = Carbon::parse($meeting->meeting_date." ".$meeting->start_time);
        
            $userTimeZone = (new \App\Helpers\CommonHelper)->formatDateDatabase($meetingStartTime);
            $meetingEndTime = Carbon::parse($userTimeZone)->addMinutes($meeting->slot_minute)->format("Y-m-d H:i:s");
            $this->meetingEvents[] = (Object) array(
                "title" => $meeting->full_name." " .$meeting->operator->first_name . " ".$meeting->operator->last_name,
                "start" => $userTimeZone,
                "end" => $meetingEndTime,
                "constraint" => 'availableForMeeting',
                "is_meeting" => true,
                "url" => (auth()->user()->type_id == 3 || auth()->user()->type_id == 4 ) ?   route("meeting.edit",$meeting->id) : '',
                'view_url' => route("meeting.view",$meeting->id),
                'obj_type' => auth()->user()->type_id,
                'cancel_url' =>  route("meeting.cancel",$meeting->id),
                "color" => '#7267ab'
            );
        });
        //$temp = Carbon::parse($startDate;
    }
    
    /**
     * check already schedule exist for given operator
     */
    public function isAlreadySchedule($requestData)
    {
        $schedulers = Scheduler::where('operator_id',$requestData['operator_id'])->where('from_date',$requestData['from_date'])->where(function($q) use($requestData) {
            $q->where(function($q0) use($requestData){
                $q0->where('start_time',">=",$requestData['start_time'])->where('start_time',"<=",$requestData['end_time']);
            })->orWhere(function($q2) use($requestData){
                $q2->where('start_time',"<=",$requestData['start_time'])->where('end_time',">=",$requestData['end_time']);
            });
            // ->orWhere(function($q1) use($requestData){
            //         $q1->where('start_time',"<=",$requestData['start_time'])->where('end_time',"<=",$requestData['end_time']);
            // });
        })->count();
        return $schedulers;
        // $isExist = 0; 
        // $schedulers->filter(function($schedule) use ($isExist,$requestData) {
            
        // });
    }

}
