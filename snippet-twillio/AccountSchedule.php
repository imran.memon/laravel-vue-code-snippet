<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AccountSchedule extends Model
{
    use SoftDeletes;
    protected $table = 'account_schedules';
    protected $guarded = [
        
    ];
    
    
}