<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OperatorType; 
use DataTables;
use Validator;

class OperatorTypesController extends Controller
{
    
    //view data
    private $viewData; 
    
    //view path
    private $viewPath;

    /** 
     * default settings 
     */
    public function __construct()
    {
        $this->viewPath = "accounts.tabs.operator_types.";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->account_id;
        $model = OperatorType::where('account_id',$id);   
        
        return DataTables::eloquent($model)->addColumn('edit_delete',function($data) {
            $deleteUrl  = route('delete-operator_type',$data->id);
            $editUrl = route('operator-types.edit',$data->id);
            return '<button class="mb-1 mr-2 px-2 py-1 btn  ml-2  btn-danger deleteStatus" data-route="'.$deleteUrl.'" type="button"><i class="fa fa-trash"></i></button><a class="newOperatorTypes" href="javascript:void(0)" data-action="'.$editUrl.'"><button class="mb-1 px-2 py-1 btn btn-info" type="button"><i class="fa fa-edit"></i></button></a>';
        })->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $this->viewData['account_id'] = $request->account_id;
        $this->viewData['html'] = view($this->viewPath."add",$this->viewData)->render(); 
        return response()->json($this->viewData,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(empty($request->title) || empty($request->account_id)) {
            return response()->json(['msg'=> __("A title is required.")], 401);
        }
        $insertData = array(
            'account_id'=>$request->account_id,
            'title' => $request->title
        );
        if(OperatorType::create($insertData)) {
            return response()->json(['msg'=>__("An operator type has been added.")], 200);
        }
        return response()->json(['msg'=>__("Something went wrong.")], 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        //
        $type = OperatorType::findOrFail($id);
        $this->viewData['id'] = $id;
        $this->viewData['account_id'] = $request->account_id;
        $this->viewData['type'] = $type;
        $this->viewData['html'] = view($this->viewPath."edit",$this->viewData)->render(); 
        return response()->json($this->viewData,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'title' => 'required',
        );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return response()->json(['msg'=>$validator->errors()->first()], 401);
        }
        $type = OperatorType::find($id);
        $type->title = $request->title;
        if($type->save()) {
            return response()->json(['msg'=>__("An operator type has been updated.")], 200);
        }
        return response()->json(['msg'=>__("Something went wrong.")], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $updateData = [
            'deleted_at'=>\Carbon\Carbon::now(), 
            'deleted_by'=> auth()->user()->id 
        ];
        if(OperatorType::where(['id'=>$id])->update($updateData)) { 
            return response()->json(['msg'=>"An operator type has been deleted."], 200);
        }
        return response()->json(['msg'=>__("Something went wrong.")], 404);
    }

    /**
     * get list of operator type by account id 
     */
    public function getOperatorTypes(Request $request)
    {
        $id = $request->account_id;
        $types = OperatorType::where('account_id',$id)->get();
        return $types;
    }

    /** 
     * list of operator type by manager id
     * @param managerId
     */
    public function getOperatorTypesByManager(Request $request,$id)
    {
        $html = "<option >".__("Choose"). " ...</option>";
        $manager  = \App\Managers::where('id',$id)->first();
        if(empty($manager) || empty($manager->account_id)) {
            return response()->json(['html'=>$html], 200);
        }
        $account_id = $manager->account_id;
        $types = OperatorType::select('id','title')->where('account_id',$account_id)->get();
        
        foreach($types as $type) {
            $html .= "<option value='$type->id'>".$type->title."</option>";
        }
        return response()->json(['html'=>$html], 200);
    }
}
