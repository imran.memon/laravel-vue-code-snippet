
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="ElectraPro">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <link rel="icon" type="image/x-icon" href="{{asset('img/website_logo.png')}}">

   <title>{{env('APP_NAME')}} - {{ $page_title }}</title>

   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
   <script type="text/javascript" src="{{ asset('vendor/jquery/dist/jquery.js')}}"></script><!-- BOOTSTRAP-->
    <link rel="stylesheet" href="{{ asset('dist/css/demo.css')}}">
   <link rel="stylesheet" href="{{ asset('dist/css/dropify.min.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/brands.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/regular.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/solid.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/fontawesome.css')}}"><!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css')}}"><!-- =============== BOOTSTRAP STYLES ===============-->
   
   <!-- BOOTSTRAP 3.3.7-->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/flatly/bootstrap.min.css">
   <link href="{{ asset('icon/css/icon-picker.min.css')}}" media="all" rel="stylesheet" type="text/css" />  
   <!--  ANIMATE.CSS-->
   <link rel="stylesheet" href="{{ asset('vendor/animate.css/animate.css')}}">
   <!-- WHIRL (spinners)-->
   <link rel="stylesheet" href="{{ asset('vendor/whirl/dist/whirl.css')}}"> 
   <!-- Datatables-->
   <link rel="stylesheet" href="{{ asset('vendor/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css')}}">
   <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css')}}">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
   <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css')}}">  
   <link rel="stylesheet" href="{{ asset('css/style.css?'.time())}}">
   <link rel="stylesheet" href="{{ asset('css/morris.css')}}">
  
   <!-- =============== BOOTSTRAP STYLES ===============-->
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">  
    <script type="text/javascript" src="{{ asset('js/custom.js?'.time())}}"></script>
    <script type="text/javascript" src="{{asset('/js/ckfinder/ckfinder.js')}}"></script>

    <!-- sweet alert -->
    <script type="text/javascript" src="{{asset('/js/sweetalert.min.js')}}"></script>
    

  <!-- ckeditor -->
  <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>
  <script src="{{ asset('vendor/ckeditor/config.js')}}"></script>
  <!-- <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script> -->

  <script>CKFinder.config( { connectorPath: '{{asset("/ckfinder/connector")}}' } );</script>
  <script type="text/javascript">      
        function getsiteurl()
        {
          var BASE_URL = {!! json_encode(url('/')) !!}

          return BASE_URL;
        }
        var mapBoxKey="{{env('MAP_BOX_KEY')}}";
    </script>
   <script src="https://kit.fontawesome.com/d565f77dcb.js" crossorigin="anonymous"></script>
<style type="text/css" media="screen">
.aside-container {
display: none;
}
.session_info div#calling_map .calling {
flex-basis: 30%;
}
.top-map {
flex-basis: 68%;
}
.topnavbar-wrapper .navbar-nav{
display: none;
}
.topnavbar-wrapper .navbar {
justify-content: flex-start;
}
@media (min-width: 768px){
.wrapper .section-container {
margin-left: 0 !important;
}
}
@media (max-width: 1024px){
.session_info div#calling_map {
height: 100%;
padding: 5px;
background-color: #fff;
}
.top-map {
height: 600px;
}
.top-map ul.map_list li {
width: 100%;
margin-bottom: 10px;
border-right: 0;
border-bottom: 1px solid #cfdbe2;
padding-bottom: 10px;
}
.session_info .session_info_info .info_details.user ul {
flex-direction: column;
}
.session_info .session_info_info .info_details.user ul li {
margin-bottom: 20px;
}
.session_info .nav-tabs {
display: flex;
padding: 20px;
}
.session_info {
margin-bottom: 50px;
}
.session_info .top-map ul.map_list {
top: 5px;
flex-direction: column;
}
}
@media (max-width: 767px){
.top-map {
height: 800px;
}

}
</style>
   @yield('style')
</head>

<body>
<div class="customLoader"> <div class="mySpinner"></div></div>
   <div class="wrapper">

 <!-- top navbar-->
      <header class="topnavbar-wrapper">
         <!-- START Top Navbar-->
         <nav class="navbar topnavbar">
            <!-- START navbar header-->
            <div class="navbar-header">
            <a class="navbar-brand" href="javascript:void(0)">
              <div class="brand-logo">
                <img class="img-fluid" height="40px" src="{{url((new \App\Libraries\GeneralSetting)->getSettingInfo()['app_logo'])}}" alt="App Logo" style="padding-bottom:4px;height: 40px">
                
              </div>
              <div class="brand-logo-collope">
                <img class="" height="40px" src="{{asset('img\icon.png')}}" alt="App Logo">
              </div>
            </a></div><!-- END navbar header-->
            <!-- START Left navbar-->
            
            
         </nav><!-- END Top Navbar-->
      </header><!-- sidebar-->
    <section class="section-container">
    <div class="content-wrapper">
        <div class="content-heading justify-content-between">
            <?php 
$date = date('Y-m-d h:i:sa');

?>
            <div>{{__('Session / ')}} <?php echo (new \App\Helpers\CommonHelper)->formatDate($date)?> <label id="nameDisplay" style="font-weight:normal"></label></div> 

            <!-- <div class="share_back">
                <button class="btn btn-info shareData ml-3" type="button" id="share"  data-toggle="modal" data-target="#shareModal">{{__('Share')}}</button>
                <button class="backbtn btn btn-info float-right" id="back">{{__('Back')}}</button>
            </div> -->
        </div>        
        

         <div class="session_info" role="tabpanel">
            <ul class="nav nav-tabs nav-fill bg-white" role="tablist">
                @if(($session->allow_location == 1 || ($session->allow_recording == 1 && !empty($session->composition_status)) ))
                    <li class="nav-item" role="presentation"> 
                        <a class="nav-link bb0 active applicationGeneral genBtn" href="#session" aria-controls="session" role="tab" data-toggle="tab" aria-selected="true">
                            <input type="hidden" name="sessionId" id="sessionId" value="{{$sessionId}}" />
                            {{__('Session')}}
                        </a>
                    </li>
                @endif
                <li class="nav-item" role="presentation"> 
                    <a class="nav-link bb0 apiSettings apiBtn" href="#session_info" aria-controls="session_info" role="tab" data-toggle="tab" aria-selected="false">
                        {{__('Session info')}}
                    </a>
                </li>
            </ul>

            <div class="tab-content p-0 bg-white">
                <div class="tab-pane {{((!empty($session->composition_status) && ($session->allow_recording == 1))
                    || $session->allow_location == 1)  ? 'active' : ''}}" id="session" role="tabpanel">
                    <div id="calling_map">
                        @if($session->allow_recording == 1 && !empty($session->composition_status))
                        <div class="calling">
                            <div class="outgoing_call">
                                <h1>{{__('Video call')}} 
                                    <!-- <span>{{'00:00'}}</span> -->
                                </h1>
                            </div>
                            <div id="call_screen" class="h-100">
                                @if(!is_null($videoPath))
                                <div class="operatorVideoDiv1">
                                    <video controls>
                                        <source src="{{ $videoPath}}" type="video/mp4">
                                        {{__('Your browser does not support the video tag')}}.
                                    </video>
                                </div>
                                @elseif($session->allow_recording == 0)
                                    <div class="d-flex h-100 w-100 align-items-center justify-content-center">
                                        <h1>
                                            {{__('No Video Recording Permission')}}
                                        </h1>
                                    </div>
                                @else
                                    <div class="d-flex h-100 w-100 align-items-center justify-content-center">
                                        <h1>
                                            {{__('Video is not composed yet!')}}
                                        </h1>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @endif
                        @if($session->allow_location == 1)
                        <div class="top-map">
                            <ul class="map_list">
                                <li>
                                    <div class="man_content">
                                        <div class="user-name" id="appUserFullName"></div>
                                        <div class="user-number" id="appUserNumber"></div>
                                    </div>
                                </li>
                                <li class="address" id="address"></li>
                                <li >
                                    <div class="man">
                                        <img style="width: 23px;" src="{{asset('image/app_user/phone.svg')}}" alt="">
                                    </div>
                                   
                                    <div class="man_content">
                                        <div id="deviceType"></div>
                                        <div class="more"  id="deviceInfo"  data-toggle="modal" data-target="#deviceInfoModel">{{__('More')}}</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="man">
                                        <img alt="https://via.placeholder.com/100x100" src="{{url('uploads/'.$details->image)}}">
                                    </div>
                                    <div class="man_content">
                                        <span id="operatorName"></span>
                                    </div>
                                </li>
                            </ul>
                            <div id="map" style="width: 100%; height: 400px;"></div>
                        </div>
                        @endif
                    </div>
                </div>

                    <div class="tab-pane {{((empty($session->composition_status) && ($session->allow_recording == 0))
                        && $session->allow_location == 0)  ? 'active' : ''}}" id="session_info" role="tabpanel">
                        <div id="calling_map" class="h-100 pb-5">
                            <div class="session_info_info">
                                <div class="info_details">
                                    <h3 class="title">{{__('Info')}}</h3>
                                    <ul class="info_list">
                                        <li>{{__('Status')}} : @if(!empty($status))  <strong  style="color:{{$status->color_code}};">{{$status->status_name}}</strong> @endif  </li>
                                        <li>{{__('Started')}} : 
                                            <strong>
                                                {{-- {{$session->call_start_date_time}} --}}

                                                {{ !empty($session->call_start_date_time) ? (new \App\Helpers\CommonHelper)->formatDate($session->call_start_date_time) : '00/00/0000 00:00:00' }}
                                            </strong>
                                        </li>
                                        <li>{{__('Ended')}}: 
                                            <strong>
                                                {{-- {{ $session->call_end_date_time }} --}}
                                                
                                                {{-- {{ $session->call_end_date_time }} --}}
                                                {{($session->call_end_date_time != '' && !empty($session->call_start_date_time))?(new \App\Helpers\CommonHelper)->formatDate($session->call_end_date_time):'00/00/0000 00:00:00'}}
                                            </strong>
                                        </li>
                                        <li>{{__('Total Time')}} : 
                                            <strong>
                                                @if(!empty($session->duration))
                                                    {{(new \App\Helpers\CommonHelper)->displayDuration($session->duration)}} {{__('minutes')}}
                                                @else
                                                    00:00 {{__('minutes')}}
                                                @endif
                                            </strong>
                                        </li>
                                    </ul>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th colspan="2">{{__('Room Sid')}}</th>
                                            <td colspan="2">{{$session->room_sid}}</td>
                                        </tr>
                                         <tr>
                                            <th colspan="2">{{__('Room Name')}}</th>
                                            <td colspan="2">{{$session->room_name}}</td>
                                        </tr>
                                         <tr>
                                            <th></th>
                                            <th>{{__('Duration')}}</th>
                                            <th>{{__('Identity')}}</th>
                                            <th>{{__('PID')}}</th>
                                        </tr>
                                        @foreach($participants as $key=>$participant)
                                        <tr>
                                            <td>{{($participant->type == 'customer') ? __("Participant Time ") . $key : __("Operator Time")}}</td>
                                            <td>
                                            @if($participant->type != 'customer')
                                                {{(new \App\Helpers\CommonHelper)->displayDuration($session->duration)}}
                                            @else 
                                                {{(new \App\Helpers\CommonHelper)->displayDuration($participant->duration)}}
                                            @endif
                                            </td>
                                            <td>{{$participant->identity}}</td>
                                            <td>{{$participant->participant_id}}</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <th >{{__('Recording Time')}}</th>
                                            <td colspan="3"><strong>
                                                @if(!empty($session->duration))
                                                    {{(new \App\Helpers\CommonHelper)->displayDuration($session->duration)}}
                                                @else
                                                    00:00
                                                @endif
                                            </strong></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="info_details user">
                                    <h3 class="title">{{__('User')}}</h3>
                                    <ul class="user_list">
                                        <li>
                                           <!--  <div class="man">
                                                <img src="https://via.placeholder.com/100x100" alt="">
                                            </div> -->
                                            <div class="man_content">
                                                <strong>{{$appUser->name}}</strong>
                                                <a href="#">{{$appUser->number}}</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="man">
                                                <img style="width: 23px;" src="{{asset('image/app_user/phone.svg')}}" alt="">
                                            </div>
                                            <div class="man_content">
                                                <strong>{{$appUser->device_type}}</strong>            
                                            </div>
                                        </li>
                                    </ul>
                                    <p>{{__('Location')}} : <strong>{{$session->address}}</strong> </p>
                                </div>
                                <div class="info_details user">
                                    <h3 class="title">{{__('Other Participants')}}</h3>
                                    @php $i =0; @endphp
                                        @if($appUserOther)
                                         
                                        @foreach($appUserOther as $appUserVal)
                                       
                                       <ul class="user_list">
                                        <li>
                                            <div class="man_content">
                                                <strong>{{$appUserVal['name']}}</strong>
                                                <a href="#">{{$appUserVal['number']}}</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="man">
                                                <img style="width: 23px;" src="{{asset('image/app_user/phone.svg')}}" alt="">
                                            </div>
                                            <div class="man_content">
                                                <strong>{{$appUserVal['device_type']}}</strong>            
                                            </div>
                                        </li>
                                         </ul>
                                        @php $i++; @endphp
                                        @endforeach
                                        @endif
                                </div>
                                <div class="info_details user operator">
                                    <h3 class="title">{{__('Operator')}}</h3>
                                 
                                    <ul>
                                        <li>
                                            <div class="man">
                                                <img src="{{url('uploads/'.$details->image)}}" alt="https://via.placeholder.com/100x100">
                                            </div>
                                            <div class="man_content">
                                                <strong>
                                                    @if($details && !empty($details))
                                                        {{$details->first_name.' '.$details->last_name}}
                                                    @endif
                                                </strong>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row p-3">
                                    <div class="info_details user files col-md-6">
                                        <h3 class="title">{{__('Incoming files')}}</h3>
                                         <ul>
                                            @if(array_key_exists('customer',$filesList))
                                                @foreach($filesList['customer'] as $key => $val)
                                                     <li><a  href="{{url('/videocallAttachments/'.$val['file'])}}"
                                                            target="_blank">{{$val['file']}}</a>{{($val['file_size'])?round($val['file_size']/1024).'kb':''}}</li>
                                                 @endforeach
                                             @endif
                                         </ul>
                                    </div>
                                    <div class="info_details user files col-md-6">
                                        <h3 class="title">{{__('Outgoing files')}}</h3>
                                         <ul>
                                             @if(array_key_exists('operator',$filesList))
                                                @foreach($filesList['operator'] as $key => $val)
                                                      <li><a  href="{{url('/videocallAttachments/'.$val['file'])}}"
                                                              target="_blank">{{$val['file']}}</a>{{($val['file_size'])?round($val['file_size']/1024).'kb':''}}</li>
                                                 @endforeach
                                             @endif
                                        </ul> 
                                    </div>
                                </div>
                            </div>

                            <div class="operator_section">
                                <h3>{{__('Operator comments')}}</h3>
                                <div>{!! $session->operator_comment !!}</div>
                                <!--<textarea name="operatorComment1" id="operatorComment1">-->
                                    
                               <!-- </textarea>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
       
</section>
<script type="text/javascript">
   

    $('#startShareSession').click(function() {
        alert("here");
        var phoneNumer = $('#phone').val();
        var name       = $('#name').val();
        var email       = $('#email').val();
        var emailSubject       = $('#emailSubject').val();
        var emailMessage       = $('#emailMessage').val();

        if(phoneNumer == ''){
            $('#phoneError').text('Please enter Phone number');
        }

        if(name == ''){
            $('#nameError').text('Please enter Name');
        }
        if(email == ''){
            $('#emailError').text('Please enter Name');
        }if(emailSubject == ''){
            $('#emailSubjectError').text('Please enter Name');
        }if(name == ''){
            $('#emailMessageError').text('Please enter Name');
        }
        if(emailMessage != '' && name != ''){
            $('#newShareSessionForm').submit();
        }
    });
</script>
       
<footer class="footer-container">
    <span>{{((new \App\Libraries\GeneralSetting)->getSettingInfo()['reserved_text'])}}</span> 
</footer>

 </div>
 @yield('modal')

<!-- Operator Dashboard -->
<div class="modal" id="startNewSessoinModal">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">

        <div class="modal-header">
            <h4 class="modal-title d-block align-center">{{__('New Session')}}</h4>
        </div>
        
        <?php /*
        @if($errors->has('phone_number'))
            <span class="text-danger">{{ $errors->first('phone_number') }}</span>
        @endif

        @if($errors->has('name'))
            <span class="text-danger">{{ $errors->first('name') }}</span>
        @endif
        
        @if(Session::has('diplicate'))
            <span class="text-danger">
                {{ Session::get('diplicate') }}
            </span>
        @endif
        */ ?>

        <div class="modal-body">
            <?php
                $country = DB::table('country')->orderBy('country_code', 'asc')->groupBy('country_code')->get();
            ?>
            <form action="{{ url('/start_video_call') }}" method="post" id="newSessionForm">
                @csrf
                <div class="form-group">
                    <label for="phone_number">{{__('Phone Number')}}</label>
                    <div class="d-flex">
                        <select id="country_code" name="country_code" class="form-control w-50 px-0">
                            @foreach($country as $co)
                            <option value="{{"+".$co->country_code}}" {{$co->country_name_en=="Israel"?"selected":" "}}>{{"+".$co->country_code}}</option>
                            @endforeach
                        </select>
                        <input type="text" name="phone_number" id="phone_number" class="form-control allownumericwithoutdecimal" value="" autocomplete="off" maxlength="15" />
                    </div>
                    <div id="phoneNumError" class="text-danger"></div>
                </div>
                <div class="form-group">
                    <label for="name">{{__('Name')}}</label>
                    <input type="text" name="name" id="name" class="form-control" value="" autocomplete="off" />
                    <div id="nameError" class="text-danger"></div>
                </div>
                <p class="text-warning">{{__('This will send a text message to the client with a URL to join a video call')}}</p>
                
                <div class="form-group submit align-center d-flex">
                    <input type="button" class="btn btn-success m-auto" value="{{__('Start')}}" id="start" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div class="modal" id="shareModal">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">

        <div class="modal-header">
            <h4 class="modal-title d-block align-center">{{__('Share Session')}}</h4>
        </div>
        

        <div class="modal-body">

            <form action="{{ url('/send_share_seesion') }}" method="post" id="newShareSessionForm">
                @csrf
                <div class="row">
                <input type="hidden" value=""  name="share_session_id" id= "share_session_id"/>
                <div class="form-group col-sm-6">
                    <label for="name">{{__('Name')}}</label>
                    <input type="text" name="nameShare" id="nameShare" class="form-control" value="{{(old('nameShare'))?old('nameShare'):''}}" autocomplete="off" />
                    <div id="nameShareError" class="text-danger"></div>
                     @if ($errors->has('nameShare'))
                  <span class="text-danger">{{ $errors->first('nameShare') }}</span>
                  @endif
                </div>
                <div class="form-group col-sm-6">
                    <label for="name">{{__('Phone')}}</label>
                    <input type="number" name="phone" id="phone"hone class="form-control" value="{{(old('phone'))?old('phone'):''}}" autocomplete="off" />
                    <div id="phoneError" class="text-danger"></div>
                     @if ($errors->has('phone'))
                  <span class="text-danger">{{ $errors->first('phone') }}</span>
                  @endif
                </div>
                </div>
                <div class="form-group">
                    <label for="name">{{__('Email')}}</label>
                    <input type="email" name="email" id="email" class="form-control" value="{{(old('email'))?old('email'):''}}" autocomplete="off" />
                    <div id="emailError" class="text-danger"></div>
                     @if ($errors->has('email'))
                  <span class="text-danger">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <div class="form-group">
                    <label for="name">{{__('Email Subject')}}</label>
                    <input type="text" name="emailSubject" id="emailSubject" class="form-control" value="{{(old('emailSubject'))?old('emailSubject'):''}}" autocomplete="off" />
                    <div id="emailSubjectError" class="text-danger"></div>
                     @if ($errors->has('emailSubject'))
                  <span class="text-danger">{{ $errors->first('emailSubject') }}</span>
                  @endif
                </div>
                <div class="form-group">
                    <label for="name">{{__('Email message')}}</label>
                    <input type="text" name="emailMessage" id="emailMessage" class="form-control" value="{{(old('emailMessage'))?old('emailMessage'):''}}" autocomplete="off" />
                    <div id="emailMessageError" class="text-danger"></div>
                     @if ($errors->has('emailMessage'))
                  <span class="text-danger">{{ $errors->first('emailMessage') }}</span>
                  @endif
                </div>
                
                <div class="form-group submit align-center d-flex">
                    <input type="button" class="btn btn-success m-auto" value="{{__('Send')}}" id="startShareSession" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- Operator Dashboard -->
<div class="modal" id="deviceInfoModel">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">

        <div class="modal-header">
            <h4 class="modal-title d-block align-center">{{__('Device Info')}}</h4>
        </div>
        <div class="modal-body">
              @if(isset($appUser->device_type))
               <strong> {{$appUser->device_type}}</strong>
              @endif
        </div>
    </div>
  </div>
</div>
 <!-- =============== VENDOR SCRIPTS ===============-->
  <script>
  $(document).ready(function(){
	$('li.Session').addClass('active');

	var markers = [];
	displayMap();

    function displayMap(){
        //alert("in");
    	var sessionId = $('#sessionId').val();

    	$.ajax({
            type: "GET",
            url: getsiteurl()+'/get/sharesession/'+sessionId,
            success: function(response) {    
                //alert("success");
			   // var myLatLng = {lat: parseFloat(response.callDetail.latitude), lng: parseFloat(response.callDetail.longitude)};
                
                var map = L.map('map').setView([parseFloat(response.callDetail.latitude), parseFloat(response.callDetail.longitude)], 13);
                        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        maxZoom: 18,
                        id: 'mapbox/streets-v11',
                        tileSize: 512,
                        zoomOffset: -1,
                        accessToken: 'pk.eyJ1IjoiZHVzaHlkIiwiYSI6ImNrajluazMycDE4dngzMG10NXg4aHlhbzUifQ.FhOotjl3GW_S1fIuKoJpdg'
					}).addTo(map);
					
				// var marker = new google.maps.Marker({
				// 	position: myLatLng,
				// 	map: map,
                // 	icon: getsiteurl()+'/image/map_marker.png'
				// });
				var myIcon = L.icon({
					iconUrl: getsiteurl()+'/image/map_marker.png',
				});
				L.marker([parseFloat(response.callDetail.latitude), parseFloat(response.callDetail.longitude)],{icon: myIcon}).addTo(map);
				// var map = new google.maps.Map(document.getElementById('map'), {
				//     zoom: 18,
				//     center: myLatLng,
				// 	disableDefaultUI: true,
					

				// });

				// var marker = new google.maps.Marker({
				// 	position: myLatLng,
				// 	map: map,
                //                         icon: getsiteurl()+'/image/map_marker.png'
				// });

				// markers.push(marker);

                // var lat  = (typeof response.callDetail.latitude != 'undefined' && response.callDetail.latitude != 0) ? parseFloat(response.callDetail.latitude) : 31.7964453;
                // var lng = (typeof response.callDetail.longitude != 'undefined' && response.callDetail.longitude != 0) ? parseFloat(response.callDetail.longitude) : 35.1053183;
                
				// var astorPlace = {lat, lng};
				// panorama = map.getStreetView();
				// panorama.setPosition(astorPlace);
				// panorama.setPov(/** @type {google.maps.StreetViewPov} */({
				// 	heading: 265,
				// 	pitch: 0
				// }));
				// panorama.setVisible(true);
				

				$('#address').text(response.callDetail.address);	
				$('#deviceType').text(response.appUser.device_type);	
				$('#operatorName').text(response.details.first_name+" "+response.details.last_name);	
				$('#nameDisplay').text(response.appUser.name);
				$('#appUserFullName').text(response.appUser.name);	
				$('#appUserNumber').text(response.appUser.number);	
            }
        });
	}

	CKEDITOR.editorConfig = function (config) {
	    config.language = 'es';
	    config.uiColor = '#F7B42C';
	    config.height = 300;
	    config.toolbarCanCollapse = true;

	};
	
	/*CKEDITOR.replace('operatorComment', {
	    toolbar: [
		    { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		    { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo' ] },
		    { name: 'editing', groups: [ 'find', 'selection' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		    { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
		    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		    { name: 'insert', items: [ 'Flash','HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak' ] },
		    { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
		    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		    { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
		    { name: 'others', items: [ '-' ] }
		]
	});
*/
	$('#back').click(function(){
		window.location.replace(getsiteurl()+'/session');
	});
});
  (function() {
    'use strict';

    $(initDatatables);

    function initDatatables() {

        if (!$.fn.DataTable) return;

        //Zero configuration

        $('#datatable1').DataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'stateSave': true,
            "order":[[0,'desc']],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
             "infoCallback": function( settings, start, end, max, total, pre ) {
              return "{{__('Showing')}} "+start +" {{__('to')}} "+ end+ " {{__('of')}} " +total+ " {{__('entries')}}";
            },
            oLanguage: {
               sEmptyTable: "{{__('Empty Data Dable')}}",
               sZeroRecords: "{{__('No records')}}",
                sLengthMenu: '_MENU_ {{__("records per page")}}',
                sSearch: "{{__('Search')}}",
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                }
            }
        });


        // Filter

        $('#datatable2').DataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'stateSave': true,
            "order":[[0,'desc']],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                }
            },
            // Datatable Buttons setup
            dom: 'Bfrtip',
            buttons: [
                { extend: 'copy', className: 'btn-info' },
                { extend: 'csv', className: 'btn-info' },
                { extend: 'excel', className: 'btn-info', title: 'XLS-File' },
                { extend: 'pdf', className: 'btn-info', title: $('title').text() },
                { extend: 'print', className: 'btn-info' }
            ]
        });

        $('#datatable3').DataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'stateSave': true,
            "order":[[0,'desc']],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                }
            },
            // Datatable key setup
            keys: true
        });

    }

})();
@if (Session::has('message'))
   (function() {
      'use strict';

      $(initCustom);
       function initCustom() {

         // custom code
         $.notify("{{ session('message') }}", {"status":"{{ session('class') }}"});
      }

   })();
@endif 
  </script>
 
  <script src="{{ asset('vendor/modernizr/modernizr.custom.js')}}"></script><!-- STORAGE API-->
   <script src="{{ asset('vendor/js-storage/js.storage.js')}}"></script><!-- i18next-->
   <script src="{{ asset('vendor/i18next/i18next.js')}}"></script>
   <script src="{{ asset('vendor/i18next-xhr-backend/i18nextXHRBackend.js')}}"></script><!-- JQUERY-->
   <script src="{{ asset('vendor/popper.js/dist/umd/popper.js')}}"></script>
   <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.js')}}"></script><!-- PARSLEY-->

    <!-- Datatables-->
   <script src="{{ asset('vendor/datatables.net/js/jquery.dataTables.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-bs4/js/dataTables.bootstrap4.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/dataTables.buttons.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.colVis.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.flash.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.html5.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.print.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-keytable/js/dataTables.keyTable.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-responsive/js/dataTables.responsive.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
   <script src="{{ asset('vendor/parsleyjs/dist/parsley.js')}}"></script>
   <script src="{{ asset('vendor/jszip/dist/jszip.js')}}"></script>
   <script src="{{ asset('vendor/pdfmake/build/pdfmake.js')}}"></script>
   <script src="{{ asset('vendor/pdfmake/build/vfs_fonts.js')}}"></script>
   <script src="{{ asset('vendor/js-storage/js.storage.js')}}"></script><!-- SCREENFULL-->
   <script src="{{ asset('vendor/nestable/jquery.nestable.js')}}"></script><!-- =============== APP SCRIPTS ===============-->
   <script src="{{ asset('vendor/parsleyjs/dist/parsley.js')}}"></script><!-- =============== APP SCRIPTS ===============-->
   <script src="{{ asset('js/app.js')}}"></script>
   <script src="{{ asset('js/moment.min.js')}}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.js')}}"></script>
   <script src="{{ asset('dist/js/dropify.min.js')}}"></script>
   <script src="{{ asset('dist/js/bootstrap-select.min.js')}}"></script>
   <script src="{{asset('js/raphael-min.js')}}"></script>
   <script src="{{asset('js/morris.min.js')}}"></script>
       

   
         <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}" id="bscss">
         <!--  APP STYLES -->
         <link rel="stylesheet" href="{{ asset('css/app.css')}}" id="maincss"> 
   
    <!-- END LTR -->
  <!--    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
 -->  
 <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 
   <!-- nestable-->
   <script src="{{ asset('vendor/html5sortable/dist/html5sortable.js')}}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script> 

   <script src="{{asset('icon/js/iconPicker.min.js')}}"></script>
  
@if(!empty($link))
    <script>
        //var parentCollapse = $('.{{$page_title}}').parents('#{{$page_title}}Collapse').find('.{{$page_title}}Collapse');
        //parentCollapse.trigger('click');
        $('.{{$link}}').addClass('active');
         
         
    </script>
@endif
<script>
            $(document).ready(function(){
                // Basic
                $('.dropify').dropify();

               
            });
        </script>
@if(Session::has('addManagerType')) 
<script>
         //alert('hlo');
            $(".bb1").trigger("click"); 
</script>
@endif  
<script>

function goBack() {
  window.history.back();
}


</script>


<script type="text/javascript">
   $(function () {
               $(".icon-picker").iconPicker();
           });
</script>

<script>
      $('#summernote').summernote({
        placeholder: 'Describe here',
        tabsize: 2,
        height: 100
      });
    </script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>

<!--- RTL Mode  Code---> 

<script type="text/javascript">
   function selectFileWithCKFinder( elementId,imageId ) {

      CKFinder.modal( {
         chooseFiles: true,
         width: 800,
         height: 600,
         onInit: function( finder ) {
            finder.on( 'files:choose', function( evt ) {
               var file = evt.data.files.first();
               var output = document.getElementById( elementId );
               output.value = file.getUrl();
               //var image = document.getElementById( imageId );
               
               $('#'+imageId).attr("src", output.value );


            } );

            finder.on( 'file:choose:resizedImage', function( evt ) {
               var output = document.getElementById( elementId );
               output.value = evt.data.resizedUrl;
            } );
         }
      } );
   }
   //loader
  $(function()
  {     
      setTimeout(function()
        {
          $('.customLoader').fadeOut();
        }, 1000);   

  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $(".menu_li").click(function(){
      //alert($(this).attr("id"));
    });
  });

</script>

<!-- Operator Dashboard Script -->
<script type="text/javascript">
    $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
       $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $('#startNewSession').click(function(){
        $('#phoneNumError').text('');
        $('#nameError').text('');
    });

    $('#start').click(function() {
        var phoneNumer = $('#phone_number').val();
        var name       = $('#name').val();

        if(phoneNumer == ''){
            $('#phoneNumError').text('Please enter Phone number');
        }

        if(name == ''){
            $('#nameError').text('Please enter Name');
        }

        if(phoneNumer != '' && name != ''){
            $('#newSessionForm').submit();
        }
    });
     $('#startShareSession').click(function() {
        //alert("here");
        var sessionid = $('#sessionId').val();
        $('#share_session_id').val(sessionid);
        var phoneNumer = $('#phone').val();
        var name       = $('#nameShare').val();
        var email       = $('#email').val();
        var emailSubject       = $('#emailSubject').val();
        var emailMessage       = $('#emailMessage').val();

        if(phoneNumer == ''){
            $('#phoneError').text('Please enter Phone number');
        }

        if(name == ''){
            $('#nameShareError').text('Please enter Name');
        }
        if(email == ''){
            $('#emailError').text('Please enter Email');
        }if(emailSubject == ''){
            $('#emailSubjectError').text('Please enter Email subject');
        }if(name == ''){
            $('#emailMessageError').text('Please enter email message');
        }
        if(emailMessage != '' && name != ''){
            $('#newShareSessionForm').submit();
        }
    });
    
</script>

<!-- Operator Dashboard Script -->
@yield('script')

</body>
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}"/>
    <script type="text/javascript" src="{{ asset('js/leaflet.js?')}}"></script>

    {{-- <script type="text/javascript" src="{{'https://maps.googleapis.com/maps/api/js?key='.(new \App\Libraries\GeneralSetting)->getApiInfo()['google_maps_api_key']}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5.1/jquery.googlemap.js"></script> --}}
    <script type="text/javascript" src="{{ asset('js/ckeditor.js?'.time())}}"></script>
    <script type="text/Javascript" src="{{asset('js/videocall/edit_session.js?'.time())}}"></script>
</html>






