<!DOCTYPE html>
<html>
<head>
  <title></title>
    <script type="text/javascript" src="{{ asset('vendor/jquery/dist/jquery.js')}}"></script>
    <script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVoI-SJ-B0RsVcF6uEAYTeyq-CFnVEOTc"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5.1/jquery.googlemap.js"></script>

    <script>
        $('#unmute').hide();
        $('#callLoad').hide();
        var tempRoom = '';

        Twilio.Video.createLocalTracks({
           audio: true,
           video: { width: 400 }
        }).then(function(localTracks) {
           return Twilio.Video.connect('{{ $accessToken }}', {
               name: '{{ $roomName }}',
               tracks: localTracks,
               video: { width: 400 }
           });
        }).then(function(room) {
            tempRoom = room;

           room.participants.forEach(participantConnected);

           var previewContainer = document.getElementById(room.localParticipant.sid);
           if (!previewContainer || !previewContainer.querySelector('video')) {
               participantConnected(room.localParticipant);
           }

           room.on('participantConnected', function(participant) {
               participantConnected(participant);
           });

           room.on('participantDisconnected', function(participant) {
               participantDisconnected(participant);
           });
        });
        
        function participantConnected(participant) {
           const div = document.createElement('div');
           div.id = participant.sid;
           div.setAttribute("style", "float: left; margin: 10px;");
           // div.innerHTML = "<div style='clear:both'>"+participant.identity+"</div>";


           participant.tracks.forEach(function(track) {
               trackAdded(div, track)
           });

           participant.on('trackAdded', function(track) {
               trackAdded(div, track)
           });
           participant.on('trackRemoved', trackRemoved);

           document.getElementById('media-div').appendChild(div);
           $('#callLoad').show();
        }

        function participantDisconnected(participant) {
           participant.tracks.forEach(trackRemoved);
           document.getElementById(participant.sid).remove();
        }

        function trackAdded(div, track) {
           div.appendChild(track.attach());
           var video = div.getElementsByTagName("video")[0];
           if (video) {
               video.setAttribute("style", "max-width:400px;");
           }
        }

        function trackRemoved(track) {
           track.detach().forEach( function(element) { element.remove() });
        }

        $('#mute').click(function(){
            $(this).hide('')
            $('#unmute').show();
            muteVideo();
        });

        $('#unmute').click(function(){
            $(this).hide('')
            $('#mute').show();
            unMuteVideo();
        });

        function muteVideo(){
            tempRoom.localParticipant.videoTracks.forEach(function (track) {
                track.disable();
            });
        }

        function unMuteVideo(){
            tempRoom.localParticipant.videoTracks.forEach(function (track) {
                track.enable();
            });
        }

        $('.endCall').click(function(){
            swal({
                title: "{{__('Are you sure you want to end the call ?')}}",
                icon: 'warning',
                buttons: {
                    cancel: {
                        text: "{{__('No')}}",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true
                    },
                    confirm: {
                        text: "{{__('Yes')}",
                        value: true,
                        visible: true,
                        className: "bg-danger",
                        closeModal: false
                    }
                }
            }).then(function(isConfirm) {
                if (isConfirm) {
                    var roomSID = $('#roomSID').val();

                    $.ajax({
                        type: "GET",
                        url: '/get/call_duration',
                        data: {roomSID:roomSID},
                        success: function(response) {
                            console.log(response.duration);
                            window.location.replace('/session');
                        }
                    });
                }
            });
        });

        var timer2 = "0:01";
        var interval = setInterval(function() {
            var timer = timer2.split(':');
            var minutes = parseInt(timer[0], 10);
            var seconds = parseInt(timer[1], 10);
            
            ++seconds;

            if(seconds > 60){
                seconds = 0;
                minutes = minutes+1;
            }

            $('.countdown').html(String(minutes).padStart(2, '0') + ':' + String(seconds).padStart(2, '0'));
            timer2 = minutes + ':' + seconds;
        }, 1000)
    </script>

    <script type="text/Javascript" src="{{asset('js/videocall/room.js')}}"></script>
</head>
<body>
    <section  id="sectionManager"class="section-container">
        <div class="content-wrapper">
            <div class="content-heading" style="background-color: #f05050">
                <div class="heading text-white">
                    {{__('Video call')}} 
                    <div class="countdown"></div>
                </div>

                <div class="ml-auto">
                    <button class="btn btn-lg endCall" type="button" style="background-color: #5151b1;border-color: #5151b1;color:white">{{__('End Call')}}</button>
                </div>
            </div>

            <div class="row mt-3 ml-1">
              <div class="col-sm-11">
                <div class="card">
                  <div class="card-body">
                    
                    <div class="row col-sm-12">
                        <div id="media-div"></div>
                    </div>

                    <div id="displayUserInfoMap">
                        <div class="row col-sm-12">
                            <div class="col-sm-2">
                                <img 
                                    class="img-thumbnail rounded-circle" 
                                    id="appUserImage" 
                                    src="" 
                                    alt="Avatar" 
                                    width="60" 
                                    height="60"
                                >
                                
                                <div id="appUserName"></div>    
                                
                                <a href="" id="callAppUser">
                                    <span id="appUserPhone"></span>
                                </a>
                            </div>
                        </div>

                        <div class="row col-sm-12">
                            <div id="map" style="width: 100%; height: 400px;"></div>
                        </div>
                    </div>

                    <div class="row col-sm-12" id="callLoad">
                        <input type="hidden" name="roomSID" id="roomSID" value="{{$roomSID}}" />
                        <button class="btn btn-success ml-1" name="mute" id="mute">{{__('Mute')}}</button>
                        <button class="btn btn-danger ml-1" name="unmute" id="unmute">{{__('Un Mute')}}</button>
                        <button class="btn btn-danger ml-1 endCall" name="hangup" id="hangup">{{__('Hang Up')}}</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
</body>
</html>