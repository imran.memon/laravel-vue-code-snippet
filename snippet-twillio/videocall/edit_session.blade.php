@extends('layouts.header')

@section('script')
    {{-- <script type="text/javascript" src="{{'https://maps.googleapis.com/maps/api/js?key='.(new \App\Libraries\GeneralSetting)->getApiInfo()['google_maps_api_key']}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5.1/jquery.googlemap.js"></script> --}}
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}"/>
    <script type="text/javascript" src="{{ asset('js/leaflet.js?')}}"></script>
    <script type="text/javascript" src="{{ asset('js/ckeditor.js?'.time())}}"></script>
    <script type="text/Javascript" src="{{asset('js/videocall/edit_session.js?'.time())}}"></script>
@endsection

@section('content')
<section class="section-container">
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible text-center">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible text-center">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ session('error') }}
            </div>
        @endif
        <div class="content-heading justify-content-between">
        <div>{{__('Session / Edit')}} (#{{$session->id}}) ({{(isset($session->account) && !empty($session->account)) ? $session->account->name : ''}}) </div> 

            <div class="share_back ml-auto">
                <?php if(Auth::user()->type != 'operator' || ((new \App\Libraries\GeneralSetting)->getSettingInfo()['shared_session']) == '1'){ ?>
                <button class="btn btn-success shareData mr-2" type="button" id="share"  data-toggle="modal" data-target="#shareModal">{{__('Share')}}</button>
            <?php } ?>
                <button class="backbtn btn btn-info float-right" id="back">{{__('Back')}}</button>
            </div>
        </div>        
        

         <div class="session_info" role="tabpanel">
            <ul class="nav nav-tabs nav-fill bg-white" role="tablist">
                @if(($session->allow_location == 1 || ($session->allow_recording == 1 && !empty($session->composition_status)) ))
                <li class="nav-item" role="presentation"> 
                    <a class="nav-link bb0 active applicationGeneral genBtn" href="#session" aria-controls="session" role="tab" data-toggle="tab" aria-selected="true">
                        <input type="hidden" name="sessionId" id="sessionId" value="{{$sessionId}}" />
                        {{__('Session')}}
                    </a>
                </li>
                @endif
                <li class="nav-item" role="presentation"> 
                    <a class="nav-link bb0 apiSettings apiBtn" href="#session_info" aria-controls="session_info" role="tab" data-toggle="tab" aria-selected="false">
                        {{__('Session info')}}
                    </a>
                </li>
            </ul>

            <div class="tab-content p-0 bg-white">
            <div class="tab-pane {{((!empty($session->composition_status) && ($session->allow_recording == 1))
                        || $session->allow_location == 1)  ? 'active' : ''}}" id="session" role="tabpanel">
                    <div id="calling_map">
                        @if($session->allow_recording == 1 && !empty($session->composition_status))
                        <div class="calling">
                            <div class="outgoing_call">
                                <h1>{{__('Video call')}} 
                                    <!-- <span>{{'00:00'}}</span> -->
                                </h1>
                            </div>
                            <div id="call_screen" class="h-100">
                                @if(!is_null($videoPath))
                                <div class="operatorVideoDiv1">
                                    <video controls>
                                        <source src="{{ $videoPath }}" type="video/mp4">
                                        <!--<source src="{{ url('/').$videoPath}}" type="video/mp4">-->
                                        {{__('Your browser does not support the video tag')}}.
                                    </video>
                                </div>
                                @elseif($session->allow_recording == 0)
                                    <div class="d-flex h-100 w-100 align-items-center justify-content-center">
                                        <h1>
                                            {{__('No Video Recording Permission')}}
                                        </h1>
                                    </div>
                                @elseif($videoError == 1 && empty($session->composition_status))
                                `   <div class="d-flex h-100 w-100 align-items-center justify-content-center">
                                        <h1>
                                            {{__('Improper files generated!')}}
                                        </h1>
                                    </div>
                                @else
                                    <div class="d-flex h-100 w-100 align-items-center justify-content-center">
                                        <h1>
                                            {{__('Video is not composed yet!')}}
                                        </h1>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @endif
                        @if($session->allow_location == 1)
                            <div class="top-map">
                                <ul class="map_list">
                                <li>
                                    <div class="man_content-1">
                                        <div class="user-name" id="appUserFullName"></div>
                                        <div style="direction:ltr" class="user-number" id="appUserNumber"></div>
                                    </div>
                                </li>
                                <li class="address" id="address"></li>
                                <li >
                                    <div class="man">
                                        <img style="width: 23px;" src="{{asset('image/app_user/phone.svg')}}" alt="">
                                    </div>
                                    <div class="man_content">
                                        <div id="deviceType"></div>
                                        <div class="more"  id="deviceInfo"  data-toggle="modal" data-target="#deviceInfoModel">{{__('More')}}</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="man">
                                        @if(!empty($details))
                                            <img alt="https://via.placeholder.com/100x100" src="{{url('uploads/'.$details->image)}}">
                                        @endif
                                    </div>
                                    <div class="man_content">
                                        <span id="operatorName"></span>
                                    </div>
                                </li>
                            </ul>
                                <div id="map" style="width: 100%; height: 400px;"></div>
                            </div>
                        @endif
                        
                    </div>
                </div>
                
                <div class="tab-pane {{((empty($session->composition_status) ||  ($session->allow_recording == 0))
                        && $session->allow_location == 0)  ? 'active' : ''}}" id="session_info" role="tabpanel">
                        <div id="calling_map" class="h-100 pb-5">
                            <div class="session_info_info">
                                <div class="info_details">
                                    <h3 class="title">{{__('Info')}}</h3>
                                    <ul class="info_list">
                                        <li>{{__('Status')}} : 
                                        @if(!empty($status))  <strong  style="color:{{$status->color_code}};">{{$status->status_name}}</strong>@endif  </li>
                                        <li>{{__('Started')}} : 
                                            <strong>
                                                {{-- {{$session->call_start_date_time}} --}}

                                                {{ !empty($session->call_start_date_time) ? (new \App\Helpers\CommonHelper)->formatDate($session->call_start_date_time) : '00/00/0000 00:00:00' }}
                                            </strong>
                                        </li>
                                        <li>{{__('Ended')}}: 
                                            <strong>
                                                {{-- {{ $session->call_end_date_time }} --}}
                                                {{($session->call_end_date_time != '' && !empty($session->call_start_date_time))?(new \App\Helpers\CommonHelper)->formatDate($session->call_end_date_time):'00/00/0000 00:00:00'}}
                                            </strong>
                                        </li>
                                        
                                        
                                    </ul>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th colspan="2">{{__('Room Sid')}}</th>
                                            <td colspan="2">{{$session->room_sid}}</td>
                                        </tr>
                                         <tr>
                                            <th colspan="2">{{__('Room Name')}}</th>
                                            <td colspan="2">{{$session->room_name}}</td>
                                        </tr>
                                         <tr>
                                            <th></th>
                                            <th>{{__('Duration')}}</th>
                                            <th>{{__('Identity')}}</th>
                                            <th>{{__('PID')}}</th>
                                        </tr>
                                        @foreach($participants as $key=>$participant)
                                        <tr>
                                            <td>{{($participant->type == 'customer') ? __("Participant Time ") . $key : __("Operator Time")}}</td>
                                            <td>
                                            @if($participant->type != 'customer')
                                                {{(new \App\Helpers\CommonHelper)->displayDuration($session->duration)}}
                                            @else 
                                                @if(!empty($session->duration))
                                                    {{(new \App\Helpers\CommonHelper)->displayDuration($participant->duration)}}
                                                @else
                                                    00:00
                                                @endif
                                            @endif
                                            </td>
                                            <td>{{$participant->identity}}</td>
                                            <td>{{$participant->participant_id}}</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <th >{{__('Recording Time')}}</th>
                                            <td colspan="3"><strong>
                                                @if(!empty($session->duration))
                                                    {{(new \App\Helpers\CommonHelper)->displayDuration($session->duration)}}
                                                @else
                                                    00:00
                                                @endif
                                            </strong></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="info_details user">
                                    <h3 class="title">{{__('User')}}</h3>
                                    @php $i =0; @endphp
                                        @if($appUser)
                                         <ul class="user_list">
                                        @foreach($appUser as $appUserVal)
                                       @if($i == 0)
                                        <li>
                                            <div class="man_content">
                                                <strong>
                                                    {{$appUserVal['name']}}
                                                </strong>
                                                <a href="#">
                                                    {{$appUserVal['number']}}
                                                </a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="man">
                                                <img style="width: 23px;" src="{{asset('image/app_user/phone.svg')}}" alt="">
                                            </div>
                                            <div class="man_content">
                                                <strong>
                                                    {{$appUserVal['device_type']}}
                                                </strong>       
                                            </div>
                                        </li>
                                        @endif
                                        @php $i++; @endphp
                                        @endforeach
                                         </ul>
                                        @endif
                                    <p>{{__('Location')}} : <strong>{{$session->address}}</strong> </p>
                                </div>
                                <div class="info_details user">
                                    <h3 class="title">{{__('Other Participants')}}</h3>
                                    @php $i =0; @endphp
                                        @if($appUser)
                                         
                                        @foreach($appUser as $appUserVal)
                                       @if($i != 0)
                                       <ul class="user_list">
                                        <li>
                                            <div class="man_content">
                                                <strong>{{$appUserVal['name']}}</strong>
                                                <a href="#">{{$appUserVal['number']}}</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="man">
                                                <img style="width: 23px;" src="{{asset('image/app_user/phone.svg')}}" alt="">
                                            </div>
                                            <div class="man_content">
                                                <strong>{{$appUserVal['device_type']}}</strong>            
                                            </div>
                                        </li>
                                         </ul>
                                        @endif
                                        @php $i++; @endphp
                                        @endforeach
                                        @endif
                                </div>
                                <div class="info_details user operator">
                                    <h3 class="title">{{__('Operator')}}</h3>
                                 
                                    <ul>
                                        <li>
                                            <div class="man">
                                                @if(!empty($details))
                                                    <img src="{{url('uploads/'.$details->image)}}" alt="https://via.placeholder.com/100x100">
                                                @endif
                                            </div>
                                            <div class="man_content">
                                                <strong>
                                                    @if($details && !empty($details))
                                                        {{$details->first_name.' '.$details->last_name}}
                                                    @endif
                                                </strong>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row p-3">
                                    <div class="info_details user files col-6">
                                        <h3 class="title">{{__('Incoming files')}}</h3>
                                         <ul>
                                            @if(array_key_exists('customer',$filesList))
                                                @foreach($filesList['customer'] as $key => $val)
                                                     <li><a  href="{{url('/videocallAttachments/'.$val['file'])}}"
                                                            target="_blank">{{$val['file']}}</a>{{($val['file_size'])?round($val['file_size']/1024).'kb':''}}</li>
                                                 @endforeach
                                             @endif
                                             @if(array_key_exists('operator',$filesList))
                                                 @foreach($filesList['operator'] as $key => $val)
                                                    @if($val['is_snap']==1)
                                                     <li><a  href="{{url('/videocallAttachments/'.$val['file'])}}"
                                                            target="_blank">{{$val['file']}}</a>{{($val['file_size'])?round($val['file_size']/1024).'kb':''}}</li>
                                                    @endif
                                                 @endforeach
                                             @endif
                                        </ul> 
                                    </div>
                                    <div class="info_details user files col-6">
                                        <h3 class="title">{{__('Outgoing files')}}</h3>
                                         <ul>
                                             @if(array_key_exists('operator',$filesList))
                                                @foreach($filesList['operator'] as $key => $val)
                                                    @if($val['is_snap']!=1)
                                                      <li><a  href="{{url('/videocallAttachments/'.$val['file'])}}"
                                                              target="_blank">{{$val['file']}}</a>{{($val['file_size'])?round($val['file_size']/1024).'kb':''}}</li>
                                                    @endif
                                                 @endforeach
                                             @endif
                                        </ul> 
                                    </div>
                                </div>
                            </div>

                            <div class="operator_section">
                                <h3>{{__('Operator comments')}}</h3>

                                <textarea name="operatorComment" id="operatorComment">
                                    {{$session->operator_comment}}
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
</section>
@endsection

