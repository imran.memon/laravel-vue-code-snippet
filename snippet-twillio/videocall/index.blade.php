@extends('layouts.header')

@section('content')

<section class="section-container">
    <div class="content-wrapper">
        <div class="content-heading justify-content-between">
            <div class="">
                {{__('Session')}}
            </div>
            
            <div class="">
               <!--  <a href="{{url('session_export')}}" class="btn btn-info float-right export-btn">
                    {{__('Export')}}
                </a> -->
                <button class="btn btn-info btn-lg sessionExport" type="button">{{__('Export')}}</button>
            </div>
        </div>        
        <div class="content-body">
            <div class="content-wrapper">
                <div class="card m-3">                                      
                    <div class="card-body p-0">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif

                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif

                        <div class="table-responsive">
                        <table class="table table-striped my-4 w-100" id="ajxTable">
                            <thead>
                                <tr>
                                    <th data-priority="1">{{__('Id')}}</th>
                                    <th>{{__("Account Name")}} </th>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('User')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('User Phone')}}</th>
                                    <th>{{__('Operator')}}</th>
                                    <th>{{__('Total Time (Minute)')}}</th>
                                    <th>{{__('Location')}}</th>
                                    <th style="width:100px"></th>
                                </tr>
                            </thead>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </div>
     </div>
</section>
@endsection

@section('script')
    <script type="text/Javascript" src="{{asset('js/videocall/video.js')}}"></script>
    <script>
    $(document).ready(function(){
        function newexportaction(e, dt, button, config) {
            var self = this;
            var oldStart = dt.settings()[0]._iDisplayStart;
            dt.one('preXhr', function (e, s, data) {
                // Just this once, load all data from the server...
                data.start = 0;
                data.length = 2147483647;
                dt.one('preDraw', function (e, settings) {
                    // Call the original action function
                    if (button[0].className.indexOf('buttons-copy') >= 0) {
                        $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                    } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                        $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                            $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                            $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                    } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                        $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                            $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                            $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                    } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                        $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                            $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                            $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                    } else if (button[0].className.indexOf('buttons-print') >= 0) {
                        $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                    }
                    dt.one('preXhr', function (e, s, data) {
                        // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                        // Set the property to what it was before exporting.
                        settings._iDisplayStart = oldStart;
                        data.start = oldStart;
                    });
                    // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                    setTimeout(dt.ajax.reload, 0);
                    // Prevent rendering of the full data to the DOM
                    return false;
                });
            }); 
            dt.ajax.reload();
        };

        $("#ajxTable").DataTable({
            serverSide: true,
            ajax: "{{ route('sessionListing') }}",
            dom: "<'row'<'col-sm-12 col-md-6'Bl><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            "buttons": [
                           {
                               "extend": 'excel',
                               "text": '<i class="fa fa-files-o" style="color: green;"></i>',
                               "titleAttr": 'excel',                               
                               "action": newexportaction
                           }],
           
            columns: [
                 { name: 'id', data:'id' },
                 { name: 'account.name',data:'account_name' },
                 { name: 'created_at',data:'created_at_text'},
                 { name: 'user.name',data:'user'},
                 { name: 'status',data:'status', },
                 { name: 'user.number',data:'user_phone'},
                 { name: 'operator.first_name',data:'operator'},
                 { name: 'duration',data:'duration'},
                 { name: 'address',data:'address'},
                 {name:'edit_delete', data:'edit_delete',orderable: false },
            ],
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'stateSave': true,
            'order': [[0,'desc']],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
             "infoCallback": function( settings, start, end, max, total, pre ) {
              return "{{__('Showing')}} "+start +" {{__('to')}} "+ end+ " {{__('of')}} " +total+ " {{__('entries')}}";
            },
            processing: true,
            oLanguage: {
               sEmptyTable: "{{__('Empty Data Dable')}}",
               sZeroRecords: "{{__('No records')}}",
                sLengthMenu: '_MENU_ {{__("records per page")}}',
                sSearch: "{{__('Search')}}",
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: "{{__('No records available')}}",
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                },
                sProcessing: "{{__('Loading...')}}",
            }
        });

        $(".sessionExport").click(function(){
          $(".buttons-excel").click();
        });

        $(".buttons-excel").hide();

        /*var searchVal = 'NULL';
        $(".sessionExport").click(function(){
            searchVal = $('input[type="search"]').val();
            var url = 'session_export/'+searchVal;
            window.location.replace(url);
        });*/
    }); 
</script>
@endsection

