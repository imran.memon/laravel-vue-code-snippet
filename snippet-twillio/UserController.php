<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\UsersExport;
use App\Exports\UserOrderExport;
use App\Exports\UserCallExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\User;
use App\Call;
use App\Order;
use Cookie;
use Auth;
use DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('sectionAuth');
        
    }

    public function users(){
        $data['users'] = User::all();
        $data['page_title']='User';
        $data['link']='User';
        $data['menu']='Application_Users';
        $data['subMenu']='Users';
        return view('users.user',$data);
    }

    public function editUser(Request $request, $user){
    	if($request->isMethod('post')){
            $request->validate([
                "name" => "required",
                "phone" => "required|max:15",
                "email" => "required",
                "address" => "required",
                "city" => "required",
            ]);

            DB::beginTransaction();
            try{
                $userData=collect($request->all())->merge(['updated_at'=>Carbon::now(),
                    'updated_by' => Auth::user()->id ])->forget('_token')->toArray();
                $user = User::where('id',$user)->update($userData);
                DB::commit();
                \Session::flash('message', __('Successfully Updated User'));
                \Session::flash('class', 'success');   
                return redirect()->route('userListing');
            }catch (\Exception $e){
                // something went wrong
                DB::rollback();
                \Session::flash('message', __('Unsuccessfully Updated User'));
                \Session::flash('class', 'danger');
                return redirect()->route('userListing');   
            }

        }else{
    		$data['user'] = User::find($user);
            $data['orders'] = Order::Where('user_id',$user)->get();
            $data['calls'] = Call::Where('user_id',$user)->get();
            $data['page_title']='User';
            $data['link']='User';
            $data['menu']='Application_Users';
            $data['subMenu']='Users';
            return view('users.editUser',$data);
    	}
    	
    }

    public function userExport(){
        $headings = ["id","device_type","app_version","last_use","install_date","did_order","did_call","city","name","phone","email"];
        return Excel::download(new UsersExport($headings), Now().'_Users.xlsx');
    }

    public function userOrderExport($userId){
        return Excel::download(new UserOrderExport($userId), 
            Now().'_UserOrders.xlsx');
    }

    public function userCallExport($userId){
        return Excel::download(new UserCallExport($userId), 
            Now().'_UserCalls.xlsx');
    }
}
