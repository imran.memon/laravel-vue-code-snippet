<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VideoCall extends Model
{
	use SoftDeletes;

    protected $table = 'video_call';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'operator_id', 'room_sid', 'app_user_id', 'latitude' ,'longitude', 'address', 'duration', 'app_user_join', 'operator_comment', 'call_start_date_time', 'call_end_date_time', 'file', 'composition_sid','composition_status', 'allow_recording'
    ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /** 
     * join with account
     */
    public function account() 
    {
        return $this->hasOne(\App\Account::class,'id','account_id'); 
    }

    /** 
     * get operator data
     */
    public function operator()
    {
        return $this->hasOne(\App\Managers::class,'id','operator_id');
    }

    /** 
     * get status data
     */
    public function status()
    {
        return  $this->hasOne(\App\StatusSettings::class,'id','status_id');
    }

    /**
     * get app user
     */
    public function user()
    {
        return $this->hasOne(\App\AppUser::class,'id','app_user_id');
    }
}
