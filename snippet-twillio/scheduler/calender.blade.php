@extends('layouts.header') 
@section('content')


<body>
<!-- Main section-->
<section id="sectionManager" class="section-container">
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading">
        <div class="heading">{{__('Schedule Management')}}</div>
        <div class="ml-auto">
          <div class="form-inline scheduler-tool">
            @if(@$authUserType == 4)
              <select style="min-width: 200px" class=" custom-select login_lang_dropdown_list form-control account_change mr-4" required name="account_id">
                <option value="">{{__("Select Account")}}</option>
                @foreach($accounts as $account)
                  <option   value="{{$account->id}}">{{__($account->name)}}</option>
                @endforeach
              </select>
            @endif
            @if(@$authUserType == 4 || $authUserType == 3)
                <select style="min-width: 200px" class=" custom-select login_lang_dropdown_list form-control operator_list mr-4" required name="operator_id">
                  <option value="">{{__("Select Operator")}}</option>
                  @foreach($operators as $operator)
                    <option  value="{{$operator->id}}">{{$operator->first_name}} {{$operator->last_name}}</option>
                  @endforeach 
                </select>
            @endif
            @if(auth()->user()->type_id != 2) 
                <a href="{{route('schedule.create')}}" class="btn btn-success">{{__("New Schedule")}}</a>
            @endif
          </div>
        </div><!-- END  Delete Btn-->
      </div>

     <div class="card m-3 mb-5">
       <div class="card-body p-0 ">
          <div class="ajax_res_msg alert {{session()->has('success') ? 'alert-success' : 'hide' }}  alert-dismissible " role="alert">
            <strong class="msg_text">{{ session()->get('success') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div id='calendar' class="schedule-calendar"></div>
       </div>
     </div>
    </div>
    <input type="hidden" class="cancel_meeting_url" value="">
    <form method="post" name="deleteSchedulerForm" id="deleteSchedulerForm" action="" >
      @csrf
      @method('DELETE')
    </form>
</section>
@endsection

@section('modal')
  <div class="modal" id="ScheduleModal">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
              <a href="" class="editScheduleLink btn-success btn-block btn mb-2 btn-md ">{{__("Edit Schedule")}}</a>
              <a href="" class="createNewMeeting btn-info btn-block btn mb-2 btn-md ">{{__("Create Meeting")}}</a>
              <a href="" class="deleteScheduleLink btn-danger btn-block mb-0 btn btn-md ">{{__("Delete Schedule")}}</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="mettingModal">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
              <a href="" class="viewMeeting btn-info btn-block btn mb-2 btn-md ">{{__("View Meeting")}}</a>
              <a href="javascript:void(0)" data-toggle="modal" data-target="#deleteMeetingFormModal" class="cancelMeeting btn-danger btn-block mb-0 btn btn-md ">{{__("Cancel Meeting")}}</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="operatorModal">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
      
        <div class="modal-body ">
          <h3 class="mb-4 font-weight-light">Select operator</h3>
            <div class="operator_list_event"></div>
            <div class="text-right mt-2">
              <a href="javascript:void(0)" data-dismiss="modal" class="cancelMeeting btn-danger mb-0 btn btn-md ">{{__("Close")}}</a>  
            </div>
        </div>
       
        
      </div>
    </div>
  </div>
@endsection
@section('script')
<link href="{{asset('full-calender/core/main.min.css')}}" rel='stylesheet' />
<link href="{{asset('full-calender/daygrid/main.min.css')}}" rel='stylesheet' />
<link href="{{asset('full-calender/timegrid/main.min.css')}}" rel='stylesheet' />
<link href="{{asset('full-calender/list/main.min.css')}}" rel='stylesheet' />

<script src="{{asset('full-calender/core/main.min.js')}}"></script>
<script src="{{asset('full-calender/interaction/main.min.js')}}" ></script>
<script src="{{asset('full-calender/daygrid/main.min.js')}}" ></script>
<script src="{{asset('full-calender/timegrid/main.min.js')}}" ></script>
<script src="{{asset('full-calender/list/main.min.js')}}" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales-all.min.js"></script>
    <script>
      var obj ,calendar;
       document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

    calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,listMonth,DayGrid'
      },
      locale: "{{app()->getLocale()}}",
      direction:"{{session('lang_direction','ltr')}}",
      defaultDate: "{{$currentDate}}",
      navLinks: true, // can click day/week names to navigate views
      businessHours: true, // display business hours
      editable: false,
      defaultView:'timeGridWeek',
      slotDuration: "00:15:00",
      eventSources: [ 
        {
          url: "{{route('get.schedules')}}",
          method: 'POST',
          extraParams: {
            _token: "{{ csrf_token() }}"
          }
        }
      ],
      eventRender: function(event) {
          if(typeof event.event.extendedProps.is_schedule != 'undefined' && event.event.extendedProps.is_schedule  ) {
            var title = '<span data-id='+event.event.id+' data-startTime='+event.event.start+'  data-endTime='+event.event.end+' class="schedule-time">'+event.event.title+"</span>";
            console.log(event.el.querySelector('.fc-title'));
                if(typeof event.el.querySelector('.fc-title') != 'undefined' && event.el.querySelector('.fc-title') != null)  {
                    event.el.querySelector('.fc-title').innerHTML = title;
                }
          } 
          if(typeof event.event.extendedProps.is_meeting != 'undefined' && event.event.extendedProps.is_meeting == true) {
            var title = '<span class="meeting-time">'+event.event.title+"</span>";
            if(typeof event.el.querySelector('.fc-title') != 'undefined' && event.el.querySelector('.fc-title') != null) {
                event.el.querySelector('.fc-title').innerHTML = title;
            }
          }
      }, 
      eventClick: function(calEvent, jsEvent, view) {
        
        if(typeof calEvent.event.extendedProps.more != 'undefined') {
            console.log(JSON.parse(calEvent.event.extendedProps.more));
            openOperator(JSON.parse(calEvent.event.extendedProps.more));
            return false;
        }
        if(typeof calEvent.event.extendedProps.is_schedule != 'undefined' && calEvent.event.extendedProps.is_schedule  ) {
          if(calEvent.event.extendedProps.obj_type != 2 ){
              $("#ScheduleModal").modal("show");
              var editRoute = '{{ route("schedule.edit", ":id") }}';
              editRoute = editRoute.replace(':id', calEvent.event.id);
              $(".editScheduleLink").prop("href", editRoute);
              $(".createNewMeeting").prop("href",calEvent.event.extendedProps.meeting_url);
              var deleteRoute = '{{ route("schedule.destroy", ":id") }}';
              deleteRoute = deleteRoute.replace(':id', calEvent.event.id);
              $(".deleteScheduleLink").prop("href", deleteRoute);
          }
        } 
        if(typeof calEvent.event.extendedProps.is_meeting != 'undefined' && calEvent.event.extendedProps.is_meeting == true) {
            if(calEvent.event.extendedProps.obj_type == 2) {
                $("#mettingModal").modal("show");
                $(".cancel_meeting_url").val(calEvent.event.extendedProps.cancel_url);
                $(".viewMeeting").prop('href',calEvent.event.extendedProps.view_url);
            } 
        }
      }
    });
    
    calendar.on('dateClick', function(info) {
      console.log(info);
    });
    
    calendar.render();
     obj = calendar.getEventSources();
});
    $(document).ready(function(){
      $(".account_change").on("change",function(){
        var accountId = $(this).val();
        var data = {
            accountId
        };
        var source  = calendar.getEventSources();
        var len = source.length;
        for (var i = 0; i < len; i++) { 
          source[i].remove(); 
        }
        console.log(calendar);
        calendar.addEventSource( {
          url: "{{route('get.schedules')}}",
          method: 'POST',
          extraParams: {
            _token: "{{ csrf_token() }}",
            accountId: accountId,
            operatorId: 0,
          }
        });
        $.post("{{route('scheduler.get.operators')}}",data,(response,status)=> {
            $(".operator_list").html(response.html)
        }).fail(function(response) {
            $(".operator_list").html('');
        });
      });

      $(".operator_list").on("change",function(){
        var source  = calendar.getEventSources();
        var len = source.length;
        for (var i = 0; i < len; i++) { 
          source[i].remove(); 
        } 

        calendar.addEventSource( {
          url: "{{route('get.schedules')}}",
          method: 'POST',
          extraParams: {
            _token: "{{ csrf_token() }}",
            accountId: $(".account_change").val(),
            operatorId: $(this).val()
          }
        });
      });

      $(document).on("click",'.clickOperator',function(){
          $("#operatorModal").modal("hide");
          var id = $(this).attr('data-id');
          var meeting_url  = $(this).attr('data-meeting');
          $("#ScheduleModal").modal("show");
          var editRoute = '{{ route("schedule.edit", ":id") }}';
              editRoute = editRoute.replace(':id', id);
              $(".editScheduleLink").prop("href", editRoute);
              $(".createNewMeeting").prop("href",meeting_url);
              var deleteRoute = '{{ route("schedule.destroy", ":id") }}';
              deleteRoute = deleteRoute.replace(':id', id);
              $(".deleteScheduleLink").prop("href", deleteRoute);
          })
      $(document).on("click",".deleteScheduleLink",function(e){
          e.preventDefault();
          $("#deleteSchedulerForm").prop("action",$(this).prop("href"));
          $("#deleteSchedulerForm").submit();
      }) 
      
    });
    function LetMeCancelMeeting()  {
        $('.customLoader').show();
        $("#deleteMeetingFormModal").modal("hide");
        var url = $(".cancel_meeting_url").val();
        $.get(url,(response,status)=> {
          $('.customLoader').hide();
              $(".msg_text").html(response.msg);
               $(".ajax_res_msg").removeClass("alert-danger").addClass("alert-success").removeClass("hide")

               // $(".operator_list").html(response.html)
               console.log(response);
               setTimeout(function(){
                    window.location.href = response.redirect;
               },2000);
               //$("")
        }).fail(function(response) {
              $('.customLoader').hide();
              var msg = response.responseJSON.msg;
              $(".msg_text").html(msg);
              $(".ajax_res_msg").removeClass("alert-success").addClass("alert-danger").removeClass("hide");
                //$(".operator_list").html('');
        });
    }

    function openOperator(events) 
    {
       var html = ""; 
       $.each(events,function(index,event) {
         console.log(event);
         html += '<div><a href="javascript:void(0)" class="clickOperator mb-2 btn-md " data-id="'+event.id+'" data-meeting="'+event.meeting_url+'">'+event.name+'</a></div>';
       })
       $(".operator_list_event").html(html);
       $("#operatorModal").modal("show");

    }
    </script>
@endsection