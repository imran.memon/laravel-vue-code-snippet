@extends('layouts.header')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment-with-locales.min.js"></script>
@section('content')
<section  id="sectionManager" class="section-container">
    <div class="content-wrapper">
        <form>
        <div class="content-heading">
            <div class="heading">{{__('Schedule Management')}}/{{__('Meeting info')}}</div><!-- START Language list-->
            <div class="ml-auto">
                @if($meeting->status == 1) 
                <button class="btn btn-danger btn-lg delete_meeting " type="button" data-toggle="modal" data-target="#deleteMeetingFormModal"  >{{__('Cancel Meeting')}}</button>
                @if(auth()->user()->role == 'operator' && $show_session_btn) 
                    <a href="{{route('start-meeting',$meeting->id)}}" class="btn btn-info start-meeting-text btn-lg" type="button">{{__('Start Session')}}</a>
                @endif
            @endif
                <a href="{{route('schedulerListing')}}" class="btn btn-danger btn-lg back-btn" >{{__('Back')}}</a>

            </div><!-- END Language list-->
        </div><!-- START cards box-->
        <div class="card" role="tabpanel">
            <div class="tab-content p-0 bg-white">
                <div class="tab-pane active" id="schedule_mail" role="tabpanel">
                    <div class="row p-4">
                        <div class="col-sm-5">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="meeting_title"><strong>{{__("Meeting info")}}</strong></div>
                                        </div>
                                        <div class="col-sm-12 meeting-info">
                                            <div class="col-sm-2 remove_spacing">
                                                <img src="{{$operator_profile}}" height="50" width="50"/>
                                            </div>
                                            <div class="col-sm-4 remove_spacing middle-text">
                                              
                                                <div class="small-font">{{$operator_name}}</div>
                                            </div>
                                            <div class="col-sm-6">
                                            <div class="small-font">{{__("Meeting time")}}</div>
                                                <div class="small-font">
                                                    <div class="meeting-date">{{$meeting_date}}, {{$meeting_time}}
                                                    </div>
                                                </div>
                                            </div>  
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 ">
                            <div class="card fixed_height-80">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p><strong>{{__("User Info")}}</strong></p>
                                        </div>
                                        <div class="col-sm-12 user-info">
                                            <div class="col-sm-6 remove_spacing">
                                                <div class="form-group ">
                                                    <label class="col-form-label small-font">{{__('Full Name')}} </label>
                                                    {{$meeting->full_name}}
                                                </div>
                                                
                                                <div class="form-group remove_spacing">
                                                    <label class="col-form-label small-font">{{__('Phone')}}</label>
                                                    <span class="num">{{$meeting->country_code}}-{{$meeting->phone}}</span>
                                                </div>
                                                <div class=" remove_spacing">
                                                    <div class="form-group">
                                                        <label class="col-form-label small-font">{{__('Email')}} </label>
                                                        {{$meeting->email}}
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-form-label small-font">{{__('Note')}}</label>{!!nl2br($meeting->note)!!}</div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
    function LetMeCancelMeeting()  {
        $("#deleteMeetingFormModal").modal("hide");
        $.get("{{route('meeting.cancel',$meeting->id)}}",(response,status)=> {
               // $(".operator_list").html(response.html)
               console.log(response);
               setTimeout(function(){
                    window.location.href = response.redirect;
               },2000);
               //$("")
        }).fail(function(response) {
                //$(".operator_list").html('');
        });
    }
</script>
@endsection