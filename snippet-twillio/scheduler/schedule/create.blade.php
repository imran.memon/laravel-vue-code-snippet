@extends('layouts.header')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment-with-locales.min.js"></script>
@section('content')
<section  id="sectionManager" class="section-container">
    <div class="content-wrapper">
        <form name="createScheduler" id="createScheduler" method="post" autocomplete="off" >
        <div class="content-heading">
            <div class="heading">{{__('Schedule Management')}}/{{__('New Schedule')}}</div><!-- START Language list-->
            <div class="ml-auto">
                   <button class="btn btn-success btn-lg  " type="submit">{{__('Save')}}</button>
                   <a href="{{route('schedulerListing')}}" class="btn btn-danger btn-lg back-btn" >{{__('Back')}}</a>

            </div><!-- END Language list-->
        </div><!-- START cards box-->
        <div class="card" role="tabpanel">
            <div class="tab-content p-0 bg-white">
                <div class="tab-pane active" id="schedule_mail" role="tabpanel">
                    <div class="row p-4">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="ajax_res_msg alert alert-success alert-dismissible hide" role="alert">
                                        <strong class="msg_text"></strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                      
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                @if($login_as == 'top_manager')
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-form-label">{{__('Account')}} <sup class="text-danger">*</sup></label>
                                                        <select class="login_lang_dropdown_list     form-control  borderLeft account_change" required name="account_id">
                                                            <option value="">{{__("Select Account")}}</option>
                                                            @foreach($accounts as $account)
                                                               <option   value="{{$account->id}}">{{__($account->name)}}</option>
                                                            @endforeach 
                                                         </select>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-form-label">{{__('Operator')}} <sup class="text-danger">*</sup></label>
                                                        <select class="login_lang_dropdown_list     form-control  borderLeft operator_list" required name="operator_id">
                                                            
                                                            @foreach($operators as $operator)
                                                               <option  value="{{$operator->id}}">{{$operator->first_name}} {{$operator->last_name}}</option>
                                                            @endforeach 
                                                         </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="col-form-label">{{__('From Date')}} <sup class="text-danger">*</sup></label>
                                                        <div class='input-group date' id='from_datepicker'>
                                                            <input id="from_date" name="from_date"  type='text' class="form-control" 
                                                            required
                                                            />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="col-form-label">{{__('Start Time')}} <sup class="text-danger">*</sup></label>
                                                        <div class='input-group date' id='start_time'>
                                                            <input name="start_time"  type='text' class="form-control" required />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-time"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="col-form-label">{{__('Until Time')}} <sup class="text-danger">*</sup></label>
                                                        <div class='input-group date' id='end_time'>
                                                            <input name="end_time"  type='text' class="form-control" required  />
                                                            <span class="input-group-addon ">
                                                                <span class="glyphicon glyphicon-time"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="col-form-label">{{__('Repeat')}}</label>
                                                        <div class="input-group mb-3 new_view_btn">
                                                            <select class="login_lang_dropdown_list     form-control  borderLeft" required name="is_repeat" id="is_repeat">
                                                                <option value="0">{{__("No Repeat")}}</option>
                                                                <option value="1">{{__("Custom")}} </option>
                                                             </select>
                                                             <div class="input-group-append custom_view_btn hide">           
                                                                <!-- <label class="col-form-label"></label> -->
                                                                    <button type="button" class="btn-sm btn-primary " data-toggle="modal" data-target="#schedule_repeate_modal"><i class="fa fa-eye"></i>
                                                                    </button>
                                                              </div>
                                                         </div>
                                                    </div>
                                                    <input type="hidden" value="" name="custom_repeat" id="custom_repeat">
                                                </div>                                            
    
    
                                                <!-- <div class="col-sm-1 custom_view_btn hide">
                                                    <div class="form-group">
                                                        <label class="col-form-label"></label>
                                                        <button type="button" class="btn-sm btn-primary " data-toggle="modal" data-target="#schedule_repeate_modal"><i class="fa fa-eye"></i>
                                                        </button>
                                                    </div>
                                                </div> -->
    
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="col-form-label">{{__('Meeting slot time')}}</label>
                                                        <select class="login_lang_dropdown_list     form-control  borderLeft" required name="time_slot">
                                                            @for($initTime = 5; $initTime <=120; $initTime = $initTime+5 ) 
                                                                <option value="{{$initTime}}">{{$initTime}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</section>
@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script>
<script type="text/javascript">
   
    $(document).ready(function(){
        now = moment()
        $(".end_on").first().prop("checked",true);
        $('#from_datepicker').datetimepicker({
            format: 'L',
        });
        $('#from_datepicker').on('dp.change', function(e){ $("#is_repeat").val(0).trigger('change'); $("#repeat_every").val(1).trigger('change') });
        $("#from_date").inputmask("99/99/9999",{"placeholder": "mm/dd/yyyy",
            onincomplete: function() {
                $(this).val('');
        }
        });
          //static mask

        var startTime = $('#start_time').datetimepicker({
            format: 'HH:mm',
            stepping: 10,
        });
        $('#end_time').datetimepicker({
            format: 'HH:mm',
            stepping: 10
        });
        $("input[name='start_time']").inputmask("99:99",{"placeholder": "HH:MM",
            onincomplete: function() {
                $(this).val('');
        }
        })
        $("input[name='end_time']").inputmask("99:99",{"placeholder": "HH:MM",
            onincomplete: function() {
                $(this).val('');
        }
        })
        $(".revert_repeat").on("click",function(){
           // $(".custom_recurrence").trigger("reset");
            //$("select[name='is_repeat']").val(0).trigger("change")
            $("#schedule_repeate_modal").modal("hide");
        });
        //$(".input-group-addon").addClass("input-group-text").removeClass("input-group-addon");
        $("#is_repeat").on("change",function(){
           // $(".custom_view_btn").toggleClass("hide");
            if($(this).val() == 1) {
                $(".custom_view_btn").removeClass("hide");
                $("#schedule_repeate_modal").modal("show");
            } else {
                $(".custom_view_btn").addClass("hide");
            }
        });
        $("#repeat_every").on("change",function(){
            if($(this).val() == 2) {
                $(".month-select").removeClass('hide');
                $(".week-select").addClass('hide');
                try{ 
                    var date = $("#from_datepicker").find("input").val();
                } catch(e) {
                    var date = $("#from_date").val();
                }
                var day = moment(date,"MM/DD/YYYY").format("D");
                var week  = parseInt(day/7);
                var modulo = day%7;
                week = week+((modulo == 0) ? 0 : 1);
                console.log(moment(date,"MM/DD/YYYY").add(0, 'month').startOf('month').isoWeek());
               // var week = moment(date,"MM/DD/YYYY").week() - moment(date,"MM/DD/YYYY").add(0, 'month').startOf('month').week();
                var dayThree = moment(date,"MM/DD/YYYY").format("ddd");
                var fullDayName = moment(date,"MM/DD/YYYY").format("dddd");
                var displayField = ""; 
                var databaseValue = "";
                console.log(date);
                console.log(moment(date,"MM/DD/YYYY").weeks());
                console.log(moment(date,"MM/DD/YYYY").add(0, 'month').startOf('month').weeks());
               console.log(week);
                if(week == 1)  {
                    displayField = "First "+fullDayName; 
                    databaseValue = "1_"+dayThree; 
                }
                if(week == 2)  {
                    displayField = "Second "+fullDayName; 
                    databaseValue = "2_"+dayThree; 
                }

                if(week == 3)  {
                    displayField = "Third "+fullDayName; 
                    databaseValue = "3_"+dayThree; 
                }
                if(week == 4)  {
                    displayField = "Fourth "+fullDayName; 
                    databaseValue = "4_"+dayThree; 
                } 

                if(week == 5)  {
                    displayField = "Fifth "+fullDayName; 
                    databaseValue = "5_"+dayThree; 
                }
                var day = moment(date,"MM/DD/YYYY").format("DD");
                var dropDownHtml = "<option value='"+day+"'>Monthly on day "+day+ "</option>"; 
                dropDownHtml += "<option value='"+databaseValue+"'>Monthly on the "+displayField+ "</option>"; 
                console.log(dropDownHtml);
                $("#month_days").html(dropDownHtml);
            } else {
                $(".week-select").removeClass('hide');
                $(".month-select").addClass('hide');
            }
        });

        $(".end_on").on("click",function(){
            console.log($(this).val());
            if($(this).val() == 0) {
                var disable1 = $(this).attr("data-disable1");
                var disable = $(this).attr("data-disable");
                $("#"+disable).prop("disabled",true);
                $("#"+disable1).prop("disabled",true); 
              //  return false;
            } else {
                var enable = $(this).attr("data-enable");
                var disable = $(this).attr("data-disable");
                $("#"+enable).prop('disabled',false);
                $("#"+disable).prop('disabled',true);
            }
        });

        $('#schedule_repeate_modal').on('hide.bs.modal', function (event) {
            
        });

        $(".account_change").on("change",function(){
            var accountId = $(this).val();
            var data = {
                accountId
            };
            $.post("{{route('scheduler.get.operators')}}",data,(response,status)=> {
                $(".operator_list").html(response.html)
            }).fail(function(response) {
                $(".operator_list").html('');
            });
        });
        $("#createScheduler").validate({
              errorClass:'text-danger custom-error'
        });
        jQuery.extend(jQuery.validator.messages, {
            required: "",
        });
        $("#createScheduler").on("submit",function(e){
            e.preventDefault();
            if(!$(this).valid()) {
                return false;
            }
            var data = $(this).serialize();
            $('.customLoader').show();
            $.post("{{route('schedule.store')}}",data,(response,status)=> {
               $(".msg_text").html(response.msg);
               $(".ajax_res_msg").removeClass("alert-danger").addClass("alert-success").removeClass("hide");
               setTimeout(function(){
                  window.location.href = response.redirect;
               },3000);
               $('.customLoader').hide();
            }).fail(function(response) {
                console.log(response);
                $('.customLoader').hide();
                var msg = response.responseJSON.msg;
                $(".msg_text").html(msg);
                $(".ajax_res_msg").removeClass("alert-success").addClass("alert-danger").removeClass("hide");
                //$(".operator_list").html('');
            });
        });

        $('.custom_recurrence').validate({
            rules: {
                repeat_count: {
                    required:true
                }, 
                end_date: {
                    required: ".end_date:checked"
                }, 
                occurrences: {
                    required:".occurrences:checked"
                },
                is_repeat: {
                    required: true
                },
                repeat_on_months: {
                    required: function(){
                        return ($("#repeat_every").val() == 2) ? true: false;
                    }
                }
            }
        });
        jQuery.validator.addClassRules('.weekday', {
            required: function(){
                return ($("#repeat_every").val() == 1) ? true: false;
            }
        });
        $(".save_repeat").on("click",function(){
            if( $('.custom_recurrence').valid())  {
                $("#schedule_repeate_modal").modal("hide");
                var customRepeat = getFormData();
                $("#custom_repeat").val(JSON.stringify(customRepeat));
            }

            console.log( $('.custom_recurrence').valid());
        });
    }); 
    $(function(){
        var dtToday = new Date();
        
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        
        var maxDate = year + '-' + month + '-' + day;
        $('.custom_date_modal').attr('min', maxDate);
    });
    function getFormData($form){
        var unindexed_array = $(".custom_recurrence").serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }
</script>
@endsection