@extends('layouts.header')
@section('content')

<!---Side Bar End-->
      <!-- Main section-->
      <section  id="sectionManager" class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
           <!-- Start ManagerInfo Form --->
           <div class="content-heading">
          <div class="heading">{{__('Operators')}} / #{{$operators->id}} </div>

        </div>
               <div class="" role="tabpanel">
                     <!-- Nav tabs-->
                     @if (Auth::user()->role != 'operator')
                     <ul class="nav nav-tabs nav-fill pt-3 pr-3 pl-3 bg-white" role="tablist">  
                        <li class="nav-item" role="presentation">
                           <a class="nav-link bb0 active" href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true">
                              {{__('Operator Info')}}
                           </a>
                        </li>
                        <li class="nav-item" role="presentation">
                           <a class="nav-link bb1" href="#sessions" aria-controls="profile" role="tab" data-toggle="tab" aria-selected="false">
                             {{__('Sessions')}}
                           </a>
                        </li>                      
                     </ul><!-- Tab panes-->
                     @endif
                     <div class="tab-content p-0 m-3">
                        <div class="tab-pane active" id="home" role="tabpanel">
                          <form action="{{route('editOperators',$operators->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="tab-content p-0">
                              <div class="row"> 
                                <div class="col-sm-6 "></div>
                                <div class="col-sm-6 text-right ">
                                   <button class="btn btn-success btn-lg  " type="submit">{{__('Update')}}</button>
                                   <button class="btn btn-danger btn-lg back-btn" type="button" onclick="goBack()">{{__('Back')}}</button>
                                </div>
                            </div><!-- START cards box-->
                              <div class="row">
                              
                                 <div class="col-sm-6">
                                      <!-- START card-->
                                         <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                           <label class="col-form-label">{{__('First Name')}}</label>
                                                           <input class="form-control  @error('first_name') is-invalid @enderror borderLeft"  name="first_name" type="text"  value="{{$operators->first_name}}" required>
                                                           @if ($errors->has('first_name'))
                                                                  <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                                            @endif
                                                        </div>                                              
                                                    </div>
                                                      <div class="col-sm-6">
                                                          <div class="form-group">
                                                           <label class="col-form-label">{{__('Last Name')}}</label>
                                                           <input class="form-control  @error('last_name') is-invalid @enderror borderLeft"  name="last_name" type="text" value="{{$operators->last_name}}" required>
                                                           @if ($errors->has('last_name'))
                                                                  <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                                            @endif
                                                        </div>            
                                                    </div>
                                                    @if (Auth::user()->role == 'Top Manager')
                                                    <div class="col-sm-6">
                                                      <div class="form-group">
                                                          <label class="col-form-label">{{__('Manager')}}</label>
                                                          
                                                      <select class="form-control @error('manager') is-invalid @enderror borderLeft {{$roleClass}}" name="manager" required>
                                                              <option value="">{{__('Choose')}}....</option>
                                                              @foreach($manager as $manager)
                                                              <option value="{{$manager->id}}"

                                                               {{$operators->parent_manager_id == $manager->id ? "selected"  : ""}} >{{$manager->first_name}}</option>
                                                              
                                                              @endforeach
                                                          </select>

                                                         @if ($errors->has('manager'))
                                                               <span class="text-danger">{{ $errors->first('manager') }}</span>
                                                         @endif
                                                        </div>
                                                      </div>
                                                      @endif
                                                      <div class="col-sm-6">
                                                          <div class="form-group">
                                                           <label class="col-form-label">{{__('Phone Number')}}</label>
                                                           <input class="form-control  @error('phone') is-invalid @enderror borderLeft"  name="phone" type="phone" value="{{$operators->phone}}"  required>
                                                           @if ($errors->has('phone'))
                                                                  <span class="text-danger">{{ $errors->first('phone') }}</span>
                                                            @endif
                                                        </div>                                            
                                                    </div>

                                                      <div class="col-sm-6">
                                                          <div class="form-group">
                                                           <label class="col-form-label">{{__('Email Address')}}</label>
                                                           <input class="form-control  @error('email') is-invalid @enderror borderLeft"  name="email" type="email" value="{{$operators->email}}" required autocomplete="off" >
                                                           @if ($errors->has('email'))
                                                                  <span class="text-danger">{{ $errors->first('email') }}</span>
                                                            @endif
                                                        </div>                                            
                                                    </div>

                                                    <div class="col-sm-6">
                                                <div class="form-group">
                                                 <label class="col-form-label">{{__('Password')}}</label>
                                                 <input class="form-control @error('password') is-invalid @enderror borderLeft"  name="password" type="password" autocomplete="new-password" >
                                                 @if ($errors->has('password'))
                                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                                  @endif
                                              </div>                                            
                                          </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                 <label class="col-form-label">{{__('Confirm Password')}}</label>
                                                 <input class="form-control @error('confirm_password') is-invalid @enderror borderLeft"  name="confirm_password" type="password" autocomplete="off" >
                                                 @if ($errors->has('confirm_password'))
                                                        <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                                  @endif
                                              </div>                                            
                                          </div>
                                          <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Operator Type')}}</label>
                                                    <select class="form-control op-type @error('op_type') is-invalid @enderror borderLeft" name="op_type" required>
                                                        <option value="">{{__('Choose')}}....</option>
                                                        @foreach($types as $type)
                                                            <option value="{{$type->id}}" {{ $operators->op_type == $type->id ?"selected":""  }} >{{$type->title}} </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('op_type'))
                                                        <span class="text-danger">{{ $errors->first('op_type') }}</span>
                                                    @endif
                                            </div>
                                          </div>
                                                    <div class="col-sm-6 text">
                                                          <div class="form-group">
                                                           <label class="col-form-label">{{__('Avatar Image')}}</label>
                                                           <input class="form-control dropify  @error('image') is-invalid @enderror borderLeft"  type="file" data-default-file="{{url('/').'/uploads/'.$operators->image}}" name="image" id="input-file-now" data-allowed-file-extensions="jpg png jpeg"  >
                                                            @if ($errors->has('Image'))
                                                                  <span class="text-danger">{{ $errors->first('image') }}</span>
                                                            @endif
                                                        </div>                                            
                                                    </div>
                                                   
                                                </div>                                           
                                            </div>

                                         </div><!-- END card-->
                                 </div>
                              </div>
                            </div>
                            </form>
                        </div>
                        @if (Auth::user()->role != 'operator')
                        <div class="tab-pane" id="sessions" role="tabpane2">
                        <div class="row">
                        <div class="col-sm-6 pb-3"></div>
                                <div class="col-sm-6 text-right pb-3">
                                    <a href="{{route('operatorsSessionExport',$operators->id)}}"><button class="btn btn-info btn-lg export-btn appUserSessionExport" type="button">{{__('Export')}}</button></a>
                                </div>
                            <div class="col-sm-12">
                                <div class="card">                                      
                                    <div class="card-body p-0">
                                       <table class="table table-striped my-4 w-100" id="ajxTable">
                                          <thead>
                                             <tr>
                                                 <th data-priority="1">{{__('Id')}}</th>
                                                 <th>{{__('Date')}}</th>
                                                 <th>{{__('User')}}</th>
                                                 <th>{{__('Status')}}</th>
                                                 <th>{{__('User Phone')}}</th>
                                                 <th>{{__('Operator')}}</th>
                                                 <th>{{__('Total Time')}}</th>
                                                 <th>{{__('Location')}}</th>
                                                 <th style="width:160px"></th>
                                             </tr>
                                          </thead>
                                          
                                        </table>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                     </div>
                  </div>
              
              <!-- End ManagerInfo Form --->
         </div>
      </section>
@endsection


@section('script')
    <script>
    $(document).ready(function(){
        $("#ajxTable").DataTable({
            serverSide: true,
            ajax: "{{ $page_url }}",
            columns: [
                 { name: 'id', data:'id' },
                 { name: 'created_at',data:'created_at_text'},
                 { name: 'user',data:'user'},
                 { name: 'status',data:'status', },
                 { name: 'user_phone',data:'user_phone'},
                 { name: 'operator',data:'operator'},
                 { name: 'duration',data:'duration'},
                 { name: 'address',data:'address'},
                 {name:'edit_delete', data:'edit_delete',orderable: false },
            ],
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'stateSave': true,
            'order': [[0,'desc']],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
             "infoCallback": function( settings, start, end, max, total, pre ) {
              return "{{__('Showing')}} "+start +" {{__('to')}} "+ end+ " {{__('of')}} " +total+ " {{__('entries')}}";
            },
            processing: true,
            oLanguage: {
               sEmptyTable: "{{__('Empty Data Dable')}}",
               sZeroRecords: "{{__('No records')}}",
                sLengthMenu: '_MENU_ {{__("records per page")}}',
                sSearch: "{{__('Search')}}",
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: "{{__('No records available')}}",
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                },
                sProcessing: "{{__('Loading...')}}",
            }
        });
    }); 
         $(document).ready(function(){
            $(".top_manager_class").on("change",function(){
                managerId  = $(this).val();
                var url  = "{{route('operator-type-by-manager',['id'=>':id'])}}";
                url = url.replace(':id', managerId);
                $(".op-type").html("");
                $.get(url,(response,status)=> {
                    $(".op-type").html(response.html)
                }).fail(function(response) {
                   
                });
            });
        });
</script>
@endsection
