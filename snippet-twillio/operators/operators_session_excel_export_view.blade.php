
@if(count($appUserSession)>0)
    <table class="table table-striped my-4 w-100" id="datatable1">
       <thead>
          <tr>
              <th data-priority="1">Id</th>
              <th>Date</th>
              <th>User</th>
              <th>Status</th>
              <th>User Phone</th>
              <th>Operator</th>
              <th>Total Time</th>
              <th>Location</th>
              <th></th>
          </tr>
       </thead>
       <tbody>
        @foreach($appUserSession['appUserSession'] as $session)
        <tr class="gradeX">
            <td>{{$session['id']}}</td>
            <td  class="text-capitalize">{{(new \App\Helpers\CommonHelper)->formatDate($session['videoDate'])}}</td>
            <td  class="text-capitalize">{{$session['name']}}</td>
            <td  class="text-capitalize">{{$session['status_name']}}</td>
            <td  class="text-capitalize">{{($session['number'])}}</td>
            <td  class="text-capitalize">{{$session['operator_name']}}</td>
            <td  class="text-capitalize">{{$session['duration']}}</td>
            <td  class="text-capitalize" style="width:20%;">{{$session['address']}}</td>
            <td class="text-right">
                <button class="mb-1 btn btn-danger mr-2 deleteOnElement" data-route="#" type="button">
                    <i class="fa fa-trash" aria-hidden="true"></i> 
                </button>  
                <a href="#">
                    <button class="mb-1 btn btn-info" type="button">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                </a>
            </td>
        @endforeach
         </tbody>
    </table>
@else
 <span class="p-4"><h5><center>{{__('No record found')}}!</center></h5></span>
@endif