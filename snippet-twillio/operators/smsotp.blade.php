<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <link rel="icon" type="image/x-icon" href="{{url('img/website_logo.png')}}">
   <title>{{env('APP_NAME')}} - View Session</title>
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/brands.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/regular.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/solid.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/fontawesome.css')}}"><!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css')}}"><!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="{{ asset('css/bootstrap.css" id="bscss')}}"><!-- =============== APP STYLES ===============-->
  
   <!-- Datatables-->
   <link rel="stylesheet" href="{{ asset('vendor/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css')}}">
   <link rel="stylesheet" href="{{ asset('vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css')}}"><!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}} " id="bscss"><!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="{{ asset('css/app.css')}} " id="maincss">

<script src="https://kit.fontawesome.com/d565f77dcb.js" crossorigin="anonymous"></script>
</head>
<?php 
$decode = base64_decode($id);
$explodedata = explode("_",$decode);
//echo "<pre>";print_r($explodedata);exit;
?>
<body>
   <div class="wrapper">

<!---Block Start-->

      <div class="block-center mt-5 wd-xl">
         <!-- START card-->
         <div class="card card-flat ">
            <div class="card-header text-center bg-dark">
                <img class="img-fluid px-3" src="{{url((new \App\Libraries\GeneralSetting)->getSettingInfo()['app_logo'])}}">
            </div>
            <div class="card-body">
            
            @if (session('status'))
               <div class="alert alert-success" role="alert">
                     {{ session('status') }}
               </div>
            @endif
             @if (session('message'))
               <div class="alert alert-danger" role="alert">
                     {{ session('message') }}
               </div>
            @endif
           
               <p class="text-center py-2">{{__('View Session')}}</p>
               
                <form method="POST" action="{{ route('verifictionOtp') }}" class="mb-3" id="verifictionform"  novalidate >
                    @csrf
                  <div class="form-group">
                     <div class="input-group with-focus">
                         <input type="hidden" value="{{(old('sessionId'))?old('sessionId'):$explodedata['1']}}" name="sessionId" />
                         <input type="hidden" value="{{(old('id'))?old('id'):$explodedata['2']}}" name="id" />
                        <input class="form-control border-right-0 borderLeft form-control @error('otp') is-invalid @enderror" id="otp" type="number" placeholder="Enter SMS code" autocomplete="off" name="otp" value="{{(old('otp'))?old('otp'):''}}" required>
                        <div class="input-group-append">
                            <span class="input-group-text text-muted bg-transparent border-left-0">
                                <em class="fa fa-envelope"></em>
                            </span>
                        </div>

                        @error('otp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{__('Invalid SMS Code')}}</strong>
                            </span>
                        @enderror
                     </div>
                  </div>
                 
                  <div class="clearfix mt-4">
                     
                     
                  </div><button class="btn btn-block btn-themebutton mt-4" type="submit">{{__('Send')}}</button>
               </form>               
            </div>
         </div><!-- END card-->
          <div class="p-3 text-center">{{((new \App\Libraries\GeneralSetting)->getSettingInfo()['reserved_text'])}}</div>
 
{{-- <footer class="footer-container"><span>&copy; 2020 - {{config('constants.project_name')}}</span></footer> --}}

 </div>
 <!-- =============== VENDOR SCRIPTS ===============-->

  <script src="{{ asset('vendor/jquery/dist/jquery.js')}}"></script><!-- BOOTSTRAP-->
  <script src="{{ asset('vendor/modernizr/modernizr.custom.js')}}"></script><!-- STORAGE API-->
   <script src="{{ asset('vendor/js-storage/js.storage.js')}}"></script><!-- i18next-->
   <script src="{{ asset('vendor/i18next/i18next.js')}}"></script>
   <script src="{{ asset('vendor/i18next-xhr-backend/i18nextXHRBackend.js')}}"></script><!-- JQUERY-->
   <script src="{{ asset('vendor/popper.js/dist/umd/popper.js')}}"></script>
   <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.js')}}"></script><!-- PARSLEY-->

    <!-- Datatables-->
   <script src="{{ asset('vendor/datatables.net/js/jquery.dataTables.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-bs4/js/dataTables.bootstrap4.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/dataTables.buttons.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.colVis.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.flash.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.html5.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.print.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-keytable/js/dataTables.keyTable.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-responsive/js/dataTables.responsive.js')}}"></script>
   <script src="{{ asset('vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
   <script src="{{ asset('vendor/jszip/dist/jszip.js')}}"></script>
   <script src="{{ asset('vendor/pdfmake/build/pdfmake.js')}}"></script>
   <script src="{{ asset('vendor/pdfmake/build/vfs_fonts.js')}}"></script>
   <script src="{{ asset('vendor/js-storage/js.storage.js')}}"></script><!-- SCREENFULL-->
   <script src="{{ asset('vendor/nestable/jquery.nestable.js')}}"></script><!-- =============== APP SCRIPTS ===============-->
   <script src="{{ asset('vendor/parsleyjs/dist/parsley.js')}}"></script><!-- =============== APP SCRIPTS ===============-->
   <script src="{{ asset('js/app.js')}}"></script>

   <script>

function goBack() {
  window.history.back();
}
</script>
</body>
</html>