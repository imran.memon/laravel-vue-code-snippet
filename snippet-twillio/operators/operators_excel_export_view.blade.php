    <table class="table table-striped my-4 w-100" id="datatable1">
       <thead>
          <tr>
              <th data-priority="1">Id</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Status</th>
              <th>Last Login</th>
              <th>Total Time</th>
              <th>Total Sessions</th>
              <th>Created</th>
              <th>Updated</th>
          </tr>
       </thead>
       <tbody>
      @foreach($data_new['operator'] as $key => $operator)
      <tr class="gradeX">
          <td>{{$operator->id}}</td>
          <td  class="text-capitalize">{{$operator->first_name.' '.$operator->last_name}}</td>
          <td  class="text-capitalize">{{($operator->email)}}</td>
          <td  class="text-capitalize">{{($operator->phone)}}</td>
          <td  class="text-capitalize">{{$operator->online == 1 ? 'online' : 'offline'}}</td>
          <td  class="text-capitalize">{{ (new \App\Helpers\CommonHelper)->formatDate($operator->last_enter)}}</td>
          <td>{{(new App\Helpers\CommonHelper)->convertSecondToMinute(array_sum($operator->getVideoCall->pluck('duration')->toArray()))}}</td>
          <td  class="text-capitalize">{{$data_new['session'][$key]}}</td>
          <td  class="text-capitalize">{{(new \App\Helpers\CommonHelper)->formatDate($operator->created_at)}}</td>
          <td  class="text-capitalize">{{(new \App\Helpers\CommonHelper)->formatDate($operator->updated_at)}}</td>
      @endforeach
       </tbody>
    </table>
                                       