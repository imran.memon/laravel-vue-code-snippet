@extends('layouts.header')
@section('content')
 <!-- Main section-->
 <section  id="sectionManager"class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
            <div class="content-heading">
               <div class="heading">{{__('Operators')}}</div><!-- START Delete Btn -->
               <div class="ml-auto">
                  <!-- <a href="{{route('operatorsExport')}}"><button class="btn btn-info btn-lg appUserExport" type="button">{{__('Export')}}</button></a> -->
                  <button class="btn btn-info btn-lg operatorsExport" type="button">{{__('Export')}}</button>
                  <a href="{{route('addOperators')}}"><button class="btn btn-info btn-lg" type="button">{{__('New')}}</button></a>
               </div><!-- END  Delete Btn-->
            </div><!-- START cards box-->
            <div class="card m-3 mb-5">                                      
               <div class="card-body p-0">
                  <!-- Datatable Start-->
                  <div class="table-responsive">
                  <table class="table table-striped my-4 w-100" id="ajxTable">
                     <thead>

                        <tr>
                           <th data-priority="1">{{__('Id')}}</th>
                           <th>{{__('Name')}}</th>
                           <th>{{__('Account Name')}} </th>
                           <th>{{__('Email')}}</th>
                           <th>{{__('Phone')}}</th>
                           <th>{{__("Type")}}</th>
                           <th>{{__('Online')}}</th>
                           <th>{{__('Last login')}}</th>
                           <th>{{__('Total Time')}}</th>
                           <th>{{__('Total Sessions')}}</th>
                           <th>{{__('Created')}}</th>
                           <th>{{__('Updated')}}</th>
                           <th  style="width:160px"></th>
                        </tr>
                     </thead>
                    
                  </table>
                  </div>
                     <!-- Datatable Start-->
               </div>
            </div>
         </div>
      </section>
      
@endsection

@section('script')
    <script>
    $(document).ready(function(){
        function newexportaction(e, dt, button, config) {
            var self = this;
            var oldStart = dt.settings()[0]._iDisplayStart;
            dt.one('preXhr', function (e, s, data) {
                // Just this once, load all data from the server...
                data.start = 0;
                data.length = 2147483647;
                dt.one('preDraw', function (e, settings) {
                    // Call the original action function
                    if (button[0].className.indexOf('buttons-copy') >= 0) {
                        $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                    } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                        $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                            $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                            $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                    } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                        $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                            $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                            $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                    } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                        $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                            $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                            $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                    } else if (button[0].className.indexOf('buttons-print') >= 0) {
                        $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                    }
                    dt.one('preXhr', function (e, s, data) {
                        // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                        // Set the property to what it was before exporting.
                        settings._iDisplayStart = oldStart;
                        data.start = oldStart;
                    });
                    // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                    setTimeout(dt.ajax.reload, 0);
                    // Prevent rendering of the full data to the DOM
                    return false;
                });
            }); 
            dt.ajax.reload();
        };
        
        $("#ajxTable").DataTable({
            serverSide: true,
            ajax: "{{ route('operatorsListing') }}",
            dom: "<'row'<'col-sm-12 col-md-6'Bl><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            "buttons": [
                           {
                               "extend": 'excel',
                               "text": '<i class="fa fa-files-o" style="color: green;"></i>',
                               "titleAttr": 'excel',                               
                               "action": newexportaction
                           }],
           
            columns: [
                 { name: 'id', data:'id' },
                 { name:'first_name', data:'name'},
                 { name: 'account_name',data:'account_name' },
                 { name: 'email',data:'email'},
                 { name: 'phone',data:'phone'},
                 { name:'op_type', data:'op_type_val'},
                 { name: 'online',data:'online' },
                 { name: 'last_enter',data:'last_login_text'},
                 { name: 'total_duration',data:'total_duration'},
                 { name: 'total_session',data:'total_session'},
                 { name: 'created_at',data:'created_at_text'},
                 { name: 'updated_at',data:'updated_at_text'},
                 {name:'edit_delete', data:'edit_delete',orderable: false },
            ],
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'stateSave': true,
            'order': [[0,'desc']],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50]],
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
             "infoCallback": function( settings, start, end, max, total, pre ) {
              return "{{__('Showing')}} "+start +" {{__('to')}} "+ end+ " {{__('of')}} " +total+ " {{__('entries')}}";
            },
            processing: true,
            oLanguage: {
               sEmptyTable: "{{__('Empty Data Dable')}}",
               sZeroRecords: "{{__('No records')}}",
                sLengthMenu: '_MENU_ {{__("records per page")}}',
                sSearch: "{{__('Search')}}",
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: "{{__('No records available')}}",
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                },
                sProcessing: "{{__('Loading...')}}",
            }
        });
        $(".operatorsExport").click(function(){
          $(".buttons-excel").click();
        });

        $(".buttons-excel").hide();

        
    }); 
</script>
@endsection

