<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scheduler; 
use App\Meeting;
use App\Remainder;
use Validator;
use Carbon\Carbon;
use App\AccountSchedule;

class MeetingController extends Controller
{
    private $viewPath; 
    private $viewData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($scheduler_id,$operator_id,$date)
    {
        //
        $scheduler = Scheduler::findOrFail($scheduler_id);
        $datetemp = Carbon::parse($scheduler->start_time);
        $datetemp = (new \App\Helpers\CommonHelper)->formatDateDatabaseObj($datetemp);
        $date = $date." ".$datetemp->format("H:i:s");
        $date = (new \App\Helpers\CommonHelper)->cnvTimetoUserTimezone($date,"Y-m-d H:i:s");
        $date= (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($date,3);
       
        $avaialbleTimes = (new \App\Helpers\CommonHelper)->getAvaialbeSchedulerTime($scheduler_id, $date);
        
        $this->viewData['slots'] = $avaialbleTimes['slots'];
        $this->viewData['dateSlots'] = $avaialbleTimes['datesSlots']; 
        $this->viewData['dateSlotsUser'] = $avaialbleTimes['tempSlotUser'];

        $this->viewData['account_name'] = !empty($scheduler->account) ? $scheduler->account->name : "";
        $this->viewData['operator_profile'] = !empty($scheduler->operator) ? url('uploads/'.$scheduler->operator->image) : "";
        $this->viewData['operator_name'] = !empty($scheduler->operator) ? $scheduler->operator->first_name . " ".$scheduler->operator->last_name  : "";
        $this->viewData['operator_timezone'] = !empty($scheduler->operator) ? $scheduler->operator->timezone  : "";
        

        $this->viewData["schedulerId"] = $scheduler_id;
        $this->viewData["meetingDate"] = reset($avaialbleTimes['datesSlots']);
        $this->viewData['meeting_date'] = (new \App\Helpers\CommonHelper)->formatDate(reset($avaialbleTimes['datesSlots']),1);
        $this->viewData['page_title'] = __("Create Meeting");
        $this->viewData['timezones'] = \DB::table('timezones')->get();
        $this->viewData['country'] = \DB::table('country')->orderBy('country_code', 'asc')->groupBy('country_code')->get();
        $this->viewData['link'] ="Schedule_Management";
        return view("scheduler.meetings.create",$this->viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$scheduler_id,$date)
    {
        $requestData = $request->all();
        $validation = Validator::make($requestData,[
            'country_code' => 'required',
            'phone' => 'required', 
            'start_time'=> 'required',
            'email' => 'required',
            'full_name'=>'required',
            'user_timezone' => 'required'
        ]);

        if($validation->fails()) {
            if($request->ajax()){
                return response()->json(['msg'=>__("validation error")],401);
            }
        }
        $scheduler = Scheduler::findOrFail($scheduler_id);
        $operator = $scheduler->operator;
        $account = $scheduler->account;
        if(empty($operator) || empty($account)) {
            return response()->json(['msg'=>__("Unable to create a meeting.")], 404);
        }
        $requestData['scheduler_id'] = $scheduler->id; 
        $requestData['operator_id'] = $scheduler->operator_id;
        $requestData['meeting_date'] = $date;
        $requestData['slot_minute'] = $scheduler->time_slot;
        $requestData['default_lang'] = empty($scheduler->account->language) ? 'english' : $scheduler->account->language;
        $requestData['operator_timezone'] = $scheduler->operator->timezone;
        $requestData['added_user_type'] = 2; 
        $requestData['added_by'] = auth()->user()->id; 
        $requestData['app_user_id'] = $this->registerAppUser($requestData['full_name'],$requestData['country_code'].$requestData['phone']);

        $tempDate = $requestData['meeting_date']." ".$requestData['start_time']; 
        $tempdateObj = (new \App\Helpers\CommonHelper)->cnvTimetoGivenTimezone($tempDate,$scheduler->operator->timezone,"Y-m-d H:i");
        
        $requestData['meeting_date'] = (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,1);
        $requestData['start_time'] =  (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,2);
        
        if($meeting = Meeting::create($requestData)) {
            $this->addRemainder($scheduler,$meeting);
            return response()->json(['msg'=>__("The meeting successfully added."),'redirect'=>route('schedulerListing')], 200);
        }
        return response()->json(['msg'=>__("Something went wrong")],404);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $meeting = Meeting::findOrFail($id);
        //sdd($meeting->scheduler_id);
        $scheduler_id = $meeting->scheduler_id; 
        $date  = $meeting->meeting_date;
        $scheduler = Scheduler::findOrFail($meeting->scheduler_id);
        
        $avaialbleTimes  = (new \App\Helpers\CommonHelper)->getAvaialbeSchedulerTime($scheduler_id, $date,$id);
        $this->viewData['slots'] = $avaialbleTimes['slots'];
        
        $this->viewData['dateSlots'] = $avaialbleTimes['datesSlots']; 
        $this->viewData['dateSlotsUser'] = $avaialbleTimes['tempSlotUser'];

        $this->viewData['account_name'] = !empty($scheduler->account) ? $scheduler->account->name : "";
        $this->viewData['operator_profile'] = !empty($scheduler->operator) ? url('uploads/'.$scheduler->operator->image) : "";
        $this->viewData['operator_name'] = !empty($scheduler->operator) ? $scheduler->operator->first_name . " ".$scheduler->operator->last_name  : "";
        $this->viewData['operator_timezone'] = !empty($scheduler->operator) ? $scheduler->operator->timezone  : "";
        $meetingTime = Carbon::parse($meeting->meeting_date." ".$meeting->start_time);
        // $this->viewData['meeting_time'] = (new \App\Helpers\CommonHelper)->formatDateDatabase($meeting->start_time,3);
        $cdate =(new \App\Helpers\CommonHelper)->cnvtoUserTimeYMD($meetingTime,$meeting->operator_timezone);
        $this->viewData['meeting_date'] = $cdate->format("d/m/y");
        $this->viewData['meeting_time'] = $cdate->format("H:i");
        if(empty($this->viewData['slots'])) {
            $this->viewData['slots']= array(
                $cdate->format("H:i") => $cdate->format("H:i") ." - ".$cdate->addMinutes($meeting->slot_minute)->format("H:i")
            );
        } 
        if(!isset($this->viewData['slots'][$this->viewData['meeting_time']])) {
            $this->viewData['slots'][$this->viewData['meeting_time']] = $cdate->format("H:i") ." - ".$cdate->addMinutes($meeting->slot_minute)->format("H:i");
            asort($this->viewData['slots']);
        }
        
        $this->viewData['meeting'] = $meeting;
        $this->viewData["schedulerId"] = $scheduler_id;
        $this->viewData['page_title'] = __("Edit Meeting");
        $this->viewData['timezones'] = \DB::table('timezones')->get();
        $this->viewData['country'] = \DB::table('country')->orderBy('country_code', 'asc')->groupBy('country_code')->get();
        $this->viewData['link'] ="Schedule_Management";
        return view("scheduler.meetings.edit",$this->viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $requestData = $request->except('_token','_method');

        $validation = Validator::make($requestData,[
            'country_code' => 'required',
            'phone' => 'required', 
            'start_time'=> 'required',
            'email' => 'required',
            'full_name'=>'required',
            'user_timezone' => 'required',
            'meeting_date' => 'required'
        ]);

        if($validation->fails()) {
            if($request->ajax()){
                return response()->json(['msg'=>"validation error"],401);
            }
        }
        $oldMeeting = Meeting::findOrFail($id);
        $requestData['app_user_id'] = $this->registerAppUser($requestData['full_name'],$requestData['country_code'].$requestData['phone']);
        if($requestData['app_user_id'] != $oldMeeting->app_user_id || $requestData['start_time'].":00" != $oldMeeting->start_time) {
            $this->deleteRemaider($oldMeeting);
            $addRemainder = true;
        }
        $scheduler = Scheduler::find($oldMeeting->scheduler_id);
        
        $tempDate = $requestData['meeting_date']." ".$requestData['start_time']; 
        //$tempdateObj = (new \App\Helpers\CommonHelper)->cnvTimetoUserTimezone($tempDate,"Y-m-d H:i");
        $tempdateObj = (new \App\Helpers\CommonHelper)->cnvTimetoGivenTimezone($tempDate,$oldMeeting->operator_timezone,"Y-m-d H:i");

        $requestData['meeting_date'] = (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,1);
        $requestData['start_time'] =  (new \App\Helpers\CommonHelper)->convertUserTimeToServerTime($tempdateObj,2);
       // dd($requestData);
        if($meeting = Meeting::where('id',$id)->update($requestData)) {
            
            if(isset($addRemainder)) {
                $meeting = Meeting::find($id);
                $this->addRemainder($scheduler,$meeting);
            }
            return response()->json(['msg'=>__("The meeting successfully updated."),'redirect'=>route('schedulerListing')], 200);
        }
        return response()->json(['msg'=>__("Something went wrong")],404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * register app user 
     */
    public function registerAppUser($name, $number) {
        $appUser = \App\AppUser::where('number', $number)->first();
        if (empty($appUser)) {
            $appUser = new \App\AppUser();
            $appUser->name = $name;
        }
        $appUser->online = 1;
        $appUser->number = $number;
        $appUser->save();

        return $appUser->id;
    }

    /** 
     * add remainder 
     */
    public function addRemainder($schedulerData,$meetingData)
    {
        $accountSchedule = AccountSchedule::where('account_id',$schedulerData->account_id)->first();
        if(empty($accountSchedule)) {
            return false;
        }
        if(empty($accountSchedule->message_via_email) &&  empty($accountSchedule->message_via_sms) ) {
            return false;
        }
        $remainderTimes = [
            0 => [
                'set' => "first_meeting_reminder", 
                'type' => "first_meeting_value_type", 
                'value' => "first_meeting_value"
            ], 
            1 => [
                'set' => "second_meeting_reminder", 
                'type' => "second_meeting_value_type", 
                'value' => "second_meeting_value"
            ],
            2 => [
                'set' => "third_meeting_reminder", 
                'type' => "third_meeting_value_type", 
                'value' => "third_meeting_value"
            ],
        ];

        foreach($remainderTimes as $remainderTime) {
            if($accountSchedule->{$remainderTime['set']}) {
                
              //  if($accountSchedule->{$remainderTime['type']} == 1) {
                    $meetingDateTime = $meetingData->meeting_date. " ".$meetingData->start_time;
                    if(empty($accountSchedule->{$remainderTime['value']})) {
                        continue;
                    }
                    if($accountSchedule->{$remainderTime['type']} == 1 ) {
                        $remainder = Carbon::parse($meetingDateTime)->subDays($accountSchedule->{$remainderTime['value']});
                    } elseif($accountSchedule->{$remainderTime['type']} == 2 ) {
                        $remainder = Carbon::parse($meetingDateTime)->subHours($accountSchedule->{$remainderTime['value']});
                    } elseif($accountSchedule->{$remainderTime['type']} == 3 ) {
                        $remainder = Carbon::parse($meetingDateTime)->subMinutes($accountSchedule->{$remainderTime['value']});
                    }
                    $meetingData->meeting_date; 
                    if(!$remainder->isPast()) {
                        $remainderObj = new Remainder;
                        $remainderObj->meeting_id = $meetingData->id; 
                        $remainderObj->account_id = $schedulerData->account_id; 
                        $remainderObj->on_time = $remainder->format("Y-m-d H:i:s");
                        $remainderObj->via = ($accountSchedule->message_via_email && $accountSchedule->message_via_sms ) ? 3 : (($accountSchedule->message_via_email && !$accountSchedule->message_via_sms) ? 1 : (( !$accountSchedule->message_via_email && $accountSchedule->message_via_sms) ? 2 : 0));
                        $remainderObj->send_to = $meetingData->app_user_id;
                        $remainderObj->role = 1; 
                        $remainderObj->save();

                        if(!empty($accountSchedule->meeting_reminder)) {
                            $remainderObj = new Remainder;
                            $remainderObj->meeting_id = $meetingData->id; 
                            $remainderObj->account_id = $schedulerData->account_id; 
                            $remainderObj->on_time = $remainder->format("Y-m-d H:i:s");
                            $remainderObj->via = ($accountSchedule->message_via_email && $accountSchedule->message_via_sms ) ? 3 : (    ($accountSchedule->message_via_email && !$accountSchedule->message_via_sms) ? 1 : (( !$accountSchedule->message_via_email && $accountSchedule->message_via_sms) ? 2 : 0));
                            $remainderObj->send_to = $meetingData->operator_id;
                            $remainderObj->role = 2; 
                            $remainderObj->save();
                        }
                    }
               // }
            }
        }
    } 

    /**
     * deletes remainder
     */
    public function deleteRemaider($meetingData)
    {
        Remainder::where('meeting_id',$meetingData->id)->delete();
    }
    
    /**
     * cancel meeting
     */
    public function cancelMeeting($id)
    {
        $meeting = Meeting::findOrFail($id);
        if($meeting->status != 1) {
            return response()->json(['msg'=>__("We are unable to delete the meeting."),'redirect'=>route('schedulerListing')], 200);
        }
        $meeting->status =  4; 
        $meeting->cancel_by = auth()->user()->id;
        $meeting->cancel_at = Carbon::now();
        $meeting->cancel_user_type = 2;
        if($meeting->save()) {
            $this->sendCancelMail($meeting);
            return response()->json(['msg'=>__("The meeting has been canceled."),'redirect'=>route('schedulerListing')], 200);
        }
        return response()->json(['msg'=>__("Something went wrong")],404);
    }
    
    /** 
     * send meeting cancel mail to user
     */
    public function sendCancelMail($meeting)
    {
        $slug  = 'meeting_was_canceled_by_operator_email';
         
        $content =$this->getContent($meeting,$slug);
        if(empty($content)) return false;
        try {
            
        
            if(empty($meeting->email)) return false;
            $name = $meeting->full_name;
            $email = $meeting->email;
            \Mail::send('scheduler.remainder_mail', ['text'=> $content['content'],'direction'=>$content['direction']], function ($m) use ($email,$name) {
                $m->to($email, $name)->subject(__("Your meeting is canceled")); // receiver Email
            });
            $remainder->status = 2;
            $remainder->save();
        } catch(\Exception $e){
        }
    }

    /** 
     * get content 
     */
    public function getContent($meeting,$slug)
    {
        $settings = [];
        if(isset($meeting->schduler->account_id) && !empty($meeting->schduler->account_id) ) {
            $settings = \App\MobileContent::where('slug', $slug)->where('account_id',$meeting->schduler->account_id)->first();
        } 
            
         
        if(empty($settings)) {
            $settings = \App\MobileContent::where('slug',$slug)->whereNull('account_id')->first();
        }
        if(empty($settings)) {
           return "";
        }
        $language = \App\Language::where('slug',$meeting->default_lang)->first();
        if(empty($language)) {
            return "";
        }
        $langDir = $language->direction;
        $sdata = $settings->content->where('language_id',$language->id)->first();
        $string = !empty($sdata) ? $sdata->value : '';
        $name = $meeting->full_name;
        $string = str_replace("#name#", $name, $string);
        $url = url($meeting->schedule->account->url_name."/schedule");
        $string = str_replace("#url#", $url, $string);
        $return = [
            'content'=>$string, 
            'direction' =>$langDir
        ];
        return $return;
    }

    /** 
     * view meeting
     * @id
     */
    public function viewMeeting(Request $request,$id)
    {
        $meeting = Meeting::findOrFail($id);
        //sdd($meeting->scheduler_id);
        $scheduler_id = $meeting->scheduler_id; 
        $date  = $meeting->meeting_date;
        $scheduler = Scheduler::findOrFail($meeting->scheduler_id);
        
        $this->viewData['slots']  = (new \App\Helpers\CommonHelper)->getAvaialbeSchedulerTime($scheduler_id, $date,$id);
        $this->viewData['operator_profile'] = !empty($scheduler->operator) ? url('uploads/'.$scheduler->operator->image) : "";
        $this->viewData['operator_name'] = !empty($scheduler->operator) ? $scheduler->operator->first_name . " ".$scheduler->operator->last_name  : "";
        $meetingTime = Carbon::parse($meeting->meeting_date." ".$meeting->start_time);
        $this->viewData['meeting_date'] = (new \App\Helpers\CommonHelper)->formatDate($meetingTime,1);

        $this->viewData['meeting_time'] = (new \App\Helpers\CommonHelper)->formatDateDatabase($meetingTime,3);
        
        $tempDate = Carbon::parse($meeting->meeting_date." ".$meeting->start_time);
        
        $accountSchedule = \App\AccountSchedule::where("account_id",$scheduler->account_id)->first();
        $currentTime = Carbon::now();
        $diff = $tempDate->diffInMinutes($currentTime);
        $this->viewData['show_session_btn'] = true;
        if($tempDate->isPast() &&  (empty($accountSchedule->max_wait_time) || $accountSchedule->max_wait_time <= $diff ) ) {
            $this->viewData['show_session_btn']  = false;
        }
        $this->viewData['show_session_btn'] = true;

        $endTime = $tempDate->addMinutes($meeting->slot_minute)->format("H:i");
        $end_time  = (new \App\Helpers\CommonHelper)->formatDateDatabase($endTime,3);
        $this->viewData['meeting_time']  .= " - ".$end_time;
        //dd($end_time);
        $this->viewData['meeting'] = $meeting;
        $this->viewData['page_title'] = __("View Meeting");
        $this->viewData['link'] ="Schedule_Management";
        return view("scheduler.meetings.view",$this->viewData);
    }
}
