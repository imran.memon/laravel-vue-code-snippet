<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NewsCategory;
class Call extends Model
{
    protected $table = 'calls';
    protected $guarded = [

    ];

    public function supplier(){
        return $this->belongsTo('App\Supplier',"store_id","id");
    }

    public function product(){
        return $this->belongsTo('App\Product',"product_id","id");
    }
    
}

