<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TwilioRestClient;
use TwilioJwtAccessToken;
use TwilioJwtGrantsVideoGrant;
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Auth;

class VideoTestController extends Controller
{
  protected $sid;
  protected $token;
  protected $key;
  protected $secret;

  public function __construct()
  {
  $this->sid = 'AC103cb70a317cf1c0db64d7548f85ae0c';
  $this->token = 'c99d7ea5aaa4a5d79a67e51609624993';
  $this->key = 'SKda9241879a922880bb9433d606b92a7c';
  $this->secret ='Str4bXS5RPXrfoMm4wZT1UExNfS4v2lO';
  }
  public function index()
  {
    $rooms = [];
    try {
    $client = new Client($this->sid, $this->token);
    $allRooms = $client->video->rooms->read([]);

    $rooms = array_map(function ($room) {
    return $room->uniqueName;
    }, $allRooms);
    } catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    }
    return view('test.index', ['rooms' => $rooms]);
  }

  public function createRoom(Request $request)
  {
    $client = new Client($this->sid, $this->token);
    $exists = $client->video->rooms->read(['uniqueName' => $request->roomName]);
    if (empty($exists)) {
      $client->video->rooms->create([
      'uniqueName' => $request->roomName,
      'type' => 'group',
      'recordParticipantsOnConnect' => true
     ]);
    \Log::debug("created new room: " . $request->roomName);
    }
   return redirect()->action('VideoTestController@joinRoom', [
    'roomName' => $request->roomName
   ]);
  }

public function joinRoom($roomName)
{
  $client = new Client($this->sid, $this->token);
  // A unique identifier for this user
  // $identity = Auth::user()->name;
  $identity = rand(3,1000);
  \Log::debug("joined with identity: $identity");
  $token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);
  $videoGrant = new VideoGrant();
  $videoGrant->setRoom($roomName);
  $token->addGrant($videoGrant);
  $rooms=$client->video->v1->rooms($roomName)
                          ->fetch();
  return view('test.room', ['accessToken' => $token->toJWT(), 'roomName' => $roomName]);
}


}//end of the file