<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Libraries\GeneralSetting;
Route::get('shareSession/{combine}','OperatorController@shareSession');
Route::get('/test_design1',function(){
    $data['page_title']='Video Call';
	return view('test_design1',$data);
});

Route::get('/test_design2',function(){
    $data['page_title']='Video Call';
	return view('test_design2',$data);
});

Route::get('permission_denied',function(){
	return view('permission_denied');
});

// App User Route 
Route::get('allow_recording', 'AppUserController@allowRecording');

Route::group(['middleware'=>'localization'],function ()
{
//Account Schedule Session Starts	
	Route::get('/{accountUrl}/schedule','AccountController@accountSessionHome');

	Route::get('/{accountUrl}/accountSessionLoginScreen','AccountController@accountSessionLogin')->name('accountSessionLoginScreen');
	Route::post('/{accountUrl}/accountSessionLoginScreen','AccountController@accountSessionLogin')->name('accountSessionLoginScreen');

	Route::get('/{accountUrl}/{userId}/accountSessionOTPScreen','AccountController@accountSessionOTP')->name('accountSessionOTPScreen');
	Route::post('/{accountUrl}/{userId}/accountSessionOTPScreen','AccountController@accountSessionOTP')->name('accountSessionOTPScreen');

	Route::get('/{accountUrl}/{userId}/accountSessionSendOtpEmail','AccountController@accountSessionSendOtpEmail')->name('accountSessionSendOtpEmail');

	Route::get('/{accountUrl}/accountSessionManagerList','AccountController@accountSessionManagerList')->name('accountSessionManagerList');
	
	Route::get('getOperatorMeetingList','AppUserController@getOperatorMeetingList')->name('getOperatorMeetingList');
	Route::post('bookOperatorMeeting','AppUserController@bookOperatorMeeting')->name('bookOperatorMeeting');
	Route::get('bookingSuccess/{accountUrl}','AppUserController@bookingSuccess')->name('bookingSuccess');
	Route::get('meeting/{accountUrl}/{meeting_id}','AppUserController@bookingSuccessEmail')->name('bookingSuccessEmail');
	Route::get('cancelMeeting/{meeting_id}','AppUserController@cancelMeeting')->name('cancelMeeting');
	Route::get('bookingChangeDate/{schedule_id}/{meeting_id}','AppUserController@bookingChangeDate')->name('bookingChangeDate');
	Route::get('bookingChangeDateAjax','AppUserController@bookingChangeDateAjax')->name('bookingChangeDateAjax');
	Route::post('changeBookOperatorMeeting/{meeting_id}','AppUserController@changeBookOperatorMeeting')->name('changeBookOperatorMeeting');
	
	Route::get("meeting/status/update/{id}","AppUserController@checkMeetingStatus")->name("meeting-status");
	Route::get("success/meeting/{meeting_id}","AppUserController@successMeetingScreen")->name("success-meeting-screen");
	Route::get("cancel/meeting/{meeting_id}","AppUserController@cancelMeetingScreen")->name("cancel-meeting-screen");

//Account Schedule Session Ends

Route::get('/joinsession/{type}/{roomSID}/{customerID}','AppUserController@index');
Route::get('/call/{type}/{roomSID}/{customerID}/{latitude}/{longitude}','VideoRoomController@joinRoom');
Route::get('/terms_condition/{type}/{roomSID}/{customerID}','AppUserController@termsCondition');
Route::get('join/{type}/{roomSID}/{customerID}', 'VideoRoomController@joinRoom')->name('joinScreen');
Route::get('get/call_duration', 'VideoRoomController@getCallDuration');
Route::get('get/room_status', 'VideoRoomController@getRoomStatus');
Route::get('save/room_creation_time', 'VideoRoomController@saveRoomCreationTime');
Route::get('set/app_user/live_location', 'VideoRoomController@setAppUserLiveLocation');
Route::get('smsotp/{id}', 'OperatorController@smsotp');
Route::post('verifictionOtp', 'OperatorController@verifictionOtp')->name('verifictionOtp');
Route::post('add/participant', 'VideoRoomController@addParticipant');
Route::any('getVideoCallBack', 'VideoRoomController@getVideoCallBack');
Route::post('add/video_attachment', 'VideoRoomController@addVideoAttachment');
Route::post('save_snap','VideoRoomController@save_snap');
Route::get('update/attachment_status', 'VideoRoomController@updateAttachmentStatus');
Route::get('composVideos', 'VideoRoomController@composVideos');
Route::get('storeVideos', 'VideoRoomController@storeVideos');
Route::get('disconnect', 'VideoRoomController@disConnectParticipants');
Route::any('check-composition', 'VideoRoomController@checkConposition');
Route::get("app_user/joined/{customerID}/{roomSID}",'AppUserController@updateUserJoinedTime');
Route::get("app_user/switch/room/{roomSID}",'VideoRoomController@createUpdateRoom')->name('switch-room');

Route::get('app_user/thankyou', function(){
	if(session('lang') == 'it') {
		\App::setLocale("it");
	}
	return view('app_user.thankyou');
});
});
Route::get('app_user/checkbrowser', function(){
	return view('app_user.checkbrowser');
});
// App User Route 

Route::get('/', function () {
	return redirect()->route('login');
});
Route::match(['get', 'post'], '/logout', 'Auth\LoginController@logout');
Auth::routes(['register' => false]);
Route::get('get/sharesession/{id}','OperatorController@getShareSessionById');

Route::group(['middleware'=>['localization','auth','account']],function ()
{
	// Account 
	Route::resource('accounts', 'AccountController');
	Route::get('accounts','AccountController@index')->name("accountsListing");
	Route::get('view/account','AccountController@editAccountForManager')->name("viewAccountListing");
	Route::get('account/mobile/content','AccountController@mobileContentView')->name('accountMobileContentListing');
	Route::post('account/mobile/content/update','AccountController@updateMobileContent')->name('AccountUpdateMobileContent');
	Route::post('account/schedule/update','AccountController@updateSchedule')->name('AccountUpdateSchedule');
	Route::post('getMobileContent','AccountController@getMobileContent')->name('accountGetMobileContent');
	Route::post('account/get/tab/content',"AccountController@getTabContent")->name('accountTabContent');
	// Status Settings Starts
	Route::get('account/status/setting/list','AccountController@ViewStatus')->name('accountStatusSettingsListing');
	Route::get('account/status/setting/add','AccountController@addStatusSettings')->name('accountInsertStatusSettings');
	Route::post('account/addStatusSettings','AccountController@addStatusSettings')->name('accountInsertPostStatusSettings');
	Route::get('account/status/setting/edit/{id}','AccountController@editStatusSettings')->name('accountEditStatusSettings');
	Route::post('account/status/setting/edit/{id}','AccountController@editStatusSettings')->name('accountEditStatusSettings');
	Route::get('account/status/setting/delete/{id}','AccountController@deleteStatusSettings')->name('accountDeleteStatusSettings');
	Route::get('account/status/setting/data/{account_id}','AccountController@getStatusData')->name('accountStatusDataByAccount');
	
	// Status Settings Ends

	// Operator - Session
	Route::get('session','VideoRoomController@index')->name('sessionListing');
	Route::post('start_video_call','VideoRoomController@createRoom');
    Route::get('get/locationData', 'VideoRoomController@getLocationData');
    Route::get('get/graphSessionData', 'VideoRoomController@getSessionGraphData');
	Route::get('check/app_user_join', 'VideoRoomController@isAppUSerJoin');
	Route::get('get/app_user/live_location', 'VideoRoomController@getAppUserLiveLocation');
	Route::get('session_export/{searchVal?}','VideoRoomController@sessionDataExport');

    Route::get('get/locationData', 'VideoRoomController@getLocationData');
    // Route::get('get/graphSessionData', 'VideoRoomController@getSessionGraphData');

	Route::get('delete/session/{id}','OperatorController@deleteSession')->name('deleteSession')->middleware('auth','sectionAuth');
	Route::get('edit/session/{id}','OperatorController@editSession')->name('editSession')->middleware('auth','sectionAuth');
	Route::get('get/latest/session/{id}','OperatorController@getLatestSessionId');
	Route::get('get/session/{id}','OperatorController@getSessionById');
	Route::get('save/operator_comment','OperatorController@saveOperatorComment');

        Route::get('/dashboard', 'HomeController@index')->name('dashboardListing');
        Route::prefix('system')->group(function () {
			//Manager List Starts
				// import dictionary 
				Route::get('import-data', 'System\DictionaryController@importData');
				Route::get('download/demo/language/excel','System\DictionaryController@downloadImportExcel')->name("download-sample");
				Route::get('dictionary/import','System\DictionaryController@importDictionaryView')->name("import-dictionary");
				Route::post('dictionary/import','System\DictionaryController@importDictionary')->name("import-dictionary-post");

				Route::get('managers','System\ManagersController@managers')->name('managersListing');
				Route::get('addManager','System\ManagersController@addManager')->name('addManagerAdd');
				Route::post('addManager','System\ManagersController@addManager')->name('addManagerAdd');
				Route::get('deleteManager/{id}','System\ManagersController@deleteManager')->name('deleteManager');
				Route::get('editManager/{id}','System\ManagersController@editManager')->name('editManager');
				Route::post('editManager/{id}','System\ManagersController@editManager')->name('editManager');
			//Manager List Ends

			//Manager Type List Starts
				Route::get('addManagerType','System\ManagersController@addManagerType')->name('addManagerTypeAdd');
				Route::post('addManagerType','System\ManagersController@addManagerType')->name('addManagerTypeAdd');
				Route::get('editManagerType/{id}','System\ManagersController@editManagerType')->name('editManagerType');
				Route::post('editManagerType/{id}','System\ManagersController@editManagerType')->name('editManagerType');
				Route::get('deleteManagerType/{id}','System\ManagersController@deleteManagerType')->name('deleteManagerType');
			//Manager Type List Ends

			//Section List Starts
				Route::get('sections','System\SystemSectionsController@sections')->name('sectionsListing');
				Route::get('addSection','System\SystemSectionsController@addSection')->name('addSectionAdd');
				Route::post('addSection','System\SystemSectionsController@addSection')->name('addSectionAdd');
				Route::get('editSection/{id}','System\SystemSectionsController@editSection')->name('editSection');
				Route::post('editSection/{id}','System\SystemSectionsController@editSection')->name('editSection');
				Route::get('reOrder','System\SystemSectionsController@reOrder')->name('reOrder');
				Route::post('reOrder','System\SystemSectionsController@reOrder')->name('reOrder');
				Route::get('deleteSection/{id}','System\SystemSectionsController@deleteSection')->name('deleteSection');
			//Section List Ends

			//Dictionary Starts
				Route::get('dictionary','System\DictionaryController@dictionary')->name('dictionaryListing');
				Route::get('addDictionary','System\DictionaryController@addDictionary')->name('addDictionaryAdd');
				Route::post('addDictionary','System\DictionaryController@addDictionary')->name('addDictionaryAdd');
				Route::get('editDictionary/{id}','System\DictionaryController@editDictionary')->name('editDictionary');
				Route::post('editDictionary/{id}','System\DictionaryController@editDictionary')->name('editDictionary');
				Route::get('deleteDictionary/{id}','System\DictionaryController@deleteDictionary')->name('deleteDictionary');
				
				
			//Dictionary Ends

			//Files Starts 
				Route::get('files','System\FilesController@index')->name('filesListing');
			//Files Ends

				//General Settings Starts
				Route::get('GeneralSettings','System\GeneralSettingsController@index')->name('GeneralSettingsListing');
				Route::post('updateGeneralSettings','System\GeneralSettingsController@updategeneralSettings')->name('updateGeneralSettings');
				Route::post('updateApplicationMenu','System\GeneralSettingsController@updateApplicationMenu')->name('updateApplicationMenu');
				Route::post('updateAPI','System\GeneralSettingsController@updateAPI')->name('updateAPI');
				Route::post('getMobileContent','System\GeneralSettingsController@getMobileContent')->name('getMobileContent');
				Route::post('updateMobileContent','System\GeneralSettingsController@updateMobileContent')->name('updateMobileContent');

				//Screen Text Starts
					Route::post('getScreenText','System\GeneralSettingsController@getScreenText')->name('getScreenText');
					Route::post('addScreenText','System\GeneralSettingsController@addScreenText')->name('addScreenText');
					Route::post('updateScreenText','System\GeneralSettingsController@updateScreenText')->name('updateScreenText');
					Route::get('deleteScreenText/{id}','System\GeneralSettingsController@deleteScreenText')->name('deleteScreenText');
				//Screen Text Ends

				//Message Text Starts
					Route::post('getMessageText','System\GeneralSettingsController@getMessageText')->name('getMessageText');
					Route::post('addMessageText','System\GeneralSettingsController@addMessageText')->name('addMessageText');
					Route::post('updateMessageText','System\GeneralSettingsController@updateMessageText')->name('updateMessageText');
					Route::get('deleteMessage/{id}','System\GeneralSettingsController@deleteMessage')->name('deleteMessage');
				//Message Text Ends

				//Template Text Starts
					Route::post('getTemplateText','System\GeneralSettingsController@getTemplateText')->name('getTemplateText');
					Route::post('addTemplate','System\GeneralSettingsController@addTemplate')->name('addTemplate');
					Route::post('updateEmailTemplate','System\GeneralSettingsController@updateEmailTemplate')->name('updateEmailTemplate');
					
					Route::get('deleteTemplate/{id}','System\GeneralSettingsController@deleteTemplate')->name('deleteTemplate');
					//Route::get('testParseContent','System\GeneralSettingsController@testParseContent')->name('testParseContent');
				//Template Text Ends
			//General Setting Ends

			//Recycle Bin Starts
				Route::get('recycleBin','System\RecycleBinController@index')->name('recycleBinListing');

				Route::get('deleteFromTrash/{id}/{table}','System\RecycleBinController@deleteFromTrash')->name('deleteFromTrash');
				Route::get('deleteAllRecordFromTrash','System\RecycleBinController@deleteAllRecordFromTrash')->name('deleteAllRecordFromTrash');
				Route::get('restoreFromTrash/{id}/{table}','System\RecycleBinController@restoreFromTrash')->name('restoreFromTrash');
			//Recycle Bin Ends

			// Status Settings Starts
	            Route::get('statusSettings','System\StatusSettingsController@index')->name('statusSettingsListing');
	            Route::get('addStatusSettings','System\StatusSettingsController@addStatusSettings')->name('addStatusSettings');
	            Route::post('addStatusSettings','System\StatusSettingsController@addStatusSettings')->name('addStatusSettings');
	            Route::get('editStatusSettings/{id}','System\StatusSettingsController@editStatusSettings')->name('editStatusSettings');
	            Route::post('editStatusSettings/{id}','System\StatusSettingsController@editStatusSettings')->name('editStatusSettings');
	            Route::get('deleteStatusSettings/{id}','System\StatusSettingsController@deleteStatusSettings')->name('deleteStatusSettings');
			// Status Settings Ends
	        
			// languages routes
			Route::resource('languages', 'LanguageController');
			Route::get('languages','LanguageController@index')->name('LanguagesListing');
			Route::post("language/default","LanguageController@markAdDefaultLanguage")->name('language.default');
		});

	
	//System Route Ends

	//Opertor Starts 

	    Route::get('operatorsListing','OperatorController@index')->name('operatorsListing');
        Route::get('operatorsExport','OperatorController@operatorsExport')->name('operatorsExport');
	    Route::get('addOperators','OperatorController@addOperators')->name('addOperators');
	    Route::post('addOperators','OperatorController@addOperators')->name('addOperators');
	    Route::get('editOperators/{id}','OperatorController@EditOperators')->name('editOperators');
	    Route::post('editOperators/{id}','OperatorController@EditOperators')->name('editOperators');
	    Route::get('deleteOperators/{id}','OperatorController@deleteOperators')->name('deleteOperators');
	    Route::get('operatorsSessionExport/{id}','OperatorController@operatorsSessionExport')->name('operatorsSessionExport');
        Route::post('send_share_seesion','OperatorController@send_share_seesion');
	//Opertor Ends
	//User Route Starts

                
    //AppUser Starts
    Route::get('appUser','AppUserController@appUserListing')->name('appUserListing')->middleware('auth','sectionAuth');
    Route::get('appUserExport/{searchVal?}','AppUserController@appUserExport')->name('appUserExport')->middleware('auth','sectionAuth');
    Route::get('appUserSessionExport/{id}','AppUserController@appUserSessionExport')->name('appUserSessionExport')->middleware('auth','sectionAuth');
    Route::get('editAppUser/{id}','AppUserController@editAppUser')->name('editAppUser')->middleware('auth','sectionAuth');
    Route::post('editAppUser/{id}','AppUserController@editAppUser')->name('editAppUser')->middleware('auth','sectionAuth');
    Route::get('deleteAppUser/{id}','AppUserController@deleteAppUser')->name('deleteAppUser')->middleware('auth','sectionAuth');
    //AppUser Ends

    //User Route Starts
    Route::get("users","UserController@users")->name('userListing');
    Route::get('editUser/{user}','UserController@editUser')->name('editUser');
    Route::post('editUser/{user}','UserController@editUser')->name('editUser');
    Route::get('userExport','UserController@userExport')->name('userExport');
    Route::get('userOrderExport/{id}','UserController@userOrderExport')->name('userOrderExport');
    Route::get('userCallExport/{id}','UserController@userCallExport')->name('userCallExport');
    //User Routes Ends

   //Supplier List Starts
    Route::get('Supplier','SupplierController@Supplier')->name('supplierListing');
    Route::get('addSupplier','SupplierController@addSupplier')->name('addSupplier');
    Route::post('addSupplier','SupplierController@addSupplier')->name('addSupplier');
    Route::get('editSupplier/{id}','SupplierController@editSupplier')->name('editSupplier');
    Route::post('editSupplier/{id}','SupplierController@editSupplier')->name('editSupplier');
    Route::get('deleteSupplier/{id}','SupplierController@deleteSupplier')->name('deleteSupplier');
    Route::get('supplierExport','SupplierController@supplierExport')->name('supplierExport');

    Route::get('addSupplierProduct/{supplierId}','SupplierController@addSupplierProduct')->name('addSupplierProduct');
    Route::post('addSupplierProduct/{supplierId}','SupplierController@addSupplierProduct')->name('addSupplierProduct');

    Route::get('editSupplierProduct/{supplierId}/{supplierProductId}','SupplierController@editSupplierProduct')->name('editSupplierProduct');
    Route::post('editSupplierProduct/{supplierId}/{supplierProductId}','SupplierController@editSupplierProduct')->name('editSupplierProduct');

    Route::get('deleteSupplierProduct/{id}','SupplierController@deleteSupplierProduct')->name('deleteSupplierProduct');
    Route::get('supplierOrderExport/{id}','SupplierController@supplierOrderExport')->name('supplierOrderExport');
    Route::get('supplierCallExport/{id}','SupplierController@supplierCallExport')->name('supplierCallExport');
    //Supplier List Ends


   //City Starts
    Route::get('importExportView', 'CityController@importExportView');
    Route::post('import', 'CityController@import')->name('import');
    //City Ends

    Route::get('setLocale/{locale}','System\DictionaryController@setLocale')->name('setLocale');
    Route::get('changePassword','Auth\ResetPasswordController@changePassword')->name('changePassword');
    Route::get('/unauthorized', function () {
            $data['page_title']='unauthorized';
            return view('unauthorized',$data);
    })->name('unauthorized');
    Route::get('/comingSoon', function () {
            $data['page_title']='comingSoon';
            return view('comingsoon',$data);
    })->name('comingSoon');

	Route::prefix('scheduler_management')->group(function () {
		Route::resource('schedule', 'SchedulersController');
		Route::get("calender","SchedulersController@index")->name("schedulerListing");
		Route::resource('meeting', 'MeetingController');
		Route::get("meeting/create/{scheduler_id}/{operator_id}/{date}","MeetingController@create")->name("meeting.create");
		Route::post("meeting/create/{scheduler_id}/{date}","MeetingController@store")->name("meeting.store");
		Route::get('meeting/cancel/{id}',"MeetingController@cancelMeeting")->name('meeting.cancel');
		Route::post('get/operators','SchedulersController@getOperators')->name('scheduler.get.operators');
		Route::post('get/schedules',"SchedulersController@getSchedules")->name("get.schedules");
		Route::get('meeting/view/{id}',"MeetingController@viewMeeting")->name('meeting.view');
		Route::get("meeting/start/{id}","VideoRoomController@startMeeting")->name("start-meeting");
	}); 
	Route::resource('operator-types', 'OperatorTypesController');
	Route::get("operator-type/delete/{id}",'OperatorTypesController@destroy')->name("delete-operator_type");
	Route::get("operator-type/get/by/manager/{id}",'OperatorTypesController@getOperatorTypesByManager')->name("operator-type-by-manager");
});


Route::get("remainder","RemaindersController@getRemainders");
Route::get('/selection/import', 'HomeController@importSectionData');
//Route::get('/calender', 'SchedulersController@index');
Route::get('/account-content-slug-update', 'AccountController@updateSlugForOldRecord');
Route::post('add/js/log',"AppUserController@jsLog")->name("js-logger");
Route::post('incidentLog',"AppUserController@incidentLog")->name("incidentLog");
Route::post('userMediaError',"VideoRoomController@userMediaError")->name("userMediaError");


Route::get('/videocalltest', "VideoTestController@index");
Route::get('join/{roomName}', 'VideoTestController@joinRoom');
Route::post('create', 'VideoTestController@createRoom');
Route::get('daily', function(){
	return view('test.test');
});
