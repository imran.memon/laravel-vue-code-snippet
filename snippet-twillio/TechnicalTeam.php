<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cookie;
use Auth;

class TechnicalTeam  extends Model
{
    protected $table = 'teachnical_team';
    use SoftDeletes;
    protected $softDelete = true;
    protected $guarded = [
        
    ];
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
   	

    
    
    
}
