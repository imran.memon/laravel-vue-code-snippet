<!DOCTYPE html>
<html>
<head>
	<title>Thank You</title>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<!-- <link rel="stylesheet" type="text/css" href="http://68.183.179.16/anydeal/backend/public/css/app.css"> -->
</head>
<body class="d-flex align-items-center justify-content-center p-0 m-0" style="height: 100vh">
	<div class="text-center">
		<div class="mb-4">
			<img class="mb-4" width="150px" src="https://www.theanydealapp.com/wp-content/uploads/2019/11/logo.png">
		</div>
		<i class="fa fa-smile-o mb-4 " style="color: #8dc63d; font-size: 200px;" aria-hidden="true"></i>
		<h1 class="text-white mb-2">Thank you!</h1>
		<h2 class="text-white mb-0">Signature has been submitted successfully.</h2>
	</div>

</body>
</html>