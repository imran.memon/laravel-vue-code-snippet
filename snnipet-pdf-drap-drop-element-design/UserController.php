<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\AffiliateRevenue;
use App\AffiliateSetting;
use App\AffiliatePlan;
use App\Subscriptions;
use App\AffiliatePlanConfig;
use App\ManageCommision;
use App\Visitor;
use App\Survey;
use App\SkipEngine;
use Response;
use Validator;
use Redirect;
use Auth;
use File;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $apiUrl;
    protected $wpRenewUrl;
    protected $client;

    public function __construct(){
        $this->apiUrl     = config('constant.wp_url').'/api_anydeal.php';
        $this->wpRenewUrl = 'https://www.theanydealapp.com/renew.php';
        // $this->wpRenewUrl = config('constant.wp_url').'/renew.php';
        $this->client     = new \GuzzleHttp\Client();
    }

    // 
    public function manageAffiliate(Request $request){
        $totPay    = $totCom = 0;
        $filDate   = date('m/01/Y').' - '.date('m/t/Y');
        $filPlanId = $filSDate  = $filEDate = null;
        $planes    = AffiliatePlan::pluck('plan_name','wp_id')->prepend('All','all')->toArray();

        if($request->isMethod('post')){
            $filPlanId = ($request->input('plan') == 'all')?null:$request->input('plan');
            if(!empty($request->input('date'))){
                $filDate  = $request->input('date');
            }
        }

        $temp     = explode("-",$filDate);
        $filSDate = date('Y-m-d',strtotime($temp[0]));
        $filEDate = $tempFilEDate = date('Y-m-d',strtotime($temp[1]));

        // Fetch data from wordpress side
        if($filEDate > config('constant.noDataAffDate')){
           $tempFilEDate = config('constant.noDataAffDate'); 
        }

        if($filSDate <= config('constant.noDataAffDate')){
            $wpresults = $this->manageAffWPData($filSDate,$tempFilEDate);
        }else{
            $wpresults['wp_net_totpay']  = 0;
            $wpresults['wp_net_totcomm'] = 0;
            $wpresults['wp_result']      = array();
        }

        // Fetch data from laravel side
        $data   = User::where('role_id','!=',config('constant.adminRoleId'))
                ->orderBy('id','desc')
                ->get();

        foreach($data as $index => $value){
            $uids = User::getRefUserDateWise($value->affiliate_id,$filSDate,$filEDate);

            $amount1 = AffiliateRevenue::leftjoin('subscriptions','affiliate_revenue.subscription_id','=','subscriptions.id')
                ->where(function($query) use ($filSDate,$filEDate){
                    $query->whereDate('subscriptions.trial_ends_at','>=',$filSDate);
                    $query->whereDate('subscriptions.trial_ends_at','<=',$filEDate);
                    $query->where('subscriptions.trial_ends_at','<',date('Y-m-d H:i:s'));
                })
                ->whereIn('subscriptions.subscription_status',['active','completed'])
                ->where('affiliate_revenue.referal_id',$value->affiliate_id)
                ->sum('bonus_amount');
            $value->comission_earning = $amount1;
            $totCom +=$amount1;

            $count = Visitor::where('referral_user',$value->wp_user_id)
                    ->whereDate('created_at','>=',$filSDate)
                    ->whereDate('created_at','<=',$filEDate)
                    ->count();
            $value->no_of_visitor = $count;
 
            $subscriptions = Subscriptions::where('user_id',$value->id)
                            ->where('subscription_status','active')
                            ->orderBy('id','desc')
                            ->first();
            $value->sub_status = ($subscriptions)?'Active':'Not Active';
            $planId            = ($subscriptions)?$subscriptions->plan_id:null;
            $affiliatePlan     = AffiliatePlan::where('wp_id',$planId)->first();
            $value->plan       = ($affiliatePlan)?$affiliatePlan->plan_name:'-';

            // $amount = Subscriptions::whereIn('user_id',$uids)->sum('price');
            $amount = Subscriptions::whereIn('user_id',$uids)
                    ->whereIn('subscription_status',['active','completed'])
                    ->whereDate('trial_ends_at','>=',$filSDate)
                    ->whereDate('trial_ends_at','<=',$filEDate)
                    ->where('trial_ends_at','<',date('Y-m-d H:i:s'))
                    ->sum('price');
            $value->revenue_amount   = $amount;
            $totPay+=$amount;

            // 
            if(!is_null($filPlanId) && $filPlanId != $planId){
                unset($data[$index]);
            }

            // $value->converted_users = User::where('referal_id',$value->affiliate_id)
            // ->whereDate('created_at','>=',$filSDate)
            // ->whereDate('created_at','<=',$filEDate)
            // ->count();
            $value->converted_users = Subscriptions::whereIn('user_id',$uids)
            ->whereIn('subscriptions.subscription_status',['active','completed'])
            ->whereDate('created_at','>=',$filSDate)
            ->whereDate('created_at','<=',$filEDate)
            ->count();

            //
            if(\Request::route()->getName() == 'manageAffiliate' && array_key_exists($value->wp_user_id,$wpresults['wp_result'])){
             $value->converted_users+= $wpresults['wp_result'][$value->wp_user_id]['referred_user'];
             $value->revenue_amount+= $wpresults['wp_result'][$value->wp_user_id]['total_pay'];
             $value->comission_earning+= $wpresults['wp_result'][$value->wp_user_id]['total_comm'];
            }
        }        

        if(\Request::route()->getName() == 'manageAffiliateComm'){
            return view('admin.manageAffComm.index',compact('data','filDate','filSDate','filEDate','totPay','totCom','wpresults'));
        }else{
            return view('admin.manageAffiliate.index',compact('data','planes','filPlanId','filDate','filSDate','filEDate'));
        }
    }

    public function manageAffWPData($filSDate,$filEDate){
        $users  = User::where('role_id','!=',config('constant.adminRoleId'))->get();
        $affRef = array();

        foreach($users as $userinfo){
            $refUsers = User::where('referal_id',$userinfo->affiliate_id)
                    ->pluck('wp_user_id')
                    ->toArray();

            $affRef[$userinfo->wp_user_id] = $refUsers;
        }

        $input = array(
            "api_name"       => "get_all_renew_referal",
            "duration_start" => $filSDate,
            "duration_end"   => $filEDate,
            "users"          => $affRef
        );

        $response = $this->client->request('POST', $this->wpRenewUrl, [
            'body' => json_encode($input)
        ]);

        $statusCode = $response->getStatusCode();
        $result     = json_decode($response->getBody());
        $data       = array();
        $netTot1    = $netTot2 = 0;

        $data['wp_net_totpay']  = $netTot1;
        $data['wp_net_totcomm'] = $netTot2;
        $data['wp_result']      = array();

        if($result->Status == 'Success'){
            $settings         = AffiliateSetting::first();
            $allowRefToGetBon = ($settings && $settings->allow_referral_when_renew)?true:false;

            foreach($result->data as $key => $value){
                $user  = User::where('wp_user_id',$key)->first();
                $temp1 = $temp2   = 0;

                $data['wp_result'][$key]['affiliate_id']   = $user->affiliate_id;
                $data['wp_result'][$key]['affiliate_user'] = $user->name;
                $data['wp_result'][$key]['referred_user']  = count($value);

                foreach($value as $planDetail){
                    $temp1+=$planDetail->amount;
                    $netTot1+=$planDetail->amount;

                    $bonusAmount = 0;
                    $planConfig  = AffiliatePlanConfig::getPlanConfigInfo($planDetail->plan_id);
                    if($allowRefToGetBon && $user->aff_status && $planConfig && $planConfig->affiliate_recurring_referral_disable == 1){

                        $manageCommision = ManageCommision::where('user_id',$user->id)
                            ->where('plan_id',$planDetail->plan_id)
                            ->whereNotNull('referral_amount')
                            ->first();

                        if($manageCommision){
                            $rate = $manageCommision->referral_amount;

                            if($manageCommision->referral_type == 0){
                                $bonusAmount = (($planDetail->amount * $rate) / 100);
                            }else{
                                $bonusAmount = $rate;
                            }
                        }else{
                            $rate = (is_null($planConfig->affiliate_recurring_referral_rate))?0:$planConfig->affiliate_recurring_referral_rate;

                            if($rate > 0){
                                if($planConfig->affiliate_recurring_referral_type == 'percentage'){
                                    $bonusAmount = (($planDetail->amount * $rate) / 100);
                                }else{
                                    $bonusAmount = $rate;
                                }
                            }
                        }
                    }

                    $temp2+=$bonusAmount;
                    $netTot2+=$bonusAmount;                    
                }

                $data['wp_result'][$key]['total_pay']  = $temp1;
                $data['wp_result'][$key]['total_comm'] = $temp2;
            }

            $data['wp_net_totpay']  = $netTot1;
            $data['wp_net_totcomm'] = $netTot2;
        }

        return $data;
    }

    public function changeAffStatus(Request $request){
        $id     = $request->input('id');
        $status = $request->input('status');

        $obj = User::find($id);
        if($obj){
            $obj->aff_status = $status;
            $obj->save();
        }

        return Response::json(array(
            'status'  => 'success',
            'message' => 'Status Changed Successfully!'
        ));
    }

    public function deleteAffiliate(Request $request){
        $affiliateId     = $request->input('affiliateId');
        User::where('affiliate_id',$affiliateId)->delete();

        return Response::json(array(
            'status'  => 'success',
            'message' => 'Affiliate User Deleted Successfully!'
        ));
    }

    public function convertedUsers(Request $request){
        $results     = $wpresults = array();
        $affiliateId = $request->input('affiliateId');
        
        $temp     = explode("-",$request->input('date'));
        $filSDate = date('Y-m-d',strtotime($temp[0]));
        $filEDate = $tempFilEDate = date('Y-m-d',strtotime($temp[1]));
        $user     = User::where('affiliate_id',$affiliateId)->first();

        // Laravel
        $uids         = User::getRefUserDateWise($affiliateId,$filSDate,$filEDate);
        $referalUsers = Subscriptions::leftjoin('users','subscriptions.user_id','users.id')
        ->whereIn('users.id',$uids)
        ->whereIn('subscriptions.subscription_status',['active','completed'])
        ->where(function($query) use ($filSDate,$filEDate){
            $query->whereDate('subscriptions.created_at','>=',$filSDate);
            $query->whereDate('subscriptions.created_at','<=',$filEDate);
        })
        ->get(['users.*','subscriptions.trial_ends_at']);

        // Wordpress
        if($filEDate > config('constant.noDataAffDate')){
           $tempFilEDate = config('constant.noDataAffDate'); 
        }

        if($filSDate <= config('constant.noDataAffDate')){
            $refUsers = User::where('referal_id',$affiliateId)->pluck('wp_user_id')->toArray();
            $affRef[$user->wp_user_id] = $refUsers;

            $input = array(
                "api_name"       => "get_all_renew_referal",
                "duration_start" => $filSDate,
                "duration_end"   => $tempFilEDate,
                "users"          => $affRef
            );

            $response = $this->client->request('POST', $this->wpRenewUrl, [
                'body' => json_encode($input)
            ]);

            $statusCode = $response->getStatusCode();
            $result     = json_decode($response->getBody());

            if($result->Status == 'Success'){
                foreach($result->data as $key => $value){
                    foreach($value as $index => $planDetail){
                        $ruser = User::where('wp_user_id',$planDetail->user_id)->first();
                        $wpresults[$index]['referred_user'] = ($ruser)?$ruser->name:'';
                        $wpresults[$index]['email']         = ($ruser)?$ruser->email:'';

                        // $subscriptions = Subscriptions::where('user_id',$ruser->id)
                        // ->whereIn('subscriptions.subscription_status',['active','completed'])
                        // // ->where(function($query) use ($filSDate,$tempFilEDate){
                        // //     $query->whereDate('created_at','>=',$filSDate);
                        // //     $query->whereDate('created_at','<=',$tempFilEDate);
                        // // })
                        // ->orderBy('id','desc')
                        // ->first();

                        // $wpresults[$index]['trial_ends_at'] = ($subscriptions)?$subscriptions->trial_ends_at:null;
                    }
                }
            }
        }
        
        $data = view('admin.manageAffiliate.list')
                ->with('user',$user)
                ->with('referalUsers',$referalUsers)
                ->with('wpresults',$wpresults)
                ->render();

        return Response::json(array(
            'result' => $data
        ));
    }

    // 
    public function manageReferral(Request $request,$isDashboardReq = false){
        $results   = array();
        $filPlanId = $filDate = $filSDate = $filEDate = null;

        $planes = AffiliatePlan::pluck('plan_name','wp_id')->prepend('All','all')->toArray();
        $users  = User::where('role_id','!=',config('constant.adminRoleId'))
                ->orderBy('id','desc')->get(['id','name','affiliate_id']);
		
        if($request->isMethod('post')){
            $filPlanId = ($request->input('plan') == 'all')?null:$request->input('plan');

            if(!empty($request->input('date'))){
                $filDate  = $request->input('date');

                $temp     = explode("-",$request->input('date'));
                $filSDate = date('Y-m-d',strtotime(str_replace("/","-",$temp[0])));
                $filEDate = date('Y-m-d',strtotime(str_replace("/","-",$temp[1])));
            }
        }
        
    	foreach($users as $index => $user){
            $referalUsers = User::where('referal_id',$user->affiliate_id)
                ->where('is_referral_delete',0)
                ->where(function($query) use ($filSDate,$filEDate){
                    if(!is_null($filSDate) && !is_null($filEDate)){
                        $query->whereDate('created_at','>=',$filSDate);
                        $query->whereDate('created_at','<=',$filEDate);
                    }
                })
                ->get(['id','name','created_at']);
        
            foreach($referalUsers as $referalUser){
                $subscriptions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
                ->leftjoin('affiliate_revenue','subscriptions.id','affiliate_revenue.subscription_id')
                ->where(function($query) use ($filPlanId){
                    if(!is_null($filPlanId)){
                        $query->where('subscriptions.plan_id',$filPlanId);
                    }
                })
                ->where('subscriptions.user_id',$referalUser->id)
                ->where('affiliate_revenue.is_referral_delete',0)
                ->select('subscriptions.id','subscriptions.payment_date','affiliate_plan.plan_name','affiliate_revenue.bonus_amount','subscriptions.trial_ends_at','affiliate_revenue.is_bonus_provide','subscriptions.subscription_status')
                ->get();

                foreach($subscriptions as $key => $subscription){
                    $tempDate = date('Y-m-d',strtotime($referalUser->created_at));

                    $results[$tempDate][$user->name]
                    [$referalUser->name]
                    [$subscription->plan_name]['sub_id'] = $subscription->id;

                    $bonusAmount = '-';
                    if($subscription->subscription_status == 'active' || $subscription->subscription_status == 'completed'){
                        $bonusAmount = ($subscription->trial_ends_at < date('Y-m-d H:i:s'))?$subscription->bonus_amount.' USD':'-';
                    }

                    $results[$tempDate][$user->name]
                    [$referalUser->name]
                    [$subscription->plan_name]['bonus_amount'] = $bonusAmount;

                    $results[$tempDate][$user->name]
                    [$referalUser->name]
                    [$subscription->plan_name]['sub_status'] = $subscription->subscription_status;

                    $results[$tempDate][$user->name]
                    [$referalUser->name]
                    [$subscription->plan_name]['trial_ends_at'] = $subscription->trial_ends_at;

                    $results[$tempDate][$user->name]
                    [$referalUser->name]
                    [$subscription->plan_name]['referral_date'] = $referalUser->created_at;

                    $results[$tempDate][$user->name]
                    [$referalUser->name]
                    [$subscription->plan_name]['payment_date'] = date('d-m-Y H:i',strtotime($subscription->payment_date));

                    $results[$tempDate][$user->name]
                    [$referalUser->name]
                    [$subscription->plan_name]['user_id'] = $referalUser->id;

                    $results[$tempDate][$user->name]
                    [$referalUser->name]
                    [$subscription->plan_name]['is_bonus_provide'] = $subscription->is_bonus_provide;
                }
            }
    	}

        ksort($results);
        $results = array_reverse($results);
        
        if($isDashboardReq){
            return $results;
        }else{
            return view('admin.manageReferral.index',compact('results','planes','filPlanId','filDate'));
        }
    }

    public function manageReferralCommission(Request $request){
        $subId  = $request->input('subId');
        $amount = $request->input('amount');

        if($amount == ''){
            $amount = 0;
        }

        $obj               = AffiliateRevenue::where('subscription_id',$subId)->first();
        $obj->bonus_amount = $amount;
        $obj->save();

        return Response::json(array(
            'status'  => 'success',
            'message' => 'Referral Updated Successfully!',
            'amount'  => $obj->bonus_amount
        ));
    }

    public function manageReferralDelete(Request $request){
        $subId         = $request->input('subId');
        $subscriptions = Subscriptions::find($subId);

        $affrev = AffiliateRevenue::where('subscription_id',$subId)->first();
        $affrev->is_referral_delete = 1;
        $affrev->save();

        $count = AffiliateRevenue::where('user_id',$subscriptions->user_id)
            ->where('is_referral_delete',0)
            ->count();

        if($count == 0){
            $obj                     = User::find($subscriptions->user_id);
            $obj->is_referral_delete = 1;
            $obj->save();
        }
        
        return Response::json(array(
            'status'  => 'success',
            'message' => 'Referral Deleted Successfully!',
        ));
    }

    //
    public function profile(){
        return view('admin.profile');
    }

    public function saveProfile(Request $request,$userId = null){
        $rules = array(
            'name'         => 'required',
            'email'        => 'required|email',
            'pass'         => 'same:cpass',
            'profileImage' => 'mimes:jpeg,jpg,bmp,png,gif,svg'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            $errors = $validator->messages();     
            return Redirect::back()->withErrors($errors);
        }

        $user = is_null($userId)?Auth::user():User::find($userId);
        $user->name = $request->input('name');

        if(!empty($request->input('pass'))){
            $user->password = \Hash::make($request->input('pass'));
        }

        $path = public_path('user_image');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        if($request->has('profileImage')){
            $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.'.$request->profileImage->extension();
            $request->profileImage->move(public_path('user_image'), $fileName);
            $user->profile_image = $fileName;
        }

        $user->save();

        return Redirect::back()->with('success','Profile Saved Successfully!');
    }

    // 
    public function getPaymentHistory($isDashboardReq = false){
        $histories  = array();
        $users      = User::where('role_id','!=',config('constant.adminRoleId'))
                    ->orderBy('id','desc')
                    ->get();

        foreach($users as $index => $user){
            $result        = array();
            $subscriptions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
                ->where('subscriptions.user_id',$user->id)
                ->select('subscriptions.*','affiliate_plan.plan_name','affiliate_plan.plan_amount','affiliate_plan.plan_type')
                ->get();

            foreach($subscriptions as $key => $subscription){
                $result[$key]['payment_date']       = is_null($subscription->payment_date)?'-':date('d-m-Y H:i',strtotime($subscription->payment_date));
                $result[$key]['sub_id']             = $subscription->id;
                $result[$key]['transaction_id']     = $subscription->transaction_id;
                $result[$key]['transaction_status'] = $subscription->transaction_status;
                $result[$key]['invoice_id']         = $subscription->invoice_id;
                $result[$key]['payment_getway']     = $subscription->payment_getway;
                $result[$key]['payment_type']       = $subscription->payment_type;

                $result[$key]['plan_name']   = $subscription->plan_name;
                $result[$key]['plan_amount'] = $subscription->plan_amount;
                $result[$key]['plan_type']   = $subscription->plan_type;
            }

            $histories[$index]['userId'] = $user->id;
            $histories[$index]['name']   = $user->name;
            $histories[$index]['email']  = $user->email;
            $histories[$index]['info']   = $result; 
        }

        if($isDashboardReq){
            return $histories;
        }else{
            return view('admin.paymentHistory.payment_history',compact('histories'));
        }
    }

    public function viewPaymentHistory($transactionId){
        $subscriptions = Subscriptions::where('transaction_id',$transactionId)->first();
        if(!$subscriptions){
            return Redirect::back()->with('error','No Record Found!');
        }

        $user     = User::find($subscriptions->user_id);
        $planInfo = AffiliatePlan::getPlanInfo($subscriptions->plan_id);
        
        return view('admin.paymentHistory.view_payment_history')->with('subscriptions',$subscriptions)->with('user',$user)->with('planInfo',$planInfo);
    }

    public function deletePaymentHistory($transactionId){
        $subscriptions = Subscriptions::where('transaction_id',$transactionId)->first();
        $subscriptions->delete();
        return Redirect::back()->with('success','Transaction Deleted Successfully!');
    }

    // 
    public function getMemberDetails(){
        $transactions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
                ->where('subscriptions.user_id',Auth::user()->id)
                ->select('subscriptions.*','affiliate_plan.plan_name','affiliate_plan.plan_amount','affiliate_plan.plan_type')
                ->orderBy('subscriptions.id','desc')
                ->get();

        $wpUserId      = Auth::user()->wp_user_id;
        $wp_user_name  = Auth::user()->name;
        $makePayment   = config('constant.wp_url').'/member-plan/?payment=make&wp_id='.$wpUserId;
        $cancelPayment = config('constant.wp_url').'/member-details/?payment=cancle&wp_id='.$wpUserId;
        $upgradePlan   = config('constant.wp_url').'/register/?wp_id='.$wpUserId.'&payment=upgrade&title=renew';
        //$cancelPayment = '/wordpress';

        return view('member.member-details',compact('transactions','makePayment','cancelPayment','upgradePlan'));
    }

    public function cancelPayment(Request $request){
        $planId = $request->input('hidPlanId');
        $survey = $request->input('survey');

        try{
            $user = User::where('wp_user_id',Auth::user()->wp_user_id)->first();
            if(!$user){
                return Redirect::back()->with('error','User Not Found!');
            }

            $oldSub = Subscriptions::where('user_id',$user->id)->get();
            foreach($oldSub as $sub){
                if($sub->subscription_status == 'active'){
                    // $sub->subscription_status = 'completed';
                    $sub->subscription_status = 'Cancel By User';
                    $sub->save();
                }
            }

            $user->status = 0;
            $user->save();

            $sobj          = new Survey();
            $sobj->user_id = $user->id;
            $sobj->type    = 'cancel_payment';
            $sobj->reason  = $survey;
            $sobj->save();

            $input = array(
                "api_name" => "cancle_user_subscription",
                "user_id"  => Auth::user()->wp_user_id, 
                "plan_id"  => $planId 
            );

            $this->client->request('POST', $this->apiUrl, [
                'body' => json_encode($input)
            ]);

            Auth::logout();
            session()->flash('success', "Subscription Canceled Successfully.Sorry for the break up, 
            come back any time!");
            return redirect('/login');
        }catch(\Exception $e) {
            return Redirect::back()->with('error','Oppps! something went wrong, please try again...');
        }
    }
    //

    public function getAffiliateLink(Request $request){
        $facebook  = $twitter = $linkedin = $email = null;
        $filDate   = date('m/01/Y').' - '.date('m/t/Y');
        $filSDate  = $filEDate = null;
        $results   = $wpresults = array();

        if($request->isMethod('post')){
            $filDate  = $request->input('date');
        }

        $temp     = explode("-",$filDate);
        $filSDate = date('Y-m-d',strtotime($temp[0]));
        $filEDate = $tempFilEDate = date('Y-m-d',strtotime($temp[1]));

        // Laravel
        // $users = User::where('referal_id',Auth::user()->affiliate_id)->get();
        $users = User::where('referal_id',Auth::user()->affiliate_id)
                // ->whereDate('created_at','>=',$filSDate)
                // ->whereDate('created_at','<=',$filEDate)
                ->get();

        foreach($users as $index => $user){
            $array         = array();
            // $subscriptions = Subscriptions::where('user_id',$user->id)->get();
            $subscriptions = Subscriptions::where('user_id',$user->id)
            ->whereIn('subscription_status',['active','completed'])
            ->where(function($query) use ($filSDate,$filEDate){
                $query->whereDate('created_at','>=',$filSDate);
                $query->whereDate('created_at','<=',$filEDate);
            })
            ->get();

            foreach($subscriptions as $key => $subscription){
                $planInfo = AffiliatePlan::getPlanInfo($subscription->plan_id);
                
                $array[$key]['trial_ends_at'] = $subscription->trial_ends_at;
                $array[$key]['date']          = $subscription->payment_date;
                // $array[$key]['date']          = $subscription->created_at;
                $array[$key]['plan']          = ($planInfo)?$planInfo->plan_name:null;
                $array[$key]['amount']        = ($planInfo)?$planInfo->plan_amount:null;
            }

            $results[$index]['name']  = $user->name;
            $results[$index]['email'] = $user->email;
            $results[$index]['info']  = $array;
        }

        // Wordpress
        if($filEDate > config('constant.noDataAffDate')){
           $tempFilEDate = config('constant.noDataAffDate'); 
        }

        if($filSDate <= config('constant.noDataAffDate')){
            $refUsers = User::where('referal_id',Auth::user()->affiliate_id)->pluck('wp_user_id')->toArray();
            $affRef[Auth::user()->wp_user_id] = $refUsers;

            $input = array(
                "api_name"       => "get_all_renew_referal",
                "duration_start" => $filSDate,
                "duration_end"   => $tempFilEDate,
                "users"          => $affRef
            );

            $response = $this->client->request('POST', $this->wpRenewUrl, [
                'body' => json_encode($input)
            ]);

            $statusCode = $response->getStatusCode();
            $result     = json_decode($response->getBody());

            if($result->Status == 'Success' && isset($result->data)){
                foreach($result->data as $key => $value){
                    foreach($value as $index => $planDetail){
                        $ruser    = User::where('wp_user_id',$planDetail->user_id)->first();
                        $planInfo = AffiliatePlan::getPlanInfo($planDetail->plan_id);

                        $wpresults[$index]['referred_user'] = ($ruser)?$ruser->name:'';
                        $wpresults[$index]['email']         = ($ruser)?$ruser->email:'';
                        $wpresults[$index]['pname']         = ($planInfo)?$planInfo->plan_name:'';
                        $wpresults[$index]['pamount']       = ($planInfo)?$planInfo->plan_amount:'';
                        $wpresults[$index]['date']          = $planDetail->date;
                    }
                }
            }
        }

        $data             = array();
        $data['api_name'] = 'get_user_refrral_details';
        $data['user_id']  = Auth::user()->wp_user_id;
        $aff_user_id      = Auth::user()->affiliate_id;
        $referal_url      = AffiliateSetting::select('referral_url')->first();
        if(substr($referal_url->referral_url, -1) == '/'){
            $share_url = substr($referal_url->referral_url, 0, -1).'/'.$aff_user_id;
        }else{
            $share_url = $referal_url->referral_url.'/'.$aff_user_id;
        }        

        $postData  = json_encode($data);
        //$affiliate = $this->getCurlRequest($this->apiUrl,$postData);

        if($aff_user_id){
            $facebook = 'https://www.facebook.com/sharer.php?u='.$share_url;
            $twitter  = 'https://twitter.com/share?url='.$share_url;
            $linkedin = 'https://www.linkedin.com/cws/share?url='.$share_url;
            $email    = $share_url;
        }        
        
        return view('member.affiliate-link',compact('results','facebook','twitter','linkedin','email','share_url','filDate','filSDate','filEDate','wpresults'));
    }

    // 
    public function manageMembers(Request $request,$isDashboardReq = false){
        $ftype  = ($request->has('ftype'))?$request->input('ftype'):'stripe';
        $status = ($request->has('status'))?$request->input('status'):'active';
        
        $users = Subscriptions::leftjoin('users','subscriptions.user_id','users.id')
                ->leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id');

        if($isDashboardReq){
            $users = $users->limit(5)->orderBy('users.id','desc')
                ->get(['users.*','subscriptions.subscription_status','affiliate_plan.plan_name',
                'subscriptions.plan_id','subscriptions.payment_getway','subscriptions.id as sid']);

            return $users;
        }else{
            $users = $users->where(function($query) use ($status){
                    if($status != 'active'){
                        $query->where('subscriptions.subscription_status','!=','active');
                    }else{
                        $query->where('subscriptions.subscription_status','active');
                    }
                })
                ->where(function($query) use ($ftype){
                    $query->where('subscriptions.payment_getway',$ftype);
                })
                ->orderBy('users.id','desc')
                ->get(['users.*','subscriptions.subscription_status','affiliate_plan.plan_name',
                'subscriptions.plan_id','subscriptions.payment_getway','subscriptions.id as sid']);

            return view('admin.manageMembers.index',compact('users','ftype','status'));
        }
    }

    // public function manageMembers($isDashboardReq = false){
    //     $users = User::where('role_id','!=',config('constant.adminRoleId'))
    //         ->orderBy('id','desc');

    //     $users = ($isDashboardReq)?$users->limit(5)->get():$users->get();

    //     foreach($users as $user){
    //         $subscriptions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
    //         ->where('subscriptions.user_id',$user->id)
    //         ->orderBy('subscriptions.id','desc')
    //         ->select('subscriptions.subscription_status','affiliate_plan.plan_name',
    //             'subscriptions.plan_id')
    //         ->first();

    //         $user->subscription_status = ($subscriptions)?$subscriptions->subscription_status:null;
    //         $user->plan_name           = ($subscriptions)?$subscriptions->plan_name:null;
    //         $user->plan_id             = ($subscriptions)?$subscriptions->plan_id:null;
    //     }

    //     if($isDashboardReq){
    //         return $users;
    //     }else{
    //         return view('admin.manageMembers.index',compact('users'));
    //     }
    // }

    public function cancelMemSub($uId,$planId){
        try{
            $user = User::find($uId);
            if(!$user){
                return Redirect::back()->with('error','User Not Found!');
            }

            $oldSub = Subscriptions::where('user_id',$user->id)->get();
            foreach($oldSub as $sub){
                if($sub->subscription_status == 'active'){
                    $sub->subscription_status = 'Cancel By Admin';
                    $sub->save();
                }
            }

            $user->status = 0;
            $user->save();

            $input = array(
                "api_name" => "cancle_user_subscription",
                "user_id"  => $user->wp_user_id, 
                "plan_id"  => $planId 
            );

            $this->client->request('POST', $this->apiUrl, [
                'body' => json_encode($input)
            ]);

            return Redirect::back()->with('success', 'Subscription Cancel Successfully.');
        }catch(\Exception $e) {
            return Redirect::back()->with('error','Oppps! something went wrong, please try again...');
        }
    }

    public function viewMember($id){
        $user = User::find($id);

        $subscriptions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
            ->where('subscriptions.user_id',$user->id)
            ->orderBy('subscriptions.id','desc')
            ->select('subscriptions.*','affiliate_plan.plan_name','affiliate_plan.plan_type','affiliate_plan.plan_amount')
            ->get();

        return view('admin.manageMembers.view-member')->with('user',$user)->with('subscriptions',$subscriptions)->with('memberId',$id);
    }

    public function editMember($id){
        $user = User::find($id);

        $subscriptions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
            ->where('subscriptions.user_id',$user->id)
            ->orderBy('subscriptions.id','desc')
            ->select('subscriptions.*','affiliate_plan.plan_name','affiliate_plan.plan_type','affiliate_plan.plan_amount')
            ->get();
            
        return view('admin.manageMembers.edit-member')->with('user',$user)->with('subscriptions',$subscriptions);
    }

    public function affPayment(Request $request,$affId,$filSDate = null,$filEDate = null){
        $results = array();
        $user    = User::where('affiliate_id',$affId)->first();
        $totCom  = $totAmt = 0;

        if(is_null($filSDate) && is_null($filEDate)){
            $filDate = date('m/01/Y').' - '.date('m/t/Y');

            if($request->isMethod('post')){
                if(!empty($request->input('date'))){
                    $filDate  = $request->input('date');
                }
            }
            
            $temp     = explode("-",$filDate);
            $filSDate = date('Y-m-d',strtotime($temp[0]));
            $filEDate = date('Y-m-d',strtotime($temp[1]));
        }else{
            $filDate = date('m/d/Y',strtotime($filSDate)).' - '.date('m/d/Y',strtotime($filEDate));
        }

        $tempFilEDate = $filEDate;
        // 
        // if($user){
        //     $planes = AffiliatePlan::pluck('plan_name','wp_id')->prepend('All','all')->toArray();
        //     $referalUsers = User::where('referal_id',$user->affiliate_id)
        //                 ->get(['id','name','created_at']);
    
        //     foreach($referalUsers as $referalUser){
        //         $subscriptions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
        //         ->leftjoin('affiliate_revenue','subscriptions.id','affiliate_revenue.subscription_id')
        //         ->where(function($query) use ($filSDate,$filEDate){
        //             // $query->orWhere('subscriptions.trial_ends_at','<',date('Y-m-d H:i:s'));
        //             $query->whereDate('subscriptions.trial_ends_at','>=',$filSDate);
        //             $query->whereDate('subscriptions.trial_ends_at','<=',$filEDate);
        //             // $query->orWhereNull('subscriptions.trial_ends_at');
        //         })
        //         ->whereIn('subscriptions.subscription_status',['active','completed'])
        //         ->where('subscriptions.user_id',$referalUser->id)
        //         ->select('subscriptions.id','subscriptions.payment_date','affiliate_plan.plan_name','affiliate_revenue.bonus_amount','subscriptions.trial_ends_at','affiliate_revenue.is_bonus_provide','subscriptions.subscription_status')
        //         ->get();

        //         foreach($subscriptions as $key => $subscription){
        //             $tempDate = date('Y-m-d',strtotime($referalUser->created_at));

        //             $results[$tempDate][$user->name]
        //             [$referalUser->name]
        //             [$subscription->plan_name]['sub_id'] = $subscription->id;

        //             $results[$tempDate][$user->name]
        //             [$referalUser->name]
        //             [$subscription->plan_name]['bonus_amount'] = $subscription->bonus_amount;

        //             $totCom+=$subscription->bonus_amount;

        //             $results[$tempDate][$user->name]
        //             [$referalUser->name]
        //             [$subscription->plan_name]['trial_ends_at'] = $subscription->trial_ends_at;

        //             $results[$tempDate][$user->name]
        //             [$referalUser->name]
        //             [$subscription->plan_name]['referral_date'] = $referalUser->created_at;

        //             $results[$tempDate][$user->name]
        //             [$referalUser->name]
        //             [$subscription->plan_name]['user_id'] = $referalUser->id;

        //             $results[$tempDate][$user->name]
        //             [$referalUser->name]
        //             [$subscription->plan_name]['is_bonus_provide'] = $subscription->is_bonus_provide;

        //             $results[$tempDate][$user->name]
        //             [$referalUser->name]
        //             [$subscription->plan_name]['subscription_status'] = $subscription->subscription_status;
                    
        //         }
        //     }
            
        //     ksort($results);
        //     $results = array_reverse($results);
        // }
        // 
                   
        $subscriptions = User::leftjoin('subscriptions','users.id','subscriptions.user_id')
            ->leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
            ->leftjoin('affiliate_revenue','subscriptions.id','affiliate_revenue.subscription_id')
            ->where(function($query) use ($filSDate,$filEDate){
                $query->whereDate('subscriptions.trial_ends_at','>=',$filSDate);
                $query->whereDate('subscriptions.trial_ends_at','<=',$filEDate);
                $query->where('subscriptions.trial_ends_at','<',date('Y-m-d H:i:s'));
            })
            ->where('users.referal_id',$user->affiliate_id)
            // Below condition we have add
            ->where('affiliate_revenue.referal_id',$user->affiliate_id)
            // 
            ->whereIn('subscriptions.subscription_status',['active','completed'])
            ->select('subscriptions.id','subscriptions.payment_date','affiliate_plan.plan_name','affiliate_revenue.bonus_amount','subscriptions.trial_ends_at','affiliate_revenue.is_bonus_provide','subscriptions.subscription_status','users.id as uid','users.name','users.created_at','affiliate_plan.plan_amount')
            ->get();

        foreach($subscriptions as $key => $subscription){
            $tempDate = date('Y-m-d',strtotime($subscription->trial_ends_at));

            $results[$tempDate][$user->name]
            [$subscription->name]
            [$subscription->plan_name]['sub_id'] = $subscription->id;

            $results[$tempDate][$user->name]
            [$subscription->name]
            [$subscription->plan_name]['plan_amount'] = $subscription->plan_amount;

            $totAmt+=$subscription->plan_amount;

            $results[$tempDate][$user->name]
            [$subscription->name]
            [$subscription->plan_name]['bonus_amount'] = $subscription->bonus_amount;

            $totCom+=$subscription->bonus_amount;

            $results[$tempDate][$user->name]
            [$subscription->name]
            [$subscription->plan_name]['trial_ends_at'] = $subscription->trial_ends_at;

            $results[$tempDate][$user->name]
            [$subscription->name]
            [$subscription->plan_name]['referral_date'] = $subscription->created_at;

            $results[$tempDate][$user->name]
            [$subscription->name]
            [$subscription->plan_name]['user_id'] = $subscription->uid;

            $results[$tempDate][$user->name]
            [$subscription->name]
            [$subscription->plan_name]['is_bonus_provide'] = $subscription->is_bonus_provide;
        }
        
        ksort($results);
        $results = array_reverse($results);

        // Fetch Data From Wordpress Side
        if($filEDate > config('constant.noDataAffDate')){
           $tempFilEDate = config('constant.noDataAffDate'); 
        }

        if($filSDate <= config('constant.noDataAffDate')){
            $wpresults = $this->affPayHisWPData($affId,$user,$filSDate,$tempFilEDate);
        }else{
            $wpresults['wp_totamt']  = 0;
            $wpresults['wp_totcomm'] = 0;
            $wpresults['wp_result']  = array();
        }        

        return view('admin.manageAffiliate.history',compact('results','affId','filDate','filSDate','filEDate','totCom','totAmt','wpresults'));
    }

    public function provideComm(Request $request){
        try{
            $affId = $request->input('affId');
            $subId = $request->input('subId');
            $uId   = $request->input('uId');
            $state = $request->input('state');
            $name  = $request->input('name');

            if($name == 'comm_chk'){
                // Laravel
                $revenue = AffiliateRevenue::where('user_id',$uId)->where('referal_id',$affId)
                        ->where('subscription_id',$subId)->first();

                if($revenue){
                    $revenue->is_bonus_provide = ($state == 'true')?1:0;
                    $revenue->save();
                }

                return Response::json(array(
                    'status'  => 'success',
                    'message' => 'Saved Successfully!'
                ));
            }else{
                // Wordpress
                $input = array(
                    "api_name"          => "update_bonus",
                    "user_id"           => $uId,
                    "arm_log_id"        => $affId,
                    "is_bonus_provided" => ($state == 'true')?1:0
                );

                $response = $this->client->request('POST', $this->wpRenewUrl,[
                    'body' => json_encode($input)
                ]);

                $statusCode = $response->getStatusCode();
                $result     = json_decode($response->getBody());

                if($result->Status == 'Success'){
                    return Response::json(array(
                        'status'  => 'success',
                        'message' => 'Saved Successfully!'
                    ));
                }else{
                    return Response::json(array(
                        'status'  => 'error',
                        'message' => 'Oppps! something went wrong, please try again...'
                    ));
                }
            }
        }catch(\Exception $e) {
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Oppps! something went wrong, please try again...'
            ));
        }
    }

    public function referralCode(Request $request){
        try{
            $userId = $request->input('userId');
            $value  = $request->input('value');

            $isExists = User::where('id','!=',$userId)
                        ->where('referral_code',strtoupper($value))
                        ->exists();
            if($isExists){
                return Response::json(array(
                    'status'  => 'info',
                    'message' => 'Referral Code Already Exists,Please try different!'
                ));
            }

            $user = User::find($userId);
            if($user){
                $user->referral_code = !is_null($value)?strtoupper($value):null;
                $user->save();
            }

            return Response::json(array(
                'status'  => 'success',
                'message' => 'Saved Successfully!'
            ));
        }catch(\Exception $e){
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Oppps! something went wrong, please try again...'
            ));
        }
    }

    public function moveToFree(Request $request){
        try{
            $subId = $request->input('subId');
            $wpUid = $request->input('wp_uid');

            $input = array(
                "api_name" => "convert_to_free",
                "user_id"  => $wpUid
            );

            $response = $this->client->request('POST', $this->apiUrl, [
                'body' => json_encode($input)
            ]);

            $statusCode = $response->getStatusCode();
            $result     = json_decode($response->getBody());

            if($result->Status == 'Success'){
                sleep(5);

                $subscription = Subscriptions::find($subId);
                if($subscription){
                    // $subscription->subscription_status = 'Cancel';
                    $subscription->subscription_status = 'completed';
                    $subscription->save();
                }

                $obj                      = new Subscriptions();
                $obj->user_id             = $subscription->user_id;
                $obj->plan_id             = $result->planId;
                $obj->stripe_id           = '-';
                $obj->subscription_status = 'active';
                $obj->quantity            = 1;
                $obj->trial_ends_at       = date('Y-m-d H:m:s');
                $obj->ends_at             = 'Never Expires';
                $obj->price               = 0.00;
                $obj->currency            = 'USD';
                $obj->starts_on           = date('Y-m-d');
                $obj->expires_on          = 'Never Expires';
                $obj->cycle_date          = date('Y-m-d').' ( Auto Debit )';
                $obj->transaction_id      = '-';
                $obj->transaction_status  = 'success';
                $obj->invoice_id          = null;
                $obj->payment_getway      = 'manual';
                $obj->payment_type        = 'subscription';
                $obj->payment_date        = date('Y-m-d H:m:s');
                $obj->coupon_code         = null;
                $obj->coupon_discount     = null;
                $obj->coupon_amount       = null;
                $obj->purchase_cent       = $subscription->purchase_cent;
                $obj->used_cent           = $subscription->used_cent;
                $obj->forwarded_cent      = $subscription->forwarded_cent;
                $obj->save();

                // 
                $obj->subscription_status = 'active';
                $obj->save();

                $user = User::find($subscription->user_id);
                $user->status = 1;
                $user->save();
                // 
                
                return Response::json(array(
                    'status'  => 'success',
                    'message' => 'User Moved Successfully!'
                ));
            }else{
                return Response::json(array(
                    'status'  => 'error',
                    'message' => 'Oppps! something went wrong, please try again...'
                ));
            }
        }catch(\Exception $e){
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Oppps! something went wrong, please try again...'
            ));
        }
    }

    public function affPayHisWPData($affId,$user,$startDate,$endDate){
        $refUsers = User::where('referal_id',$affId)
                    ->pluck('wp_user_id')
                    ->toArray();

        $input = array(
            "api_name"       => "get_renew_referal",
            "user_id"        => $user->wp_user_id,
            "duration_start" => $startDate,
            "duration_end"   => $endDate,
            "inner_user"     => $refUsers
        );

        $response = $this->client->request('POST', $this->wpRenewUrl, [
            'body' => json_encode($input)
        ]);

        $statusCode = $response->getStatusCode();
        $result     = json_decode($response->getBody());
        $wpTotAmt   = $wpTotCom = 0;

        $data['wp_totamt']  = $wpTotAmt;
        $data['wp_totcomm'] = $wpTotCom;
        $data['wp_result']  = array();

        if($result->Status == 'Success'){
            $settings         = AffiliateSetting::first();
            $allowRefToGetBon = ($settings && $settings->allow_referral_when_renew)?true:false;

            foreach($result->data as $value){
                $bonusAmount           = 0;
                $value->affiliate_user = $user->name;
                $value->commission     = $bonusAmount;
                $wpTotAmt+=$value->amount;

                $planConfig = AffiliatePlanConfig::getPlanConfigInfo($value->plan_id);
                if($allowRefToGetBon && $user->aff_status && $planConfig && $planConfig->affiliate_recurring_referral_disable == 1){

                    $manageCommision = ManageCommision::where('user_id',$user->id)
                            ->where('plan_id',$value->plan_id)
                            ->whereNotNull('referral_amount')
                            ->first();

                    if($manageCommision){
                        $rate = $manageCommision->referral_amount;

                        if($manageCommision->referral_type == 0){
                            $bonusAmount = (($value->amount * $rate) / 100);
                        }else{
                            $bonusAmount = $rate;
                        }
                    }else{
                        $rate = (is_null($planConfig->affiliate_recurring_referral_rate))?0:$planConfig->affiliate_recurring_referral_rate;

                        if($rate > 0){
                            if($planConfig->affiliate_recurring_referral_type == 'percentage'){
                                $bonusAmount = (($value->amount * $rate) / 100);
                            }else{
                                $bonusAmount = $rate;
                            }
                        }
                    }

                    $value->commission = $bonusAmount;
                    $wpTotCom+=$bonusAmount;
                }
            }

            $data['wp_totamt']  = $wpTotAmt;
            $data['wp_totcomm'] = $wpTotCom;
            $data['wp_result']  = $result->data;
        }

        return $data;
    }

    public function viewSkipTraceInfo($id = null){
        if(is_null($id)){
            $result   = SkipEngine::orderBy('id','desc')->get();
            return view('admin.skipTrace.index',compact('result'));
        }else{
            $result   = SkipEngine::find($id)->first();
            return view('admin.skipTrace.view',compact('result'));
        }
    }
}
