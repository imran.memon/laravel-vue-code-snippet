<script type="text/javascript" src="{{ asset('js/jquery.min.js?'. time()) }}"></script>
<script type="text/javascript" src="{{ asset('js/popper.min.js?'. time()) }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{asset('js/jquery.validate.min.js?'.time())}}"></script>
<script src="{{asset('js/additional-methods.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/jquery.signature.js?'.time())}}"></script>
<script src="https://cdn.jsdelivr.net/npm/pdfjs-dist@2.0.385/build/pdf.min.js"></script>
<script type="text/javascript" src="{{asset('js/konva.min.js?')}}"></script>
<script type="text/javascript" src="{{asset('js/jspdf.min.js?')}}"></script>
<!-- <script type="text/javascript" src="{{asset('js/html2canvas.js?'.time())}}"></script> -->
<script type="text/javascript" src="{{asset('js/html2canvas_alpha.js?'.time())}}"></script>
<script src="{{asset('js/sweetalert2.min.js')}}"></script>

<script type="text/javascript">
    function getsiteurl(){
      var BASE_URL = {!! json_encode(url('/')) !!}
      return BASE_URL;
    }
	var asset_url = "{{asset('/')}}";
    var upload_url = "{{route('saveAudio')}}";
    var upload_directory = "{{asset('audio_uploads/')}}";
</script>

<script type="text/javascript" src="{{asset('js/jquery.ui.touch-punch.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
<script type="text/javascript" src="{{asset('member_js/prepare.js?'.time())}}"></script>

<!-- <script src="https://www.webrtc-experiment.com/RecordRTC.js"></script>
<script src="https://www.webrtc-experiment.com/gif-recorder.js"></script>
<script src="https://www.webrtc-experiment.com/getScreenId.js"></script>
<script src="https://www.webrtc-experiment.com/DetectRTC.js"> </script> -->

<!-- for Edige/FF/Chrome/Opera/etc. getUserMedia support -->
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<!-- <script type="text/javascript" src="{{asset('member_js/audio.js?'.time())}}"></script> -->
<script src="https://cdn.rawgit.com/mattdiamond/Recorderjs/08e7abd9/dist/recorder.js"></script>
<script type="text/javascript" src="{{asset('member_js/app.js?'.time())}}"></script>


<input type="hidden" name="displaystatus" id="displaystatus" value="{{isset($displaystatus)? $displaystatus:''}}">
<div class="modal" tabindex="-1" role="dialog" id="documentcount" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <?php $urlRedirect = request()->getHost()."/anydeal/"; ?>
                <div class="text-center" style="font-size:16px">Your have created maximum contracts for this month. please upgrade your plan from below link <br/> <a href="{{url('../../')}}" style="color:red"><u>Upgrade Plan</u></a> </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>