<html>

<head>
    <title>Electronic Record and Signature Disclosure</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta name="robots" content="noindex, noarchive" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <script language="javascript" src="script/Popup.js?vers=20.4.0.16840" type="text/javascript"></script>
    <style media="print">
    #btnPrint,
    #btnClose {
        display: none
    }
    </style>
</head>

<body class="WindowBackground" onload="initPage()">
    <h1><span style="color: #99cc00;"><strong>E-Sign Disclosure and Consent for AnyDeal &ndash; Real
                Estate</strong></span></h1>
    <p>This is an official e-sign disclosure and consent agreement for AnyDeal &ndash; Real Estate. It refers to all the
        communications that will be made to you electronically including your use of services under your registered
        account whether you use the services on our website or mobile app.</p>
    <p>The terms used as &ldquo;us&rdquo;, &ldquo;we&rdquo;, &ldquo;our&rdquo; are referred to AnyDeal &ndash; Real
        Estate whereas the terms referred as &ldquo;you&rdquo; and &ldquo;your&rdquo; is directed towards the user
        entitled or has a registered account.</p>
    <p>The formal terms used in this disclosure such as &ldquo;Account&rdquo; refers to your registered account that you
        have with us on the website. The term &ldquo;Communications&rdquo; refer to all those correspondence that may
        occur on our website or mobile app let it be emails, notices, agreements, contracts, acknowledgments, terms and
        conditions, privacy policy or any other communication or information that we are obliged to provide you in
        writing by the US laws.</p>
    <h2><span style="color: #99cc00;"><strong>Information that needs to be provided you in electronic
                form</strong></span></h2>
    <p>When you plan to use any service through the platform of AnyDeal &ndash; Real Estate let it be website or its
        mobile app, you are required to give your intended consent that allow us to send and maintain electronic
        communications with you. Furthermore, it is important for you to agree on the below-mention points in order to
        use e-sign or any other associated services at AnyDeal &ndash; Real Estate:</p>
    <ul>
        <li>You allow us to send you official emails related to any legal notices or updates related to your registered
            account with us</li>
        <li>You allow us to send you updates on the terms of use of your account if we apply any changes</li>
        <li>You allows us to notify you whenever we update our legal policies</li>
    </ul>
    <h2><span style="color: #99cc00;"><strong>Forms in which we will send electronic communications</strong></span></h2>
    <p>We may contact or send electronic communications through different methods including but not limited to:</p>
    <ul>
        <li>Using our official email only <strong><em><a href="mailto:{{$user->email}}">{{$user->email}}</a></em></strong></li>
        <li>Through a separate webpage link that we may send you enclosed in one of our emails</li>
        <li>By sending you a secured pdf file enclosed in an email</li>
        <li>By sending you a downloadable pdf link</li>
    </ul>
    <h2><span style="color: #99cc00;"><strong>Requesting paper copies from AnyDeal &ndash; Real Estate</strong></span>
    </h2>
    <p>Please note that we will not be sending paper copies for any of the electronic communications that are accessible
        through online platforms until or unless requested to us through a formal email being sent to us using your
        email address that&rsquo;s registered with us. Or that we may find it necessary to send you a paper copy based
        on our records.</p>
    <p>You can receive a paper copy either by printing it through your email where you initially received the electronic
        records or you can mail us on our official email address to receive a paper copy. However, you should
        understand, that we may require a reasonable amount of time to send any paper copies based on the time you first
        received the electronic copy of that particular document or so.</p>
    <h2><span style="color: #99cc00;"><strong>Update your information at AnyDeal &ndash; Real Estate</strong></span>
    </h2>
    <p>It is of utmost importance that you should provide us with accurate, true and unforged information about your
        legal name and contact information that you may use for electronic communications or at your registered account
        at AnyDeal &ndash; Real Estate.</p>
    <p>If at any time, you plan to change your email address at AnyDeal &ndash; Real Estate then you will be required to
        send us an email stating your full name with which you have your account with us along with the new contact
        information let it be email or contact number that you may want us to update in our records.</p>
    <p>To avoid any delays in the information updates, please send us an email on our official email address
        <strong><em><a href="mailto:{{$user->email}}">{{$user->email}}</a></em></strong></p>
    <h2><span style="color: #99cc00;"><strong>How you can withdraw your consent at AnyDeal &ndash; Real
                Estate</strong></span></h2>
    <p>You may withdraw your consent to receive disclosure, notices, updates or any other communications in electronic
        form by emailing us at <em><a href="mailto:{{$user->email}}">{{$user->email}}</a>. </em>Please note that to withdraw your consent you
        will be required to send us an email stating that you no longer wishes to use any of the services at AnyDeal
        &ndash; Real Estate and that you are&nbsp; withdrawing at your own will, without any pressure.</p>
    <p>Furthermore, the withdrawal of consent may only be effective once we receive an official email and have
        reasonable amount of time to process your request.</p>
    <h2><span style="color: #99cc00;"><strong>Operating system and software requirements</strong></span></h2>
    <p>In order to use our services without any lags, delays or disruptions you may have the following:</p>
    <ul>
        <li>An operating system, preferably a laptop or a mobile phone with latest iOS or Android versions</li>
        <li>A stable internet connection</li>
        <li>An email account that you will use to register an account at AnyDeal &ndash; Real Estate website or mobile
            app</li>
        <li>An internet browser preferably Chrome, Safari, Firefox or Opera (latest version)</li>
    </ul>
    <h2><span style="color: #99cc00;"><strong>Written Communication </strong></span></h2>
    <p>Please note that all the communications let it be electronic or in paper format will be done in
        &ldquo;writing&rdquo; only to keep the records intact for both users and us. We recommend you to download or
        print a copy of this document for your records.</p>
    <h2><span style="color: #99cc00;"><strong>Federal Law </strong></span></h2>
    <p>You acknowledge and agree that your consent to electronic communications is being provided in connection to the
        Federal Electronic Signatures in Global and National Commerce Act (ESIGN ACT) and Uniform Electronic
        Transactions Act (UETA).</p>
    <p>Please note that as per these legal acts all electronic signatures hold a valid position and are considered by
        law, and they cannot be disregarded just because they are electronic signatures.</p>
    <h2><span style="color: #99cc00;"><strong>Changes or Terminations</strong></span></h2>
    <p>AnyDeal &ndash; Real Estate reserves the right to change, update, or terminate any of the legalities or processes
        in electronic communications or in our terms and conditions, privacy policy or any other legal policies,
        content, or information. However, we will always send you an official email stating about the changes that we
        may plan to do in future.</p>
    <h2><span style="color: #99cc00;"><strong>Acknowledgement and Consent for Receiving Electronic
                Communications</strong></span></h2>
    <p>You agree that we may hold your information for record retention that may be used in future by both parties; you
        and us.</p>
    <p>You agree that all the deals made through AnyDeal &ndash; Real Estate using e-signatures are done with agreement
        and consent of both parties, you and other parties.</p>
    <p>You are required to select, &ldquo;I have read and understood all the details given above and hereby agree with
        all of the above terms and conditions&rdquo;.</p>
    <p>I agree to the above-mentioned terms and show my intended consent to continuously receive electronic
        communications from AnyDeal &ndash; Real Estate in form of notices, disclosures, contracts, policy updates, and
        acknowledgements or any other important information during the course of my relationship with AnyDeal &ndash;
        Real Estate, until or unless stated otherwise.</p>
    <script type="text/javascript">
    function initPage() {
        if (!window.print) {
            var button = document.getElementById("btnPrint");
            button.style.display = 'none';
        }
    }
    </script>
</body>

</html>