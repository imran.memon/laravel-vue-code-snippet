$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   
	$('#timeline').hide();
    jQuery.validator.addMethod("valid_email", function (value, element) {
        return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
    }, 'Please Enter Valid Email');

    jQuery.validator.addMethod("email_exists", function (value, element) {
        var emailExists = false;

        $.ajax({
            type:'GET',
            url: getsiteurl()+'/buy_sell/email/exists',
            async: false,
            data: {contractId:$('#contractId').val(),email:value,byuSelId:$('#byuSelId').val()},
            success: (response) => {
                emailExists = !response.status;            
            }
        });

        return emailExists;
    }, 'Email already exists');
	   jQuery.validator.addMethod("email_exists_new", function (value, element) {
		
        var emailExists = false;

        $.ajax({
            type:'GET',
            url: getsiteurl()+'/buy_sell/email/exists',
            async: false,
            data: {contractId:$('#contractId').val(),email:value,byuSelId:$(element).data('id')},
            success: (response) => {
                emailExists = !response.status;            
            }
        });

        return emailExists;
    }, 'Email already exists');

	$("#addPersonFrm").validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                valid_email:true,
                email_exists:true
            },
            role: {
                required: true
            },
            add1: {
                required: true
            },
            country: {
                required: true
            },
            state: {
                required: true
            },
            city: {
                required: true
            },
            postalCode: {
                required: true
            },
            photo: {
                accept: "image/jpeg,image/jpg,image/bmp,image/svg,image/png"
            }
        },messages: {
            name: {
                required: "Please Enter Name"
            },
            email: {
                required: "Please enter Email"
            },
            role: {
            	required: "Please Select Role"
            },
            add1: {
                required: "Please Enter Address Line 1"
            },
            country: {
                required: 'Please Enter Country'
            },
            state: {
                required: 'Please Enter State'
            },
            city: {
                required: 'Please Enter City'
            },
            postalCode: {
                required: 'Please Enter Postal Code'
            },
            photo: {
                accept: "Please select jpeg,jpg,bmp,svg or png image file"
            }
        },errorPlacement: function(error, element) {
            if (element.attr("name") == "photo") {
                 error.insertAfter("#photoValidation");
            }else{
                error.insertAfter(element);
            }
        },submitHandler: function(form) {
            var formData = new FormData(document.getElementById("addPersonFrm"));

            $.ajax({
                type:'POST',
                url: getsiteurl()+'/save/person',
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (response) => {
                    $('#addPersonModal').modal('hide');
                    if(response.status == 'success'){
                        $('#renderBuySelListing').empty();
                        $('#renderBuySelListing').append(response.result);

                        Swal.fire({
                            type: 'success',
                            title: response.message
                        });
                    }else{
                        Swal.fire({
                            type: 'error',
                            title: response.message
                        });
                    }
                }
            });
        }
    });
		$("#add_person_form_new").validate({
			ignore: ":disabled",
        rules: {
            'name[]': {
                required: true
            },
            'email[]': {
                required: true,
                valid_email:true,
                email_exists_new:true
            },
            'permission[]': {
                required: true
            },
            
        },messages: {
            'name[]': {
                required: "Please Enter Name"
            },
            'email[]': {
                required: "Please enter Email"
            },
            'permission[]': {
            	required: "Please Select Role"
            }
          
        },errorPlacement: function(error, element) {
            if (element.attr("name") == "photo") {
                 error.insertAfter("#photoValidation");
            }else{
                error.insertAfter(element);
            }
        }
    });

    $('#addPerson').click(function(){
        // Reset Modal Form Control
        $('#byuSelId').val('');
        $('#addPersonFrm')[0].reset();
        $('select[name="role"]').val('');
        $('#preview').attr('src', $('#defaultPre').val());

        // Reset Modal Form Validation
        $("#addPersonFrm").validate().resetForm();
        $("#addPersonFrm").find(".error").removeClass("error");

        // Show Modal
        $('#addPerModTitle').text('Add Person');
        $('#addPersonModal').modal('show');
    });

    $('#addPerson2').click(function(){
        //close existing modal
        //alert('hello');
        $('#shareDocModal').modal('hide');

        $('#byuSelId').val('');
        $('#addPersonFrm')[0].reset();
        $('select[name="role"]').val('');
        $('#preview').attr('src', $('#defaultPre').val());

        // Reset Modal Form Validation
        $("#addPersonFrm").validate().resetForm();
        $("#addPersonFrm").find(".error").removeClass("error");

        // Show Modal
        $('#addPerModTitle').text('Add Person');
        $('#addPersonModal').modal('show');
    });
    
    $(document).on("click", ".editPerson", function(){
        var id = $(this).attr('id');

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/edit/person',
            data: {id:id},
            success: function(response) {
                // Reset Modal Form Validation
                $("#addPersonFrm").validate().resetForm();
                $("#addPersonFrm").find(".error").removeClass("error");

                // Set Data
                $('#addPerModTitle').text('Edit Person');
                $('#byuSelId').val(id);
                $('#name').val(response.data.name);
                $('#email').val(response.data.email);
                $('select[name="role"]').val(response.data.role_id);
                $('#add1').val(response.data.address1);
                $('#add2').val(response.data.address2);
                $('#country').val(response.data.country);
                $('#state').val(response.data.state);
                $('#city').val(response.data.city);
                $('#postalCode').val(response.data.postal_code);
                $('#preview').attr('src', response.image);

                // Show Modal
                $('#addPersonModal').modal('show');
            }
        });
    });

    $(document).on("click", ".deletePerson", function(){
        var id = $(this).attr('id');

        Swal.fire({
          title: 'Are you sure you want to delete this person?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "GET",
                    url: getsiteurl()+'/delete/person',
                    data: {id:id},
                    success: function(response) {
                        $('#renderBuySelListing').empty();
                        $('#renderBuySelListing').append(response.result);

                        Swal.fire({
                            type: 'success',
                            title: response.message
                        });
                    }
                });
            }
        });
    });
    
    $('#photo').change(function(){
        previewImage(this,'preview');
    });

    function previewImage(input, previewId){
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+previewId).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".dropzone").change(function() {
        readFile(this);
    });

    function readFile(input) {
        var draft=$("#draft").val();
        if(draft=='draft'){
        if (input.files && input.files[0]) {
            if(input.files[0].type.indexOf("application/pdf")==-1){
                Swal.fire({
                    type: 'error',
                    title: 'Please Select PDF File!'
                });
            }else{
                $('#docProgBar').css({'display':'block'});

                $('#draftFrm').ajaxSubmit({
                    beforeSend: function() {
                        // 
                    },
                    uploadProgress:function(event, position, total, percentComplete){
                        $('.progress-bar').text(percentComplete + '%');
                        $('.progress-bar').css('width', percentComplete + '%');
                    },success:function(response){
                        var url=getsiteurl()+"/"+"prepare_draft"+'/'+response.draft_id;
                        $('#draftFrm')[0].reset();
                        $('.progress-bar').text('0%');
                        $('.progress-bar').css('width', '0%');
                        $('#docProgBar').css({'display':'none'});
                        $(".prepareDraft").attr("href",url);

                        $('#renderDocListing').empty();
                        $('#renderDocListing').append(response.result);

                        $('#next_step').removeAttr('disabled');

                        Swal.fire({
                            type: response.status,
                            title: response.message
                        });
                    }
                });
            } 
        }
        }else{
            if (input.files && input.files[0]) {
                if(input.files[0].type.indexOf("application/pdf")==-1){
                    Swal.fire({
                        type: 'error',
                        title: 'Please Select PDF File!'
                    });
                }else{
                    $('#docProgBar').css({'display':'block'});

                    $('#documentFrm').ajaxSubmit({
                        beforeSend: function() {
                            // 
                        },
                        uploadProgress:function(event, position, total, percentComplete){
                            $('.progress-bar').text(percentComplete + '%');
                            $('.progress-bar').css('width', percentComplete + '%');
                        },success:function(response){
                            $('#documentFrm')[0].reset();
                            $('.progress-bar').text('0%');
                            $('.progress-bar').css('width', '0%');
                            $('#docProgBar').css({'display':'none'});

                            $('#renderDocListing').empty();
                            $('#renderDocListing').append(response.result);
    						$('#next_step').removeAttr('disabled');

                            if(response.status == 'success'){
                                Swal.fire({
                                    type: 'success',
                                    title: response.message
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: response.message
                                });
                            }
                        }
                    });
                } 
            }
        }
    }
    //  function readFile(input) {
    //     if (input.files && input.files[0]) {
    //         if(input.files[0].type.indexOf("application/pdf")==-1){
    //             Swal.fire({
    //                 type: 'error',
    //                 title: 'Please Select PDF File!'
    //             });
    //         }else{
    //             $('#docProgBar').css({'display':'block'});

    //             $('#draftFrm').ajaxSubmit({
    //                 beforeSend: function() {
    //                     // 
    //                 },
    //                 uploadProgress:function(event, position, total, percentComplete){
    //                     $('.progress-bar').text(percentComplete + '%');
    //                     $('.progress-bar').css('width', percentComplete + '%');
    //                 },success:function(response){
    //                     var url="prepare_draft"+'/'+response.draft_id;
    //                     $('#draftFrm')[0].reset();
    //                     $('.progress-bar').text('0%');
    //                     $('.progress-bar').css('width', '0%');
    //                     $('#docProgBar').css({'display':'none'});
    //                     $(".prepareDraft").attr("href",url);

    //                     $('#renderDocListing').empty();
    //                     $('#renderDocListing').append(response.result);
    //                     $('#next_step').removeAttr('disabled');

    //                     if(response.status == 'success'){
    //                         Swal.fire({
    //                             type: 'success',
    //                             title: response.message
    //                         });
    //                     }else{
    //                         Swal.fire({
    //                             type: 'error',
    //                             title: response.message
    //                         });
    //                     }
    //                 }
    //             });
    //         } 
    //     }
    // }

    $(document).on("click", ".deleteDocument", function(){
        var id = $(this).attr('id');

        Swal.fire({
          title: 'Are you sure you want to delete this document?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "GET",
                    url: getsiteurl()+'/delete/document',
                    data: {id:id},
                    success: function(response) {
                        $('#renderDocListing').empty();
                        $('#renderDocListing').append(response.result);
						if($('#document_table td').length > 1){
							$('#next_step').removeAttr('disabled');
							$('#step_2').removeAttr('disabled');
						}else{
							$('#next_step').attr('disabled','disabled');
							$('#step_2').attr('disabled','disabled');
						}

                        Swal.fire({
                            type: 'success',
                            title: response.message
                        });
                    }
                });
            }
        });
    });

    $(document).on("click", ".share", function(){
        var id = $(this).attr('id');

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/get/document',
            data: {id:id},
            success: function(response) {
                $('#documentName').text(response.document.path);
                $('#shareDocId').val(id);

                $('#renderShareDocPepListing').empty();
                $('#renderShareDocPepListing').append(response.result);

                $('#shareDocModal').modal('show');
            }
        });
    });

    $(document).on("change", ".permission", function(){
        var contractId    = $(this).attr('data-cid');
        var docId         = $('#shareDocId').val();
        var buyerSellerId = $(this).attr('id');
        var permissionId  = $(this).children("option:selected").val();

        if(permissionId != ''){
            $.ajax({
                type: "GET",
                url: getsiteurl()+'/save/documents/permission',
                data: {contractId:contractId,docId:docId,buyerSellerId:buyerSellerId,permissionId:permissionId},
                success: function(response) {
                    removePerSuccErrorClass();

                    if(response.status == 'success'){
                        $('#perSuccError').addClass('text-success'); 
                        $('#perSuccError').text(response.message); 
                    }else{
                        $('#perSuccError').addClass('text-danger'); 
                        $('#perSuccError').text(response.message);
                    }
                    $('#perSuccError').css({'display':'block'});

                    setTimeout(function(){
                        $('#perSuccError').css({'display':'none'});
                    }, 3000);
                }
            });
        }
    });

    function removePerSuccErrorClass(){
        $('#perSuccError').removeClass('text-success');
        $('#perSuccError').removeClass('text-danger');
        $('#perSuccError').removeClass('text-info');
    }

    $(document).on("click", "#sendEmail", function(){
        var docId = $('#shareDocId').val();
        $(this).prop('disabled', true);

        $.ajax({
            type:'GET',
            url: getsiteurl()+'/send/documents/email',
            data: {docId:docId},
            success: (response) => {
                $(this).prop('disabled', false);
                removePerSuccErrorClass();

                if(response.status == 'success'){
                    $('#perSuccError').addClass('text-success'); 
                    $('#perSuccError').text(response.message);
                }else if(response.status == 'info'){
                    $('#perSuccError').addClass('text-info'); 
                    $('#perSuccError').text(response.message); 
                }else{
                    $('#perSuccError').addClass('text-danger'); 
                    $('#perSuccError').text(response.message);
                }
                $('#perSuccError').css({'display':'block'});

                setTimeout(function(){
                    $('#perSuccError').css({'display':'none'});
                }, 3000);
            }
        }); 
    });

    $(document).on("change", ".type", function(){
        var conTranId = $(this).val();
        var conId     = $(this).attr('data-id');

        if(conTranId != '' && conId != ''){
            $.ajax({
                type:'GET',
                url: getsiteurl()+'/get/contract_status',
                data: {conId:conId,conTranId:conTranId},
                success: (response) => {
                    $('#transStatusErrSucc').css({'display':'block'});
                    $('#transStatusErrSucc').removeClass('text-success');
                    $('#transStatusErrSucc').removeClass('text-danger');

                    if(response.status == 'success'){
                        $('#transStatusErrSucc').addClass('text-success');
                        $('#renderStatus_'+conId).empty();
                        $('#renderStatus_'+conId).append(response.result);
                    }else{
                        $('#transStatusErrSucc').addClass('text-danger');
                    }

                    $('#transStatusErrSucc').text(response.message);
                    setTimeout(function(){
                        $('#transStatusErrSucc').css({'display':'none'});
                    }, 3000);
                }
            });
         }
    });

    $(document).on("change", ".status", function(){
        var conStatId = $(this).val();
        var conId     = $(this).attr('data-id');

        if(conStatId != '' && conId != ''){
            $.ajax({
                type:'GET',
                url: getsiteurl()+'/save/contract_status',
                data: {conId:conId,conStatId:conStatId},
                success: (response) => {
                    $('#transStatusErrSucc').css({'display':'block'});
                    $('#transStatusErrSucc').removeClass('text-success');
                    $('#transStatusErrSucc').removeClass('text-danger');

                    if(response.status == 'success'){
                        $('#transStatusErrSucc').addClass('text-success');
                    }else{
                        $('#transStatusErrSucc').addClass('text-danger');
                    }

                    $('#transStatusErrSucc').text(response.message);
                    setTimeout(function(){
                        $('#transStatusErrSucc').css({'display':'none'});
                    }, 3000);
                }
            });
        }
    });
	$(document).on("click", "#add_new_receipient", function(){
       
            $.ajax({
                type:'post',
                url: getsiteurl()+'/add_new_receipient',
                data: {last_id:$("#add_receipient_div .receipt_div" ).last().attr( "id" )},
                success: function(response){
                    $('#add_receipient_div').append(response);
					if($("#add_receipient_div .receipt_div" ).length > 1){
						$('.delete_recepient').show();
					}else{
						$('.delete_recepient').hide();
					}
					 $(".custom-select").select2({
				minimumResultsForSearch : -1
			});
			
                }
            });
        
    });
	$(document).on("click", "#next_step", function(){
        
            $('#timeline').show();
			$('#step_1').hide();
			$('#step_2').hide();
			if($("#add_receipient_div .receipt_div" ).length > 1){
						$('.delete_recepient').show();
					}else{
						$('.delete_recepient').hide();
					}
					$('#step-1').removeClass('progressIndicator-current');
			$('#step-2').addClass('progressIndicator-current');
        
    });
		$(document).on("click", "#submit_doc", function(){
		if ($('#add_person_form_new').valid()) {
          $('#add_person_form_new').submit();
		}
        
    });
	
	$(document).on('click','.delete_recepient',function(){
		if($(this).attr('data-id') == ""){
			$('#'+$(this).attr('row_id')).remove();
			if($("#add_receipient_div .receipt_div" ).length > 1){
						$('.delete_recepient').show();
					}else{
						$('.delete_recepient').hide();
					}
		}else{
			var row_id=$(this).attr('row_id');
			 $.ajax({
                type:'post',
                url: getsiteurl()+'/delete_receipient',
                data: {
					"id" : $(this).attr('data-id'),
					"contract_id":$('#contractId').val()
					},
                success: function(response){
					
                    $('#'+row_id).remove();
					console.log('test');
					if($("#add_receipient_div .receipt_div" ).length > 1){
						$('.delete_recepient').show();
					}else{
						$('.delete_recepient').hide();
					}
					
                }
            });
			//alert($("#add_receipient_div .receipt_div" ).length);
			
		}
	});
	$('#back_button').on('click',function(){
		location.reload();
		});
		$(document).on('click','#step-1',function(){
			location.reload();
		})
		$(document).on('click','#step-2',function(){
			if($('#document_table td').length == 1){
				return false;
			}
			$('#step-1').removeClass('progressIndicator-current');
			$(this).addClass('progressIndicator-current');
			 $('#timeline').show();
			$('#step_1').hide();
			$('#step_2').hide();
			if($("#add_receipient_div .receipt_div" ).length > 1){
						$('.delete_recepient').show();
					}else{
						$('.delete_recepient').hide();
					}
		})
    
		
});
    