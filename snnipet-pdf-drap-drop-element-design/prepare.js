$(document).ready(function() {
    var documentId        = $('#documentId').val();
    var path              = $('#path').val();
    var docName           = $('#docName').val();
    var signCoordinates   = [];
    var saveSignIni       = [];
    var assFieldModDragId = '';
	$('.hide_button').hide();

    var _PDF_HEIGHT  = 0;
    var _HTML_HEIGHT = 0;
    var _ENV_CODE    = '';

    var clickType       = '';
    var clickId         = '';
    var sigCanId        = '';
    var chooseAdoptSign = false;

    $(document).ajaxStart(function(){
        $("#loader").css("display","flex");
    });

    $(document).ajaxComplete(function(){
        $("#loader").css("display","none");
    });

    function renderPDF(url, canvasContainer, options) {
        $("#loader").css("display","flex");
        options = options || { scale: 1.5 };
        
        function renderPage(page) {
            var viewport = page.getViewport(options.scale);
            var wrapper = document.createElement("div");
            wrapper.className = "canvas-wrapper";
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var renderContext = {
              canvasContext: ctx,
              viewport: viewport
            };

            canvas.height = viewport.height;
            canvas.width = viewport.width;
            
            canvasContainer.style.width = parseInt(canvas.width)+parseInt(20)+"px"; // set container width same as PDF

			var new_div = document.createElement("div");
			new_div.id = "title_div";
            new_div.setAttribute("data-html2canvas-ignore", true);
            _ENV_CODE = "AnyDeal Envelope ID : "+window.btoa($('#documentId').val()+$('#created_at').val());
			new_div.innerHTML=_ENV_CODE;
			wrapper.appendChild(new_div);		
            wrapper.appendChild(canvas);

            _HTML_HEIGHT = _HTML_HEIGHT + canvas.height + 25;
            _PDF_HEIGHT = canvas.height + 25;

            canvasContainer.width = viewport.width;
            canvasContainer.appendChild(wrapper);
            
            page.render(renderContext);
        }
    
        function renderPages(pdfDoc) {
            for(var num = 1; num <= pdfDoc.numPages; num++)
                pdfDoc.getPage(num).then(renderPage);
        }

        PDFJS.disableWorker = true;
        PDFJS.getDocument(url).then(renderPages);

        renderAssignField();
    }
    renderPDF(path, document.getElementById('holder'));
	

    function renderAssignField(){
        var draft_id=$("#draftId").val();
        setTimeout(function(){
            $.ajax({
                type:'GET',
                url: getsiteurl()+'/get/coordinates',
                data: {'docId':documentId,'draft_id':draft_id},
                success: (response) => {
                    if(response.status == 'success' && response.data != null){
                        $("#share").attr("disabled", false);

                        Object.values(response.data).map(point => {
                            var rndId       = btoa(Math.random()).substring(0,12);
                            var rndSignId   = btoa(Math.random()).substring(0,12);
                            var rndDragId   = btoa(Math.random()).substring(0,12);

                            if(point.type == 'add_text'){
                                $("#holder").prepend('<div class="ui-widget-content" id="'+rndId+'" style="width: auto;line-height:15px;"></div>');
                                $("#"+rndId).append('<div class="text-center mt-1"><p class="mb-0">'+point.lines+'</p></div>');
                                $("#"+rndId).css({top: point.y, left: point.x, position:'absolute'});                                
                            }else if(point.type == 'assign_field_audio'){
                                if(point.path != 'PATH'){
                                    $("#holder").prepend('<div data-html2canvas-ignore class="ui-widget-content" id="'+rndId+'" style="width: auto;height: 40px"><div class="resizeBox float-left" data-html2canvas-ignore id="resize-'+rndId+'"></div></div>');
                                    $("#"+rndId).append('<div class="text-center mt-1 float-left ml-1"><p class="mb-0"> <i class="far fa-play-circle fa-2x text-success cp playAudio" data-recorded-audio-id="'+rndId+'"></i> <audio controls class="d-none" id="recordedAudio-'+rndId+'"> <source src="'+point.path+'"></source></audio></p></div>');
                                    $("#"+rndId).css({top: point.y, left: point.x, position:'absolute'});                                
                                    $("#resize-"+rndId).css({top: point.section_top, left: point.section_left, width:point.section_width, height:point.section_height});                                
                                    
                                    
                                    /*$("#"+rndId+" audio").each(function(){
                                        var player = new Audio();
                                        player.src = point.path;
                                        // player.play();
                                    });*/
                                }
                            }else if((point.type == 'sign' || point.type == 'assign_field' || point.type == 'assign_field_initial') && point.isSign == true){
                                if(point.lines == null){
                                    if(point.txtSigIni !== undefined){
                                        $("#holder").prepend('<div id="'+rndDragId+'" class="ui-widget-content sign-border" style="width: auto;height: 105px"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                        '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                        '<div><p class="my-sign" style="margin-top: -4px;" >'+window.btoa(point.uniqueId)+'</p></div></div>');

                                        $("#"+rndDragId).css({top: point.y, left: point.x, position:'absolute'});
                                        $('#'+rndSignId).html(point.txtSigIni).addClass('named_sign').css({'font-family': point.font});
                                    }
                                }else{
                                    $("#holder").prepend('<div class="sign-border sign-border-none"><div id="'+rndDragId+'" class="sign-border" style="width: 250px;height: 80px"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                    '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                    '<div><p class="my-sign" style="text-align:center">'+window.btoa(point.uniqueId)+'</p></div></div></div>');
                                    
                                    $("#"+rndDragId).css({top: point.y, left: point.x, position:'absolute'});
                                    $('#'+rndSignId).signature();

                                    if(point.lines.indexOf('lines') == -1){
                                        var txt1 = "<textarea class='text-center' name='my_sign' id='my_sign' style='font-family: Brush Script MT; height:75px; width:100%; font-size:30px; resize: none;' disabled>"+point.lines+"</textarea>"; 
                                        $('#'+rndSignId).html(txt1);
                                    }else{
                                        $('#'+rndSignId).signature('draw', point.lines);
                                    }

                                    $('#'+rndSignId).signature('disable');
                                    
                                }
                            }else if(point.type == 'owner_name'){
                                $("#holder").prepend('<div class="sign-border sign-border-none"><div id="'+rndDragId+'" class="sign-border" style="width: 250px;height: 80px"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                '<div><p class="my-sign alpha" style="text-align:center;margin-top:-4px;">'+window.btoa(point.uniqueId)+'</p></div></div>')

                                $("#"+rndDragId).css({top: point.y, left: point.x, position:'absolute'});
                                $('#'+rndSignId).html(point.lines).addClass('named_sign').css({'font-family': 'alaskan-malamute-regular'});
                            }else if(point.type == 'owner_draw'){
                                $("#holder").prepend('<div class="sign-border sign-border-none"><div id="'+rndDragId+'" class="sign-border" style="width: 250px;height: 80px"><div><p class="sign-by" style="text-align:center">Document SignedBy:</p></div>'+
                                '<div id="'+rndSignId+'"></div>'+ //Signature Container
                                '<div><p class="my-sign" style="text-align:center">'+window.btoa(point.uniqueId)+'</p></div></div>')

                                $("#"+rndDragId).css({top: point.y, left: point.x, position:'absolute'});
                                $('#'+rndSignId).signature();
                                $('#'+rndSignId).signature('draw', point.lines);
                                $('#'+rndSignId).signature('disable');
                            }else{
                                $("#holder").prepend('<div class="ui-widget-content" id="'+rndId+'" style="width: auto;height: 45px"></div>');
                                
                                var txtMsg = 'SIGN HERE';
                                if(point.type == 'assign_field_initial'){
                                    txtMsg = 'INITIAL HERE';
                                }

                                var str = point.name;
                                var matches = (str.match(/\b(\w)/g)).join('');

                                if(point.lines){
									$("#"+rndId).append('<div class="text-center mt-1"><h6><b>'+matches.toUpperCase()+' '+txtMsg+'</b></h6></div>');
								}else{
									$("#"+rndId).append('<div class="text-center mt-1"><h6><b>'+matches.toUpperCase()+' '+txtMsg+'</b></h6></div>');
								}
								
                                $("#"+rndId).css({top: point.y, left: point.x, position:'absolute'});                                
                            }
                        });

                        $(".playAudio").click(function(){
                            var mainElement = $(this);
                            var temp = $(this).attr('data-recorded-audio-id');
                            var audioElement = $("#recordedAudio-"+temp)[0];
                            if(!audioElement.paused){
                                audioElement.pause();
                                mainElement.removeClass('far fa-pause-circle text-danger pause');
                                mainElement.addClass('far fa-play-circle text-danger');
                            }else{
                                audioElement.play();
                                mainElement.removeClass('far fa-play-circle text-danger');
                                mainElement.addClass('far fa-pause-circle text-danger pause');
                            }

                            audioElement.addEventListener('ended', function() {
                                mainElement.removeClass('far fa-pause-circle text-danger pause');
                                mainElement.addClass('far fa-play-circle text-success');
                            }, false);

                            audioElement.addEventListener("timeupdate",function(){
                                // mainElement.removeClass('far fa-play-circle');
                                // mainElement.addClass('far fa-pause-circle text-danger pause');
                            });

                            audioElement.addEventListener("paused",function(){
                                mainElement.removeClass('far fa-play-circle');
                                mainElement.addClass('far fa-pause-circle text-danger pause');
                            });
                        });
                    }else{
                        $("#share").attr("disabled", true);
                    }

                    if(response.savedSignIni != null){
                        if(response.savedSignIni.type == 'draw'){
                            var sigId   = btoa(Math.random()).substring(0,12);
                            var iniId   = btoa(Math.random()).substring(0,12);

                            $('#signTable').append('<tr><td><input type="radio" checked></td>'
                            +'<td><div id='+sigId+' style="background-color: #ffffff; width: 280px;margin: 0;"></div></td>'
                            +'<td><div id='+iniId+' style="background-color: #ffffff; width: 80px;margin: 0;"></div></td>'
                            +'</tr>');

                            $('#'+sigId).signature();
                            $('#'+sigId).signature('draw',response.savedSignIni.signature);
                            $('#'+sigId).signature('disable');

                            $('#'+iniId).signature();
                            $('#'+iniId).signature('draw',response.savedSignIni.initial);
                            $('#'+iniId).signature('disable');
                        }else if(response.savedSignIni.type == 'name'){
                            var rndSignId   = btoa(Math.random()).substring(0,12);

                            $('#signTable').append('<tr><td><input type="radio" checked></td>'
                            +'<td><div id='+rndSignId+' class="save_sig" style="font-family:alaskan-malamute-regular;">'+response.savedSignIni.signature+'</div></td>'
                            +'<td><div id='+rndSignId+' class="save_sig" style="font-family:alaskan-malamute-regular;">'+response.savedSignIni.initial+'</div></td>'
                            +'</tr>');
                        }
                        
                        $('#hidSavedType').val(response.savedSignIni.type);
                        $('#hidSavedSig').val(response.savedSignIni.signature);
                        $('#hidSavedIni').val(response.savedSignIni.initial);
                    }else{
                        $('#signTable').append('<tr><td colspan="3"><center>No Record Found</center></td></tr>');
                    }
                }
            });
        }, 3000);
    }

    // var width = window.innerWidth;
    var height = window.innerHeight;

    var stage = new Konva.Stage({
        container: 'holder',
        // width: width,
        height: height,
    });

    var layer = new Konva.Layer();
    stage.add(layer);

    $(document).on("click", "#addText", function(){
        var rndTxtId      = btoa(Math.random()).substring(0,12);
        var rndDragId     = btoa(Math.random()).substring(0,12);
        var rndTxtPanelId = btoa(Math.random()).substring(0,12);

        $("#holder").prepend('<div data-html2canvas-ignore id="'+rndDragId+'" class="draggable ui-widget-content add_text_container" style="width: 197px;cursor: -webkit-grab; cursor: grab;height: 65px;position: absolute;top:'+(top+70)+'px;" data-txt-container-id="'+rndTxtId+'" >'+
        '<textarea id="'+rndTxtId+'" class="form-control text_area" placeholder="Add Text Here" div_id="'+rndTxtPanelId+'" ></textarea>'+ //Textbox Container
        '<div id="'+rndTxtPanelId+'" class="mt-1 text-center addTextPanel">'+
        '<button class="remove btn btn-danger" data-toggle="tooltip" data-placement="bottom" data-draggable-id="'+rndDragId+'" title="Delete"><i class="fas fa-trash-alt"></i></button>'+
        '<button class="clearText btn btn-secondary ml-1" data-toggle="tooltip" data-placement="bottom"  id="'+rndTxtId+'" title="Clear"><i class="fas fa-broom"></i></button>'+
        '<button class="doneAddText btn btn-success ml-1" data-toggle="tooltip" data-placement="bottom"  id="'+rndTxtPanelId+'" data-id="'+rndDragId+'" data-txt-container-id="'+rndTxtId+'" title="Done"><i class="fas fa-check-circle"></i></button><i class="fas fa-arrows-alt" ></i></div></div>');

        $('#'+rndDragId).draggable({
            containment: '#holder',
			stop: function( event, ui ) {
				element = $(this);
				var top  = element.position().top;
				var left = element.position().left;

				for(var i=0;i<signCoordinates.length;i++){
						if(signCoordinates[i].draggable_id == $(this).attr('id')){
						signCoordinates[i] = "";
					}
				}
    			signCoordinates.push({
                    'id':'owner',
                    'X':left,
                    'Y':top,
                    'lines':$(this).attr('data-txt-container-text'),
                    'type':'add_text',
        			'draggable_id':$(this).attr('id')
                });	
			}
        });

		textareaResize($('.text_area'));

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    });

    // =====================================
    $(document).on("click", ".clearText", function(){
        $('#'+$(this).attr('id')).val('');
    });

    var textareaResize = function(source) {
        var resizeInt      = null;
        var dest           = $('#'+source.attr('div_id'));

        var resizeEvent = function() {
            dest.outerWidth( source.outerWidth() );
            //dest.outerHeight(source.outerHeight());
        };

        // This provides a "real-time" (actually 15 fps)
        // event, while resizing.
        // Unfortunately, mousedown is not fired on Chrome when
        // clicking on the resize area, so the real-time effect
        // does not work under Chrome.
        source.on("mousedown", function(e) {
            resizeInt = setInterval(resizeEvent, 1000/15);
        });

        // The mouseup event stops the interval,
        // then call the resize event one last time.
        // We listen for the whole window because in some cases,
        // the mouse pointer may be on the outside of the textarea.
        $(window).on("mouseup", function(e) {
            if (resizeInt !== null) {
                clearInterval(resizeInt);
            }
            resizeEvent();
        });
    };
    

    $(document).on("click", ".doneAddText", function(){
        $("#save").attr("disabled", false);
        element = $('#'+$(this).attr('data-id'));
        var top  = element.position().top;
        var left = element.position().left;
		
		for(var i=0;i<signCoordinates.length;i++){
			if(signCoordinates[i].draggable_id == element.attr('id')){
			   signCoordinates[i] = "";
		    }
		}

        signCoordinates.push({
            'id':'owner',
            'X':left,
            'Y':top,
            'lines':$('#'+$(this).attr('data-txt-container-id')).val(),
            'type':'add_text',
			'draggable_id':element.attr('id')
        });

		element.attr('data-txt-container-text',$('#'+$(this).attr('data-txt-container-id')).val());
        element.css({'height':'40'});
        $('#'+$(this).attr('id')).css({'display':'none'});		
		$('#'+$(this).attr('data-txt-container-id')).replaceWith("<input type='hidden' id='"+$(this).attr('data-txt-container-id')+'" value="'+$('#'+$(this).attr('data-txt-container-id')).val()+"'><p class='text_pointer'>"+$('#'+$(this).attr('data-txt-container-id')).val()+'</p>');
		$('#'+$(this).attr('data-txt-container-id')).css("resize", "none")
		element.css('width','auto');
    });

    $(document).on("click", ".remove", function(){
        var removeId = $(this).attr('data-draggable-id'); 
        $('#'+removeId).remove();
    });

    $(document).on("click", ".clearSign", function(){
        var sign = $('#'+$(this).attr('id')).signature();
        sign.signature('clear');
		$('#'+$(this).attr('data-id')).remove();
    });

    $(document).on("click", ".assign", function(){
        $.ajax({
            type:'GET',
            url: getsiteurl()+'/get/buyer_seller',
            data: {'documentId':documentId},
            success: (response) => {
                if(response.status == 'success'){
                    $('#renderAssignFieldModal').empty();
                    $('#renderAssignFieldModal').append(response.result);
                    
                    assFieldModDragId = '';
                    assFieldModDragId = $(this).attr('data-draggable-id');

                    $('#assignFieldModal').modal('show');
                }else{
                    Swal.fire({
                        type: 'error',
                        title: response.message
                    });
                }
            }
        });
    });

    $(document).on("click", ".doneSign", function(){
        $("#save").attr("disabled", false);
        element = $('#'+$(this).attr('id'));

        var top  = element.position().top;
        var left = element.position().left;

        signCoordinates.push({
            'id':'owner',
            'X':left,
            'Y':top,
            'lines':$('#'+$(this).attr('data-sign-container-id')).signature('toJSON'),
            'type':'sign'
        });

        $('#'+$(this).attr('id')).css({'height':'75'});
		$('#'+$(this).attr('id')).css({'border':'none'});
        $('#'+$(this).attr('data-id')).css({'display':'none'});
        $('#'+$(this).attr('data-sign-container-id')).signature('disable');
		$('.kbw-signature').css({'border':'none'});
		$('.ui-widget-content').css({'background':'transparent'});
    });

    $(document).on("click", "#saveAssignField", function(){
        var name        = $("input[name='buySeller']:checked").attr('data-name');
        var buySellerId = $("input[name='buySeller']:checked").val();
        
        if(name){
            var str     = name;
            var matches = (str.match(/\b(\w)/g)).join('');

            $('#assignFieldModal').modal('hide');
            $("#"+assFieldModDragId).empty();

            $("#"+assFieldModDragId).attr("data-html2canvas-ignore","true");
            $("#"+assFieldModDragId).append('<div class="text-center mt-1"><h6><b class="text_pointer">'+matches.toUpperCase()+' SIGN HERE</b></h6></div>');
            $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
            '<button class="btn btn-success ml-1 doneAssField" data-buy-seller-id="'+buySellerId+'" data-buy-seller-name="'+name+'" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');

            $("#"+assFieldModDragId).css("height","85px");
        }else{
            $('.assFieldModMsg').addClass('text text-danger'); 
            $('.assFieldModMsg').text('Please select people to assign!'); 
        }
    });

    $(document).on("click", ".doneAssField", function(){
        $("#save").attr("disabled", false);
        id      = $(this).attr('data-buy-seller-id');
        name    = $(this).attr('data-buy-seller-name');
        element = $('#'+$(this).attr('data-draggable-id'));
		element.attr('s_id',id)
		element.attr('s_name',name)

        var top  = element.position().top;
        var left = element.position().left;

        signCoordinates.push({
            'id':id,
            'X':left,
            'Y':top,
            'type':'assign_field',
			'draggable_id':$(this).attr('data-draggable-id')
        });

        $(element).find('div').last().remove();
        $(element).css({'height':'45px'});
    });

	var top = 0;
	window.onscroll = function (e) {
    top = window.scrollY; // Value of scroll Y in px
};

    $(document).on("click", "#addSign", function(){
        var rndSignId   = btoa(Math.random()).substring(0,12);
        var rndDragId   = btoa(Math.random()).substring(0,12);
        var rndSigPnlId = btoa(Math.random()).substring(0,12);
		
        $("#holder").prepend('<div data-html2canvas-ignore id="'+rndDragId+'" class="draggable ui-widget-content stop_draggable" style="width: auto;height: 125px;position: absolute;top:'+(top+70)+'px;">'+
        '<div class="img-sign_view sign_box" data-id="'+rndSigPnlId+'" data-panelId="'+rndSigPnlId+'"><img class="resizebleImage" src="'+asset_url+'images/sign_icon.png" height="75" width = "75" rndDragId="'+rndDragId+'"></div><div id="'+rndSignId+'"class=""></div>'+ //Signature Container
        '<div id="'+rndSigPnlId+'" class="text-center signPanel hide_button">'+
        '<button class="remove btn border" data-toggle="tooltip" data-placement="bottom"  data-draggable-id="'+rndDragId+'" title="Delete"><i class="fas fa-trash-alt"></i></button>'+
        '<button title="Add People" data-toggle="tooltip" data-placement="bottom"  class="btn  ml-1 border assign" data-draggable-id="'+rndDragId+'"><i class="fas fa-users"></i></button>'+
        '<button title="Sign Now" data-signcanvas-id="'+rndSignId+'" data-toggle="tooltip" data-placement="bottom" class="btn ml-1 border add_own" data-text="signature" data-draggable-id="'+rndDragId+'" data-id="'+rndSigPnlId+'"><img height="auto" width = "20px" src="'+$('#hand-free').val()+'" /></button>'+
        '</div>');

        $('#'+rndDragId).draggable({
            containment: '#holder',
			stop: function( event, ui ) {
				var name        = $(this).attr('s_name');
				var id        = $(this).attr('s_id');
				
				if(name){
					element = $(this);
					var top  = element.position().top;
					var left = element.position().left;
					
                    for(var i=0;i<signCoordinates.length;i++){
                        if(signCoordinates[i].draggable_id == $(this).attr('id')){
							signCoordinates[i] = "";
						}
					}

					signCoordinates.push({
						'id':id,
						'X':left,
						'Y':top,
						'type':'assign_field',
						'draggable_id':$(this).attr('id'),
						
					});
				}
			}
        });
	
        $('#'+rndSignId).signature();
		$('.hide_button').hide();

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on("click", "#save", function(){
        $("#save").attr("disabled", true);
        $("#save").text("Saving...");

        $(".resetPath").each(function(){
            var rndDragId = $(this).attr('data-path-id');
            var path = $(this).val();
            setPathCoordinated(rndDragId, path);
        });
        
        var formData = new FormData();
        formData.append('documentId',documentId);
        formData.append('signCoordinates',JSON.stringify(signCoordinates));
        formData.append('saveSignIni',JSON.stringify(saveSignIni));
        // console.log("signCoordinates", signCoordinates, JSON.stringify(signCoordinates));
        $.ajax({    
            type:'POST',
            url: getsiteurl()+'/update/documents',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: (response) => {
                if(response.status == 'success'){
                    Swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }else{
                    Swal.fire({
                        type: 'error',
                        title: response.message
                    });
                }
                
                setTimeout(function(){ 
                    location.reload();
                }, 2000);
            }
        });
    });
    
    $(document).on("click", "#save_as_draft", function(){
        $('#draft_name').val('');
        draftValidator.resetForm();
        $('#Draftmodal').modal('show');
    });

    var draftValidator = $("#draftFrm").validate({
        rules: {
            draft_name: {
                required: true
            }
        },messages: {
            draft_name: {
                required: "Please enter template name"
            }
        },
        submitHandler: function(form) {
            var draft_name = $("#draft_name").val();
            var draft_id   = $("#draftId").val();
       
            $("#draftTxt").text("Saving...");
            
            $(".resetPath").each(function(){
                var rndDragId = $(this).attr('data-path-id');
                var path = $(this).val();
                setPathCoordinated(rndDragId, path);
            });
                
            var formData = new FormData();
            formData.append('draft_id',draft_id);
            formData.append('signCoordinates',JSON.stringify(signCoordinates));
            formData.append('draft_name',draft_name);

            $.ajax({    
                type:'POST',
                url: getsiteurl()+'/save/draft_documents',
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (response) => {
                    if(response.status == 'success'){
                        Swal.fire({
                            type: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 2000
                        });

                        $("#draftTxt").text("Save As Template");
                        $("#draft_name").val("");
                        $('#Draftmodal').modal('toggle');
                    }else{
                        Swal.fire({
                            type: 'error',
                            title: response.message
                        });
                    }
                }
            });
        }
    });

    $(document).on("click", "#share", function(){
        var displaystatus = $('#displaystatus').val();
        
        if(displaystatus != 1){
            $('#documentcount').modal({backdrop: 'static', keyboard: false})  
            $('#documentcount').modal('show');
        }else{
            $(this).attr("disabled", true);
            $(this).text("Sharing...");

            $.ajax({
                type:'GET',
                url: getsiteurl()+'/send/documents/email',
                data: {docId:documentId},
                success: (response) => {
                    $(this).attr("disabled", false);
                    $(this).text("Share");

                    if(response.status == 'success'){
                        successSwal(response.message);
    					window.location.href = $(this).attr('href');//ankit$('#back_button').attr('href');
                    }else if(response.status == 'info'){
                        infoSwal(response.message);
                    }else{
                        errorSwal(response.message);
                    }
                }
            });
        }
    });

    function errorSwal(message){
        Swal.fire({
            type: 'error',
            title: message
        });
    }

    function successSwal(message){
        Swal.fire({
            type: 'success',
            title: message,
            showConfirmButton: false,
            timer: 2000
        });
    }

    function infoSwal(message){
        Swal.fire({
            type: 'info',
            title: message
        });
    }

    // $(document).on("click", "#download", function(){
    //     $(window).scrollTop(0);
    //     $('#txtDownload').text('Downloading...');
        
    //     // $("div").each(function() {
    //     //     if(!$(this).hasClass('draggable')){
    //     //         $(this).removeAttr("data-html2canvas-ignore");
    //     //     }
    //     // });
        
    //     var HTML_Width = $(".html-content").width();
    //     var HTML_Height = $(".html-content").height();
    //     var top_left_margin = 15;
        
    //     var PDF_Width = HTML_Width+(top_left_margin*2);
    //     var PDF_Height = (PDF_Width*1.2)+(top_left_margin*2);

    //     //var PDF_Width = HTML_Width+30;
    //     //var PDF_Height = HTML_Height;

    //     var canvas_image_width = HTML_Width;
    //     var canvas_image_height = HTML_Height-80;
        
    //     var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;

    //     html2canvas($(".html-content")[0],{scale:1.15, x:0,imageTimeout: 0,allowTaint: true,useCORS: true}).then(function(canvas) {
    //         canvas.getContext('2d');
              
    //         var imgData = canvas.toDataURL("image/jpeg", 1.0);
    //         var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
    //         pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
            
    //         for (var i = 1; i <= totalPDFPages; i++) { 
    //             pdf.addPage(PDF_Width, PDF_Height);
    //             pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
    //         }
            
    //         pdf.save(docName);
    //         $('#txtDownload').text('Download');
    //     });
    // });

    $(document).on("click", "#download", function(){
        $(window).scrollTop(0);
        $('#txtDownload').text('Downloading...');
        
        // $("div").each(function() {
        //     if(!$(this).hasClass('draggable')){
        //         $(this).removeAttr("data-html2canvas-ignore");
        //     }
        // });
        
        var HTML_Width = $(".html-content").width();
        var HTML_Height = _HTML_HEIGHT;
        var top_left_margin = 0;
        
        var PDF_Width = HTML_Width+(top_left_margin*2);
        var PDF_Height = (_PDF_HEIGHT)+(top_left_margin*2);

        //var PDF_Width = HTML_Width+30;
        //var PDF_Height = HTML_Height;

        var canvas_image_width = HTML_Width;
        var canvas_image_height = HTML_Height;
        
        var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
    
        html2canvas($(".html-content")[0],{scale:1.15, x:0,imageTimeout: 0,allowTaint: true,useCORS: true}).then(function(canvas) {
            canvas.getContext('2d');
              
            var imgData = canvas.toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
            pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
            pdf.setFontSize(10);
            pdf.text(_ENV_CODE, 15,15);

            for (var i = 1; i <= totalPDFPages; i++) { 
                pdf.addPage(PDF_Width, PDF_Height);
                pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
                pdf.text(_ENV_CODE, 15,15);
            }
            
            pdf.save(docName);
            $('#txtDownload').text('Download');
        });
    });

	$(document).on('click','.img-sign_view',function(){
		$('#'+$(this).attr('data-id')).toggle();

        clickType = 'signature';
        clickId   = $(this).attr('data-panelid');
	})
	
    $(document).on('click','.img-init_view',function(){
		$('.'+$(this).attr('data-id')).toggle();

        clickType = 'initial';
        clickId   = $(this).attr('data-panelid');
	})
	
	$(document).on("click", "#addInitials", function(){
        var rndSignId   = btoa(Math.random()).substring(0,12);
        var rndInitalId   = btoa(Math.random()).substring(0,12);
        var rndDragId   = btoa(Math.random()).substring(0,12);
        var rndIniPnlId = btoa(Math.random()).substring(0,12);

        $("#holder").prepend('<div data-html2canvas-ignore id="'+rndDragId+'" class="draggable ui-widget-content" style="width: 200px;height: 125px;position: absolute;top:'+(top+70)+'px;"">'+
        '<div class="img-init_view ini_box" data-id="'+rndDragId+'" data-panelId="'+rndIniPnlId+'"><img class="" src="'+asset_url+'images/Initials_icon.png" height="75" width = "75"></div><div id="'+rndSignId+'"class=""></div>'+ //Signature Container
        '<div id="'+rndIniPnlId+'" class="'+rndDragId+' text-center initPanel hide_button"><button data-toggle="tooltip" data-placement="bottom"  title="Delete" class="remove btn border" data-draggable-id="'+rndDragId+'"><i class="fas fa-trash-alt"></i></button>'+
        '<button title="Add People" data-toggle="tooltip" data-placement="bottom"  class="btn border ml-1 assign_inial" data-draggable-id="'+rndDragId+'"><i class="fas fa-users"></i></button>'+
        '<button title="Initial Now" data-signcanvas-id="'+rndSignId+'" data-toggle="tooltip" data-placement="bottom" class="btn ml-1 border add_own" data-text="initial" data-draggable-id="'+rndDragId+'" data-id="'+rndIniPnlId+'"><img height="auto" width = "20px" src="'+$('#hand-free').val()+'" /></button></div></div>');
        //'<button class="doneSign btn btn-success ml-1" id="'+rndDragId+'" data-id="'+rndIniPnlId+'" data-sign-container-id="'+rndInitalId+'">Done</button></div></div>');

        $('#'+rndDragId).draggable({
            containment: '#holder',
				stop: function( event, ui ) {
				var name        = $(this).attr('a_name');
				var id        = $(this).attr('a_id');
				
				if(name){
                    element     = $(this);
                    var initial = name.charAt(0);
                    var ret     = name.split(" ");
                    var str1    = ret[0].charAt(0);
                    var str2    = '';

					if(ret[1]){
						var str2 = ret[1].charAt(0);
					}

					var top  = element.position().top;
					var left = element.position().left;
					
                    for(var i=0;i<signCoordinates.length;i++){
							if(signCoordinates[i].draggable_id == $(this).attr('id')){
							signCoordinates[i]="";
						}	
					}
				
					signCoordinates.push({
						'id':id,
						'X':left,
						'Y':top,
						'type':'assign_field_initial',
						'draggable_id':$(this).attr('id'),
						'lines':str1.toUpperCase()+' '+str2.toUpperCase(),
					});
				}
			}
        });
        
        $('#'+rndSignId).signature();
        $('.hide_button').hide();

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    });

    $(document).on("click", "#addAudio", function(){
        var rndDragId   = btoa(Math.random()).substring(0,12);

        $("#holder").prepend('<div data-html2canvas-ignore id="'+rndDragId+'" class="draggable ui-widget-content" style="min-width:100px; height: 125px;position: absolute;top:'+(top+70)+'px;"">'+
            '<input type="hidden" class="resetPath" data-path-id="'+rndDragId+'" id="audio-'+rndDragId+'" />'+
            '<div class="resizeBox" data-html2canvas-ignore id="resize-'+rndDragId+'"></div>'+
            '<button class="img-init_view text-left btn btn-danger mt-2 mb-2 mr-2 float-left" data-id="'+rndDragId+'">'+
                '<i class="fas fa-record-vinyl"></i>'+
            '</button>'+
            '<button class="text-left mb-2 finalAudio btn btn-success mt-2" data-final-audio="'+rndDragId+'" id="final-audio-'+rndDragId+'" disabled>'+
                '<i class="fas fa-check-circle"></i>'+
            '</button>'+
            '<div class="'+rndDragId+' text-left initPanel hide_button">'+
                '<button data-toggle="tooltip" data-placement="bottom"  title="Delete" class="remove btn border bg-white" data-draggable-id="'+rndDragId+'">'+
                    '<i class="fas fa-trash-alt"></i>'+
                '</button>'+
                '<button class="recordButton btn border ml-1 bg-white" data-recordButton="'+ rndDragId +'" id="recordButton-'+ rndDragId +'"><i class="fas fa-microphone-alt text-success"></i></button>'+
                '<button class="pauseButton  btn border ml-1 bg-white" id="pauseButton-'+ rndDragId +'" disabled><img src="'+asset_url+'images/pause_audio.png" height="22" width="15" /></button>'+
                '<button class="stopButton  btn border ml-1 bg-white" id="stopButton-'+ rndDragId +'" disabled><i class="far fa-stop-circle text-danger"></i></button>'+
                '<button class="previewButton  btn border ml-1 bg-white" id="previewButton-'+ rndDragId +'" disabled><i class="far fa-play-circle text-success"></i></button>'+
            '</div>'+
        '</div>');
        
        $('#'+rndDragId).draggable({
            containment: '#holder',
			stop: function( event, ui ) {
                var resizeElement = $('#resize-'+rndDragId);
                var top  = $(this).position().top;
                var left = $(this).position().left;

                resizeElement.attr('data-top', top);
                resizeElement.attr('data-left', left);
                resizeElement.attr('data-width', resizeElement.width());
                resizeElement.attr('data-height', resizeElement.height());
			}
        });
        $('#resize-'+rndDragId).resizable({
            stop: function(e, ui) {
                var resizeElement = $('#'+rndDragId);
                var top  = resizeElement.position().top;
                var left = resizeElement.position().left;

                $(this).attr('data-top', top);
                $(this).attr('data-left', left);
                $(this).attr('data-width', ui.size.width);
                $(this).attr('data-height', ui.size.height);
            }
        });
        
        $('[id^=recordButton-]').click(function(){
            var recordButtonId = $(this).attr('data-recordButton');
            startRecording(recordButtonId);
        });
        $('[id^=pauseButton-]').click(function(){
            pauseRecording();
        });
        $('[id^=stopButton-]').click(function(){
            stopRecording();
        });
        $('[id^=previewButton-]').click(function(){
            previewRecording();
        });
        $('[id^=final-audio-]').click(function(){
            var a_id = $(this).attr('data-final-audio'); 
            Swal.fire({
                type: 'warning',
                title: "Are you sure want to save Audio?",
                input: 'checkbox',
                inputValue: 1,
                inputPlaceholder: 'Once audio is stored it will not delete.',
                confirmButtonText: 'Save Audio',
                showCancelButton:true,
                inputValidator: (result) => {
                    return !result && 'You need to agree'
                },
            }).then((result) => {
                if (result.value) {
                    saveAudioToServer(a_id);
                    $("#save").attr("disabled", false);
                    $("."+a_id).find('.remove').hide();
                    $("#final-audio-"+a_id).hide();
                    $("#recordButton-"+a_id).hide();
                    $("#pauseButton-"+a_id).hide();
                    $("#stopButton-"+a_id).hide();
                    $("#previewButton-"+a_id).addClass('mt-2');
                }
            });
        });
        

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    });

    function setPathCoordinated(rndDragId, path){
        element = $('#'+rndDragId);
        audioPath = $("#audio-"+rndDragId).val();
        var top  = element.position().top;
        var left = element.position().left;

        var audio_section = $('#resize-'+rndDragId);
        var section_top = audio_section.attr('data-top');
        var section_left = audio_section.attr('data-left');
        var section_width = audio_section.attr('data-width');
        var section_height = audio_section.attr('data-height');

        signCoordinates.push({
            'id':'audio_data',
            'X':left,
            'Y':top,
            'section_top' : section_top,
            'section_left' : section_left,
            'section_width' : section_width,
            'section_height' : section_height,
            'lines':'',
            'path':audioPath,
            'type':'assign_field_audio',
            'draggable_id':$(this).attr('data-draggable-id'),
        });
    }

    $(document).on("click", ".assign_inial", function(){
        $.ajax({
            type:'GET',
            url: getsiteurl()+'/get/buyer_seller',
            data: {'documentId':documentId},
            success: (response) => {
                if(response.status == 'success'){
                    $('#renderAssignFieldModal').empty();
                    $('#renderAssignFieldModal').append(response.result);
                    
                    assFieldModDragId = '';
                    assFieldModDragId = $(this).attr('data-draggable-id');

                    $('#assignFieldModal2').modal('show');
                }else{
                    Swal.fire({
                        type: 'error',
                        title: response.message
                    });
                }
            }
        });
    });


    $(document).on("click", "#saveAssignField2", function(){
        var name        = $("input[name='buySeller']:checked").attr('data-name');
        var buySellerId = $("input[name='buySeller']:checked").val();
        
        // var initial     = name.charAt(0);
        // var ret = name.split(" ");
        // var str1 = ret[0].charAt(0);
        // var str2 = ''; 
        // if(ret[1]){
            // var str2 = ret[1].charAt(0);
        // }
        
        if(name){
            var str     = name;
            var matches = (str.match(/\b(\w)/g)).join('');

            $('#assignFieldModal2').modal('hide');
            $("#"+assFieldModDragId).empty();
            $("#"+assFieldModDragId).attr("data-html2canvas-ignore","true");
            $("#"+assFieldModDragId).append('<div class="text-center mt-1"><h6><b class="text_pointer">'+matches.toUpperCase()+' INITIAL HERE </b></h6></div>');
            $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
            '<button class="btn btn-success ml-1 doneAssField2" data-buy-seller-id="'+buySellerId+'" data-buy-seller-name="'+name+'" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');

            $("#"+assFieldModDragId).css("height","85px");
        }else{
            $('.assFieldModMsg').addClass('text text-danger'); 
            $('.assFieldModMsg').text('Please select people to assign!'); 
        }
    });

    $(document).on("click", ".doneAssField2", function(){
        // var name    = $("input[name='buySeller']:checked").attr('data-name');
        var name    = $(this).attr('data-buy-seller-name');
        var initial = name.charAt(0);
        var ret     = name.split(" ");
        var str1    = ret[0].charAt(0);
        var str2    = '';

        if(ret[1]){
            var str2 = ret[1].charAt(0);
        }

        $("#save").attr("disabled", false);
        id      = $(this).attr('data-buy-seller-id');
        element = $('#'+$(this).attr('data-draggable-id'));
		element.attr('a_name',name);
		element.attr('a_id',id);

        var top  = element.position().top;
        var left = element.position().left;

		for(var i=0;i<signCoordinates.length;i++){
			if(signCoordinates[i].draggable_id == $(this).attr('data-draggable-id')){			 
				signCoordinates[i] = "";
			}
	    }

        signCoordinates.push({
            'id':id,
            'X':(left+40),
            'Y':top,
            'lines':str1.toUpperCase()+' '+str2.toUpperCase(),
            'type':'assign_field_initial',
			'draggable_id':$(this).attr('data-draggable-id'),
        });

        $(element).find('div').last().remove();
        $(element).css({'height':'45px'});
    });

	$(document).on('click','#assign_table tr,#assign_table2 tr',function(){
		 $(this).find('td input:radio').prop('checked', true);
	});

	$(document).on('click','.add_people_modal',function(){
		$('#'+$(this).attr('modal_id')).modal('hide');
		$(".custom-select").select2({
			minimumResultsForSearch : -1
		});

		$('#addPersonModal').modal('show');
		jQuery.validator.addMethod("valid_email", function (value, element) {
        return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
        }, 'Please Enter Valid Email');

		jQuery.validator.addMethod("email_exists", function (value, element) {
            var emailExists = false;

            $.ajax({
                type:'GET',
                url: getsiteurl()+'/buy_sell/email/exists',
                async: false,
                data: {contractId:$('#contractId').val(),email:value,byuSelId:$('#byuSelId').val()},
                success: (response) => {
                    emailExists = !response.status;            
                }
            });

            return emailExists;
        }, 'Email already exists');

    	$("#addPersonFrm").validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    valid_email:true,
                    email_exists:true
                },
                permission: {
                    required: true
                },
               
            },messages: {
                name: {
                    required: "Please Enter Name"
                },
                email: {
                    required: "Please enter Email"
                },
                permission: {
                	required: "Please Select Permission"
                },
               
            },
    		submitHandler: function(form) {
                var formData = new FormData(document.getElementById("addPersonFrm"));

                $.ajax({
                    type:'POST',
                    url: getsiteurl()+'/save/person_new_single',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (response) => {
                        $('#addPersonModal').modal('hide');
                        if(response.status == 'success'){

                            Swal.fire({
                                type: 'success',
                                title: response.message
                            });

    						$('#addPersonModal').modal('hide');
                        }else{
                            Swal.fire({
                                type: 'error',
                                title: response.message
                            });
                        }
                    }
                });
            }
        });
	});

    // Owner Signature
    $(document).on('click','.add_own',function(){
        assFieldModDragId = $(this).attr('data-draggable-id');
        sigCanId          = $(this).attr('data-signcanvas-id');

        if(chooseAdoptSign == false){
            var rndDragId   = btoa(Math.random()).substring(0,12);
            var ownSignId   = btoa(Math.random()).substring(0,12);
            var ownIniId    = btoa(Math.random()).substring(0,12);

            $("#ownSign").append('<div id="'+rndDragId+'" class="ui-widget-content">'+
            '<div class="adoptSignature" data-id="'+ownSignId+'">'+
            '<div id="'+ownSignId+'"></div></div>'+
            '</div>');
            $('#ownSign').signature();

            $("#ownIni").append('<div id="'+rndDragId+'" class="ui-widget-content">'+
            '<div class="adoptInitial" data-id="'+ownIniId+'">'+
            '<div id="'+ownIniId+'"></div></div>'+
            '</div>');
            $('#ownIni').signature();

            displayOwnModal();
        }else{
            clickType = $(this).attr('data-text');
            clickId   = $(this).attr('data-id');

            if(clickType == 'initial'){
                $('#'+clickId).parent().find('img').remove();
                
                if($('#sign_type').val() == 'name'){                    
                    $('#'+assFieldModDragId).html('<span style="display: block; width:100%; text-align:center;">'+$('#intial_text').val()+'</span>').addClass('named_sign').css({'font-family': 'alaskan-malamute-regular'});

                    $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                    '<button class="btn btn-success ml-1 doneOwner" data-type="owner_name" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                }else if($('#sign_type').val() == 'draw'){
                    $('#'+sigCanId).css({'display':'block','margin-left': 'auto','padding-left': '25px'});
                    $('#'+sigCanId).signature('draw', $('#ownIni').signature('toJSON'));
                    $('#'+sigCanId).signature('disable');

                    $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                    '<button class="btn btn-success ml-1 doneOwner" data-type="owner_draw" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                }else if($('#sign_type').val() == 'adopt'){
                    if($('#hidSavedType').val() == 'name'){
                        $('#'+assFieldModDragId).html('<span style="display: block; width:100%; text-align:center;">'+$('#hidSavedIni').val()+'</span>').addClass('named_sign').css({'font-family': 'alaskan-malamute-regular'});

                        $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                        '<button class="btn btn-success ml-1 doneOwner" data-type="owner_name" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                    }else{
                        $('#'+sigCanId).css({'display':'block','margin-left': 'auto','padding-left': '25px'});
                        $('#'+sigCanId).signature('draw', $('#hidSavedIni').val());
                        $('#'+sigCanId).signature('disable');

                        $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                        '<button class="btn btn-success ml-1 doneOwner" data-type="owner_draw" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                    } 
                }

                $('#'+assFieldModDragId+' .initPanel').remove();
            }else if(clickType == 'signature'){
                $('#'+clickId).parent().find('img').remove();

                if($('#sign_type').val() == 'name'){
                    $('#'+assFieldModDragId).append('<span style="display: block;">'+$('#signature_text').val()+'</span>').addClass('named_sign').css({'font-family': 'alaskan-malamute-regular','padding': '10px','margin-left': '-5px','margin-top': '-16px'});

                    $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                    '<button class="btn btn-success ml-1 doneOwner" data-type="owner_name" data-buy-seller-name="'+name+'" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                }else if($('#sign_type').val() == 'draw'){
                    $('#'+sigCanId).css({'display':'block'});
                    $('#'+sigCanId).signature('draw', $('#ownSign').signature('toJSON'));
                    $('#'+sigCanId).signature('disable');

                    $("#"+assFieldModDragId).append('<div class="text-left assignFieldPanel ml-3"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                    '<button class="btn btn-success ml-1 doneOwner" data-type="owner_draw" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                }else if($('#sign_type').val() == 'adopt'){
                    if($('#hidSavedType').val() == 'name'){
                        $('#'+assFieldModDragId).append('<span style="display: block;">'+$('#hidSavedSig').val()+'</span>').addClass('named_sign').css({'font-family': 'alaskan-malamute-regular','padding': '10px','margin-left': '-5px','margin-top': '-16px'});

                        $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                        '<button class="btn btn-success ml-1 doneOwner" data-type="owner_name" data-buy-seller-name="'+name+'" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                    }else{
                        $('#'+sigCanId).css({'display':'block'});
                        $('#'+sigCanId).signature('draw', $('#hidSavedSig').val());
                        $('#'+sigCanId).signature('disable');

                        $("#"+assFieldModDragId).append('<div class="text-left assignFieldPanel ml-3"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                        '<button class="btn btn-success ml-1 doneOwner" data-type="owner_draw" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                    }
                }

                $('#'+assFieldModDragId+' .signPanel').remove();
            }
        }
    });

    function displayOwnModal(){
        resetOwnModal();
        $('#ownModal').modal('show');
    }

    function hideOwnModal(){
        $('#ownModal').modal('hide');
    }

    function resetOwnModal(){
        clearModalSign();
        clearModalIni();
        $('.own_modal_chk').prop('checked', false);
    }

    $(document).on("click", "#ownSave", function(){
        if($(".adopt_tab .active").attr("href") == '#nav-adopt'){
            if($('#signature_text').val() == '' && $('#intial_text').val() == ''){
                $('#ownError').text('Please enter signature and initial text!');
                return false;
            }else if($('#signature_text').val() == ''){
                $('#ownError').text('Please enter signature text!');
                return false;
            }else if($('#intial_text').val() == ''){
                $('#ownError').text('Please enter initial text!');
                return false;
            }
        }else if($(".adopt_tab .active").attr("href") == '#nav-draw'){
            if($('#ownSign').signature('isEmpty') && $('#ownIni').signature('isEmpty')){
                $('#ownError').text('Please draw signature and initial!');
                return false;
            }else if($('#ownSign').signature('isEmpty')){
                $('#ownError').text('Please draw signature!');
                return false;
            }else if($('#ownIni').signature('isEmpty')){
                $('#ownError').text('Please draw initial!');
                return false;
            }
        }else if($(".adopt_tab .active").attr("href") == '#nav-saved-sig-ini'){
            if($('#hidSavedType').val() == ''){
                $('#ownError').text('Please select saved signature & initial!');
                return false;
            }
        }

        if($("#confirmAdopt").prop('checked') == false){
            $("#confirmAdopt").focus();
            $('#ownError').text('Please Confirm Terms!');
            return false;
        }

        $('#ownError').text('');
        hideOwnModal();
        
        if(clickType == 'initial'){
            $('#'+clickId).parent().find('img').remove();
            
            if($('#sign_type').val() == 'name'){
                $('#'+assFieldModDragId).html('<span style="display: block; width:100%; text-align:center;">'+$('#intial_text').val()+'</span>').addClass('named_sign').css({'font-family': 'alaskan-malamute-regular'});

                $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                '<button class="btn btn-success ml-1 doneOwner" data-type="owner_name" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
            }else if($('#sign_type').val() == 'draw'){
                $('#'+sigCanId).css({'display':'block','margin-left': 'auto','padding-left': '25px'});
                $('#'+sigCanId).signature('draw', $('#ownIni').signature('toJSON'));
                $('#'+sigCanId).signature('disable');

                $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                '<button class="btn btn-success ml-1 doneOwner" data-type="owner_draw" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
            }else if($('#sign_type').val() == 'adopt'){
                if($('#hidSavedType').val() == 'name'){
                    $('#'+assFieldModDragId).html('<span style="display: block; width:100%; text-align:center;">'+$('#hidSavedIni').val()+'</span>').addClass('named_sign').css({'font-family': 'alaskan-malamute-regular'});

                    $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                    '<button class="btn btn-success ml-1 doneOwner" data-type="owner_name" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>'); 
                }else{
                    $('#'+sigCanId).css({'display':'block','margin-left': 'auto','padding-left': '25px'});
                    $('#'+sigCanId).signature('draw', $('#hidSavedIni').val());
                    $('#'+sigCanId).signature('disable');

                    $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                    '<button class="btn btn-success ml-1 doneOwner" data-type="owner_draw" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                }
            }

            $('#'+assFieldModDragId+' .initPanel').remove();
        }else if(clickType == 'signature'){
            $('#'+clickId).parent().find('img').remove();

            if($('#sign_type').val() == 'name'){
                $('#'+assFieldModDragId).append('<span style="display: block;">'+$('#signature_text').val()+'</span>').addClass('named_sign').css({'font-family': 'alaskan-malamute-regular','padding': '10px','margin-left': '-5px','margin-top': '-16px'});

                $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                '<button class="btn btn-success ml-1 doneOwner" data-type="owner_name" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
            }else if($('#sign_type').val() == 'draw'){
                $('#'+sigCanId).css({'display':'block'});
                $('#'+sigCanId).signature('draw', $('#ownSign').signature('toJSON'));
                $('#'+sigCanId).signature('disable');

                $("#"+assFieldModDragId).append('<div class="text-left assignFieldPanel ml-3"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                '<button class="btn btn-success ml-1 doneOwner" data-type="owner_draw" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
            }else if($('#sign_type').val() == 'adopt'){
                if($('#hidSavedType').val() == 'name'){
                    $('#'+assFieldModDragId).append('<span style="display: block;">'+$('#hidSavedSig').val()+'</span>').addClass('named_sign').css({'font-family': 'alaskan-malamute-regular','padding': '10px','margin-left': '-5px','margin-top': '-16px'});

                    $("#"+assFieldModDragId).append('<div class="text-center assignFieldPanel"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                    '<button class="btn btn-success ml-1 doneOwner" data-type="owner_name" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');                     
                }else{
                    $('#'+sigCanId).css({'display':'block'});
                    $('#'+sigCanId).signature('draw', $('#hidSavedSig').val());
                    $('#'+sigCanId).signature('disable');

                    $("#"+assFieldModDragId).append('<div class="text-left assignFieldPanel ml-3"><button class="btn btn-danger ml-1 remove" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-trash-alt"></i></button>'+
                    '<button class="btn btn-success ml-1 doneOwner" data-type="owner_draw" data-draggable-id="'+assFieldModDragId+'"><i class="fas fa-check-circle"></i></button></div>');
                }
            }

            $('#'+assFieldModDragId+' .signPanel').remove();
        }
    });

    $(document).on("click",".doneOwner",function(e){
        $("#save").attr("disabled", false);

        var draggableId = $(this).attr('data-draggable-id');
        var lines       = '';
        chooseAdoptSign = true;
        
        element  = $('#'+draggableId);
        var top  = element.position().top;
        var left = element.position().left;

        $('#'+draggableId).css({'height':'auto'});

        if(clickType == 'initial'){
            if($('#sign_type').val() == 'name'){
                lines = $('#intial_text').val();
                left = left + 78;
                top  = top - 10;
            }else if($('#sign_type').val() == 'draw'){
                left = left + 79;
                top  = top + 10;
                lines = $('#ownIni').signature('toJSON');
            }else if($('#sign_type').val() == 'adopt'){
                if($('#hidSavedType').val() == 'name'){
                    lines = $('#hidSavedIni').val();
                    left = left + 78;
                    top  = top - 10;
                }else{
                    left = left + 79;
                    top  = top + 10;
                    lines = $('#hidSavedIni').val();
                }
            }
        }else if(clickType == 'signature'){
            if($('#sign_type').val() == 'name'){
                top = top + 20;
                lines = $('#signature_text').val();
            }else if($('#sign_type').val() == 'draw'){
                top = top + 9;
                lines = $('#ownSign').signature('toJSON');
            }else if($('#sign_type').val() == 'adopt'){
                if($('#hidSavedType').val() == 'name'){
                    top = top + 20;
                    lines = $('#hidSavedSig').val();
                }else{
                    top = top + 9;
                    lines = $('#hidSavedSig').val();
                }
            }
        }

        signCoordinates.push({
            'id':'owner',
            'X':left,
            'Y':top,
            'lines':lines,
            'type':$(this).attr('data-type'),
            'draggable_id':draggableId
        });

        $(this).parent().remove();
        $("#"+draggableId).draggable({ disabled: true });

        if(saveSignIni.length == 0){
            if($('#sign_type').val() == 'name' && $('#saveName').prop("checked") == true){
                saveSignIni.push(
                    $('#sign_type').val(),
                    $('#signature_text').val(),
                    $('#intial_text').val()
                );
            }else if($('#sign_type').val() == 'draw' && $('#saveDraw').prop("checked") == true){
                saveSignIni.push(
                    $('#sign_type').val(),
                    $('#ownSign').signature('toJSON'),
                    $('#ownIni').signature('toJSON')
                );
            }
        }
    });

    $(document).on("keyup","#signature_text",function(e){
        $("#signatureStyle").html($(this).val());
    });

    $(document).on("keyup","#intial_text",function(e){
        $("#initialStyle").html($(this).val());
    });

    $(document).on("click", "#clearSign", function(){
        clearModalSign();
    });

    function clearModalSign(){
        $('#ownSign').signature('clear');
        var myCanvas = $('#ownSign'+' > canvas')[0];
        var ctx = myCanvas.getContext('2d');
        ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    }

    $(document).on("click", "#clearIni", function(){
        clearModalIni();
    });

    function clearModalIni(){
        $('#ownIni').signature('clear');
        var myCanvas = $('#ownIni'+' > canvas')[0];
        var ctx = myCanvas.getContext('2d');
        ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
    }

    $(document).on("click","#nav-adopt-tab",function(e){
        $("#sign_type").val('name');
    });

    $(document).on("click","#nav-draw-tab",function(e){
        $("#sign_type").val('draw');
    });

    $(document).on("click","#nav-saved-sig-ini-tab",function(e){
        $("#sign_type").val('adopt');
    });
    // 

    $(document).on("click",".add_text_container",function(e){
        $('textarea#'+$(this).attr('data-txt-container-id')).focus();
        // document.getElementById($(this).attr('data-txt-container-id')).focus();
    });
});


