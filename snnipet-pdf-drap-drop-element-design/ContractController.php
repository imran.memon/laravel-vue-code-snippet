<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contract;
use App\ContractTransaction;
use App\ContractStatus;
use App\BuyerSeller;
use App\Subscriptions;
use App\Document;
use Response;
use Auth;
use File;
use Validator;
use Redirect;
use App\Recepient;
use App\Traits\ShareDocTrait;
use App\EmailQueue;
use DB;
use Session;
use App\ActivityLog;
use App\DraftDocument;
class ContractController extends Controller
{
    use ShareDocTrait;
    public function index(Request $request){
        if($request->has('searchVal')){
            $searchVal  = $request->input('searchVal');
            $filterType = $request->input('filterType');
            $order      = $request->input('order');

            if($filterType == 1 || $filterType == 2){
                $contracts = Contract::where('owner_id',Auth::user()->id)
                            ->where('name','like','%'.$searchVal.'%')
                            ->orWhere('prop_name','like','%'.$searchVal.'%')
                            ->orderBy('id',$order)
                            ->get();
            }else{
                $conIds    = $this->filterContract($filterType);

                $contracts = Contract::where('owner_id',Auth::user()->id)
                            ->whereIn('id',$conIds)
                            ->where('name','like','%'.$searchVal.'%')
                            ->orWhere('prop_name','like','%'.$searchVal.'%')
                            ->orderBy('id',$order)
                            ->get();
            }
        }else if($request->has('filterType')){
            $filterType = $request->input('filterType');
            $order      = $request->input('order');

            if($filterType == 1 || $filterType == 2){
                $contracts = Contract::where('owner_id',Auth::user()->id)
                            ->orderBy('id',$order)
                            ->get();
            }else{
                $conIds    = $this->filterContract($filterType);

                $contracts = Contract::where('owner_id',Auth::user()->id)
                            ->whereIn('id',$conIds)
                            ->orderBy('id',$order)
                            ->get();
            }
        }else{
            $contracts = Contract::where('owner_id',Auth::user()->id)
                        ->orderBy('id','desc')
                        ->get();
        }

        $conTrans  = ContractTransaction::all();

        foreach($contracts as $contract){
            if(!is_null($contract->contract_transaction_id)){
                $conStatus = ContractStatus::where('contract_transaction_id',$contract->contract_transaction_id)->pluck('status','id')->toArray();

                $contract->status = $conStatus;
            }else{
                $contract->status = null;
            }
        }
        $transactions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')->leftjoin('affiliate_plan_config','subscriptions.plan_id','=','affiliate_plan_config.wp_id')
                ->where('subscriptions.user_id',Auth::user()->id)
                ->select('subscriptions.*','affiliate_plan.plan_name','affiliate_plan.plan_amount','affiliate_plan.plan_type','affiliate_plan_config.plan_month','affiliate_plan_config.affiliate_day','affiliate_plan_config.affiliate_day_type','affiliate_plan_config.is_trial_period','affiliate_plan_config.trial_type',   'affiliate_plan_config.trial_days','affiliate_plan_config.trial_months','affiliate_plan_config.trial_years')
                ->orderBy('subscriptions.id','desc')
                ->first();
       // echo "<pre>";print_r($transactions->toArray());exit;
        $curdate=strtotime(date('Y-m-d'));
        $expires_on=strtotime($transactions->expires_on);
        /*$startmydate=date("d",strtotime($transactions->starts_on));
        $currentyearmonth =date('Y-m');*/

        $startCurrentMonthPlanDate = $transactions->starts_on;
        //echo request()->getHost();exit;
           
        $i = 0;
        $displaystatus = 1;
        if($transactions->expires_on != "Never Expires")
        {
                if($curdate > $expires_on)
                {
                    $displaystatus = 0;
                }else{
                    if(!empty($contracts)){
                        foreach ($contracts as $key => $value) {
                           //  echo "<pre>";print_r($value['id']);
                            if($transactions->affiliate_day_type != 'Date'){
                             $mailcount = EmailQueue::where('contract_id',$value['id'])->where('created_at','>=',$startCurrentMonthPlanDate)->orderBy('created_at','desc')->count();
                                 $i = $i + $mailcount;  
                            }else{
                                $mailcount = EmailQueue::where('contract_id',$value['id'])->where('created_at','>=',$startCurrentMonthPlanDate)->where('created_at','<=',$transactions->affiliate_day)->orderBy('created_at','desc')->count();
                                 $i = $i + $mailcount;
                            }
                        }
                    }
                   // echo $i; exit;
                    if($i >= $transactions->plan_month){
                         $displaystatus = 0;
                    }else{
                         $displaystatus = 1;
                    }
                }
        }

        //
        // Subscriptions
        if($transactions && $transactions->plan_type == 'recurring' && $transactions->is_trial_period){
            $duration     = 0;
            $trialEndDate = null;
            $startsOn     = $transactions->starts_on;

            if($transactions->trial_type == 'D'){
                $duration     = $transactions->trial_days;
                $trialEndDate = date('Y-m-d', strtotime($startsOn. ' + '.$duration.' days')); 
            }else if($transactions->trial_type == 'M'){
                $duration = $transactions->trial_months;
                $trialEndDate = date('Y-m-d', strtotime($startsOn. ' + '.$duration.' months')); 
            }else if($transactions->trial_type == 'Y'){
                $duration = $transactions->trial_years;
                $trialEndDate = date('Y-m-d', strtotime($startsOn. ' + '.$duration.' years')); 
            }

            # UNCOMMENT BELOW SECTION IF NOT ALLOWED TO ACCESS CONTRACT IN TRIAL PERIOD
            // if(!is_null($trialEndDate) && date('Y-m-d') <= $trialEndDate){
            //     return view('member.contracts.trial_period');
            // }
        }

        if($transactions->plan_type == 'free' || $transactions->plan_type == 'paid_infinite'){
            $displaystatus = 1;
        }
        if($request->has('searchVal') || $request->has('filterType')){
            $result = view('member.contracts.listing')->with('contracts',$contracts)
            ->with('conTrans',$conTrans)->render();

            return Response::json(array(
                'result' => $result
            ));
        }else{
    	   return view('member.contracts.index')->with('contracts',$contracts)->with('conTrans',$conTrans)->with('displaystatus',$displaystatus);
        }
    }

    public function filterContract($filterType){
        $completed = $pending = array();
        $contracts = Contract::where('owner_id',Auth::user()->id)
                    ->orderBy('id','desc')
                    ->get();

        foreach($contracts as $key => $contract){
            $documents    = Document::where('contract_id',$contract->id)->get();

            if(count($documents) > 0){
                foreach($documents as $document){
                    if(is_null($document->assign_field_coordinates)){
                        $allSignDone = false;
                        break;
                    }else{
                        $allSignDone = true;
                        $coordinates = json_decode($document->assign_field_coordinates,true);

                        if(count($coordinates) > 0){        
                            foreach($coordinates as $coordinate){
                                if((isset($coordinate['isSign']) && $coordinate['isSign']  == false)){
                                    $allSignDone = false;
                                    break;        
                                }
                            }
                        }
                    }
                }
                
                if(!$allSignDone){
                    $pending[] = $contract->id;
                }else{
                    $completed[] = $contract->id;
                }
            }
        }

        return ($filterType == 3 || $filterType == 4)?$completed:$pending;
    }

    public function getContractStatus(Request $request){
        try{
            $conId = $request->input('conId');

            $contract = Contract::find($conId);
            $contract->contract_transaction_id = $request->input('conTranId');
            $contract->contract_status_id      = null;
            $contract->save();

            $conStatus = ContractStatus::where('contract_transaction_id',$request->input('conTranId'))->pluck('status','id')->toArray();
            $contract->status = $conStatus;

            $result    = view('member.contracts.con_status')->with('contract',$contract)->with('conId',$conId)->render();

            return Response::json(array(
                'status'  => 'success',
                'message' => 'Transaction Type Changed Successfully!',
                'result'  => $result
            ));
        }catch(\Exception $e) {
            return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong, please try again!'));
        }
    }

    public function saveContractStatus(Request $request){
        try{
            $contract = Contract::find($request->input('conId'));
            $contract->contract_status_id = $request->input('conStatId');
            $contract->save();

            return Response::json(array(
                'status'  => 'success',
                'message' => 'Status Changed Successfully!'
            ));
        }catch(\Exception $e) {
            return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong, please try again!'));
        }
    }

    public function savePrice(Request $request){
        try{
            $contract = Contract::find($request->input('conId'));
            $contract->price = $request->input('price');
            $contract->save();

            return Response::json(array(
                'status'  => 'success',
                'message' => 'Sale Price Saved Successfully!'
            ));
        }catch(\Exception $e) {
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong, while save price please try again!'
            ));
        }
    }
    // 

    public function add(){
    	return view('member.contracts.add');
    }
    public function select_draft($id)
    {
        Session::put('draftId', $id);
        return view('member.contracts.add');
    }
    public function save(Request $request){
        try{
            $obj                   = new Contract();
            $obj->owner_id         = Auth::user()->id;
            $obj->name             = $request->input('cname');
            $obj->prop_name        = $request->input('pname');
            $obj->prop_address1    = $request->input('add1');
            $obj->prop_address2    = $request->input('add2');
            $obj->prop_country     = $request->input('country');
            $obj->prop_state       = $request->input('state');
            $obj->prop_city        = $request->input('city');
            $obj->prop_postal_code = $request->input('postalCode');

           
            $path = public_path('contract');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }
 
            if($request->input('photo') != 'undefined'){
                // $extension    = $request->photo->extension();
                $extension       = $request->photo->getClientOriginalExtension();
                $fileName        = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.'.$extension;

                $request->photo->move(public_path('contract'), $fileName);
                $obj->prop_photo = $fileName;
            }

            $obj->save(); 
            $data['contract_id'] = $obj->id;

            $draft = DraftDocument::find($request->draft_id);
            if($draft && $request->draft_id != ""){
                $document                           = new Document();
                $document->draft_id                 = $request->draft_id; 
                $document->assign_field_coordinates = $draft->assign_field_coordinates;
                $document->path                     = $draft->path;   
                $document->contract_id              = $obj->id;
                $document->save();     
            }

            return Response::json(array(
                'status'  =>'success',
                'message' =>'Contract Created Successfully!',
                'data'    => $data 
            ));
        }catch(\Exception $e) {
            return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong while create contract, please try again!'));
        }
    }

    public function edit($id){
        $contract = Contract::where('id',$id)->where('owner_id',Auth::user()->id)->first();
        if($contract){
            $buyerSellers = BuyerSeller::getBuySelByConId($id);
			foreach($buyerSellers as $key=>$value){
				$permission = Recepient::where('contract_id',$contract->id)->where('buyer_seller_id',$value->id)->first();
				if(!empty($permission)){
					$buyerSellers[$key]->permission = $permission->permission_id;
				}else{
					$buyerSellers[$key]->permission = "";
				}
			}

            $documents = Document::getDocumentsByConId($id);
			if(!$documents->isEmpty()){
				foreach($documents as $key=>$value){
                    $documents[$key]->status    = 'Completed';
                    $documents[$key]->assign_to = "";
                    $documents[$key]->signed    = 0;
                    $documents[$key]->total     = 0;
                    $assign_to                  = array();
				
				    $assign_to_all = Recepient::where('contract_id',$contract->id)->where('document_id',$value->id)->where('permission_id',1)->pluck('buyer_seller_id');
				    
                    if(!$assign_to_all->isEmpty()){
                        $user_details_assign = BuyerSeller::whereIn('id',$assign_to_all)->get();
                        if(!$user_details_assign->isEmpty()){
                            foreach($user_details_assign as $u){
                                $assign_to[]=$u->name;
                            }
                        }
				    }

                    $documents[$key]->assign_to = implode(' ,',$assign_to);
                    $recepient_all              = Recepient::where('contract_id',$contract->id)
                                                ->where('document_id',$value->id)
                                                ->where('permission_id',1)
                                                ->pluck('id');
				    
                    if(!$recepient_all->isEmpty()){
                        $total                   = count($recepient_all);
                        $documents[$key]->status = 'Need to Sign';
                        $activity_log            = ActivityLog::whereIn('recepient_id',$recepient_all)
                                                ->get();
                        $signed                  = array();
					
                        if(!$activity_log->isEmpty()){
                            foreach($activity_log as $ac){
                                if($ac->activity == "Signed" && !in_array($ac->recepient_id,$signed)){
                                    $signed[] = $ac->recepient_id;
                                }
						      }
					    }

    					$documents[$key]->signed = count($signed);
    					$documents[$key]->total = count($recepient_all);
					   
                        if(count($signed) == count($recepient_all)){
                            $documents[$key]->status = 'Completed';
					    }else{
                            $documents[$key]->signed = count($signed);
                            $documents[$key]->total  = count($recepient_all);
                            if(count($signed) == 0 &&  count($recepient_all) > 0){
                                $documents[$key]->status = 'Need to Sign';
                            }else{
                                $name = array();
                                $get_unsigned = ActivityLog::whereIn('recepient_id',$recepient_all)->whereNotIn('recepient_id',$signed)->get();
                                
                                foreach($get_unsigned as $ac){
                                    $recepit_details = Recepient::find($ac->recepient_id);
                                    if(!empty($recepit_details)){
                                        $user_details = BuyerSeller::find($recepit_details->buyer_seller_id);
                                        
                                        if(!empty($user_details)){
                                            $name[]=$user_details->name;
                                        }
                                    }
                                }
                                $documents[$key]->status = 'Waiting for '.implode(', ',$name);
                            }
                        }
				    }
				    
                    $this->sigCountNameStatus($value);
				}
			}
					
            $conTrans     = ContractTransaction::all();
            if(!is_null($contract->contract_transaction_id)){
                $conStatus = ContractStatus::where('contract_transaction_id',$contract->contract_transaction_id)->pluck('status','id')->toArray();

                $contract->status = $conStatus;
            }else{
                $contract->status = null;
            }

            return view('member.contracts.edit')->with('contractId',$id)->with('buyerSellers',$buyerSellers)->with('documents',$documents)->with('contract',$contract)->with('conTrans',$conTrans);
        }else{
            return abort(401);
        }
    }
    public function addDraft(){
        return view('member.contracts.draft_edit');
    }
    
    public function sigCountNameStatus($value){
        $value->actualAssignSign = 0;
        $value->actualSignDone   = 0;
        $value->status           = 'Need to Sign';
        $value->assign_to        = '';
        $assignNames = $waitingNames = array();

        // Signature Count & Name
        if(!is_null($value->assign_field_coordinates)){
            $coordinates = json_decode($value->assign_field_coordinates,true);
            
            if(count($coordinates) > 0){
                foreach($coordinates as $coordinate){
                    if(isset($coordinate['isSign'])){
                        $value->actualAssignSign = $value->actualAssignSign + 1;

                        $buySell = BuyerSeller::find($coordinate['buyer_seller_id']);
                        $assignNames[] = ($buySell)?$buySell->name:'';
                    }

                    if(isset($coordinate['isSign']) && $coordinate['isSign'] == true){
                        $value->actualSignDone = $value->actualSignDone + 1;
                    }
                }
                $value->assign_to = implode(', ',array_unique($assignNames));
            }
        }

        // Status and Waiting Names
        if(($value->actualAssignSign > 0 && $value->actualSignDone > 0) && ($value->actualAssignSign == $value->actualSignDone)){
            $value->status = 'Completed';
        }else if($value->actualAssignSign != 0){
            $coordinates = json_decode($value->assign_field_coordinates,true);

            if(count($coordinates) > 0){
                foreach($coordinates as $coordinate){
                    if(isset($coordinate['isSign']) && $coordinate['isSign'] == false){
                        $buySell        = BuyerSeller::find($coordinate['buyer_seller_id']);
                        $waitingNames[] = ($buySell)?$buySell->name:'';
                    }
                }
                $value->status = 'Waiting for '.implode(', ',array_unique($waitingNames));
            }
        }

        // 
        $totalPerson = $donePerson = array();
        if(!is_null($value->assign_field_coordinates)){
            $coordinates = json_decode($value->assign_field_coordinates,true);
            if(count($coordinates) > 0){
                foreach($coordinates as $coordinate){
                    if(isset($coordinate['isSign'])){
                        $totalPerson[] = $coordinate['buyer_seller_id'];
                    }

                    if(isset($coordinate['isSign']) && $coordinate['isSign'] == true){
                        $donePerson[] = $coordinate['buyer_seller_id'];
                    }
                }
            }
        }
        $value->donePerson  = count(array_unique($donePerson));
        $value->totalPerson = count(array_unique($totalPerson));

        return $value;
    }

    public function buySellEmailExists(Request $request){
        $byuSelId   = $request->input('byuSelId');
        $email      = $request->input('email');
        $contractId = $request->input('contractId');
        $check      = true;
        $status     = false;

        if($byuSelId != ''){
            $data     = BuyerSeller::find($byuSelId);
            $oldEmail = $data->email;

            if($oldEmail == $email){
                $check = false;
            }
        }

        if($check){
            $buyerSeller = BuyerSeller::isEmailExists($contractId,$email);
            $status      = ($buyerSeller)?true:false;
        }

        return Response::json(array(
            'status' => $status
        ));
    }

    public function savePerson(Request $request){
        try{
            $obj = (!empty($request->input('byuSelId')))?BuyerSeller::find($request->input('byuSelId')):new BuyerSeller();
            $obj->contract_id = $request->input('contractId');
            $obj->name        = $request->input('name');
            $obj->email       = $request->input('email');
            $obj->address1    = $request->input('add1');
            $obj->address2    = $request->input('add2');
            $obj->country     = $request->input('country');
            $obj->state       = $request->input('state');
            $obj->city        = $request->input('city');
            $obj->postal_code = $request->input('postalCode');
            $obj->role_id     = $request->input('role');
            
            $path = public_path('buyer_seller');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            if($request->has('photo')){
                $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.'.$request->photo->extension();
                $request->photo->move(public_path('buyer_seller'), $fileName);
                $obj->photo = $fileName;
            }
            $obj->save();

            $buyerSellers = BuyerSeller::getBuySelByConId($request->input('contractId'));
            $result       = view('member.contracts.buyer_seller_listing')->with('buyerSellers',$buyerSellers)
            ->render();

            $message = (!empty($request->input('byuSelId')))?'Person Updated Successfully!':'Person Added Successfully!';

            return Response::json(array(
                'status'  => 'success',
                'message' => $message,
                'result'  => $result
            ));
        }catch(\Exception $e) {
            return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong while add person, please try again!'));
        }
    }
	 public function savePersonNewSingle(Request $request){
        try{
           
			$obj = new BuyerSeller();
			$obj->contract_id = $request->input('contractId');
			$obj->name        = $request->input('name');
			$obj->email       = $request->input('email');
			$obj->save();
			
			$related_documents = Document::where('contract_id',$request->input('contractId'))->get();
			
				if(!$related_documents->isEmpty()){
				foreach($related_documents as $doc){
				
                $recepient        = new Recepient();
                $recepient->token = md5(uniqid(rand(), true));
				$recepient->contract_id     = $request->input('contractId');
				$recepient->document_id     = $doc->id;
				$recepient->buyer_seller_id = $obj->id;
				$recepient->permission_id   = $request->input('permission');
				$recepient->save();
			

				}
			}
			
			
            $message = (!empty($request->input('byuSelId')))?'Person Updated Successfully!':'Person Added Successfully!';

            return Response::json(array(
                'status'  => 'success',
                'message' => $message,
                
            ));
        }catch(\Exception $e) {
            return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong while add person, please try again!'));
        }
    }
	 public function savePersonNew(Request $request){
		 //ini_set("memory_limit", "-1");
		 //set_time_limit(0);
        try{
         
			
			$name = $request->input('name');
			$related_documents = Document::where('contract_id',$request->input('contractId'))->get();
			$subject = config('constant.sendDocEmailSub');
			for($i=0;$i<count($name);$i++){
				$str = "existing_id_".$i;
				
				if(isset($request->$str)){
					$obj = BuyerSeller::find($request->$str);
				}else{
					$obj = new BuyerSeller();
				}
				//$obj = new BuyerSeller();
				$obj->contract_id = $request->input('contractId');
				$obj->name        = $name[$i];
				$obj->email       = $request->input('email')[$i];
				$obj->save();
				if(!empty($related_documents)){
				foreach($related_documents as $doc){
				$recepient = Recepient::where('contract_id',$request->input('contractId'))->where('document_id',$doc->id)
				->where('buyer_seller_id',$obj->id)->first();

            if(!$recepient){
                $recepient        = new Recepient();
                $recepient->token = md5(uniqid(rand(), true));
            }

            $recepient->contract_id     = $request->input('contractId');
            $recepient->document_id     = $doc->id;
            $recepient->buyer_seller_id = $obj->id;
            $recepient->permission_id   = $request->input('permission')[$i];
            $recepient->save();
			/*$toEmail   = $request->input('email')[$i];
            $result = $this->documentEmail($toEmail,$recepient->token,$subject);

                $isEmailSend = EmailQueue::where('contract_id',$request->input('contractId'))
                ->where('document_id',$doc->id)
                ->where('recepient_id',$recepient->id)
                ->where('status','Sent')

                if(!$isEmailSend){
                    $obj_mail               = new EmailQueue();
                    $obj_mail->contract_id  = $request->input('contractId');
                    $obj_mail->document_id  = $doc->id;
                    $obj_mail->recepient_id = $recepient->id;
                    $obj_mail->subject      = $subject;
                    $obj_mail->content      = $result['content'];
                    $obj_mail->status       = ($result['status'] == 'success')?'Sent':'Fail';
                    $obj_mail->save();
                }*/

				}
			}
		
			}
			if($related_documents->isEmpty()){
				return redirect('/edit/contract/'.$request->input('contractId'));
			}else{
				return redirect('/prepare/document/'.$related_documents[0]->id);
			}
			

       }catch(\Exception $e) {
            //return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong while add person, please try again!'));
			 echo $e->getMessage();
			 exit;
        }
                
    }
		
	public function delete_receipient(Request $request){
		
			$id = $request->input('id');
			$contract_id = $request->input('contract_id');
			BuyerSeller::find($id)->delete();
			Recepient::where('contract_id',$contract_id)->where('buyer_seller_id',$id)->delete();
		
	
	}
    public function editPerson(Request $request){
        $id = $request->input('id');
        $buyerSeller = BuyerSeller::find($id);

        $image = (!is_null($buyerSeller->photo))?asset('buyer_seller/'.$buyerSeller->photo):asset('images/no-image.png');

        return Response::json(array(
            'status' => 'success',
            'data'   => $buyerSeller,
            'image'  => $image
        ));
    }

    public function deletePerson(Request $request){
        $obj        = BuyerSeller::find($request->input('id'));
        $contractId = $obj->contract_id;
        $obj->delete();

        $buyerSellers = BuyerSeller::getBuySelByConId($contractId);
        $result       = view('member.contracts.buyer_seller_listing')->with('buyerSellers',$buyerSellers)
        ->render();

        return Response::json(array(
            'status'  => 'success',
            'message' => 'Person Deleted Successfully!',
            'result'  => $result
        ));
    }

    public function saveDocument(Request $request){
        try{
            $path = public_path('document');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            $file     = $request->document->getClientOriginalName();
            $fileName = pathinfo($file, PATHINFO_FILENAME).'_'.str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.'.$request->document->extension();
            $request->document->move(public_path('document'), $fileName);

            $obj              = new Document();
            $obj->contract_id = $request->input('contractId');
            $obj->path        = $fileName;
            $obj->save();

            $documents = Document::getDocumentsByConId($request->input('contractId'));
			if(!$documents->isEmpty()){
				foreach($documents as $key=>$value){
				$documents[$key]->status = 'Completed';
				$documents[$key]->assign_to = "";
				$documents[$key]->signed = 0;
				$documents[$key]->total = 0;
				$assign_to = array();
				
				$assign_to_all = Recepient::where('contract_id',$request->input('contractId'))->where('document_id',$value->id)->where('permission_id',1)->pluck('buyer_seller_id');
				if(!$assign_to_all->isEmpty()){
					$user_details_assign = BuyerSeller::whereIn('id',$assign_to_all)->get();
					if(!$user_details_assign->isEmpty()){
						foreach($user_details_assign as $u){
								$assign_to[]=$u->name;
						}
					
					}
				}
				$documents[$key]->assign_to = implode(' ,',$assign_to);
				$recepient_all = Recepient::where('contract_id',$request->input('contractId'))->where('document_id',$value->id)->where('permission_id',1)->pluck('id');
				if(!$recepient_all->isEmpty()){
					$total = count($recepient_all);
					$documents[$key]->status = 'Need to Sign';
					$activity_log = ActivityLog::whereIn('recepient_id',$recepient_all)->get();
					$signed = array();
					if(!$activity_log->isEmpty()){
						foreach($activity_log as $ac){
							if($ac->activity == "Signed" && !in_array($ac->recepient_id,$signed)){
									$signed[] = $ac->recepient_id;
							}
						}
					}
					$documents[$key]->signed = count($signed);
					$documents[$key]->total = count($recepient_all);
					if(count($signed) == count($recepient_all)){
						
						$documents[$key]->status = 'Completed';
					}else{
						$documents[$key]->signed = count($signed);
						$documents[$key]->total = count($recepient_all);
						if(count($signed) == 0 &&  count($recepient_all) > 0){
						$documents[$key]->status = 'Need to Sign';
						}else{
							$name = array();
							$get_unsigned = ActivityLog::whereIn('recepient_id',$recepient_all)->whereNotIn('recepient_id',$signed)->get();
							foreach($get_unsigned as $ac){
								$recepit_details = Recepient::find($ac->recepient_id);
								if(!empty($recepit_details)){
									$user_details = BuyerSeller::find($recepit_details->buyer_seller_id);
									if(!empty($user_details)){
										$name[]=$user_details->name;
									}
								}
							}
							$documents[$key]->status = 'Waiting for '.implode(', ',$name);
							
						}
					}
				}
                
                $this->sigCountNameStatus($value);
				
				}
				
			}



            $result    = view('member.contracts.document_listing')->with('documents',$documents)->render();

            return Response::json(array(
                'status'  => 'success',
                'message' => 'Document Uploaded Successfully!',
                'result'  => $result 
            ));
        }catch(\Exception $e) {
            return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong while upload document, please try again!'));
        }
    }
    public function saveDraft(Request $request){
        try{
            $path = public_path('document');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            $file     = $request->draft->getClientOriginalName();
            $fileName = pathinfo($file, PATHINFO_FILENAME).'_'.str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.'.$request->draft->extension();
            $request->draft->move(public_path('document'), $fileName);

            $obj              = new DraftDocument();
            $obj->path        = $fileName;
            $obj->owner_id    = Auth::user()->id;
            $obj->save();

            $result = view('member.contracts.editable_drafts_list')->with('obj',$obj)->render();

            return Response::json(array(
                'status'   => 'success',
                'message'  => 'Document Uploaded Successfully!',
                'draft_id' => $obj->id,
                'result'   => $result
            ));
        }catch(\Exception $e) {
            return Response::json(array(
                'status'  =>'error',
                'message' =>'Opps! Something went wrong while upload document, please try again!'
            ));
        }
    }

    public function deleteDocument(Request $request){
        $obj        = Document::find($request->input('id'));
        $contractId = $obj->contract_id;
        $obj->delete();

        $documents = Document::getDocumentsByConId($contractId);
		if(!$documents->isEmpty()){
				foreach($documents as $key=>$value){
				$documents[$key]->status = 'Completed';
				$documents[$key]->assign_to = "";
				$documents[$key]->signed = 0;
				$documents[$key]->total = 0;
				$assign_to = array();
				
				$assign_to_all = Recepient::where('contract_id',$contractId)->where('document_id',$value->id)->where('permission_id',1)->pluck('buyer_seller_id');
				if(!$assign_to_all->isEmpty()){
					$user_details_assign = BuyerSeller::whereIn('id',$assign_to_all)->get();
					if(!$user_details_assign->isEmpty()){
						foreach($user_details_assign as $u){
								$assign_to[]=$u->name;
						}
					
					}
				}
				$documents[$key]->assign_to = implode(' ,',$assign_to);
				$recepient_all = Recepient::where('contract_id',$contractId)->where('document_id',$value->id)->where('permission_id',1)->pluck('id');
				if(!$recepient_all->isEmpty()){
					$total = count($recepient_all);
					$documents[$key]->status = 'Need to Sign';
					$activity_log = ActivityLog::whereIn('recepient_id',$recepient_all)->get();
					$signed = array();
					if(!$activity_log->isEmpty()){
						foreach($activity_log as $ac){
							if($ac->activity == "Signed" && !in_array($ac->recepient_id,$signed)){
									$signed[] = $ac->recepient_id;
							}
						}
					}
					$documents[$key]->signed = count($signed);
					$documents[$key]->total = count($recepient_all);
					if(count($signed) == count($recepient_all)){
						
						$documents[$key]->status = 'Completed';
					}else{
						$documents[$key]->signed = count($signed);
						$documents[$key]->total = count($recepient_all);
						if(count($signed) == 0 &&  count($recepient_all) > 0){
						$documents[$key]->status = 'Need to Sign';
						}else{
							$name = array();
							$get_unsigned = ActivityLog::whereIn('recepient_id',$recepient_all)->whereNotIn('recepient_id',$signed)->get();
							foreach($get_unsigned as $ac){
								$recepit_details = Recepient::find($ac->recepient_id);
								if(!empty($recepit_details)){
									$user_details = BuyerSeller::find($recepit_details->buyer_seller_id);
									if(!empty($user_details)){
										$name[]=$user_details->name;
									}
								}
							}
							$documents[$key]->status = 'Waiting for '.implode(', ',$name);
							
						}
					}
				}
				
				}
				
			}
        $result    = view('member.contracts.document_listing')->with('documents',$documents)->render();

        return Response::json(array(
            'status'  => 'success',
            'message' => 'Document Deleted Successfully!',
            'result'  => $result
        ));
    }
    public function viewDownloadDraft($id){
        $draft = DraftDocument::find($id);
        return view('member.contracts.prepare_draft')->with('draft',$draft);
    }
    
    public function viewDownloadDocument($type,$id){
        $document = Document::find($id);
        $contract = Contract::find($document->contract_id);

        if(!$contract){
            return abort(401);
        }

        $userId = $contract->owner_id;
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");

        $allcontracts = Contract::where('owner_id',$userId)
                        ->orderBy('id','desc')
                        ->get();

        $transactions = Subscriptions::leftjoin('affiliate_plan','subscriptions.plan_id','=','affiliate_plan.wp_id')
                    ->leftjoin('affiliate_plan_config','subscriptions.plan_id','=','affiliate_plan_config.wp_id')
                    ->where('subscriptions.user_id',$userId)
                    ->select('subscriptions.*','affiliate_plan.plan_name','affiliate_plan.plan_amount','affiliate_plan.plan_type','affiliate_plan_config.plan_month','affiliate_plan_config.affiliate_day','affiliate_plan_config.affiliate_day_type','affiliate_plan_config.audio_feature')
                    ->orderBy('subscriptions.id','desc')
                    ->first();

        $curdate                   =strtotime(date('Y-m-d'));
        $expires_on                =strtotime($transactions->expires_on);
        $startCurrentMonthPlanDate = $transactions->starts_on;
        $i = 0;
        $displaystatus = 1;

        if($transactions->expires_on != "Never Expires"){
            if($curdate > $expires_on){
                $displaystatus = 0;
            }else{
                if(!empty($allcontracts)){
                    foreach ($allcontracts as $key => $value){
                        if($transactions->affiliate_day_type != 'Date'){
                            $mailcount = EmailQueue::where('contract_id',$value['id'])->where('created_at','>=',$startCurrentMonthPlanDate)->orderBy('created_at','desc')->count();
                            $i = $i + $mailcount;  
                        }else{
                            $mailcount = EmailQueue::where('contract_id',$value['id'])->where('created_at','>=',$startCurrentMonthPlanDate)->where('created_at','<=',$transactions->affiliate_day)->orderBy('created_at','desc')->count();
                            $i = $i + $mailcount;
                        }
                    }
                }

                if($i >= $transactions->plan_month){
                     $displaystatus = 0;
                }else{
                     $displaystatus = 1;
                }
            }
        }   

        if($transactions->plan_type == 'free' || $transactions->plan_type == 'paid_infinite'){
            $displaystatus = 1;
        }

        if($type == 'prepare'){
            $this->checkRecipentAssign($contract->id,$document->id);
        }

        $disAudio = ($transactions && $transactions->audio_feature)?true:false;

        return view('member.contracts.prepare')->with('document',$document)->with('type',$type)->with('displaystatus',$displaystatus)->with('contract',$contract)->with('disAudio',$disAudio);
    }
    public function getDraftDocuments()
    {
        $userId=Auth::user()->id;
        $data['drafts'] =  DB::table('draft_documents as dd')
                          ->leftjoin('document as doc','doc.id','=','dd.document_id')
                          ->select('*')->get();
        return view('member.contacts.draft_list',$data);

    }
    public function checkRecipentAssign($contractId,$documentId){
        $buyerSeller = BuyerSeller::getBuySelByConId($contractId);

        foreach($buyerSeller as $value){
            $recipent = Recepient::where('contract_id',$contractId)
                        ->where('document_id',$documentId)
                        ->where('buyer_seller_id',$value->id)
                        ->first();

            if(!$recipent){
                $obj                  = new Recepient();
                $obj->contract_id     = $contractId;
                $obj->document_id     = $documentId;
                $obj->buyer_seller_id = $value->id;
                $obj->permission_id   = Recepient::getRecPermission($contractId,$value->id);
                $obj->token           = md5(uniqid(rand(), true));
                $obj->save();
            }
        }
        
        return true;
    }

    public function view($id){
        $contract = Contract::where('id',$id)->where('owner_id',Auth::user()->id)->first();
        
        if($contract){
            return view('member.contracts.view')->with('contract',$contract);
        }else{
            return abort(401);
        }
    }

    public function updatePropInfo(Request $request){
        try{
            $rules = array(
                'cname'      => 'required',
                'pname'      => 'required',
                'add1'       => 'required',
                'country'    => 'required|regex:/^[\pL\s\-]+$/u',
                'state'      => 'required|regex:/^[\pL\s\-]+$/u',
                'city'       => 'required|regex:/^[\pL\s\-]+$/u',
                'postalCode' => 'required|numeric',
                'photo'      => 'mimes:jpeg,jpg,bmp,svg,png'
            );

            $messages = [
                'regex' => 'The :attribute field contain alphabet only.',
            ];

            $validator = Validator::make($request->all(),$rules,$messages);

            if($validator->fails()){
                $errors = $validator->messages();     
                return Redirect::back()->withInput()->withErrors($errors);
            }

            $obj                   = Contract::find($request->input('contractId'));
            $obj->name             = $request->input('cname');
            $obj->prop_name        = $request->input('pname');
            $obj->prop_address1    = $request->input('add1');
            $obj->prop_address2    = $request->input('add2');
            $obj->prop_country     = $request->input('country');
            $obj->prop_state       = $request->input('state');
            $obj->prop_city        = $request->input('city');
            $obj->prop_postal_code = $request->input('postalCode');

            $path = public_path('contract');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            if($request->has('photo')){
                $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.'.$request->photo->extension();
                $request->photo->move(public_path('contract'), $fileName);
                $obj->prop_photo = $fileName;
            }
            $obj->save();

            return Redirect::back()->with('success','Property Information Updated Successfully!');
        }catch(\Exception $e) {
            return Redirect::back()->with('error','Opps! Something went wrong while update property information, please try again!');
        }
    }
	public function add_new_receipient(Request $request){
		$total_count = $request->last_id;
		echo view('member.contracts.add_new_receipient',compact('total_count'))->render();
	}

    public function draft_list(){
        $drafts = DraftDocument::where('owner_id',Auth::user()->id)
                ->whereNotNull('draft_name')
                ->get();
                
        return view('member.contracts.draft_list')->with('drafts',$drafts);
    }

    public function deleteContract($cid){
        try{
            Contract::find($cid)->delete();
            return Redirect::back()->with('success','Contract Deleted Successfully.');
        }catch(\Exception $e) {
            return Redirect::back()->with('error','Something went wrong while delete contract,Please try again...');
        }
    }
}
