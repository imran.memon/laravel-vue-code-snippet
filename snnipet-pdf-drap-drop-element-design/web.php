<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Testing Routes 
Route::get('/web_rtc_demo',function(){
	return view('web_rtc_demo');
});

Route::get('/test_mail/{email}','TestController@sendTestMail');
Route::get('/test/php_mailer/{email}','TestController@sendMailWithPHPMailer');
Route::get('/track/test_mail/{id}','TestController@getImage');
Route::post('test/save/pdf','TestController@savePDF');
// 

// Skip Trace
Route::get('purchase/skip_trace/credit/{wpUserId?}', 'StripePaymentController@index');
Route::post('process/payment', 'StripePaymentController@processPayment');
// 

// Public Routes
Auth::routes(['register' => false]);

// For Mobile App
Route::get('app/password/reset/{token}','API\ResetPasswordController@appShowResetForm');
Route::post('app/password/reset','API\ResetPasswordController@appRestPass');
Route::get('app/contracts/{wpuid}','Auth\LoginController@appLoginForContract');
// 

Route::get('track/shared/document/{token}','DocumentController@trackSharedDoc');
Route::get('view/shared/document/{token}','DocumentController@viewSharedDoc');
Route::get('rec_sign/disclosure/{docOwnerId}','DocumentController@recSignDisclosure');
Route::post('update/documents','DocumentController@updateDocument');

Route::post('all/rec/sign_done','DocumentController@sendAllRecSignDoneEmail');
Route::get('success/sign','DocumentController@successSign');
Route::get('get/coordinates','DocumentController@getCoordinates');
Route::get('audio/listen','DocumentController@audioListen');

// View,Prepare,Download
Route::get('{type}/document/{id}', 'ContractController@viewDownloadDocument');

// To reset Password for migrated users only
Route::get('set/new/password/{WPUserId}','Auth\ResetPasswordController@setNewPass');
Route::post('save/set/password','Auth\ResetPasswordController@saveSetNewPass');

Route::get('/', function () {
    return view('auth.login');
});
// 

Route::group(['middleware'=>['auth','CheckUserStatus','preventBackHistory']],function(){
	Route::get('/','HomeController@index')->name('home');
	Route::get('home','HomeController@index')->name('home');

	// Route::get('/data_table/fetch_recent_member','HomeController@fetchRecentMember');
	// Route::get('/data_table/fetch_recent_referral','HomeController@fetchRecentReferral');
	// Route::get('/data_table/fetch_recent_payment','HomeController@fetchRecentPayment');
	
	Route::get('/member-details', 'UserController@getMemberDetails')->name('member-details');
	// Route::get('cancel_payment/{planId}', 'UserController@cancelPayment')->name('cancelPayment');
	Route::post('cancel_payment', 'UserController@cancelPayment')->name('cancelPayment');
	// Route::get('/affiliate-link', 'UserController@getAffiliateLink')->name('affiliate-link');
	Route::match(['GET','POST'],'/affiliate-link','UserController@getAffiliateLink')->name('affiliate-link');
	Route::get('/profile', 'HomeController@profile')->name('profile');
	Route::post('/save-profile', 'HomeController@saveProfile')->name('save-profile');
	
	// 
	Route::get('contracts', 'ContractController@index');
	Route::get('get/contract_status', 'ContractController@getContractStatus');
	Route::get('save/contract_status', 'ContractController@saveContractStatus');
	Route::get('save/price', 'ContractController@savePrice');
	Route::get('search/contract', 'ContractController@index');
	Route::get('filter/contract', 'ContractController@index');
	Route::get('delete/contracts/{cid}','ContractController@deleteContract');

	Route::get('add/contracts', 'ContractController@add');
	Route::post('save/contracts', 'ContractController@save');
	Route::post('/add_new_receipient','ContractController@add_new_receipient');

	Route::get('edit/contract/{id}', 'ContractController@edit');
	Route::get('buy_sell/email/exists', 'ContractController@buySellEmailExists');
	Route::post('save/person', 'ContractController@savePerson');
	Route::post('save/person_new', 'ContractController@savePersonNew');
	Route::post('save/person_new_single', 'ContractController@savePersonNewSingle');
	Route::post('delete_receipient','ContractController@delete_receipient');
	Route::get('edit/person', 'ContractController@editPerson');
	Route::get('delete/person', 'ContractController@deletePerson');

	Route::post('save/document', 'ContractController@saveDocument');
	Route::get('delete/document', 'ContractController@deleteDocument');

	Route::get('get/document','DocumentController@getDocument');
	Route::get('save/documents/permission','DocumentController@saveDocumentPermission');
	Route::get('send/documents/email','DocumentController@sendDocEmail');
	Route::get('get/buyer_seller','DocumentController@getBuyerSeller');

	// 
	Route::get('view/contract/{id}','ContractController@view');
	Route::post('update/property_info','ContractController@updatePropInfo');

	// 
	Route::get('view/activity/{contractId}','ActivityController@index');
	Route::get('signature/summery','ActivityController@signSummery');

	Route::get('/wordpress', function(){
		require('../../index.php');
		return loginUser(Auth::user()->username);
		});


	Route::post('saveAudio','DocumentController@saveAudio')->name('saveAudio');

	Route::get('draft_list','ContractController@draft_list')->name('draft_list');
	Route::get('select_draft/{id}','ContractController@select_draft');
	Route::get('add_draft','ContractController@addDraft');
	Route::post('save/draft', 'ContractController@saveDraft');
	Route::get('prepare_draft/{id}','ContractController@viewDownloadDraft');
	Route::post('save/draft_documents','DocumentController@saveDraftDocuments');
});

// Admin Routes
Route::group(['middleware'=>['auth','preventBackHistory'], 'prefix' => 'sa'],function(){
	Route::get('/','HomeController@index')->name('home');
	Route::get('home','HomeController@index')->name('home');

	// 
	Route::match(['GET','POST'],'manage_members','UserController@manageMembers');
	Route::get('cancel/member/sub/{uId}/{planId}','UserController@cancelMemSub');
	Route::get('view/memeber/{id}','UserController@viewMember');
	Route::get('edit/memeber/{id}','UserController@editMember');
	Route::post('save/member/{id}','UserController@saveProfile');
	Route::get('member/referral_code','UserController@referralCode');

	// 
	Route::match(['GET','POST'],'manage_affiliate','UserController@manageAffiliate')->name('manageAffiliate');
	Route::get('delete/affiliate/user','UserController@deleteAffiliate');
	Route::get('member/moveto/free','UserController@moveToFree');
	Route::get('change/aff_status','UserController@changeAffStatus');
	Route::get('converted/users','UserController@convertedUsers');
	Route::match(['GET','POST'],'manage_affiliate/payment/{affId}/{filSDate?}/{filEDate?}','UserController@affPayment');
	Route::get('manage_affiliate/provide_commission','UserController@provideComm');
	
	// 
	Route::match(['GET','POST'],'manage_affiliate_comm','UserController@manageAffiliate')->name('manageAffiliateComm');

	// 
	Route::match(['GET','POST'],'manage_referral','UserController@manageReferral');
	Route::get('manage_referral/commision','UserController@manageReferralCommission');
	Route::get('manage_referral/delete','UserController@manageReferralDelete');

	// 
	Route::get('affiliate_setting','AffiliateSettingController@index');
	Route::post('affiliate_setting/save','AffiliateSettingController@save');
	
	// 
	Route::get('manage_user_commision','AffiliatePlanController@index');
	Route::get('synch/commision','AffiliatePlanController@synchCommision');
	Route::get('manage_user_commision/add/{WPPlanId}','AffiliatePlanController@add');
	Route::post('manage_user_commision/save','AffiliatePlanController@saveUpdate');
	Route::get('manage_user_commision/edit/{WPPlanId}','AffiliatePlanController@edit');
	Route::post('manage_user_commision/update','AffiliatePlanController@saveUpdate');
	Route::get('manage_user_commision/commision/{type}','AffiliatePlanController@userCommision');

	Route::get('plan_configuration','AffiliatePlanController@planConfiguration');
	Route::get('plan_configuration/edit/{WPPlanId}','AffiliatePlanController@planConfigurationEdit');
	Route::post('plan_configuration/update','AffiliatePlanController@planConfigurationSaveUpdate');

	// 
	Route::get('payment_history','UserController@getPaymentHistory');
	Route::get('view/payment_history/{transactionId}','UserController@viewPaymentHistory');
	Route::get('delete/payment_history/{transactionId}','UserController@deletePaymentHistory');

	// 
	Route::get('profile','UserController@profile');
	Route::post('profile/save','UserController@saveProfile');

	// 
	Route::get('skip_trace/{id?}','UserController@viewSkipTraceInfo');
});

