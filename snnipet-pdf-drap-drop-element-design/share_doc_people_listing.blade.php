<table class="table table-striped table-valign-middle">
    <thead>
        <tr>
            <th></th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Permission</th>
        </tr>
    </thead>
    <tbody>
        @if(count($buyerSellers) == 0)
            <tr>
                <td colspan="5" class="text-center">{{'No Data Found'}}</td>
            </tr>
        @else
            @foreach($buyerSellers as $buyerSeller)
                <tr>
                    <td></td>
                    <td>{{$buyerSeller->name}}</td>
                    <td>{{$buyerSeller->email}}</td>
                    <td>
                        @if($buyerSeller->role_id == 0)
                            {{'Buyer'}}
                        @else
                            {{'Seller'}}
                        @endif
                    </td>
                    <td>
                        <select class="custom-select permission" name="permission" id="{{$buyerSeller->id}}" data-cid="{{$buyerSeller->contract_id}}">
                            <option value="">Select Permission</option>
                            @foreach(config('constant.docPermission') as $id => $permission)
                                @if(!is_null($buyerSeller->permission) && $buyerSeller->permission == $id)
                                    <option value="{{$id}}" selected="selected">{{$permission}}</option>
                                @else
                                    <option value="{{$id}}">{{$permission}}</option>
                                @endif
                            @endforeach
                        </select>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>