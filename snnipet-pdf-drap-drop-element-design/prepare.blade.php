@include('member.contracts.prepare_header')

<body>
    <input type="hidden" name="hand-free" id="hand-free" value="{{asset('images/hand-free.png')}}" />

    <div id="loader" class="center_loader" style="display: none;">
        <img class="img-fluid" src="{{asset('images/anydeal-loader.gif')}}" />    
    </div> 

    <div class="sticky_header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-8">
                    @if($type == 'prepare' || $type == 'view')
                        <a href="{{url('edit/contract/'.$document->contract_id)}}" class="btn btn-secondary bg-green" id="back_button">
                            <i class="fas fa-arrow-circle-left"></i> <span class="btnText">Back</span>
                        </a>
                    @endif

                    <button type="button" class="btn btn-info bg-orange" name="download" id="download">
                        <i class="fas fa-download"></i> <span id="txtDownload" class="btnText">Download</span>
                    </button>
                    
                    @if($type == 'prepare')
                        <button type="button" class="btn btn-info bg-orange" name="addText" id="addText">
                            <i class="fas fa-plus"></i> <span class="btnText">Add Text</span>
                        </button>
                        
                        <button type="button" class="btn btn-info bg-orange" name="addSign" id="addSign"> 
                            <i class="fas fa-pencil"></i> <span class="btnText">Add Signature</span>
                        </button>
                        
                        <button type="button" class="btn btn-info bg-orange" name="addInitials" id="addInitials">
                            <i class="fas fa-font"></i> <span class="btnText">Add Initials</span>
                        </button>

                        @if($disAudio)
                            <button type="button" class="btn btn-info bg-orange" name="addAudio" id="addAudio">
                                <i class="fas fa-headphones"></i> <span class="btnText">Add Audio</span>
                            </button>
                        @endif
                       
                        <section class="experiment recordrtc">
                            <ol id="recordingsList"></ol>
                        </section>
                    @endif
                </div>

                @if($type == 'prepare')
                    <div class="col-sm-6 col-4 text-right">
                        <button type="button" class="btn btn-success" name="save" id="save" disabled="disabled">
                            <i class="fas fa-save"></i> <span class="btnText">Save</span>
                        </button>
                        
                        <button type="button" href="{{ url('/member-details') }}" class="btn btn-warning" name="share" id="share" disabled="disabled">
                            <i class="fas fa-share"></i> <span class="btnText">Share</span>
                        </button>
                    </div>
                @endif
            </div>
    	</div>
    </div>
        
    <div class="container-fluid blueBg">
        <span class="mt-1 text text-muted text-primary" id="stopDraw" style="display: none;">
            Double Click To Stop Drawing Digital Signature!
        </span>

        <input type="hidden" name="path" id="path" value="{{asset('document/'.$document->path)}}"  />
        <input type="hidden" name="docName" id="docName" value="{{$document->path}}"  />
        <input type="hidden" name="documentId" id="documentId" value="{{$document->id}}"  />
    	<input type="hidden" id="created_at" value="{{date('Y',strtotime($document->created_at))}}"/>
            
        <div class="html-content" id="holder"></div>
        <input type="hidden" name="displaystatus" id="displaystatus" value="{{$displaystatus}}">
        
        <div class="modal" tabindex="-1" role="dialog" id="documentcount">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <?php $urlRedirect = request()->getHost()."/anydeal/"; ?>
                        <div class="text-center" style="font-size:16px">
                            Your have created maximum contracts for this month. please upgrade your plan from below link <br/> <a href="{{url('../../')}}" style="color:red"><u>Upgrade Plan</u></a> 
                        </div>
                    </div>
                </div>
              </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div id="renderAssignFieldModal">
        @include('member.contracts.assign_field') 
    </div>

    <div id="ownModal" class="modal fade bd-example-modal-md" tabindex="-1">
        <div class="modal-dialog modal-md mx-auto" style="margin-top: 4.4rem;">
            <div class="modal-content rounded-0 border-0  bg-light">
                <div class="modal-header justify-content-center border-0 p-1">
                    <div class="text-center">
                        <div>
                            @if($contract && !is_null($contract->prop_photo))
                                <img class=" img-fluid" src="{{asset('contract/'.$contract->prop_photo)}}"  width="100" />
                            @else
                                <img class=" img-fluid" src="{{asset('images/no_property.png')}}"  width="100" />
                            @endif
                        </div>
                        <span>{{($contract)?$contract->name:''}}</span>
                    </div>
                </div>

                <input type="hidden" name="sign_type" id="sign_type" value="name">

                <div class="modal-body mb-4 mx-lg-3 bg-white p-lg-2">
                    <span id="ownError" class="text text-danger"></span>
                    <nav>
                        <div class="nav nav-tabs adopt_tab" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-adopt-tab" data-toggle="tab" href="#nav-adopt" role="tab" aria-controls="nav-adopt" aria-selected="true">
                                SELECT STYLE
                            </a>
                            
                            <a class="nav-item nav-link" id="nav-draw-tab" data-toggle="tab" href="#nav-draw" role="tab" aria-controls="nav-draw" aria-selected="false">
                                DRAW
                            </a>

                            <a class="nav-item nav-link" id="nav-saved-sig-ini-tab" data-toggle="tab" href="#nav-saved-sig-ini" role="tab" aria-controls="nav-saved-sig-ini" aria-selected="false">
                                SAVED SIGNATURE & INITIAL
                            </a>
                        </div>
                    </nav>

                    <div class="tab-content mt-2" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-adopt" role="tabpanel" aria-labelledby="nav-adopt-tab">
                            <div class="row justify-content-between">
                                <div class="col-md-9 signature_container">
                                    <p>Signature</p>
                                    <input style="width:100%;" type="text" id="signature_text" name="signature_text" value="{{Auth::user()->name}}" maxlength="20" autocomplete="off" />
                                    <div id="signatureStyle">{{Auth::user()->name}}</div>
                                </div>
                            
                                <div class="col-md-3 initial_container">
                                    <?php
                                        $intial = '';
                                        $words = preg_split("/[\s,_-]+/", Auth::user()->name);
                                        foreach ($words as $w) {
                                            $intial .= $w[0];
                                        }
                                    ?>
                                    <p>Initial</p>
                                    <input style="width:100%;" type="text" id="intial_text" name="intial_text" value="{{$intial}}" maxlength="5" autocomplete="off" />
                                    <div id="initialStyle" style="">{{$intial}}</div>
                                </div>
                            </div>
                            
                            <div class="mt-2">
                                <input type="checkbox" class="own_modal_chk mr-1" id="saveName"/>
                                Save Signature and Initial
                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-draw" role="tabpanel" aria-labelledby="nav-draw-tab">
                            <div class="row">
                                <div class="col-md-7 signature_container">
                                    <p>Signature</p>
                                    <div id="ownSign" class="" style="border: 1px solid #000;">
                                    </div>

                                    <div>
                                        <a href="javascript:void(0);" id="clearSign">Clear</a>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 initial_container">
                                    <p>Initial</p>
                                    <div id="ownIni" class="" style="border: 1px solid #000;width: 100px !important;">
                                    </div>

                                    <a href="javascript:void(0);" id="clearIni">Clear</a>
                                </div>
                            </div>

                            <div class="mt-2">
                                <input type="checkbox" class="own_modal_chk mr-1" id="saveDraw"/>
                                Save Signature and Initial
                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-saved-sig-ini" role="tabpanel" aria-labelledby="nav-saved-sig-ini-tab">
                            <input type="hidden" name="hidSavedType" id="hidSavedType" value="" />
                            <input type="hidden" name="hidSavedSig" id="hidSavedSig" value="" />
                            <input type="hidden" name="hidSavedIni" id="hidSavedIni" value="" />

                            <table class="table table-striped border table-bordered" id="signTable">
                                <tr>
                                    <th>&nbsp;</th>
                                    <th><center>Signature</center></th>
                                    <th><center>Initial</center></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer bg-white justify-content-between">
                    <div class="checkv-ew">
                        <input type="checkbox" class="own_modal_chk" name="confirmAdopt" id="confirmAdopt" value="1" require="require"/>
                        By selecting Adopt and Sign, I agree that the signature and initials will be the electronic representation of my signature and initials for all purposes when I (or my agent) use them on documents, including legally binding contracts - just the same as a pen-and-paper signature or initial.
                    </div>
                    <div>
                        <button type="button" class="btn btn-success" id="ownSave">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

@include('member.contracts.prepare_footer')
