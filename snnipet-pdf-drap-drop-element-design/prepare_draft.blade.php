@include('member.contracts.prepare_header')

<body>
    <div class="sticky_header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <a href="{{route('draft_list')}}" class="btn btn-secondary bg-green" id="back_button">
                        <i class="fas fa-arrow-circle-left"></i> <span class="btnText">Back</span>
                    </a>
                   
                    <button type="button" class="btn btn-info bg-orange" name="download" id="download">
                        <i class="fas fa-download"></i> <span id="txtDownload" class="btnText">Download</span>
                    </button>
                                  
                    <button type="button" class="btn btn-info bg-orange" name="addText" id="addText">
                        <i class="fas fa-plus"></i> <span class="btnText">Add Text</span>
                    </button>
                        
                    <button type="button" class="btn btn-info bg-orange" name="addAudio" id="addAudio">
                        <i class="fas fa-headphones"></i> <span class="btnText">Add Audio</span> 
                    </button>
                    
                    <button type="button" class="btn btn-info bg-orange" name="save_as_draft" id="save_as_draft">
                        <i class="fas fa-save"></i> <span class="draftTxt">Save As Template</span>
                    </button>

                    <section class="experiment recordrtc">
                        <ol id="recordingsList"></ol>
                    </section>
                </div>
            </div>
        </div>
    </div>
        
    <div class="container-fluid blueBg">
        <span class="mt-1 text text-muted text-primary" id="stopDraw" style="display: none;">
            Double Click To Stop Drawing Digital Signature!
        </span>

        <input type="hidden" name="path" id="path" value="{{asset('document/'.$draft->path)}}"  />
        <input type="hidden" name="docName" id="docName" value="{{$draft->path}}"  />
        <input type="hidden" name="draftId" id="draftId" value="{{$draft->id}}"  />
        <input type="hidden" id="created_at" value="{{date('Y',strtotime($draft->created_at))}}"/>
            
        <div class="html-content" id="holder"></div>
       
        <div class="modal" tabindex="-1" role="dialog" id="draftcount">
            <div class="modal-dialog modal-dialog-centered" role="draft">
                <div class="modal-content">
                    <div class="modal-body">
                        <?php $urlRedirect = request()->getHost()."/anydeal/"; ?>
                        <div class="text-center" style="font-size:16px">
                            Your have created maximum contracts for this month. please upgrade your plan from below link <br/> <a href="{{url('../../')}}" style="color:red"><u>Upgrade Plan</u></a> 
                        </div>
                    </div>
                </div>
              </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="renderAssignFieldModal">
        @include('member.contracts.assign_field_drafts') 
    </div>

    <div class="modal fade" id="Draftmodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Save As Template</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <form action="javascript:void(0);" method="post" id="draftFrm">
                    @csrf
                    <div class="modal-body">
                        <label class="col-sm-5 col-form-label">Template Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="draft_name" id="draft_name"  autocomplete="off" />
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="draftSaveBtn">
                            Save
                        </button>
                        
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                   </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal -->

    @include('member.contracts.prepare_footer')