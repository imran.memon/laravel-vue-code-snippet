<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(isset($type) && $type == 'prepare')
        <title>Prepare Document</title>
    @elseif(isset($type) && $type == 'download')
        <title>Download Document</title>
    @else
        <title>View Document</title>
    @endif

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery.signature.css?'.time())}}">
	 <link rel="stylesheet" href="{{ asset('css/all.css?'. time()) }}">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <style type="text/css">
        body,
        #holder{
            /* background-color: #061620; */
            /* background-color: white; */
        }

        .blueBg{
            background-color: #061620;
        }

        #holder{
            padding: 0;    
        }

        .canvas-wrapper{
            margin-bottom: 1px;
        }

        canvas{
            margin: 0 auto;
            display: block;
        }

        .konvajs-content{
            height: auto !important;
            width: auto !important;
        }

        .kbw-signature { 
            /*width: 400px; */
            width: 250px; 
            height: 75px; 
           
        }
        .img-sign_view + .kbw-signature{
            display:none;
        }

        .img-init_view + .kbw-signature {
            display: none;
        }

        .img-sign_view {
            text-align: center;
            margin-bottom: 20px;
            opacity: 1;
        }
		.img-init_view{
            text-align: center;
            margin-bottom: 20px;
            opacity: 1;
        }
		.initPanel{
			display:none;
		}

        .signPanel,.assignFieldPanel,.addTextPanel {
            cursor: -webkit-grab;
        }

        .kbw-signature-disabled{
            opacity: 1;
            margin-top: -35px;
            margin-left: -55px;
        }
        .kbw-signature-disabled canvas{
            transform: scale(0.6);
        }
		#holder .form-control:disabled, #holder .form-control[readonly] {
            background-color: transparent;
            opacity: 1;
            border: 0px;
        }

        .ui-widget-content {
            border: 0px solid transparent;
            background: transparent;
        }

        #title_div{
        	position: absolute;
            margin-left: 5%;
            margin-top: 10px;
            font-size: 10px;
        }

        .sticky_header{
        	position:fixed;
        	width:100%;
        	left:0;
        	top:0;
        	background: #112938;
        	border-bottom: 1px solid #112938;
        	padding : 15px 0;
        	z-index: 9999;
        }

        body{
        	padding-top : 57px;
        }

        signatureChrome-inverse {
            color: #fff;
        }

        .signatureChrome {
            background: 0 0;
            border: none;
            font-size: 11px;
            font-weight: 700;
            line-height: 11px;
            min-width: 140px;
            padding: 0 0 0 25px;
            position: relative;
            text-align: left;
        }

        .signatureChrome:before {
            border-bottom: 2px solid #005cb9;
            border-left: 2px solid #005cb9;
            border-radius: 5px 0 0 5px;
            border-top: 2px solid #005cb9;
            content: "";
            display: block;
            height: 50px;
            left: 0;
            position: absolute;
            top: 4px;
            width: 20px;
        }

        .css-xl91qh-Signature::before {
            border-color: rgb(255, 255, 255);
        }

        img.signatureChrome_signature {
            height: 52px;
            left: -12px;
            margin: -8px 0;
            max-width: 244px;
            position: relative;
        }

        .brightness-zero {
            -webkit-filter: brightness(0) invert(100%)!important;
            filter: brightness(0) invert(100%)!important;
        }

        .signatureChrome_signature {
            display: block;
            font-size: 18px;
            font-weight: 400;
            line-height: 26px;
            margin: 5px 0;
            min-height: 26px;
        }

        .margin-top-table{
        	margin-top : -21px;
        }
        .error{
        	color:red;
        }
        .text_area{
        	resize: both;
        }

        .form-group .select2-container{
            display: block;
        }

        .form-group .select2-selection--single .select2-selection__rendered{ 
            line-height: 34px !important;
        }

        .form-group .select2-container .select2-selection--single {
            height: 35px;
        }

        .select2-container--default .select2-results__option[aria-selected],
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            position: relative;
            padding-left: 1rem;
        }

        .select2-container--default .select2-results__option[aria-selected]:before,
        .select2-container--default .select2-selection--single .select2-selection__rendered:before{
            content: '';   
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            line-height: 1;
            font-family: "Font Awesome 5 Pro";
            font-weight: 900;
            margin-right: 10px;
        } 

        .select2-container--default .select2-results__option[aria-selected]:last-child:before,
        .select2-container--default .select2-selection--single .select2-selection__rendered[title~=Needs]:before{
                content: "\f305";
        }

        .select2-container--default .select2-results__option[aria-selected]:nth-child(2):before,
        .select2-container--default .select2-selection--single .select2-selection__rendered[title~=None]:before{
                content: "\f410";
        }

        .select2-container--default .select2-results__option[aria-selected]:nth-child(3):before,
        .select2-container--default .select2-selection--single .select2-selection__rendered[title~=Receives]:before{
                content: "\f20a";
            }
        
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 30px !important;
        }
        .fixed-top{
        	z-index: 9999;
        }
    	.text_pointer{
    		cursor: pointer;
    	}
    	.modal {
            z-index: 9999;
        }
        
        .bg-green{
            background: rgb(129,175,23);
            background: linear-gradient(90deg, rgba(129,175,23,1) 0%, rgba(74,135,53,1) 100%);
            border: 0px;
        }

        .bg-orange{
            background: rgb(199,64,58);
            background: linear-gradient(90deg, rgba(199,64,58,1) 0%, rgba(233,133,85,1) 100%);
            border: 0px;
            font-size: 12px;
        }

        /*.bg-green:hover, 
        .bg-orange:hover{
            background: #4aa0e6;
            border-color: #3f9ae5;
        }
        */
        p.sign-by{
	    display: none;/*flex*/
            margin-left: 52px;
            margin-top: -22px;
            font-size: 10px;

	}

        /*.sign-border:before{
            border-bottom: 3px solid #8fc544;
            border-left: 3px solid #8fc544;
            border-radius: 15px 0 0 15px;
            border-top: 3px solid #8fc544;
            content: "";
            display: block;
            height: 50px;
            left: -10px;
            position: absolute;
            top: -15px;
            width: 50px;
	}*/
        
        .sign-border.sign-border-none:before{
            display: none;
        }

        .sign-border p.my-sign {
	    display: none;/*flex*/
            margin-left: 56px;
            margin-top: -29px;
            font-size: 10px;
	}

        .select2-container {
            z-index: 9999;
        }

        .html2canvas-container { width: auto !important }

        div#holder {
            width: auto;
            position: relative;
            margin: auto;
            overflow: auto;
        }

        /* CSS FOR AUDIO */
        .experiment, .recordrtc option[disabled], .recordrtc video, .recording-media, .media-container-format, #startRecording {
            display: none;
        }

        /* CSS FOR RESIZE */
        .resizeBox{
            height: 70px;
            width: 70px;
            border: 2px dashed red;
            cursor: move;
        } 
        .cp{
            cursor: pointer;
        }
        .draggable {
            z-index: 999;
        }

        .btnText {
            font-size: 12px;
        }
    

        #loader{
            align-items: center;
            justify-content: center;
        }

        #loader:before {
            content: '';
            position: fixed;
            left: 0;
            background: rgba(255, 255, 255, 0.8);
            z-index: 999999;
            width: 100%;
            height: 100%;
            top: 0;
        }

        #loader img{
            position: fixed;
            z-index: 999999999;
            width: 120px;
            height: 120px;
            top: 40%;
        }

        .addTextPanel .fas.fa-arrows-alt {
            float: right;
        }
        @media (max-width: 768px) {
            .btnText {
                display: none;
            }
            .bg-orange{
                font-size: 20px;
                padding: 3px;
                width: 35px;
            }
        }

        .checkv-ew {
            background-color: #061620;
            color: #fff;
            padding: 10px;
            font-size: 12px;
            display: flex;
            line-height: 16px;
            letter-spacing: 1px;
            font-weight: 300;
            border-radius: 5px;
        }
        .checkv-ew input#confirmAdopt {
            margin-right: 10px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/signature.css?'. time()) }}">
</head>
