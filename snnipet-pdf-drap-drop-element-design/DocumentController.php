<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\BuyerSeller;
use App\Recepient;
use App\EmailQueue;
use App\ActivityLog;
use App\Contract;
use App\User;
use App\DraftDocument;
use Response;
use App\Traits\ShareDocTrait;
use File;
use Auth;

class DocumentController extends Controller
{
    use ShareDocTrait;

    public function getDocument(Request $request){
		$document     = Document::find($request->input('id'));
		$buyerSellers = BuyerSeller::getBuySelByConId($document->contract_id);

        foreach($buyerSellers as $buyerSeller){
            $recepient = Recepient::where('contract_id',$buyerSeller->contract_id)
                        ->where('document_id',$document->id)->where('buyer_seller_id',$buyerSeller->id)
                        ->first('permission_id');

            $buyerSeller->permission = null; 
            if($recepient){
                $buyerSeller->permission = $recepient->permission_id; 
            }
        }

		$result = view('member.contracts.share_doc_people_listing')->with('buyerSellers',$buyerSellers)
		->render();

    	return Response::json(array(
			'status'   => 'success',
			'document' => $document,
			'result'   => $result
        ));
    }

    public function saveDocumentPermission(Request $request){
        try{
            $contractId    = $request->input('contractId');
            $documentId    = $request->input('docId');
            $buyerSellerId = $request->input('buyerSellerId');
            $permissionId  = $request->input('permissionId');
            
            $recepient = Recepient::where('contract_id',$contractId)->where('document_id',$documentId)
            ->where('buyer_seller_id',$buyerSellerId)->first();

            if(!$recepient){
                $recepient        = new Recepient();
                $recepient->token = md5(uniqid(rand(), true));
            }

            $recepient->contract_id     = $contractId;
            $recepient->document_id     = $documentId;
            $recepient->buyer_seller_id = $buyerSellerId;
            $recepient->permission_id   = $permissionId;
            $recepient->save();

        	return Response::json(array(
                'status'  => 'success',
                'message' => 'Permission Saved Successfully!'
            ));
        }catch(\Exception $e) {
            return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong while save permission, please try again!'));
        }
    }

    public function sendDocEmail(Request $request){
        try{
            $user       = Auth::user();
            $document   = Document::find($request->input('docId'));
            $contractId = $document->contract_id;
            
            $buySelIds = array();
            if(!is_null($document->assign_field_coordinates)){
                $coordinates = json_decode($document->assign_field_coordinates,true);
                if(count($coordinates) > 0){
                    foreach($coordinates as $coordinate){
                        if(isset($coordinate['isSign'])){
                            $buySelIds[] = $coordinate['buyer_seller_id'];
                        }
                    }
                }
            }

            $recepients = Recepient::leftjoin('buyer_seller','recepients.buyer_seller_id','=','buyer_seller.id')
                ->whereIn('recepients.buyer_seller_id',$buySelIds)
                ->where('recepients.contract_id',$contractId)
                ->where('recepients.document_id',$document->id)
                ->where('recepients.permission_id','!=',2)
                ->select('recepients.*','buyer_seller.name','buyer_seller.email')
                ->get();

            // if(count($recepients) == 0){
            //     return Response::json(array(
            //         'status'  => 'info',
            //         'message' => 'Please assign permission to send email!'
            //     ));
            // }

            $subject     = config('constant.sendDocEmailSub');
            $emailSended = array();

            foreach($recepients as $recepient){
                $toEmail = $recepient->email;
                
                if(!in_array($toEmail, $emailSended)){
                    $result  = $this->documentEmail($user,$toEmail,$recepient->token,$subject);
                
                    $isEmailSend = EmailQueue::where('contract_id',$contractId)
                    ->where('document_id',$document->id)
                    ->where('recepient_id',$recepient->id)
                    ->where('status','Sent')
                    ->first();

                    if(!$isEmailSend){
                        $obj               = new EmailQueue();
                        $obj->contract_id  = $contractId;
                        $obj->document_id  = $document->id;
                        $obj->recepient_id = $recepient->id;
                        $obj->subject      = $subject;
                        $obj->content      = $result['content'];
                        $obj->status       = ($result['status'] == 'success')?'Sent':'Fail';
                        $obj->save();
                    }
                }

                $emailSended[] = $toEmail;
            }

            return Response::json(array(
                'status'  => 'success',
                'message' => 'Email Send Successfully!'
            ));
        }catch(\Exception $e) {
            return Response::json(array('status'=>'error','message'=>'Opps! Something went wrong while send email, please try again!'));
        }
    }

    public function trackSharedDoc($token){
        $recepient = Recepient::where('token',$token)->first();

        $emailQueue = EmailQueue::where('contract_id',$recepient->contract_id)->where('document_id',$recepient->document_id)->where('recepient_id',$recepient->id)->where('status','Opened')->first();

        if(!$emailQueue){
            $obj               = new EmailQueue();
            $obj->contract_id  = $recepient->contract_id;
            $obj->document_id  = $recepient->document_id;
            $obj->recepient_id = $recepient->id;
            $obj->status       = 'Opened';
            $obj->save();
        }

        return response(file_get_contents(public_path('images/logo.png')))
        ->header('content-type', 'image/png');
    }

    public function viewSharedDoc($token){
	$recepient = Recepient::where('token',$token)->first();

	if(!$recepient){
            return view('member.contracts.not_found')->with('message','Opps! This link is expired.');
        }

        $document  = Document::find($recepient->document_id);

        if(!$recepient || !$document){
            return view('member.contracts.not_found')->with('message','Opps! This link is expired.');
            // return abort(404);
        }
        
        $buyer_seller_name = BuyerSeller::where('id',$recepient->buyer_seller_id)->first();
        $contract          = Contract::find($recepient->contract_id);
        $owner             = User::find($contract->owner_id);

        if($recepient->permission_id == 2){
            return abort(401);
        }

        $actLog = ActivityLog::where('recepient_id',$recepient->id)
        ->where('activity','Clicked')->first();
        if(!$actLog){
            $obj               = new ActivityLog();
            $obj->recepient_id = $recepient->id;
            $obj->activity     = 'Clicked';
            $obj->save();
        }

        $allSign = $this->isAllSignDone($document);

        return view('member.contracts.view_shared_doc')->with('document',$document)
        ->with('recepient',$recepient)->with('buyer_seller_name',$buyer_seller_name->name)->with('contract',$contract)->with('owner',$owner)->with('allSign',$allSign);
    }

    public function isAllSignDone($document){
        $allSign = true;

        if(!is_null($document->assign_field_coordinates)){
            $coordinates = json_decode($document->assign_field_coordinates,true);
            if(count($coordinates) > 0){
                foreach($coordinates as $coordinate){
                    if(isset($coordinate['isSign']) && $coordinate['isSign'] == false){
                        $allSign = false;
                        break;
                    }
                }
            }
        }

        return $allSign;
    }

    public function updateDocument(Request $request){
        // try{
            $documentId = $request->input('documentId');
            $document   = Document::find($documentId);
            $contractId = $document->contract_id;
            $user       = Auth::user();

            // Document Owner Side - Assign Field
            if($request->has('signCoordinates')){
                $signCoordinates = json_decode($request->input('signCoordinates'));
                $saveSignIni     = json_decode($request->input('saveSignIni'));

                if(count($signCoordinates) > 0){
                    foreach($signCoordinates as $key => $signCoordinate){
                        $points = (is_null($document->assign_field_coordinates))?array():json_decode($document->assign_field_coordinates,true);

                        if(!empty($signCoordinate->type)&& $signCoordinate->type == 'sign'){
                            array_push($points,[
                                'buyer_seller_id' => null,
                                'x'               => $signCoordinate->X,
                                'y'               => $signCoordinate->Y,
                                'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                                'isSign'          => true,
                                'lines'           => $signCoordinate->lines,
                                'type'            => 'sign',
                                'date_time'       => date('d/m/Y g:i A')
                            ]);

                            $document->is_allsign_email_send = 0;
                        }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'assign_field'){
                            array_push($points,[
                                'buyer_seller_id' => $signCoordinate->id,
                                'x'               => $signCoordinate->X,
                                'y'               => $signCoordinate->Y,
                                'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                                'isSign'          => false,
                                'lines'           => null,
                                'type'            => 'assign_field',
                                'date_time'       => date('d/m/Y g:i A')
                            ]);

                            $document->is_allsign_email_send = 0;
                        }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'add_text'){
                            array_push($points,[
                                'buyer_seller_id' => null,
                                'x'               => $signCoordinate->X,
                                'y'               => $signCoordinate->Y,
                                'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                                'lines'           => $signCoordinate->lines,
                                'type'            => 'add_text',
                                'date_time'       => date('d/m/Y g:i A')
                            ]);
                        }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'assign_field_initial'){
                            array_push($points,[
                                'buyer_seller_id' => $signCoordinate->id,
                                'x'               => $signCoordinate->X,
                                'y'               => $signCoordinate->Y,
                                'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                                'isSign'          => false,
                                'lines'           => $signCoordinate->lines,
                                'type'            => 'assign_field_initial',
                                'date_time'       => date('d/m/Y g:i A')
                            ]);
                            
                            $document->is_allsign_email_send = 0;
                        }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'assign_field_audio'){
                            array_push($points,[
                                'buyer_seller_id' => $signCoordinate->id,
                                'x'               => $signCoordinate->X,
                                'y'               => $signCoordinate->Y,
                                'section_top'     => $signCoordinate->section_top,
                                'section_left'    => $signCoordinate->section_left,
                                'section_width'   => $signCoordinate->section_width,
                                'section_height'  => $signCoordinate->section_height,
                                'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                                // 'path'            => $signCoordinate->path,
                                'path'            => isset($signCoordinate->path) ? $signCoordinate->path : 'PATH',
                                'type'            => 'assign_field_audio',
                                'date_time'       => date('d/m/Y g:i A')
                            ]);
                            
                            $document->is_allsign_email_send = 0;
                        }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'owner_name'){
                            array_push($points,[
                                'buyer_seller_id' => null,
                                'x'               => $signCoordinate->X,
                                'y'               => $signCoordinate->Y,
                                'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                                'lines'           => $signCoordinate->lines,
                                'type'            => $signCoordinate->type,
                                'date_time'       => date('d/m/Y g:i A')
                            ]);
                        }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'owner_draw'){
                            array_push($points,[
                                'buyer_seller_id' => null,
                                'x'               => $signCoordinate->X,
                                'y'               => $signCoordinate->Y,
                                'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                                'lines'           => $signCoordinate->lines,
                                'type'            => $signCoordinate->type,
                                'date_time'       => date('d/m/Y g:i A')
                            ]);
                        }
                        
                        $document->assign_field_coordinates = json_encode($points);
                        $document->save();
                    }
                }

                if(count($saveSignIni) > 0){
                    $result = (is_null($user->own_sig_ini))?array():json_decode($user->own_sig_ini,true);

                    array_push($result,[
                        'type'      => isset($saveSignIni[0])?$saveSignIni[0]:null,
                        'signature' => isset($saveSignIni[1])?$saveSignIni[1]:null,
                        'initial'   => isset($saveSignIni[2])?$saveSignIni[2]:null
                    ]);

                    $user->own_sig_ini = json_encode($result);
                    $user->save();
                }
            }

            // Recepients Side
            if($request->has('recepientId')){
                $documentId  = $request->input('documentId');
                $recepientId = $request->input('recepientId');
                $signType    = $request->input('signType');

                $recSignText = $request->input('recSignText');
                $recIniText  = $request->input('recIniText');
                $recSignCoor = json_decode($request->input('recSignCoor'),true);
                $recIniCoor  = json_decode($request->input('recIniCoor'),true);

                $recepient   = Recepient::find($recepientId);
                $coordinates = json_decode($document->assign_field_coordinates,true);
                foreach($coordinates as $key => $coordinate){
                    if($recepient->buyer_seller_id == $coordinate['buyer_seller_id']){
                        $coordinates[$key]['isSign']    = true;
                        $coordinates[$key]['lines']     = ($signType == 'name')?null:(($coordinates[$key]['type'] == 'assign_field')?$recSignCoor:$recIniCoor);
                        $coordinates[$key]['date_time'] = date('d/m/Y g:i A');
                        $coordinates[$key]['ipAddress'] = $request->ip();
                    }
                }

                $document->assign_field_coordinates = json_encode($coordinates);
                $document->save();

                $recepient->type      = $signType;
                $recepient->font      = ($signType == 'name')?'alaskan-malamute-regular':null;
                $recepient->signature = ($signType == 'name')?$recSignText:null;
                $recepient->initial   = ($signType == 'name')?$recIniText:null;
                $recepient->save();

                // Activity
                $actLog = ActivityLog::where('recepient_id',$recepientId)
                ->where('activity','Signed')->first();
                if(!$actLog){
                    $obj               = new ActivityLog();
                    $obj->recepient_id = $recepientId;
                    $obj->activity     = 'Signed';
                    $obj->ip_address   = $request->ip();
                    $obj->save(); 
                }

                // Check all recepient done sign
                $updatedDoc     = Document::find($documentId);
                $allRecSignDone = $this->isAllSignDone($document);

                if($allRecSignDone){
                    return Response::json(array(
                        'status'      => 'success',
                        'message'     => 'Document Updated Successfully!',
                        'allsignDone' => 1
                    ));
                } 
            }
            
            return Response::json(array(
                'status'      => 'success',
                'message'     => 'Document Updated Successfully!',
                'allsignDone' => 0
            ));
   //      }catch(\Exception $e) {
			// // $e->getMessage();
   //          return Response::json(array(
   //              'status'  => 'error',
   //              'message' => 'Opps! Something went wrong, please try again!'
   //          ));
   //      }
    }

    public function saveDraftDocuments(Request $request){
      try{
            $draft_id        = $request->input('draft_id');
            $draft           = DraftDocument::find($draft_id);
            $signCoordinates = json_decode($request->input('signCoordinates'));

            if(count($signCoordinates) > 0){
                foreach($signCoordinates as $key => $signCoordinate){
                    $points = (is_null($draft->assign_field_coordinates))?array():json_decode($draft->assign_field_coordinates,true);
                    // $points = array();

                    if(!empty($signCoordinate->type)&& $signCoordinate->type == 'sign'){
                        array_push($points,[
                            'buyer_seller_id' => null,
                            'x'               => $signCoordinate->X,
                            'y'               => $signCoordinate->Y,
                            'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                            'isSign'          => true,
                            'lines'           => $signCoordinate->lines,
                            'type'            => 'sign',
                            'date_time'       => date('d/m/Y g:i A')
                        ]);

                    }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'assign_field'){
                        array_push($points,[
                            'buyer_seller_id' => $signCoordinate->id,
                            'x'               => $signCoordinate->X,
                            'y'               => $signCoordinate->Y,
                            'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                            'isSign'          => false,
                            'lines'           => null,
                            'type'            => 'assign_field',
                            'date_time'       => date('d/m/Y g:i A')
                        ]);

                    }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'add_text'){
                        array_push($points,[
                            'buyer_seller_id' => null,
                            'x'               => $signCoordinate->X,
                            'y'               => $signCoordinate->Y,
                            'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                            'lines'           => $signCoordinate->lines,
                            'type'            => 'add_text',
                            'date_time'       => date('d/m/Y g:i A')
                        ]);
                    }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'assign_field_initial'){
                        array_push($points,[
                            'buyer_seller_id' => $signCoordinate->id,
                            'x'               => $signCoordinate->X,
                            'y'               => $signCoordinate->Y,
                            'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                            'isSign'          => false,
                            'lines'           => $signCoordinate->lines,
                            'type'            => 'assign_field_initial',
                            'date_time'       => date('d/m/Y g:i A')
                        ]);
                        
                    }else if(!empty($signCoordinate->type) && $signCoordinate->type == 'assign_field_audio'){
                        array_push($points,[
                            'buyer_seller_id' => $signCoordinate->id,
                            'x'               => $signCoordinate->X,
                            'y'               => $signCoordinate->Y,
                            'section_top'     => $signCoordinate->section_top,
                            'section_left'    => $signCoordinate->section_left,
                            'section_width'   => $signCoordinate->section_width,
                            'section_height'  => $signCoordinate->section_height,
                            'uniqueId'        => str_pad(mt_rand(1,99999999),7,'0',STR_PAD_LEFT),
                            // 'path'            => $signCoordinate->path,
                            'path'            => isset($signCoordinate->path) ? $signCoordinate->path : 'PATH',
                            'type'            => 'assign_field_audio',
                            'date_time'       => date('d/m/Y g:i A')
                        ]);
                    }

                    $draft->assign_field_coordinates = json_encode($points);
                }
            }
            
            $draft->owner_id   = Auth::user()->id;
            $draft->draft_name = $request->draft_name;
            $draft->save();

            return Response::json(array(
                'status'     => 'success',
                'message'    => 'Template Saved Successfully!',
                'cordinates' => $draft->assign_field_coordinates
            ));
        }catch(\Exception $e) {
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong, please try again!'
            ));
        }  
    }

    public function sendAllRecSignDoneEmail(Request $request){
        try{
            $contractId = $request->input('contractId');
            $documentId = $request->input('documentId');
            $docName    = $request->input('docName');
            $pdfFile    = $request->input('pdfFile');

            // Download Signed Document
            $path = public_path('sign_document');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            $filePath = public_path('sign_document/').$docName;
            // if(!file_exists($filePath)){}
            file_put_contents($filePath,base64_decode($pdfFile));
             

            // Send Signed Document Email
            $subject    = config('constant.docSigned');
            $ownerEmail = $this->getOwnerEmail($contractId);
            // $ownerEmail = 'gaurang.p@tridhya.com';
            $this->allRecSignDone($documentId,$ownerEmail,$subject,$filePath);

            $docRec = Recepient::where('contract_id',$contractId)
                    ->where('document_id',$documentId)
                    ->pluck('buyer_seller_id')
                    ->toArray();

            $docBuySell = BuyerSeller::whereIn('id',$docRec)
                        ->pluck('email')
                        ->toArray();

            $emailsArray = array();
            foreach($docBuySell as $email){
                if(!in_array($email, $emailsArray)){
                    $this->allRecSignDone($documentId,$email,$subject,$filePath);
                    $emailsArray[] = $email; 
                }
            } 
            
            $document   = Document::find($documentId);
            $document->is_allsign_email_send = 1;
            $document->save();

            return Response::json(array(
                'status'  => 'success',
                'message' => 'Document email send successfully!'
            ));
        }catch(\Exception $e) {
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong while send document email'
            ));
        }
    }

    public function getOwnerEmail($contractId){
        $contract = Contract::find($contractId);
        $ownerId  = ($contract)?$contract->owner_id:null;
        $user     = User::find($ownerId);
        
        return ($user)?$user->email:null;
    }

    public function successSign(){
        return view('member.contracts.thank_you');
    }

    public function getBuyerSeller(Request $request){
        try{
            $documentId   = $request->input('documentId');
            $document     = Document::find($documentId);
                $buyerSellers = BuyerSeller::leftjoin('recepients','buyer_seller.id','=','recepients.buyer_seller_id')
                ->where('buyer_seller.contract_id',$document->contract_id)
                ->where('recepients.contract_id',$document->contract_id)
                // ->where('recepients.document_id',$document->id)
                ->where('recepients.permission_id',1)
                ->groupBy('recepients.buyer_seller_id')
                ->get('buyer_seller.*');

                $result = view('member.contracts.assign_field')
                ->with('buyerSellers',$buyerSellers)->with('document',$document)->render();

                return Response::json(array(
                    'status'   => 'success',
                    'result'   => $result
                ));
            
        }catch(\Exception $e) {
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong, please try again!'
            ));
        }
    }

    public function getCoordinates(Request $request){
        try{
            if($request->docId=='')
            {

                $result   = null;
                $draft = DraftDocument::find($request->draft_id);
                if($draft && !is_null($draft->assign_field_coordinates)){
                    $result = json_decode($draft->assign_field_coordinates,true);

                    foreach($result as $index => $value) {
                        $buyerSeller = BuyerSeller::find($value['buyer_seller_id']);
                        $result[$index]['name'] = ($buyerSeller)?$buyerSeller->name:''; 
    					 
                        if($buyerSeller && isset($value['isSign']) && $value['isSign'] == true && $value['lines'] == null){
                            $recepient = Recepient::where('contract_id',$buyerSeller->contract_id)
                                    ->where('document_id',$document->id)
                                    ->where('buyer_seller_id',$buyerSeller->id)
                                    ->first();

                            if($recepient && $recepient->type == 'name'){
                                $result[$index]['font']      = $recepient->font;
                                $result[$index]['txtSigIni'] = ($value['type'] == 'assign_field')?$recepient->signature:$recepient->initial;
                            }
                        }
                    }
                }
            }
            else{
                  $result   = null;
                $document = Document::find($request->docId);

                if($document && !is_null($document->assign_field_coordinates)){
                    $result = json_decode($document->assign_field_coordinates,true);

                    foreach($result as $index => $value) {
                        $buyerSeller = BuyerSeller::find($value['buyer_seller_id']);
                        $result[$index]['name'] = ($buyerSeller)?$buyerSeller->name:''; 
                         
                        if($buyerSeller && isset($value['isSign']) && $value['isSign'] == true && $value['lines'] == null){
                            $recepient = Recepient::where('contract_id',$buyerSeller->contract_id)
                                    ->where('document_id',$document->id)
                                    ->where('buyer_seller_id',$buyerSeller->id)
                                    ->first();

                            if($recepient && $recepient->type == 'name'){
                                $result[$index]['font']      = $recepient->font;
                                $result[$index]['txtSigIni'] = ($value['type'] == 'assign_field')?$recepient->signature:$recepient->initial;
                            }
                        }
                    }
                }
            }

            $lastSigIni = null; 
            if(Auth::check() && !is_null(Auth::user()->own_sig_ini)){
                $info       = json_decode(Auth::user()->own_sig_ini,true);
                $lastSigIni = (count($info) > 0)?array_pop($info):null;
            }

            return Response::json(array(
                'status'       => 'success',
                'data'         => $result,
                'savedSignIni' => $lastSigIni
            ));
        }catch(\Exception $e) {
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong, while fetch coordinates!'
            ));
        }
    }

    public function recSignDisclosure($docOwnerId){
        $user = User::find($docOwnerId);
        return view('member.contracts.sign_disclosure')->with('user',$user);
    }

    public function saveAudioBKP(Request $request){
    	$data = $request->all();
        
    	if (!isset($data['audio-filename']) || empty($data['audio-filename'])) {
	        $result = 'Empty file name.';
	        return Response::json(array(
	            'status' => 'error',
	            'data'   =>  $result
	        ));
	    }
	    
	    $fileName = '';
	    $tempName = '';
	    $file_idx = '';
	    
	    if (!empty($data['audio-blob'])) {
	        $file_idx = 'audio-blob';
	        $fileName = $data['audio-filename'];
	        $tempName = $data[$file_idx]->getPathName();
	    }
	    
	    if (empty($fileName) || empty($tempName)) {
	        if(empty($tempName)) {
	            echo 'Invalid temp_name: '.$tempName;
	            return;
	        }

	        $result = 'Invalid file name: '.$fileName;
	        return Response::json(array(
	            'status' => 'error',
	            'data'   =>  $result
	        ));
	    }

	    /*
	    $upload_max_filesize = return_bytes(ini_get('upload_max_filesize'));

	    if ($data[$file_idx]['size'] > $upload_max_filesize) {
	       echo 'upload_max_filesize exceeded.';
	       return;
	    }

	    $post_max_size = return_bytes(ini_get('post_max_size'));

	    if ($data[$file_idx]['size'] > $post_max_size) {
	       echo 'post_max_size exceeded.';
	       return;
	    }
	    */

	    $filePath = 'audio_uploads/';
	    
	    // make sure that one can upload only allowed audio/video files
	    $allowed = array(
	        'webm',
	        'wav',
	        'mp4',
	        'mkv',
	        'mp3',
	        'ogg'
	    );

	    // $extension = pathinfo($filePath, PATHINFO_EXTENSION);
	    $extension = $data[$file_idx]->extension();
	    if (!$extension || empty($extension) || !in_array($extension, $allowed)) {
	        $result = 'Invalid file extension: '.$extension;
	        return Response::json(array(
	            'status' => 'error',
	            'data'   =>  $result
	        ));
	    }
	    
	    // if (!move_uploaded_file($tempName, $filePath)) {
	    if (!$data[$file_idx]->move($filePath, $fileName)) {
	        if(!empty($data["file"]["error"])) {
	            $listOfErrors = array(
	                '1' => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
	                '2' => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
	                '3' => 'The uploaded file was only partially uploaded.',
	                '4' => 'No file was uploaded.',
	                '6' => 'Missing a temporary folder. Introduced in PHP 5.0.3.',
	                '7' => 'Failed to write file to disk. Introduced in PHP 5.1.0.',
	                '8' => 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help.'
	            );
	            $error = $data["file"]["error"];

	            if(!empty($listOfErrors[$error])) {
	                $result = $listOfErrors[$error];
	            }
	            else {
	                $result = 'Not uploaded because of error #'.$data["file"]["error"];
	            }
	        }
	        else {
	            $result = 'Problem saving file: '.$tempName;
	        }
	        return Response::json(array(
	            'status' => 'error',
	            'data'   =>  $result
	        ));
	    }
	    
		return Response::json(array(
            'status' => 'success',
            'data'   =>  $fileName
        ));
    }

    public function saveAudio(Request $request){
        $data = $request->all();
        
        // echo "<pre>"; var_dump($data); exit;
        $fileName = '';
        $tempName = '';
        $file_idx = '';
        
        if (!empty($data['audio_data'])) {
            $file_idx = 'audio_data';
            $fileName = $data['audio_filename'];
            $tempName = $data[$file_idx]->getPathName();
        }
        
        if (empty($fileName) || empty($tempName)) {
            if(empty($tempName)) {
                echo 'Invalid temp_name: '.$tempName;
                return;
            }

            $result = 'Invalid file name: '.$fileName;
            return Response::json(array(
                'status' => 'error',
                'data'   =>  $result
            ));
        }

        $filePath = 'audio_uploads/';
        
        // make sure that one can upload only allowed audio/video files
        $allowed = array(
            'webm',
            'wav',
            'mp4',
            'mkv',
            'mp3',
            'ogg'
        );

        // $extension = pathinfo($filePath, PATHINFO_EXTENSION);
        $extension = $data[$file_idx]->extension();
        if (!$extension || empty($extension) || !in_array($extension, $allowed)) {
            $result = 'Invalid file extension: '.$extension;
            return Response::json(array(
                'status' => 'error',
                'data'   =>  $result
            ));
        }
        
        // if (!move_uploaded_file($tempName, $filePath)) {
        if (!$data[$file_idx]->move($filePath, $fileName)) {
            if(!empty($data["file"]["error"])) {
                $listOfErrors = array(
                    '1' => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
                    '2' => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
                    '3' => 'The uploaded file was only partially uploaded.',
                    '4' => 'No file was uploaded.',
                    '6' => 'Missing a temporary folder. Introduced in PHP 5.0.3.',
                    '7' => 'Failed to write file to disk. Introduced in PHP 5.1.0.',
                    '8' => 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help.'
                );
                $error = $data["file"]["error"];

                if(!empty($listOfErrors[$error])) {
                    $result = $listOfErrors[$error];
                }
                else {
                    $result = 'Not uploaded because of error #'.$data["file"]["error"];
                }
            }
            else {
                $result = 'Problem saving file: '.$tempName;
            }
            return Response::json(array(
                'status' => 'error',
                'data'   =>  $result
            ));
        }
        
        return Response::json(array(
            'status' => 'success',
            'data'   =>  $fileName
        ));
    }

    public function audioListen(Request $request){
        $recepientId = $request->input('recepientId');
    
        $actLog = ActivityLog::where('recepient_id',$recepientId)
        ->where('activity','audio_listen')->first();
        if(!$actLog){
            $obj               = new ActivityLog();
            $obj->recepient_id = $recepientId;
            $obj->activity     = 'audio_listen';
            $obj->ip_address   = $request->ip();
            $obj->save();
        }

        return Response::json(array(
            'status'  => 'success',
            'message' =>  'Audio Listen Status Successfully Updated.'
        ));
    }
}
	
