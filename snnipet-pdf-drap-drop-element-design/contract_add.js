$(document).ready(function() {
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	var current_fs, next_fs, previous_fs;
    var opacity;

    $(".next").click(function(e){
        e.preventDefault();

        var form = $("#newContractFrm");
        form.validate({
            rules: {
                cname: {
                    required: true
                },
                pname: {
                    required: true
                },
                add1: {
                    required: true
                },
                country: {
                    required: true
                },
                state: {
                    required: true
                },
                city: {
                    required: true
                },
                postalCode: {
                    required: true
                }
            },messages: {
                cname: {
                    required: "Please Enter Contract Name"
                },
                pname: {
                    required: "Please Enter Property Name"
                },
                add1: {
                    required: "Please Enter Address Line 1"
                },
                country: {
                    required: 'Please Enter Country'
                },
                state: {
                    required: 'Please Enter State'
                },
                city: {
                    required: 'Please Enter City'
                },
                postalCode: {
                    required: 'Please Enter Postal Code'
                }
            }
        });

        if (form.valid() === true){
            current_fs = $(this).parent();
            next_fs    = $(this).parent().next();
            var isSave = $(this).attr('data-id');

            if(isSave == 1){
                var formData = new FormData();
                formData.append('cname', $('#cname').val());
                formData.append('url_name', $('#url_name').val());
                formData.append('draft_id', $('#draft_id').val());
                formData.append('pname', $('#pname').val());
                formData.append('add1', $('#add1').val());
                formData.append('add2', $('#add2').val());
                formData.append('country', $('#country').val());
                formData.append('state', $('#state').val());
                formData.append('city', $('#city').val());
                formData.append('postalCode', $('#postalCode').val());
                formData.append('photo', $('#photo').prop('files')[0]);

                $.ajax({
                    type:'POST',
                    url: getsiteurl()+'/save/contracts',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (response) => {
                        if(response.status == 'success'){
                            var contractId = response.data.contract_id;
                            window.location.href = getsiteurl()+'/edit/contract/'+contractId;
                            // moveToNextStep(next_fs,current_fs,opacity);
                        }else{
                            Swal.fire({
                                type: 'error',
                                title: response.message
                            });
                        }
                    }
                });
            }else{
                moveToNextStep(next_fs,current_fs,opacity);
            }
        }
    });

    function moveToNextStep(next_fs,current_fs,opacity){
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        next_fs.show();
        current_fs.animate({opacity: 0}, {
        step: function(now) {
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            
            next_fs.css({'opacity': opacity});
        },duration: 600});
    }

    $(".previous").click(function(e){
        e.preventDefault();
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        previous_fs.show();

        current_fs.animate({opacity: 0}, {
        step: function(now) {
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            previous_fs.css({'opacity': opacity});
        },duration: 600});
    });

    $('.radio-group .radio').click(function(){
        $(this).parent().find('.radio').removeClass('selected');
        $(this).addClass('selected');
    });

    $(".submit").click(function(){
        return false;
    });

    $(".dropzone").change(function() {
        readFile(this);
    });

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            if(input.files[0].type.indexOf("image")==-1){
                Swal.fire({
                    type: 'error',
                    title: 'Please Select Image!'
                });
            }else if(input.files[0].size > 2097152) {
                // 2 MB
                Swal.fire({
                    type: 'warning',
                    title: 'Please select less than 2 MB file!'
                });
            }else{
                reader.onload = function(e) {
                    var htmlPreview ='<img height="100%" width="500" src="' + e.target.result + '" /> <a href="javascript:void(0);" id="clear"><i class="fas fa-times" ></i></a>';
                    var wrapperZone = $(input).parent();
                    var previewZone = $(input).parent().parent().find('.preview-zone');
                    var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

                    wrapperZone.removeClass('dragover');
                    previewZone.removeClass('hidden');
                    $('.preview-zone').css({'display':'block'});
                    boxZone.empty();
                    boxZone.append(htmlPreview);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    }

    $('.dropzone-wrapper').on('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('dragover');
    });

    $('.dropzone-wrapper').on('dragleave', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('dragover');
    });

    // Clear Selected Photo
    $(document).on("click", "#clear", function(){
        $("#photo").replaceWith($("#photo").val(''));
        $('.preview-zone').css({'display':'none'});
    });
});

