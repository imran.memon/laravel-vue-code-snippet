<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Contract Document</title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery.signature.css?'.time())}}">
    <link rel="stylesheet" href="{{ asset('css/all.css?'. time()) }}">
    <link rel="stylesheet" href="{{ asset('css/signature.css?'. time()) }}">

    <style type="text/css">
       body,
        #holder{
            /* background-color: #061620; */
        }

        .blueBg{
            background-color: #061620;
        }
        
        #holder{
            padding: 0;
            width: auto;
            position: relative;
            margin: 0;
            overflow: auto;
        }

        .canvas-wrapper{
            margin-bottom: 1px;
        }

        canvas{
            margin: 0 auto;
            display: block;
        }

        .konvajs-content{
            height: auto !important;
            width: auto !important;
        }

        .kbw-signature { 
            width: 250px; 
            height: 75px; 
        }

        .kbw-signature-disabled{
            opacity: 1;
            margin-top: -35px;
            margin-left: -55px;
        }
        .kbw-signature canvas {
            transform: scale(0.6);
        }
		
        #holder .form-control:disabled, #holder .form-control[readonly] {
            background-color: transparent;
            opacity: 1;
            border: 0px;
        }

        .ui-widget-content {
            border: 1px solid ;
            background: transparent;
        }

        #title_div{
        	position: absolute;
            margin-left: 5%;
            margin-top: 10px;
            font-size:10px;
        }

         .sticky_header{
            position:fixed;
            width:100%;
            left:0;
            top:0;
            background: #112938;
            border-bottom: 1px solid #112938;
            padding : 15px 0;
            z-index: 9999;
        }
        body{
        	/*padding-top : 57px;*/
            padding-top : 0;
        }

        .no_border{
        	border:none
        }

        #confModal, #adoptSignModal{
                top: 20px;
                width: 95%;
                max-width: 800px;
                margin: 0 auto;
                right: 0;
                z-index:9999;
                padding-left:0px !important;
        }
        #adoptSignModal {
            max-width: 550px;
        }

        #confModal .modal-dialog, #adoptSignModal .modal-dialog{
            margin-top: 0;
        }

        #confModal .modal-header, #adoptSignModal .modal-header{
            font-size: 12px;
            font-weight: 500;
            letter-spacing: 1px;
            color: #777;
            text-transform: uppercase;
        }

        #confModal .modal-body .pdf-image-view, #adoptSignModal .modal-body .pdf-image-view{
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
            height: 250px;
            object-fit: cover;
        }

        #confModal .modal-body p, #adoptSignModal .modal-body p{
            font-family: 'Roboto', sans-serif;
            font-size: 16px;
            color: #908888;
            margin: 10px 0;
        }
        .bg-green{
        background: rgb(129,175,23);
        background: linear-gradient(90deg, rgba(129,175,23,1) 0%, rgba(74,135,53,1) 100%);
        border: 0px;
        }
        .bg-orange{
        background: rgb(199,64,58);
        background: linear-gradient(90deg, rgba(199,64,58,1) 0%, rgba(233,133,85,1) 100%);
        border: 0px;
        }

        p.sign-by{
	    display: none;/*flex*/
            margin-left: 52px;
            margin-top: -22px;
            font-size: 10px;
        }

        .sign-border p.my-sign {
	    display: none; /*flex*/
            margin-left: 56px;
            margin-top: -29px;
            font-size: 10px;
	}
        
        .sign-border.sign-border-none:before{
            display: none;
        }

        /*.sign-border:before{
            border-bottom: 3px solid #8fc544;
            border-left: 3px solid #8fc544;
            border-radius: 15px 0 0 15px;
            border-top: 3px solid #8fc544;
            content: "";
            display: block;
            height: 50px;
            left: -10px;
            position: absolute;
            top: -15px;
            width: 50px;
	}*/

        .sticky_left .btn {
            top: 100px;
            position: fixed;
            left: -45px;
            z-index: 1000;
            transform: rotate(-90deg);
            background-color: #28a745;
            padding: 10px 20px 35px;
            height: 0px;
            background-color: #28a745;
            color: #fff;
            border-radius: 0px;
        }

        #holder{
            /* background-color: white; */
        }

        .html2canvas-container { width: auto !important }
        /*.html2canvas-container { width: 3000px !important; height: 3000px !important; }*/
        
        div#holder {
            width: auto;
            position: relative;
            margin: auto;
            overflow: auto;
        }
        div#holder .ui-widget-content {
            border: 0px;
        }

        .sign-border p.my-sign.alpha{
                margin-left: 52px;
                margin-top: -5px;
        }
        #rndSignId.kbw-signature canvas,
        #rndIniId.kbw-signature canvas{
            transform: unset;
        }

        #loader{
            align-items: center;
            justify-content: center;
        }

        #loader:before {
            content: '';
            position: fixed;
            left: 0;
            background: rgba(255, 255, 255, 0.8);
            z-index: 999999;
            width: 100%;
            height: 100%;
            top: 0;
        }

        #loader img{
            position: fixed;
            z-index: 999999999;
            width: 120px;
            height: 120px;
            top: 40%;
        }

        .blink-btn{
            -webkit-animation: glowing 1500ms infinite;
            -moz-animation: glowing 1500ms infinite;
            -o-animation: glowing 1500ms infinite;
            animation: glowing 1500ms infinite;
        }

        @-webkit-keyframes glowing {
          0% { background-color: #28a745; -webkit-box-shadow: 0 0 3px #28a745; }
          50% { background-color: #28a745; -webkit-box-shadow: 0 0 30px #28a745; }
          100% { background-color: #28a745; -webkit-box-shadow: 0 0 3px #28a745; }
        }

        @-moz-keyframes glowing {
          0% { background-color: #28a745; -moz-box-shadow: 0 0 3px #28a745; }
          50% { background-color: #28a745; -moz-box-shadow: 0 0 30px #28a745; }
          100% { background-color: #28a745; -moz-box-shadow: 0 0 3px #28a745; }
        }

        @-o-keyframes glowing {
          0% { background-color: #28a745; box-shadow: 0 0 3px #28a745; }
          50% { background-color: #28a745; box-shadow: 0 0 30px #28a745; }
          100% { background-color: #28a745; box-shadow: 0 0 3px #28a745; }
        }

        @keyframes glowing {
          0% { background-color: #28a745; box-shadow: 0 0 3px #28a745; }
          50% { background-color: #28a745; box-shadow: 0 0 30px #28a745; }
          100% { background-color: #28a745; box-shadow: 0 0 3px #28a745; }
        }

        .named_sign{
            margin-top: -15px;
        }


        /* CSS FOR RESIZE */
        .resizeBox{
            height: 70px;
            width: 70px;
            border: 2px dashed red;
            cursor: move;
        } 
        .cp{
            cursor: pointer;
        }

        .initial_container a#clearIni {
            display: block;
        }

        .img-sign_view{
            opacity: 1;
        }

        .img-init_view{
            opacity: 1;
        }

        .checkv-ew {
            background-color: #061620;
            color: #fff;
            padding: 10px;
            font-size: 12px;
            display: flex;
            line-height: 16px;
            letter-spacing: 1px;
            font-weight: 300;
            border-radius: 5px;
        }
        .checkv-ew input#confirmAdopt {
            margin-right: 10px;
        }
    </style>
</head>

<div class="sticky_left" style="display: none;">
    <button type="button" class="btn btn-success ml-4" style="width:80px" name="nevigate" id="nevigate">Start</button>
</div>

<body>
    <div id="loader" class="center_loader" style="display: none;">
        <img class="img-fluid" src="{{asset('images/anydeal-loader.gif')}}" />    
    </div> 

    @if($allSign)
        <input type="hidden" name="isAllSign" id="isAllSign" value="yes" />
    @else
        <input type="hidden" name="isAllSign" id="isAllSign" value="no" />
    @endif
    
    <div class="sticky_header elePanel" style="display: none;">
        <div class="container-fluid">
            @if($recepient->permission_id == 1)
                <div class="row">
                    <div class="col-sm-6 text text-warning">
                        <strong><span id="emailNotification" style="display: none;">
                            Please wait for some time,while sending document email...
                        </span></strong>
                    </div>

                    <div class="col-sm-6 text-right">
            	    	<button type="button" class="btn btn-success ml-3" name="save" id="save" disabled="disabled">
                            Submit
                        </button>
                    </div>
                </div>
            @endif
		</div>
	</div>

	<div class="container-fluid blueBg" style="margin-top: 4rem;">
        <input type="hidden" name="path" id="path" value="{{asset('document/'.$document->path)}}"  />
        <input type="hidden" name="docName" id="docName" value="{{$document->path}}"  />
        <input type="hidden" name="documentId" id="documentId" value="{{$document->id}}"  />
		<input type="hidden" id="created_at" value="{{date('Y',strtotime($document->created_at))}}"/>
        <input type="hidden" id="isAllSignEmailSend" value="{{$document->is_allsign_email_send}}"/>
        <input type="hidden" name="recepientId" id="recepientId" value="{{$recepient->id}}"  />
        <input type="hidden" name="buySellId" id="buySellId" value="{{$recepient->buyer_seller_id}}"  />
        <input type="hidden" name="buyer_seller_name" id="buyer_seller_name" value="{{$buyer_seller_name}}"  />
        <input type="hidden" name="contractId" id="contractId" value="{{($contract)?$contract->id:''}}" />
        
        <div class="html-content" id="holder" style="display: none;"></div>
    </div>
    
    <!-- Modal -->
    <div id="confModal" class="modal fade bd-example-modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg m-0">
            <div class="modal-content rounded-0 border-0  bg-light">
                <div class="modal-header justify-content-center border-0">
                    <div class="text-center">
                        <div>
                            @if($contract && !is_null($contract->prop_photo))
                                <img class=" img-fluid" src="{{asset('contract/'.$contract->prop_photo)}}"  width="120" />
                            @else
                                <img class=" img-fluid" src="{{asset('images/no_property.png')}}"  width="120" />
                            @endif
                        </div>
                        <span>{{($contract)?$contract->name:''}}</span>
                    </div>
                </div>

                <div class="modal-body mb-4 mx-lg-4 bg-white p-lg-4">
                    <div class="row" style="text-align:center;">
                        <div class="col-md-4">
                            <img class=" img-fluid border pdf-image-view"  src="{{asset('images/doc.png')}}" alt="">     
                        </div>

                        <div class="col-md-8">
                            <p>You have been requested by {{($owner)?$owner->name:''}} to sign<br><b><u>{{$document->path}}</u></b> document.</p>

                            @if($contract)
                                <p class="my-3">Please read the 
                                    <a href="{{url('rec_sign/disclosure/'.$contract->owner_id)}}" target="_blank" style=" font-weight: 600; text-decoration: underline;">Electronic Record and Signature Disclosure</a>
                                </p>
                            @endif
                            
                            <p class="mb-5">I agree to use electronic signature on document</p>
                            <div>
                                <button type="button" class="btn btn-success" id="continue">Continue</button>
                                <button type="button" class="btn btn-danger" id="decline" data-dismiss="modal">Decline</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Adopt Signature Modal -->
    <div id="adoptSignModal" class="modal fade bd-example-modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg m-0">
            <div class="modal-content rounded-0 border-0  bg-light">
                <div class="modal-header justify-content-center border-0">
                    <div class="text-center">
                        <div>
                            @if($contract && !is_null($contract->prop_photo))
                                <img class=" img-fluid" src="{{asset('contract/'.$contract->prop_photo)}}"  width="100" />
                            @else
                                <img class=" img-fluid" src="{{asset('images/no_property.png')}}"  width="100" />
                            @endif
                        </div>
                        <span>{{($contract)?$contract->name:''}}</span>
                    </div>
                </div>

                <input type="hidden" name="recepientId" value="{{$recepient->id}}">
                <input type="hidden" name="assigned_type" id="assigned_type" value="{{$recepient->type}}">
                <input type="hidden" name="assigned_font" id="assigned_font" value="{{$recepient->font}}">
                <input type="hidden" name="assigned_signature" id="assigned_signature" value="{{$recepient->signature}}">
                <input type="hidden" name="assigned_initial" id="assigned_initial" value="{{$recepient->initial}}">
                <input type="hidden" name="sign_type" id="sign_type" value="name">
                
                <div class="modal-body mb-4 mx-lg-4 bg-white p-lg-4">
                    <span id="adoptError" class="text text-danger"></span>

                    <nav>
                        <div class="nav nav-tabs adopt_tab" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-adopt-tab" data-toggle="tab" href="#nav-adopt" role="tab" aria-controls="nav-adopt" aria-selected="true">
                                SELECT STYLE
                            </a>
                            
                            <a class="nav-item nav-link" id="nav-draw-tab" data-toggle="tab" href="#nav-draw" role="tab" aria-controls="nav-draw" aria-selected="false">
                                DRAW
                            </a>
                        </div>
                    </nav>

                    <div class="tab-content mt-2" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-adopt" role="tabpanel" aria-labelledby="nav-adopt-tab">
                            <div class="row justify-content-between">
                                <div class="col-md-9 signature_container">
                                    <p>Signature</p>
                                    <input style="width:100%;" type="text" id="signature_text" name="signature_text" value="{{$buyer_seller_name}}" maxlength="20" />
                                    <div id="signatureStyle">{{$buyer_seller_name}}</div>
                                </div>
                            
                                <div class="col-md-3 initial_container">
                                    <?php
                                    $intial = '';
                                    $words = preg_split("/[\s,_-]+/", $buyer_seller_name);
                                    foreach ($words as $w) {
                                      $intial .= $w[0];
                                    }
                                    ?>
                                    <p>Initial</p>
                                    <input style="width:100%;" type="text" id="intial_text" name="intial_text" value="{{$intial}}" maxlength="5" />
                                    <div id="initialStyle" style="">{{$intial}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-draw" role="tabpanel" aria-labelledby="nav-draw-tab">
                            <div class="row justify-content-between">
                                <div class="col-md-9 signature_container">
                                    <p>Signature</p>
                                    <div id="rndSignId" class="" style="border: 1px solid #000;width:auto !important;max-width:100%;">
                                    </div>

                                    <div>
                                        <a href="javascript:void(0);" id="clearSign">Clear</a>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 initial_container">
                                    <p>Initial</p>
                                    <div id="rndIniId" class="" style="border: 1px solid #000;width: 100px !important;">
                                    </div>

                                    <a href="javascript:void(0);" id="clearIni">Clear</a>
                                </div>
                            </div>
                      </div>
                    </div>
                </div>

                <div class="modal-footer bg-white justify-content-between">
                    <div class="checkv-ew">
                        <input type="checkbox" name="confirmAdopt" id="confirmAdopt" value="1" require="require"/>
                        By selecting Adopt and Sign, I agree that the signature and initials will be the electronic representation of my signature and initials for all purposes when I (or my agent) use them on documents, including legally binding contracts - just the same as a pen-and-paper signature or initial.
                    </div>
                    <div>
                        <button type="button" class="btn btn-success" id="adoptSave">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('js/jquery.min.js?'. time()) }}"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js?'. time()) }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="{{asset('js/jquery.signature.js?'.time())}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/pdfjs-dist@2.0.385/build/pdf.min.js"></script>
    <script type="text/javascript" src="{{asset('js/konva.min.js?')}}"></script>
    <script type="text/javascript" src="{{asset('js/jspdf.min.js?')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('js/html2canvas.js?'.time())}}"></script> -->
    <script type="text/javascript" src="{{asset('js/html2canvas_alpha.js?'.time())}}"></script>

    <script src="{{asset('js/sweetalert2.min.js')}}"></script>
    
    <script type="text/javascript">
        function getsiteurl(){
          var BASE_URL = {!! json_encode(url('/')) !!}
          return BASE_URL;
        }
        var asset_url = "{{asset('/')}}";
    </script>
    
    <script type="text/javascript" src="{{asset('js/jquery.ui.touch-punch.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('member_js/view_shared_doc.js?'.time())}}"></script>
</body>
</html>
