<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('stripe');
});
Route::post('process/payment',[PaymentController::class,'processPayment'])->name('process.payment');
Route::get('/subscription-plan', function () {
    return view('subscription');
});
Route::post('subscription/save',[PaymentController::class,'createSubscription'])->name('subscriptionPlan.save');

