<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe;
use Redirect;
use DB;
use Carbon\Carbon;

class PaymentController extends Controller
{

    public function processPayment(Request $request){
        try{

            \Stripe\Stripe::setApiKey('sk_test_51L5G8gIOF32bfyMdoi9RyA8ejUTeCgrw9tbDWsVvX9AEE1gmadpxs6w5JkglBWaRYr6sDGBfoczofky6afw26O5V00kFoUAqAw');

            $stripeClient = new Stripe\StripeClient(
                'sk_test_51L5G8gIOF32bfyMdoi9RyA8ejUTeCgrw9tbDWsVvX9AEE1gmadpxs6w5JkglBWaRYr6sDGBfoczofky6afw26O5V00kFoUAqAw'
            );

            $cusObj = $stripeClient->customers->create([
                'name'    => 'test user',
                'email'   => 'test@gmail.com',
            ]);
            $customerId        = $cusObj->id;


            Stripe\Customer::createSource(
                $customerId,
                ['source' => $request->input('stripeToken')]
            );

            $result = Stripe\Charge::create ([
                "customer"    => $customerId,
                "amount"      => 30 * 100,
                "currency"    => 'EUR',
                "description" => 'test data',
            ]);

            echo 'done';exit();

        }catch(\Stripe\Exception\CardException $e){
            // $e->getHttpStatus(),$e->getError()->type,$e->getError()->code
            // $e->getError()->param,$e->getError()->message
            return Redirect::back();
        }catch (\Stripe\Exception\RateLimitException $e){
            // Too many requests made to the API too quickly
            return Redirect::back();
        }catch (\Stripe\Exception\InvalidRequestException $e){
            // Invalid parameters were supplied to Stripe's API
            return Redirect::back();
        }catch (\Stripe\Exception\AuthenticationException $e){
            // Authentication with Stripe's API failed, Maybe you changed API keys recently
            return Redirect::back();
        }catch (\Stripe\Exception\ApiConnectionException $e){
            // Network communication with Stripe failed
            return Redirect::back();
        }catch (\Stripe\Exception\ApiErrorException $e){
            // Display a very generic error to the user, and maybe send
            return Redirect::back();
        }catch (Exception $e){

            return Redirect::back();
        }
    }

    public function createSubscription(Request $request)
    {
        $stripeKey = 'sk_test_51L5G8gIOF32bfyMdoi9RyA8ejUTeCgrw9tbDWsVvX9AEE1gmadpxs6w5JkglBWaRYr6sDGBfoczofky6afw26O5V00kFoUAqAw';
        $stripe  = new StripeClient($stripeKey);
        // To create product for subscription 
        $product = $stripe->products->create(['name' => $request->name]);

        // To create price in product like monthly, Quaterly, half yearly, yearly
        // for monthy interval = month
        // for quaterly = ['interval' => 'month',"interval_count"=> 3]
        // for half yearly = ['interval' => 'month',"interval_count"=> 6]
        // for yearly = ['interval' => 'year']
        $monthly_price = $stripe->prices->create([
            'unit_amount' => $request->monthly_price * 100, // Price for monthly
            'currency'    => 'USD', // currency
            'recurring'   => ['interval' => 'month'], // interval 
            'product'     => $product->id,
        ]);

        echo 'done';exit;
    }

    public function buySubscription(Request $request)
    {
        $pmethod = $request->stripeToken;
        $token  = $stripe->paymentMethods->attach(
            $pmethod,
            ['customer' => 'customerId']
        );

        $subscription = $stripe->subscriptions->create([
                'customer' => 'customerId',
                'items' => [
                    ['price' => 'Stripe Plan ID'], // $monthly_price->id
                ],
                'default_payment_method' => $token->id,
            ]);
    }
}