<form method="post" action="{{route('subscriptionPlan.save')}}" id="SubscriptionPlanForm">
@csrf
    <div class="row mt-2">
        <div class="input-field col s6">
            <label>Name*</label><br>
            <input type="text" name="name" id="name" autocomplete="off" maxlength="50" />
        </div>
    </div>

    <div class="card-alert card orange lighten-5">
        <div class="card-content orange-text">
            <p>Stripe is not allow to update plan price. So carefully mention price.</p>
        </div>
    </div>

    <input type="text" name="check_price" id="check_price" value="0" style="visibility: hidden; height: 0px; margin:0px; padding: 0px;">  
    <div class="row">
        <div class="col s12">
            <table class="subscriptionTable">
                <thead>
                    <tr>
                        <th width="20%"><b>Duration</b></th>
                        <th width="40%" class="pl-2"><b>Price($)*</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><b>Monthly</b></td>
                        <td>
                            <div class="input-field col s12 ml-1">
                                <input type="text" class="allownumericwithdecimal monthly price1" name="monthly_price" id="monthly_price" autocomplete="off" maxlength="8" />
                                <span id="monthly_price-error" class="error1" style="color:#dc3545;"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Quaterly</b></td>
                        <td>
                            <div class="input-field col s12 ml-1">
                                <input type="text" class="allownumericwithdecimal quaterly price2" name="quaterly_price" id="quaterly_price" autocomplete="off" maxlength="8" />
                                <span id="quaterly_price-error" class="error2" style="color:#dc3545;"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Half Yearly</b></td>
                        <td>
                            <div class="input-field col s12 ml-1">
                                <input type="text" class="allownumericwithdecimal halfyearly price3" name="halfyearly_price" id="halfyearly_price" autocomplete="off" maxlength="8" />
                                <span id="halfyearly_price-error" class="error3" style="color:#dc3545;"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Yearly</b></td>
                        <td>
                            <div class="input-field col s12 ml-1">
                                <input type="text" class="allownumericwithdecimal yearly price4" name="yearly_price" id="yearly_price" autocomplete="off" maxlength="8" />
                                <span id="yearly_price-error" class="error4" style="color:#dc3545;"></span>
                            </div>
                        </td>
                    </tr>
                </tbody>
              </table>
        </div>
    </div>


    <div class="row">
        <div class="input-field col s6">
            <button class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow" type="submit" id="save">
                Save
            </button>
        </div>
    </div>
</form>