<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<form role="form" action="{{ route('process.payment') }}" method="post" class="require-validation row" data-cc-on-file="false" data-stripe-publishable-key="pk_test_51L5G8gIOF32bfyMdeEi4rqaTSjJFGk37A21l3LO2H3W9iGPfhu0Z0PGFCPxA8yvnKJCThsOwHb63riLmsGmbOWK40043v4Rc57" id="payment-form">
@csrf

<div class="col-12 field-group pt-3">

    <div class='form-row row'>
        <div class='col-md-12 error form-group d-none'>
            <div class='stripe_error alert-danger alert'>
            </div>
        </div>
    </div>

    <div class='form-row row'>
        <div class='col-12 form-group required'>
            <label class='control-label'>Name On Card</label> 
            <input type='text' class='form-control payment_input alphanumeric' size='4' autocomplete="off" maxlength="30" />
        </div>
    </div>

    <div class='form-row row'>
        <div class='col-12 form-group  required'>
            <label class='control-label'>Card No</label>
            <!-- size='20'  maxlength="16"  allownumericwithoutdecimal -->
            <input type='text' autocomplete='off' class='form-control card-number payment_input' data-inputmask="'mask': '9999 9999 9999 9999'"/>
        </div>
    </div>

    <div class='form-row row'>
        <div class='col-12 col-md-4 form-group expiration required'>
            <label class='control-label'>Exp Month</label> 
            <input type='text' class='form-control card-expiry-month payment_input allownumericwithoutdecimal' placeholder='MM' size='2' autocomplete="off" maxlength="2" />
        </div>

        <div class='col-xs-12 col-md-4 form-group expiration required'>
            <label class='control-label'>Exp Year</label> 
            <input type='text' class='form-control card-expiry-year payment_input allownumericwithoutdecimal' placeholder='YYYY' size='4' autocomplete="off" maxlength="4" />
        </div>

        <div class='col-12 col-md-4 form-group cvc required'>
            <label class='control-label'>CVC</label> 
            <input type='text' autocomplete='off' class='form-control card-cvc payment_input allownumericwithoutdecimal' placeholder='ex. 311' size='4' maxlength="3" />
        </div>
    </div>
    <div class='form-row row'>
        <button type="submit" class="btn">confirm_pay</button>
    </div>
</div>
</form>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
$(function() {
    // $("#changeBooking").on('click', function(e) {
    //     e.preventDefault();
    //     localStorage.removeItem("startDate");
    //     localStorage.removeItem("endDate");

    //     $(location).attr('href',$(this).attr('href'));
    // });

    $(".card-number").inputmask();
    var $form = $(".require-validation");

    $('form.require-validation').bind('submit', function(e) {
        var $form     = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('d-none');

        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('d-none');
                e.preventDefault();
            }
        });

        if (!$form.data('cc-on-file')) {
            e.preventDefault();
            Stripe.setPublishableKey($form.data('stripe-publishable-key'));
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }
    });

    function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .removeClass('d-none')
                .find('.alert')
                .text(response.error.message);
        }else {
            $(':input[type="submit"]').prop('disabled', true);
            $('body').addClass('loader-get');

            /* token contains id, last4, and card type */
            var token = response['id'];

            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
});
</script>




