<?php

namespace App\Http\Controllers\Zoom;

use App\Http\Controllers\Controller;
use App\Traits\ZoomJWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ZoomTable;
use App\MeetingParticipants;

class MeetingController extends Controller
{
    use ZoomJWT;

	const MEETING_TYPE_INSTANT               = 1;
	const MEETING_TYPE_SCHEDULE              = 2;
	const MEETING_TYPE_RECURRING             = 3;
	const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

	public function listMeetings($userId = null){
		$path = is_null($userId)?'users/me/meetings':'users/'.$userId.'/meetings';
		// $response = $this->zoomGet($path);

		$response = $this->zoomGet($path, [
			'page_size'       => 30,
			'next_page_token' => '',
	    ]);
		$data = json_decode($response->body(), true);

	    $data['meetings']  = array_map(function (&$m) {
	    	$m['start_at'] = $this->toUnixTimeStamp($m['start_time'], $m['timezone']);
	        return $m;
	    },$data['meetings']);

	    return [
			'success' => $response->ok(),
			'data'    => $data
	    ];
	}

	public function createMeeting(Request $request) {	
		$validator = Validator::make($request->all(), [
			'topic'      => 'required',
			'start_time' => 'required',
			'agenda'     => 'required'
	    ]);
	    
	    if ($validator->fails()) {
	        return [
				'success' => false,
				'data'    => $validator->errors(),
	        ];
	    }

		$data = $validator->validated();
		$path = 'users/me/meetings';
	   	
	   	// duration = Choose the approximate duration of the meeting. This is only for scheduling purposes. The meeting will not end after this length of time.
	   	// start_time
		// 'Asia/Calcutta','Australia/Sydney'
		// 'GMT+11:00','GMT'
	   	
	    $response = $this->zoomPost($path, [
			'topic'      => $data['topic'],
			'type'       => self::MEETING_TYPE_SCHEDULE,
			'start_time' => $this->toZoomTimeFormat($data['start_time']),
			'duration'   => 2,
			'timezone'   => 'Asia/Calcutta',
			'agenda'     => $data['agenda'],
	        'settings'   => [
				'host_video'        => false,
				'participant_video' => false,
				'waiting_room'      => true
	        ]
	    ]);

	    if($response->status()){
	    	$result = json_decode($response->body(), true);

			$obj                  = new ZoomTable();
			$obj->course_id       = 0;
			$obj->session_id      = 0;
			$obj->tutor_id        = 0;
			$obj->uuid            = $result['uuid'];
			$obj->meeting_id      = $result['id'];
			$obj->host_id         = isset($result['host_id'])?$result['host_id']:null;
			$obj->host_email      = isset($result['host_email'])?$result['host_email']:null;
			$obj->topic           = isset($result['topic'])?$result['topic']:null;
			$obj->type            = isset($result['type'])?$result['type']:null;
			$obj->status          = isset($result['status'])?$result['status']:null;
			$obj->start_time      = isset($result['start_time'])?$result['start_time']:null;
			if(isset($result['start_time']) && isset($result['timezone'])){
			$obj->conv_start_time = $this->convertGMTDate($result['start_time'],$result['timezone']);
			}
			$obj->duration        = isset($result['duration'])?$result['duration']:null;
			$obj->timezone        = isset($result['timezone'])?$result['timezone']:null;
			$obj->agenda          = isset($result['agenda'])?$result['agenda']:null;
			$obj->zoom_created_at = isset($result['created_at'])?$result['created_at']:null;
			$obj->join_url        = isset($result['join_url'])?$result['join_url']:null;
			$obj->start_url       = isset($result['start_url'])?$result['start_url']:null;
			$obj->setting         = isset($result['settings'])?json_encode($result['settings']):null;
			$obj->save();
	    }

	    return [
			'success' => $response->status() === 201,
			'data'    => json_decode($response->body(), true),
	    ];
	}

	public function getMeetingById($id) {
		$path     = 'meetings/'.$id;
		$response = $this->zoomGet($path);
		$data     = json_decode($response->body(), true);

    	if($response->ok()) {
        	$data['start_at'] = $this->toUnixTimeStamp($data['start_time'], $data['timezone']);
    	}

    	return [
			'success' => $response->ok(),
			'data'    => $data,
    	];
	}

	public function getMeetingRecordingById($id){
		$path     = 'meetings/'.$id.'/recordings';
		$response = $this->zoomGet($path);
		$data     = json_decode($response->body(), true);

		if($response->ok()) {
        	$data['start_at'] = $this->toUnixTimeStamp($data['start_time'], $data['timezone']);
    	}

		return [
			'success' => $response->ok(),
			'data'    => $data,
    	];
	}

    public function update(Request $request,$id) {
	    $validator = Validator::make($request->all(), [
			'topic'      => 'required|string',
			'start_time' => 'required|date',
			'agenda'     => 'string|nullable',
	    ]);

	    if ($validator->fails()) {
	        return [
				'success' => false,
				'data'    => $validator->errors(),
	        ];
	    }

		$data = $validator->validated();
		$path = 'meetings/'.$id;

		// Below list of parameter updated successfully check and confirmed
		// topic,start_time,duration,timezone,agenda
	    $response = $this->zoomPatch($path, [
			'topic'      => $data['topic'],
			'type'       => self::MEETING_TYPE_SCHEDULE,
			'start_time' => (new \DateTime($data['start_time']))->format('Y-m-d\TH:i:s'),
			'duration'   => 5,
			'timezone'   => 'Asia/Calcutta',
			'agenda'     => $data['agenda'],
			'settings'   => [
				'host_video'        => false,
				'participant_video' => false,
				'waiting_room'      => true,
	        ]
	    ]);

	    if($response->status()){
			$getMettingPath     = 'meetings/'.$id;
			$getMeetingResponse = $this->zoomGet($getMettingPath);

    		if($getMeetingResponse->ok()) {
				$result = json_decode($getMeetingResponse->body(), true);
				$obj    = ZoomTable::where('uuid',$result['uuid'])
						->where('meeting_id',$result['id'])->first();
				
				if($obj){
					$obj->host_id         = isset($result['host_id'])?$result['host_id']:null;
					$obj->host_email      = isset($result['host_email'])?$result['host_email']:null;
					$obj->topic           = isset($result['topic'])?$result['topic']:null;
					$obj->type            = isset($result['type'])?$result['type']:null;
					$obj->status          = isset($result['status'])?$result['status']:null;
					$obj->start_time      = isset($result['start_time'])?$result['start_time']:null;
					if(isset($result['start_time']) && isset($result['timezone'])){
						$obj->conv_start_time = $this->convertGMTDate($result['start_time'],$result['timezone']);
					}
					$obj->duration        = isset($result['duration'])?$result['duration']:null;
					$obj->timezone        = isset($result['timezone'])?$result['timezone']:null;
					$obj->agenda          = isset($result['agenda'])?$result['agenda']:null;
					$obj->zoom_created_at = isset($result['created_at'])?$result['created_at']:null;
					$obj->join_url        = isset($result['join_url'])?$result['join_url']:null;
					$obj->start_url       = isset($result['start_url'])?$result['start_url']:null;
					$obj->setting         = isset($result['settings'])?json_encode($result['settings']):null;
			    	$obj->save();
				}
    		}
	    }

	    return [
			'success' => $response->status() === 204,
			'data'    => json_decode($response->body(), true),
	    ];
	}

    public function deleteMeetingById($id) {
		$path     = 'meetings/'.$id;
		$response = $this->zoomDelete($path);

	    return [
			'success' => $response->status() === 204,
			'data'    => json_decode($response->body(), true),
	    ];
	}

	public function endMeetingById($id) {
		$path = 'meetings/'.$id.'/status';

		$response = $this->zoomPut($path, [
			'action' => 'end'
	    ]);

	    return [
			'success' => $response->status() === 204,
			'data'    => json_decode($response->body(), true),
	    ];
	}
	
	public function getMeetingHistory($id){
		// Get Meeting Information
		$path     = 'past_meetings/'.$id;
		$response = $this->zoomGet($path);
		$data     = json_decode($response->body(), true);

		if($response->ok()){
			$obj = ZoomTable::where('uuid',$data['uuid'])->where('meeting_id',$data['id'])->first();
			if($obj){
				// $obj->student_id      = 0;
				$obj->participants_count      = $data['participants_count'];
				$obj->meeting_duration        = $data['duration'];
				$obj->total_minutes           = $data['total_minutes'];
				$obj->meeting_start_time      = $data['start_time'];
				$obj->conv_meeting_start_time = $this->convertGMTDate($data['start_time'],$obj->timezone);
				$obj->meeting_end_time        = $data['end_time'];
				$obj->conv_meeting_end_time   = $this->convertGMTDate($data['end_time'],$obj->timezone);
				$obj->save();
			}
		}

		// Get Meeting Participant Information
		// $zoom    = ZoomTable::where('meeting_id',$id)->first();
		// $parPath = 'past_meetings/'.$id.'/participants';

		// $parResponse = $this->zoomGet($parPath, [
			// 'page_size'       => 300,
			// 'next_page_token' => '',
	    // ]);
		// $result = json_decode($parResponse->body(), true);

		// if($zoom && $parResponse->ok() && count($result['participants']) > 0){
		// 	foreach($result['participants'] as $value){
		// 		$temp          = new MeetingParticipants();
		// 		$temp->zoom_id = $zoom->id; 
		// 		$temp->name    = $value['name'];
		// 		$temp->save();
		// 	}
		// }

		// Get Detailed Participant Information
		$zoom    = ZoomTable::where('meeting_id',$id)->first();
		$parPath = 'report/meetings/'.$id.'/participants';

		$parResponse = $this->zoomGet($parPath, [
			'page_size'       => 300,
			'next_page_token' => '',
	    ]);
		$result = json_decode($parResponse->body(), true);

		if($zoom && $parResponse->ok() && count($result['participants']) > 0){
			foreach($result['participants'] as $value){
				$temp                  = new MeetingParticipants();
				$temp->zoom_id         = $zoom->id; 
				$temp->student_id      = 0; 
				$temp->zoom_par_id     = !empty($value['id'])?$value['id']:null; 
				$temp->name            = $value['name'];
				$temp->join_time       = $value['join_time'];
				$temp->conv_join_time  = $this->convertGMTDate($value['join_time'],$zoom->timezone);
				$temp->leave_time      = $value['leave_time'];
				$temp->conv_leave_time = $this->convertGMTDate($value['leave_time'],$zoom->timezone);
				$temp->duration        = $value['duration'];
				$temp->save();
			}
		}

		return [
			'success1'  => $response->ok(),
			'data1'     => $data,
			'success2'  => $parResponse->ok(),
			'data2'     => $result
    	];
	}

	public function createUser(Request $request){
		$path     = 'users';
		$userInfo = $request->input('user_info');

		$response = $this->zoomPost($path, [
			'action'      => $request->input('action'),
	        'user_info'   => [
				'email'      => $userInfo['email'],
				'type'       => $userInfo['type'],
				'first_name' => $userInfo['first_name'],
				'last_name'  => $userInfo['last_name']
	        ]
	    ]);

		return [
			'success' => $response->status() === 201,
			'data'    => json_decode($response->body(), true),
	    ];
	}
}
