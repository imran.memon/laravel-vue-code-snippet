<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZoomTable extends Model
{
    use SoftDeletes;
    
    protected $table = 'zoom';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid','meeting_id','host_id','host_email','topic','type','status','start_time','duration','timezone','agenda','zoom_created_at','join_url','start_url','setting','participants_count','meeting_duration','total_minutes','meeting_start_time','conv_meeting_start_time','meeting_end_time','conv_meeting_end_time','conv_start_time'
    ];
}
