@extends('admin.final')

@section('main_content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/instituteAdmin.css') }}">

    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
            <h3 class="page-title">{{'Calendar'}}</h3>
        </div>
        <div class="col-12 col-sm-6 d-flex align-items-center">
            <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                <a href="{{ route('calendar.create') }}" class="btn btn-primary"><b><i class="fa fa-plus-circle" aria-hidden="true"></i> {{'Add'}}</b></a>
            </div>
        </div>
    </div>
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
            {{ session()->get('success') }} 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
            {{ session()->get('success') }} 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        
    @endif
    <table class="transaction-history d-none" id="calendarlisting">
        <thead>
            <tr>
                 <th>{{'Id'}}</th>
                <th>{{'Course Title'}}</th>
                <th>{{'Course Code'}}</th>
                <th>{{'Session Title'}}</th>
                <th>{{'Session Number'}}</th>
                <th>{{'Session Date'}}</th>
                <th>{{'Tutor/Native Speaker'}}</th>
                <th>{{'Predefined Slot'}}</th>
                <th>{{'Status'}}</th>
                <th>{{'Actions'}}</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($calendar))
                @php
                    $i=1;
                @endphp
                @foreach ($calendar as $c)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>
                        <?php 
                            if(isset($c->course_detail)){
                                echo $c->course_detail->title;
                            }else{
                                echo 'N/A';
                            }
                        ?>
                        </td>
                        <td>
                            <?php 
                            if(isset($c->course_detail)){
                                echo $c->course_detail->code;
                            }else{
                                echo 'N/A';
                            }
                        ?>
                        </td>
                        <td><?php 
                            if(isset($c->session_detail)){
                                echo $c->session_detail->session_title;
                            }else{
                                echo 'N/A';
                            }
                        ?></td>
                        <td><?php 
                            if(isset($c->session_detail)){
                                echo $c->session_detail->session_number;
                            }else{
                                echo 'N/A';
                            }
                        ?></td>
                        <td><?php 
                            if(isset($c->session_date)){
                                echo $c->session_date;
                            }else{
                                echo 'N/A';
                            }
                        ?></td>
                        <td><?php 
                            if(isset($c->speaker_detail)){
                                echo $c->speaker_detail->name;
                            }else{
                                echo 'N/A';
                            }
                        ?></td>
                        <td><?php 
                            if(isset($c->slot_detail)){
                                echo $c->slot_detail->title;
                            }else{
                                echo 'N/A';
                            }
                        ?></td>
                       
                        <td>
                            <label class="switch">
                               

                                <input class="status" value="{{@$c->status}}" 
                                data-id = "{{@$c->id}}" type="checkbox" data-toggle="toggle" data-size="xs" data-onstyle="success" data-offstyle="outline-danger" data-on="Active" data-off="Deactive" @if($c->status == 1) checked @endif />
                            </label>
                        </td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                               
                               <a href="{{route('calendar.view',@$c->id)}}" class="btn btn-white" id="{{$c->id}}">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>

                                <a href="{{route('calendar.edit',@$c->id)}}" class="btn btn-white">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <a href="javascript:void(0);" class="btn btn-white delete" id="{{$c->id}}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            
                            </div>
                        </td>
                    </tr>
                    @php
                        $i=$i+1;
                    @endphp
                @endforeach
            @endif
        </tbody>
        
    </table>
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
@section('page_level_css')
   
@endsection
@section('page_level_js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    @include('admin.calendar.calendarJs')
@endsection
