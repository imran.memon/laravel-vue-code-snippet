
<script type="text/javascript">
   var obj ,calendar;
       document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

    calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,listMonth,DayGrid'
      },
      navLinks: true, // can click day/week names to navigate views
      businessHours: true, // display business hours
      editable: false,
      defaultView:'timeGridWeek',
      slotDuration: "00:15:00",
      eventSources: [ 
        {
          url: "{{route('calendar.getEvents')}}",
          method: 'POST',
          extraParams: {
            _token: "{{ csrf_token() }}"
          }
        }
      ],
      eventRender: function(event) {

          console.log(event);

          if(typeof event.event.extendedProps.is_schedule != 'undefined' && event.event.extendedProps.is_schedule  ) {
            var title = '<span data-id='+event.event.id+' data-startTime='+event.event.start+'  data-endTime='+event.event.end+' class="schedule-time">'+event.event.title+"</span>";
            console.log(event.el.querySelector('.fc-title'));
                if(typeof event.el.querySelector('.fc-title') != 'undefined' && event.el.querySelector('.fc-title') != null)  {
                    event.el.querySelector('.fc-title').innerHTML = title;
                }
          } 
          if(typeof event.event.extendedProps.is_meeting != 'undefined' && event.event.extendedProps.is_meeting == true) {
            var title = '<span class="meeting-time">'+event.event.title+"</span>";
            if(typeof event.el.querySelector('.fc-title') != 'undefined' && event.el.querySelector('.fc-title') != null) {
                event.el.querySelector('.fc-title').innerHTML = title;
            }
          }
      }, 
      eventClick: function(calEvent, jsEvent, view) {
        $("#ScheduleModal").modal("show");
              var editRoute = '{{ route("calendar.edit", ":id") }}';
              editRoute = editRoute.replace(':id', calEvent.event.id);
              $(".editScheduleLink").prop("href", editRoute);
              var deleteRoute = '{{ route("calendar.delete", ":id") }}';
              deleteRoute = deleteRoute.replace(':id', calEvent.event.id);
              $(".deleteScheduleLink").prop("href", deleteRoute);
              $(".view").prop("id",calEvent.event.id);
      }
    });
    
    calendar.on('dateClick', function(info) {
      console.log(info);
    });
    
    calendar.render();
     obj = calendar.getEventSources();
});

$(document).ready(function(){

	$.each($("#session_title"), function () {
        $(this).trigger('change');
    });

    $.each($("#slot_title"), function () {
        $(this).trigger('change');
    });
	
	$('#createFrm').validate({
	   	rules: {
	    	course_id: {
	        	required: true
	      	},
	      	speaker_id: {
	        	required: true
	      	},
	      	session_title: {
	        	required: true
	      	},
	      	session_number: {
	        	required: true
	      	},
	      	session_date: {
	        	required: true
	      	},
	      	slot_title: {
	        	required: true
	      	},
	      	slot_time: {
	        	required: true
	      	},
	      	status: {
	        	required: true
	      	}
	    },messages: {
	    	course_id: {
	        	required: "Please select course"
	      	},
	      	speaker_id: {
	        	required: "The Tutor/Native Speaker field is required"
	      	},
	      	session_title: {
	        	required: "Please select session title"
	      	},
	      	session_number: {
	        	required: "Please enter session number"
	      	},
	      	session_date: {
	        	required: "Please select session date"
	      	},
	      	slot_title: {
	        	required: "Please select slot title"
	      	},
	      	slot_time: {
	        	required: 'Please enter slot tite'
	      	},
	      	status: {
	        	required: 'Please Select Status'
	      	}
	    }
  	}); 

	

 
  $(document).on("click", ".delete", function(){
            var id = $(this).attr('id');

            Swal.fire({
              title: 'Are you sure you want to delete this Calendar?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    window.location.replace(getsiteurl()+'/admin/calendar/delete/'+id);
                }
            });
        });
        
    $('#session_date').datepicker();
    $('#course_id').select2({
      theme : "bootstrap"
    });
    $('#speaker_id').select2({
      theme : "bootstrap"
    });
    
    $('#slot_title').select2({
      theme : "bootstrap"
    }); 
    
    $(".transaction-history").DataTable({
      responsive: true,
      autoWidth: false,
      order: [],
      columnDefs: [{ 
         'orderable': false, 'targets': [5,6]
      }],
      oLanguage: {
         sEmptyTable: "No data available in table",
         sZeroRecords: "No records",
         sSearch: '<em class="fas fa-search"></em>',
         sLengthMenu: '_MENU_ records per page',
         sInfo: 'Showing _START_ to _END_ of _TOTAL_ entries',
         zeroRecords: "No records",
         infoEmpty: "No records",
         infoFiltered: '(filtered from MAX total records)',
         oPaginate: {
            sNext: '<em class="fa fa-caret-right"></em>',
            sPrevious: '<em class="fa fa-caret-left"></em>'
         }
      }
   });

});
$(document).on("change", "#session_title", function(){

        var session_id = $("#session_title option:selected" ).val();
      //  alert(session_id);
    	$.ajax({
            type:'GET',
            url: getsiteurl()+'/admin/get/session/number',
            data: {session_id:session_id},
            success: (response) => {
            	//alert(response);
                $('#session_number').val(response);
            }
        });
    });	
$(document).on("change", "#slot_title", function(){
        var slot_id = $("#slot_title option:selected" ).val();
        //alert(slot_id);
    	$.ajax({
            type:'GET',
            url: getsiteurl()+'/admin/get/slot/number',
            data: {slot_id:slot_id},
            success: (response) => {
            //	alert(response);
                $('#slot_time').val(response);
            }
        });
    });	
   $(document).on("change", ".status", function(){
      var id = $(this).data('id');
      var _token   = $('meta[name="csrf-token"]').attr('content');
      var statusValue = $(this).prop('checked');
      Swal.fire({
         title: 'Are you sure you want to change status of this calendar?',
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes'
      }).then((result) => {
         if (result.value) {
            $.ajax({
               url: getsiteurl()+'/admin/updateCalendarStatus',
               type: 'POST',
               data: { id: id , _token: _token, statusValue: statusValue}, 
               dataType: "json",
               success: function(data){
                  window.location.replace(getsiteurl()+'/admin/calendar');
               }
            });
         }else{
            window.location.replace(getsiteurl()+'/admin/calendar');
         }
      });
   });

   $(document).on("change", "#course_id", function(){
        var courseId = $("#course_id option:selected" ).val();

      $.ajax({
            type:'GET',
            url: getsiteurl()+'/admin/get/course/session',
            data: {courseId:courseId,module:'calendar'},
            success: (response) => {
                $('#renderSessions').empty();
                $('#renderSessions').append(response.result);
                $('#session_title').select2({
                  theme : "bootstrap"
                });
                
               
            }
        });
    });
   $('#session_title').select2({
    theme : "bootstrap"
    });
$(document).on("click", ".view", function(){
      var id = $(this).attr('id');
      $.ajax({

                url: getsiteurl()+'/admin/view/calendar/'+id,
                type: 'GET',
                success: function(oData){
                $('#modal-body-calendar').html(oData.html);
                  $.each($("#slot_title"), function () {
                        $(this).trigger('change');
                    });
                    $.each($("#session_title"), function () {
                        $(this).trigger('change');
                    });

                    $("#myModal").modal('show'); 


                

               },
            });
   });

 </script>