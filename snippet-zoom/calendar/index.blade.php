@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="card">
                <div class="card-header">
                    <div class="card-header-actions">

                        <a href="{{ route('calendar.create') }}"
                            class="btn btn-primary student_btn">
                            <b><i class="fa fa-plus-circle" aria-hidden="true"></i> {{'Add'}}</b>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                     @if(session()->has('success'))
                      <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
                        {{ session()->get('success') }} 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                    @endif
                    @if(session()->has('error'))
                      <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
                        {{ session()->get('success') }} 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                    @endif
                    <div id='calendar' class="schedule-calendar custom-calendar"></div>
                    <div class="modal" id="ScheduleModal">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
          <a href="javascript:void(0);" class="editScheduleLink btn-success btn-block btn mb-2 btn-md ">{{"Edit Session"}}</a>
          <a href="javascript:void(0);" class="btn-info btn-block btn mb-2 btn-md view" id="">{{"View Session"}}</a>
          <a href="javascript:void(0);" class="deleteScheduleLink btn-danger btn-block mb-0 btn btn-md ">{{"Delete Session"}}</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:white !important;">
          <h5 class="modal-titlepage-title">{{'Calendar'}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="padding-top: 5px; padding-bottom: 1px;" id="modal-body-calendar">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-dark" data-dismiss="modal">{{'Back'}}</button>
        </div>
      </div>
    </div>
  </div>  
   
  <meta name="csrf-token" content="{{ csrf_token() }}" />
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_css')
  <link href="{{asset('css/style.css')}}" rel='stylesheet' />
  <link href="{{asset('full-calender/core/main.min.css')}}" rel='stylesheet' />
  <link href="{{asset('full-calender/daygrid/main.min.css')}}" rel='stylesheet' />
  <link href="{{asset('full-calender/timegrid/main.min.css')}}" rel='stylesheet' />
  <link href="{{asset('full-calender/list/main.min.css')}}" rel='stylesheet' />
@endsection

@section('page_level_js')
  <script src="{{asset('full-calender/core/main.min.js')}}"></script>
  <script src="{{asset('full-calender/interaction/main.min.js')}}" ></script>
  <script src="{{asset('full-calender/daygrid/main.min.js')}}" ></script>
  <script src="{{asset('full-calender/timegrid/main.min.js')}}" ></script>
  <script src="{{asset('full-calender/list/main.min.js')}}" ></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales-all.min.js"></script>
  @include('admin.calendar.calendarJs')
@endsection