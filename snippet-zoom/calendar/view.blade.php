@extends('admin.final')

@section('title')
    <title>{{'View Course'}}</title>
@endsection

@section('page_level_css') 
    <!--  -->
@endsection

@section('main_content')
<div class="page-header row no-gutters py-4">
    <div class="col-12 mb-4 mt-3">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    {{'Calendar'}}
                </h6>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item p-3">
                    <div class="row">
                        <div class="col">
                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Course'}}</label>
                                        <select name="course_id" id="course_id" class="form-control" disabled="disabled">
                                            <option value="">Select</option>
                                               @if(!empty($course))
                                                    @foreach($course as $c)
                                                        @if($calendar->course_id == $c->id)
                                                            <option value="{{$c->id}}" selected="selected">
                                                                {{$c->title}}
                                                            </option>
                                                        @else
                                                            <option value="{{$c->id}}">
                                                                {{$c->title}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                              @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Tutor/Native Speaker'}}</label>
                                        <select name="speaker_id" id="speaker_id" class="form-control" disabled="disabled">
                                            <option value="">Select</option>
                                               @if(!empty($speaker))
                                                    @foreach($speaker as $c)
                                                        @if($calendar->speaker_id == $c->id)
                                                            <option value="{{$c->id}}" selected="selected">
                                                                {{$c->name}}
                                                            </option>
                                                        @else
                                                            <option value="{{$c->id}}">
                                                                {{$c->name}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                              @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Session Title'}}</label>
                                        <select name="session_title" id="session_title" class="form-control" disabled="disabled">
                                            <option value="">Select</option>
                                               @if(!empty($session))
                                                    @foreach($session as $c)
                                                        @if($calendar->session_id == $c->id)
                                                            <option value="{{$c->id}}" selected="selected">
                                                                {{$c->session_title}}
                                                            </option>
                                                        @else
                                                            <option value="{{$c->id}}">
                                                                {{$c->session_title}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                              @endif
                                        </select>
                                    </div>
                                </div>
                        

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Session Number'}}</label>
                                         <input type="text" name="session_number" class="form-control" id="session_number" placeholder="Session Number" value="" readonly="">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Session Date'}}</label>
                                        <input type="text" class="form-control" name="session_date" id="session_date" autocomplete="off" value="{{date('m/d/Y',strtotime($calendar->session_date))}}" disabled="disabled" />
                                    </div>
                                </div>

                                 <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Slot Title'}}</label>
                                        <select name="slot_title" id="slot_title" class="form-control" disabled="disabled">
                                            <option value="">Select</option>
                                               @if(!empty($slot))
                                                    @foreach($slot as $c)
                                                        @if($calendar->slot_id == $c->id)
                                                            <option value="{{$c->id}}" selected="selected">
                                                                {{$c->title}}
                                                            </option>
                                                        @else
                                                            <option value="{{$c->id}}">
                                                                {{$c->title}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                              @endif
                                        </select>
                                    </div>
                                </div>
                        

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Slot Time'}}</label>
                                         <input type="text" name="slot_time" class="form-control" id="slot_time" placeholder="Session Number" value="" readonly="">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Status'}}</label>
                                        <select id="status" name="status" class="form-control col-md-6" disabled="">
                                        @foreach(config('constant.status') as $key => $value)
                                            @if(isset($calendar->status) && $calendar->status == $key)
                                                <option value="{{$key}}" selected="selected">{{$value}}</option>
                                            @else
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <a href="{{route('calendar.index')}}" class="btn btn-warning">
                                    {{'Back'}}
                                </a>
                            </form>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection

@section('page_level_js')
    <!--  -->
    @include('admin.calendar.calendarJs')
    
@endsection
