<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeetingParticipants extends Model
{
    use SoftDeletes;
    
    protected $table = 'meeting_participants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'zoom_id','student_id','zoom_par_id','name','join_time','conv_join_time','leave_time','conv_leave_time','duration'
    ];
}
