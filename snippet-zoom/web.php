<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function(){
	Route::get('login', 'Auth\AdminLoginController@login')->name('login');
	

	Route::get('password_reset', 'Auth\AdminLoginController@passwordReset')->name('password_reset');
	Route::post('login', 'Auth\AdminLoginController@adminLogin')->name('adminLogin');
	Route::post('logout', 'Auth\AdminLoginController@logout')->name('logout');
});

Route::group(['prefix' => config('constant.adminPrefix'),'middleware' =>['auth','localization', 
	'permissionAuth']], function(){
	
	Route::get('/unauthorized', function () {
		$data['page_title']='unauthorized';
		return view('unauthorized',$data);
	})->name('unauthorized');
	
	Route::get('/dashboard', 'AdminHomeController@homeAdmin')->name('dashboard');
	Route::get('profile', 'AdminHomeController@edit_profile')->name('profile');
	Route::post('/update_profile', 'AdminHomeController@update_profile')->name('update_profile');

	Route::get('setLocale/{locale}','Admin\DictionaryController@setLocale')->name('setLocale');

	//CMS Management 	
	Route::post('updateCmsStatus','Admin\CmsController@updateCmsStatus')->name('updateCmsStatus');
	Route::match(array('GET','POST'),'cms_pages','Admin\CmsController@index')->name('cms');
	Route::get('/add_cms_page','Admin\CmsController@add_cms_page')->name('cms.add_cms_page');
	Route::post('/store_cms_page','Admin\CmsController@store_cms_page')->name('cms.store_cms_page');
	Route::get('/edit_cms_page/{id}','Admin\CmsController@edit_cms_page')->name('cms.edit_cms_page');
	Route::get('/delete_cms_page/{id}','Admin\CmsController@delete_cms_page')->name('cms.delete_cms_page');
	Route::post('/update_cms_page','Admin\CmsController@update_cms_page')->name('cms.update_cms_page');
	Route::get('/detail_cms/{id}','Admin\CmsController@detail_cms')->name('cms.detail_cms');

	//Template Management 
	Route::match(array('GET','POST'),'/template','Admin\TemplateController@index')->name('template');
	Route::post('updateTemplateStatus','Admin\TemplateController@updateTemplateStatus')->name('updateTemplateStatus');
	Route::get('/add_template','Admin\TemplateController@add_template')->name('template.add_template');
	Route::post('/store_template','Admin\TemplateController@store_template')->name('template.store_template');
	Route::get('/edit_template/{id}','Admin\TemplateController@edit_template')->name('template.edit_template');
	Route::get('/delete_template/{id}','Admin\TemplateController@delete_template')->name('template.delete_template');
	Route::post('/update_template','Admin\TemplateController@update_template')->name('template.update_template');
	Route::get('/detail_template/{id}','Admin\TemplateController@detail_template')->name('template.detail_template');

	//FAQ Management 
	Route::post('updateFaqStatus','Admin\FaqController@updateFaqStatus')->name('updateFaqStatus');
	Route::match(array('GET','POST'),'/faq','Admin\FaqController@index')->name('faq');
	Route::get('/add_faq','Admin\FaqController@add_faq')->name('addFaq');
	Route::post('/store_faq','Admin\FaqController@store_faq')->name('storeFaq');
	Route::get('/edit_faq/{id}','Admin\FaqController@edit_faq')->name('editFaq');
	Route::get('/delete_faq/{id}','Admin\FaqController@delete_faq')->name('deleteFaq');
	Route::post('/update_faq','Admin\FaqController@update_faq')->name('updateFaq');
	Route::get('/detail_faq/{id}','Admin\FaqController@detail_faq')->name('detailFaq');

	//Dictionary Management
	
	Route::get('Dictionary','Admin\DictionaryController@index')->name('Dictionary.index');
	Route::get('Dictionary/create','Admin\DictionaryController@create')->name('Dictionary.create');
	Route::post('Dictionary/store','Admin\DictionaryController@store')->name('Dictionary.store');
	Route::get('Dictionary/edit/{id}','Admin\DictionaryController@edit')->name('Dictionary.edit');
	Route::post('Dictionary/update/{id}','Admin\DictionaryController@update')->name('Dictionary.update');
	Route::get('Dictionary/delete/{id}','Admin\DictionaryController@delete')->name('Dictionary.delete');
	

	// InstituteAdmin Management
	Route::match(array('GET','POST'),'instituteAdmin','Admin\InstituteAdminController@index')->name('instituteAdmin.index');	
	Route::get('instituteAdmin/create','Admin\InstituteAdminController@create')->name('instituteAdmin.create');
	Route::post('instituteAdmin/store','Admin\InstituteAdminController@store')->name('instituteAdmin.store');
	Route::get('instituteAdmin/show/{id}','Admin\InstituteAdminController@show')->name('instituteAdmin.show');
	Route::get('instituteAdmin/edit/{id}','Admin\InstituteAdminController@edit')->name('instituteAdmin.edit');
	Route::post('instituteAdmin/update/{id}','Admin\InstituteAdminController@update')->name('instituteAdmin.update');
	Route::get('instituteAdmin/delete/{id}','Admin\InstituteAdminController@delete')->name('instituteAdmin.delete');
	Route::post('updateUserStatus','Admin\InstituteAdminController@updateUserStatus')->name('updateUserStatus');

	Route::get('instituteAdmin/import','Admin\InstituteAdminController@importView')->name('instituteAdmin.importView');
	Route::post('instituteAdmin/import/data','Admin\InstituteAdminController@import')->name('instituteAdmin.import');
	Route::get('instituteAdmin/downloadFormat','Admin\InstituteAdminController@downloadFormat')->name('instituteAdmin.downloadFormat');

	Route::get('getInstituteCoordinators','Admin\InstituteAdminController@getInstituteCoordinators')->name('getInstituteCoordinators');
	Route::get('getInstituteCoordinatorsNotification','Admin\InstituteAdminController@getInstituteCoordinatorsNotification')->name('getInstituteCoordinatorsNotification');
	Route::get('getInstituteCoordinatorsCourse','Admin\InstituteAdminController@getInstituteCoordinatorsCourse')->name('getInstituteCoordinatorsCourse');
	
	Route::get('generateInstituteCode','Admin\InstituteAdminController@generateInstituteCode')->name('generateInstituteCode');
	

	// Co-Ordinator Management
	Route::match(array('GET','POST'),'co_ordinator','Admin\CoordinatorController@index')->name('coOrdinator');	
	Route::get('create/co_ordinator','Admin\CoordinatorController@create')->name('createCoOrdinator');
	Route::post('save/co_ordinator','Admin\CoordinatorController@save')->name('saveCoOrdinator');
	Route::get('edit/co_ordinator/{id}','Admin\CoordinatorController@edit')->name('editCoOrdinator');
	Route::get('view/co_ordinator/{id}','Admin\CoordinatorController@edit')->name('viewCoOrdinator');
	Route::get('delete/co_ordinator/{id}','Admin\CoordinatorController@delete')->name('deleteCoOrdinator');
	Route::get('co_ordinator/import','Admin\CoordinatorController@importView')->name('coOrdinator.importView');
	Route::post('co_ordinator/import/data','Admin\CoordinatorController@import')->name('coOrdinator.import');
	Route::get('co_ordinator/downloadFormat','Admin\CoordinatorController@downloadFormat')->name('coOrdinator.downloadFormat');

	// Tutor/Native Speaker
	Route::match(array('GET','POST'),'speaker','Admin\TutorNativeSpeakerController@index')->name('speaker');
	Route::get('create/speaker','Admin\TutorNativeSpeakerController@create')->name('createSpeaker');
	Route::post('save/speaker','Admin\TutorNativeSpeakerController@save')->name('saveSpeaker');
	Route::get('edit/speaker/{id}','Admin\TutorNativeSpeakerController@edit')->name('editSpeaker');
	Route::get('view/speaker/{id}','Admin\TutorNativeSpeakerController@edit')->name('viewSpeaker');
	Route::get('delete/speaker','Admin\TutorNativeSpeakerController@delete')->name('deleteSpeaker');
	Route::get('change/speaker_status','Admin\TutorNativeSpeakerController@changeSpeakerStatus')->name('changeSpeakerStatus');
	
	Route::get('speaker/import','Admin\TutorNativeSpeakerController@importView')->name('speaker.importView');
	Route::post('speaker/import/data','Admin\TutorNativeSpeakerController@import')->name('speaker.import');
	Route::get('speaker/downloadFormat','Admin\TutorNativeSpeakerController@downloadFormat')->name('speaker.downloadFormat');

	// Student Management
	Route::match(array('GET','POST'),'student','Admin\StudentControllter@index')->name('student.index');
	Route::get('student/create','Admin\StudentControllter@create')->name('student.create');
	Route::post('student/store','Admin\StudentControllter@store')->name('student.store');
	Route::get('student/show/{id}','Admin\StudentControllter@show')->name('student.show');
	Route::get('student/edit/{id}','Admin\StudentControllter@edit')->name('student.edit');
	Route::post('student/update/{id}','Admin\StudentControllter@update')->name('student.update');
	Route::get('student/delete/{id}','Admin\StudentControllter@delete')->name('student.delete');
	Route::get('student/import','Admin\StudentControllter@importView')->name('student.importView');
	Route::post('student/import/data','Admin\StudentControllter@import')->name('student.import');
	Route::get('student/downloadFormat','Admin\StudentControllter@downloadFormat')->name('student.downloadFormat');
	Route::post('updateStudentStatus','Admin\StudentControllter@updateStudentStatus')->name('updateStudentStatus');

	// Testimonial Management
	Route::post('updateTestimonialStatus','Admin\TestimonialControllter@updateTestimonialStatus')->name('updateTestimonialStatus');

	Route::match(array('GET','POST'),'testimonial','Admin\TestimonialControllter@index')->name('testimonial.index');
	Route::get('testimonial/create','Admin\TestimonialControllter@create')->name('testimonial.create');
	Route::post('testimonial/store','Admin\TestimonialControllter@store')->name('testimonial.store');
	Route::get('testimonial/edit/{id}','Admin\TestimonialControllter@edit')->name('testimonial.edit');
	Route::post('testimonial/update/{id}','Admin\TestimonialControllter@update')->name('testimonial.update');
	Route::get('testimonial/delete/{id}','Admin\TestimonialControllter@delete')->name('testimonial.delete');
	Route::get('testimonial/detail/{id}','Admin\TestimonialControllter@detail')->name('testimonial.detail');

	// Session Management
	Route::match(array('GET','POST'),'session','Admin\SessionControllter@index')->name('session.index');
	Route::post('updateSessionStatus','Admin\SessionControllter@updateSessionStatus')->name('updateSessionStatus');
	Route::get('session/create/{courseId?}','Admin\SessionControllter@create')->name('session.create');
	Route::post('session/store','Admin\SessionControllter@store')->name('session.store');
	Route::get('session/edit/{id}','Admin\SessionControllter@edit')->name('session.edit');
	Route::post('session/update/{id}','Admin\SessionControllter@update')->name('session.update');
	Route::get('session/delete/{id}','Admin\SessionControllter@delete')->name('session.delete');
    Route::get('view/session/document/{file}','Admin\SessionControllter@viewSessionDoc')->name('viewSessionDoc');
    Route::get('view/session/{id}','Admin\SessionControllter@view')->name('session.view');

    // Calendar Management
    Route::post('updateCalendarStatus','Admin\CalendarController@updateCalendarStatus')->name('updateCalendarStatus');
	Route::get('calendar','Admin\CalendarController@index')->name('calendar.index');
	Route::get('calendar/create','Admin\CalendarController@create')->name('calendar.create');
	Route::get('calendar/edit/{id}','Admin\CalendarController@edit')->name('calendar.edit');
	Route::get('get/session/number','Admin\CalendarController@getSessionNumber');
	Route::get('get/slot/number','Admin\CalendarController@getSlotNumber');
	Route::post('calendar/store','Admin\CalendarController@store')->name('calendar.store');
	Route::post('calendar/update','Admin\CalendarController@update')->name('calendar.update');
	Route::get('calendar/delete/{id}','Admin\CalendarController@delete')->name('calendar.delete');
	Route::get('view/calendar/{id}','Admin\CalendarController@view')->name('calendar.view');
	Route::post('getEvents','Admin\CalendarController@getEvents')->name('calendar.getEvents');
	
	// Social Media Management
	Route::get('social_media','Admin\SocialMediaController@index')->name('socialMedia');
	Route::post('save/social_media','Admin\SocialMediaController@save')->name('saveSocialMedia');

	// Course
	Route::match(array('GET','POST'),'course','Admin\CourseController@index')->name('course');
	Route::get('create/course','Admin\CourseController@create')->name('createCourse');
	Route::post('save/course','Admin\CourseController@save')->name('saveCourse');
	Route::get('view/course/{courseId}','Admin\CourseController@edit')->name('viewCourse');
	Route::get('edit/course/{courseId}/{courseStep}','Admin\CourseController@edit')->name('editCourse');

	Route::post('update/course/details','Admin\CourseController@updateCourseDetails')->name('updateCourseDetails');
	Route::post('update/course/dates','Admin\CourseController@updateCourseDateDetails')->name('updateCourseDateDetails');
	Route::post('update/course/weeks','Admin\CourseController@updateCourseAvailabeWeeks')->name('updateCourseAvailabeWeeks');
	Route::post('update/course/timeSlot','Admin\CourseController@updateCourseTimeSlot')->name('updateCourseTimeSlot');
	Route::post('update/course/classes','Admin\CourseController@updateCourseClasses')->name('updateCourseClasses');
	Route::get('delete/course/{courseId}','Admin\CourseController@delete')->name('deleteCourse');
	Route::get('view/course/document/{file}','Admin\CourseController@viewCourseDoc')->name('viewCourseDoc');
	Route::post('uploadCourseDocument','Admin\CourseController@uploadCourseDocument')->name('uploadCourseDocument');
	Route::get('delete/course/document/{file}/{courseId}','Admin\CourseController@deleteCourseDoc')->name('deleteCourseDoc');
	Route::get('download/coachingform/pdf/{id}','Admin\CourseController@DownloadCoachingFormPDF')->name('DownloadCoachingFormPDF');
	Route::get('get/institute/coordinator','Admin\CourseController@getInsCoordinator')->name('getInsCoordinator');
	Route::get('getCourseInstituteCoordinators','Admin\CourseController@getCourseInstituteCoordinators')->name('getCourseInstituteCoordinators');
	Route::get('getInstituteCoordinatorEmail','Admin\CourseController@getInstituteCoordinatorEmail')->name('getInstituteCoordinatorEmail');
	Route::get('getCourseTypeData','Admin\CourseController@getCourseTypeData')->name('getCourseTypeData');
	Route::match(array('GET','POST'),'newCourseFormRequest','Admin\CourseController@newCourseFormRequest')->name('newCourseFormRequest');
	Route::match(array('GET','POST'),'activeCourse','Admin\CourseController@activeCourse')->name('activeCourse');
	Route::match(array('GET','POST'),'pastCourse','Admin\CourseController@pastCourse')->name('pastCourse');
	Route::match(array('GET','POST'),'futureCourse','Admin\CourseController@futureCourse')->name('futureCourse');
	Route::get('activeCourse/detail/{id}','Admin\CourseController@activeCourseDetails')->name('activeCourseDetails');
	
	Route::post('view/save/course_doc','Admin\CourseController@saveCourseDoc')->name('saveCourseDoc');
	Route::get('addMore/Coursebooking','Admin\CourseController@addMoreCourseBooking')->name('addMoreCourseBooking');
	Route::get('addMore/CourseClasses','Admin\CourseController@addMoreCourseClasses')->name('addMoreCourseClasses');
	

	// Course Type Management
	Route::match(array('GET','POST'),'courseType','Admin\courseTypeController@index')->name('courseType');
	Route::get('create/courseType','Admin\courseTypeController@create')->name('createcourseType');
	Route::post('save/courseType','Admin\courseTypeController@save')->name('savecourseType');
	Route::get('edit/courseType/{courseTypeId}','Admin\courseTypeController@edit')->name('editcourseType');
	Route::post('update/courseType/{courseTypeId}','Admin\courseTypeController@update')->name('updatecourseType');

	Route::get('view/courseType/{courseTypeId}','Admin\courseTypeController@edit')->name('viewcourseType');
	Route::get('delete/courseType/{courseTypeId}','Admin\courseTypeController@delete')->name('deletecourseType');


	// Slot Management
	Route::match(array('GET','POST'),'slot','Admin\SlotController@index')->name('slot');
	Route::get('create/slot','Admin\SlotController@create')->name('createSlot');
	Route::post('save/slot','Admin\SlotController@save')->name('saveSlot');
	Route::get('edit/slot/{slotId}','Admin\SlotController@edit')->name('editSlot');
	Route::get('view/slot/{slotId}','Admin\SlotController@edit')->name('viewSlot');
	Route::get('delete/slot/{slotId}','Admin\SlotController@delete')->name('deleteSlot');
	Route::get('get/slot/to_time','Admin\SlotController@getSlotToTime')->name('getSlotToTime');
	Route::get('change/slot_status','Admin\SlotController@changeSlotStatus')->name('changeSlotStatus');

	// Common (Slot,Review)
	Route::get('get/institute/course','HomeController@getInsCourse')->name('getInsCourse');
	Route::get('get/course/session','HomeController@getCourseSession')->name('getCourseSession');
	Route::get('change/status','HomeController@changeStatus')->name('changeStatus');
	
	// Notification Management
	Route::match(array('GET','POST'),'notification','Admin\NotificationController@index')->name('notification');
	Route::get('notification','Admin\NotificationController@index')->name('notification');
	Route::get('create/notification','Admin\NotificationController@create')->name('createNotification');
	Route::post('save/notification','Admin\NotificationController@save')->name('saveNotification');
	Route::get('edit/notification/{notificationId}','Admin\NotificationController@edit')->name('editNotification');
	Route::get('show/notification/{notificationId}','Admin\NotificationController@show')->name('showNotification');
	Route::post('update/notification/{notificationId}','Admin\NotificationController@update')->name('updateNotification');
	Route::get('delete/notification/{notificationId}','Admin\NotificationController@delete')->name('deleteNotification');
	Route::get('getInstituteCourse','Admin\NotificationController@getInstituteCourse')->name('getInstituteCourse');

	// Global Setting
	Route::get('globalSetting','Admin\GlobalSettingController@globalSetting')->name('globalSetting');
	Route::post('globalSetting','Admin\GlobalSettingController@globalSettingSave')->name('globalSetting');

	// Document Management
	Route::match(array('GET','POST'),'document','Admin\DocumentController@index')->name('document');

	// Review Management 
	Route::match(array('GET','POST'),'review','Admin\ReviewController@index')->name('review');
	Route::get('create/review','Admin\ReviewController@create')->name('createReview');
	Route::post('save/review','Admin\ReviewController@save')->name('saveReview');
	Route::get('edit/review/{Id}','Admin\ReviewController@edit')->name('editReview');
	Route::get('view/review/{id}','Admin\ReviewController@edit')->name('viewReview');
	Route::get('delete/review/{id}','Admin\ReviewController@delete')->name('deleteReview');

	//Course Booking Student
	Route::get('course/checkout/{courseId}','Admin\StudentControllter@courseCheckout')->name('courseCheckout');
	Route::post('charge','Admin\StudentControllter@charge')->name('charge');
	Route::get('courseBooking','Admin\StudentControllter@courseBooking')->name('courseBooking');
	Route::get('bookingHistory','Admin\StudentControllter@bookingHistory')->name('bookingHistory');
	Route::get('myCalendar','Admin\StudentControllter@myCalendar')->name('myCalendar');
	Route::get('studentAccount/delete/{id}','Admin\StudentControllter@Accountdelete')->name('studentAccount.delete');

	// Tutor/Native Speaker Role
	Route::get('active_session','Admin\TutorNativeSpeakerController@activeSession')->name('activeSession');
	Route::get('future_session','Admin\TutorNativeSpeakerController@futureSession')->name('futureSession');
	Route::get('create/availability','Admin\TutorNativeSpeakerController@addAvailability')->name('addAvailability');
	Route::get('add_more/booking','Admin\TutorNativeSpeakerController@addMoreBooking')->name('addMoreBooking');
	Route::post('save/availability','Admin\TutorNativeSpeakerController@saveAvailability')->name('saveAvailability');
	Route::get('edit/availability/{id}','Admin\TutorNativeSpeakerController@editAvailability')->name('editAvailability');
	Route::get('delete/break/{elementId}','Admin\TutorNativeSpeakerController@deleteBreak')->name('deleteBreak');
	Route::get('delete/block/{elementId}','Admin\TutorNativeSpeakerController@deleteBLock')->name('deleteBLock');
});


// Mailchimp
Route::get('test_email','HomeController@testEmail');
Route::get('get/email/{id}','HomeController@getEmailById');

// Zoom Routes
Route::get('list/meetings/{userId?}','Zoom\MeetingController@listMeetings')->name('listMeetings');
Route::get('get/meeting/{id}','Zoom\MeetingController@getMeetingById')->name('getMeetingById');
Route::get('get/meeting/{id}/recordings','Zoom\MeetingController@getMeetingRecordingById')->name('getMeetingRecordingById');
Route::get('get/meeting/history/{id}','Zoom\MeetingController@getMeetingHistory')->name('getMeetingHistory');


// Stripe Routes
Route::get('stripe','StripePaymentController@stripe');
Route::post('process/payment','StripePaymentController@processPayment')->name('processPayment');
Route::get('get/payment/info/{cid}','StripePaymentController@getPaymentInfo')->name('getPaymentInfo');
Route::get('get/all/payment','StripePaymentController@getAllPaymentInfo')->name('getAllPaymentInfo');