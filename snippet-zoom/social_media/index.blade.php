@extends('admin.layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{'Home'}}</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('socialMedia') }}">{{'Social Media Management'}}</a></li>
   <!--  <li class="breadcrumb-item active"><a href="#">{{__('Add Institute Admin')}}</a></li> -->
  </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"> 
                      <b>{{'Social Media Management'}}</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
                                    {{ session()->get('success') }} 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
                                    {{ session()->get('error') }} 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif

                                <form method="post" id="createFrm" name="createFrm"
                                action="{{route('saveSocialMedia')}}" enctype="multipart/form-data">
                                    @csrf
                                    

                                    <div class="form-group">
                                        <label class="col-form-label" for="institute_logo">{{'Facebook'}}</label>
                                        <input type="text" class="form-control" name="facebook" id="facebook" placeholder="Facebook" autocomplete="off" value="{{isset($data)?$data->facebook:''}}" />

                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Twitter'}}</label>

                                         <input type="text" class="form-control" name="twitter" id="twitter" placeholder="Twitter" autocomplete="off" value="{{isset($data)?$data->twitter:''}}" />
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Instagram'}}</label>
                                            
                                       <input type="text" class="form-control" name="instagram" id="instagram" placeholder="Instagram" autocomplete="off" value="{{isset($data)?$data->instagram:''}}" />
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label">{{'Linkedin'}}</label>
                                         <input type="text" class="form-control" name="linkedin" id="linkedin" placeholder="Linkedin" autocomplete="off" value="{{isset($data)?$data->linkedin:''}}" />
                                        
                                    </div>

                                   
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary" type="submit" value="Save">Save</button>
                                        <a href="{{ route('socialMedia') }}" class="btn btn-dark">{{'Back'}}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('page_level_css') 
@endsection
@section('page_level_js')
    @include('admin.social_media.js')
@endsection
