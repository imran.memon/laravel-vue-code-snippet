@extends('admin.final')

@section('title')
    <title>{{'View Course'}}</title>
@endsection

@section('page_level_css') 
    <!--  -->
@endsection

@section('main_content')
<div class="page-header row no-gutters py-4">
    <div class="col-12 mb-4 mt-3">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    {{'Session'}}
                </h6>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item p-3">
                    <div class="row">
                        <div class="col">
                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Course'}}</label>
                                        <select name="course_id" id="course_id" class="form-control" disabled="disabled">
                                            <option value="">Select</option>
                                               @if(!empty($course))
                                                    @foreach($course as $c)
                                                        @if($session->course_id == $c->id)
                                                            <option value="{{$c->id}}" selected="selected">
                                                                {{$c->title}}
                                                            </option>
                                                        @else
                                                            <option value="{{$c->id}}">
                                                                {{$c->title}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                              @endif
                                        </select>
                                    </div>
                                </div>

                        

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Session Title'}}</label>
                                        <input type="text" class="form-control" name="session_title" id="session_title" autocomplete="off" value="{{$session->session_title}}" disabled="disabled" />
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Session Number'}}</label>
                                        <input type="text" class="form-control" name="session_number" id="session_number" autocomplete="off" value="{{$session->session_number}}" disabled="disabled" />
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Documents'}}</label>
                                        <ul class="list-group" id="selectedDocuments">
                                            @if(!is_null($session->documents))
                                                @foreach(json_decode($session->documents,true) as $d)
                                                    <li class="list-group-item">
                                                        <a href="{{route('viewSessionDoc',$d)}}" target="_blank">
                                                            {{$d}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Language Level'}}</label>
                                        <select class="form-control" name="language_level" id="language_level" disabled="disabled">
                                            <option value="">Select</option>
                                            @foreach(config('constant.languageLevel') as $index => $level)
                                                @if($session->language_level == $index)
                                                    <option value="{{$index}}" selected="selected">{{$level}}</option>
                                                @else
                                                    <option value="{{$index}}">{{$level}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Description'}}</label>
                                        <textarea class="form-control" name="description" id="description" rows="5" cols="5" disabled="disabled">{{$session->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Status'}}</label>
                                        <select id="status" name="status" class="form-control col-md-6" disabled="">
                                        @foreach(config('constant.status') as $key => $value)
                                            @if(isset($session->status) && $session->status == $key)
                                                <option value="{{$key}}" selected="selected">{{$value}}</option>
                                            @else
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <a href="{{route('session.index')}}" class="btn btn-warning">
                                    {{'Back'}}
                                </a>
                            </form>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection

@section('page_level_js')
    <!--  -->
@endsection
