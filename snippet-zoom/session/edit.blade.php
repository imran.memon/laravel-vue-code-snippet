@extends('admin.layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="#">{{'Home'}}</a></li>
    <li class="breadcrumb-item"><a href="#">{{'Session'}}</a></li>
    <li class="breadcrumb-item active"><a href="#">{{'Edit Session'}}</a></li>
  </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"> 
                      <b>{{'Edit Session'}}</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" id="createFrm" name="createFrm"
                                action="{{ route('session.update',@$session->id)}}" enctype="multipart/form-data">
                                    @csrf
                                    
                                    <input type="hidden" name="sessionId" id="sessionId" value="{{$session->id}}" />
                                    <div class="form-group">
                                        <label class="col-form-label" for="institute_logo">{{'Course'}}<span class="text text-danger">*</span></label>
                                        <select name="course_id" id="course_id" class="form-control">
                                            <option value="">Select</option>
                                         @if(!empty($course))
                                            @foreach($course as $c)
                                                @if($session->course_id == $c->id)
                                                    <option value="{{$c->id}}" selected="selected">
                                                        {{$c->title}}
                                                    </option>
                                                @else
                                                    <option value="{{$c->id}}">
                                                        {{$c->title}}
                                                    </option> 
                                                @endif
                                            @endforeach
                                        @endif
                                        </select>

                                        @if ($errors->has('course_id'))
                                            <span class="text text-danger">
                                                <strong>{{'The Course field is required.'}}</strong>
                                            </span>
                                        @endif

                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Session Title'}}<span class="text text-danger">*</span></label>
                                       <input type="text" name="session_title" class="form-control" id="session_title" placeholder="Session Title" value="{{$session->session_title}}">
                                        @if ($errors->has('session_title'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('session_title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Session Number'}}<span class="text text-danger">*</span></label>
                                      <input type="number" name="session_number" class="form-control" id="session_number" placeholder="Session Number" value="{{$session->session_number}}">
                                        @if ($errors->has('session_number'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('session_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-form-label">{{'Documents'}}</label>
                                          <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="documents[]" id="documents" multiple="multiple" />
                                                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                            </div>

                                            <ul class="list-group" id="selectedDocuments">
                                                @if(!is_null($session->documents))
                                                    @foreach(json_decode($session->documents,true) as $d)
                                                        <li class="list-group-item">
                                                            <a href="{{route('viewSessionDoc',$d)}}" target="_blank">
                                                                {{$d}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Language Level'}}</label>
                                         <select class="form-control" name="language_level" id="language_level">
                                            <option value="">Select</option>
                                            @foreach(config('constant.languageLevel') as $index => $level)
                                                @if($session->language_level == $index)
                                                    <option value="{{$index}}" selected="selected">{{$level}}</option>
                                                @else
                                                    <option value="{{$index}}">{{$level}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Description'}}</label>
                                         <textarea class="form-control" name="description" id="description" rows="5" cols="5">{{$session->description}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Status'}}<span class="text text-danger">*</span></label>
                                         <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                                @foreach(config('constant.status') as $key => $value)
                                                    @if(isset($session) && $session->status == $key)
                                                    <option value="{{$key}}" selected="selected">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                            @if ($errors->has('status'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary" type="submit" value="Save">Save</button>
                                        <a href="{{ route('session.index') }}" class="btn btn-dark">{{'Back'}}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('page_level_css') 
@endsection
@section('page_level_js')
    @include('admin.session.sessionJs')
@endsection
