@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="accordion mb-3" id="accordionExample">
                <div class="card rounded">
                    <div class="card-header collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo">
                        <h4 class="mb-0"><b>{{'Search'}}</b><i class="fa fa-angle-down" style="float:right;"></i></h4>
                    </div>
                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                        data-parent="#accordionExample">
                        <form method="post"
                        action="{{route('session.index')}}">
                         @csrf
                        <div class="card-body">
                           
                               @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Session Title'}}:</label>
                                                <select name="searchSessionTitle" id="searchSessionTitle" class="form-control">
                                                <option value="">Select</option>
                                                @foreach(@$sessionData as $val)
                                                    @if(@$searchSessionTitle == $val->session_title)
                                                        <option selected value="{{@$val->session_title}}">{{@$val->session_title}}</option>
                                                    @else
                                                        <option value="{{@$val->session_title}}">{{@$val->session_title}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Session Number'}}:</label>
                                                <select name="searchSessionNumber" id="searchSessionNumber" class="form-control">
                                                <option value="">Select</option>
                                                @foreach(@$sessionData as $val)
                                                    @if(@$searchSessionNumber == $val->session_number)
                                                        <option value="{{@$val->session_number}}" selected>{{@$val->session_number}}</option>
                                                    @else
                                                        <option value="{{@$val->session_number}}">{{@$val->session_number}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Corse Title'}}</label>
                                                <select name="searchCourseTitle"id="searchCourseTitle"class="form-control">
                                                <option value="">Select</option>
                                                @foreach(@$courseData as $val)
                                                    @if(@$searchCourseTitle == $val->title)
                                                        <option value="{{@$val->title}}" selected>{{@$val->title}}</option>
                                                    @else
                                                        <option value="{{@$val->title}}">{{@$val->title}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Course Number'}}</label>
                                                <select name="searchCourseNumber" id="searchCourseNumber" class="form-control">
                                                <option value="">Select</option>
                                                @foreach(@$courseData as $val)
                                                    @if(@$searchCourseNumber == $val->code)
                                                        <option value="{{@$val->code}}" selected>{{@$val->code}}</option>
                                                    @else
                                                        <option value="{{@$val->code}}">{{@$val->code}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Status'}}</label>
                                                <select name="searchStatus" id="searchStatus" class="form-control">
                                                <option value="">Select</option>
                                                @foreach(config('constant.searchStatus') as $key => $value)
                                                    @if($searchStatus == $key)
                                                        <option value="{{$key}}" selected>{{$value}}</option>
                                                    @else
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <button type="submit" name="search_submit" value="1"
                                class="btn btn-primary">{{'Search'}}</button>
                            <a href="{{route('session.index')}}"
                                class="btn btn-dark">{{'Reset'}}</a>
                            </div>
                        </div>
                        
                    </form>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="card-header-actions">

                        <a href="{{ route('session.create') }}"
                            class="btn btn-primary student_btn">
                            <b><i class="fa fa-plus-circle" aria-hidden="true"></i> {{'Add'}}</b>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                     @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
                            {{ session()->get('success') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
                            {{ session()->get('error') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        
                    @endif
                    <table class="table table-striped table-bordered transaction-history">
                        <thead>
                            <tr>
                                <th>{{'Id'}}</th>
                                <th>{{'Course Title'}}</th>
                                <th>{{'Course Code'}}</th>
                                <th>{{'Session Title'}}</th>
                                <th>{{'Session Number'}}</th>
                                <th>{{'Status'}}</th>
                                <th>{{'Actions'}}</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if (!empty($session))
                            @php
                                $i=1;
                            @endphp
                            @foreach ($session as $s)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>
                                    <?php 
                                        if(isset($s->course_detail)){
                                            echo $s->course_detail->title;
                                        }else{
                                            echo 'N/A';
                                        }
                                    ?>
                                    </td>
                                    <td>
                                        <?php 
                                        if(isset($s->course_detail)){
                                            echo $s->course_detail->code;
                                        }else{
                                            echo 'N/A';
                                        }
                                    ?>
                                    </td>
                                    <td>{{@$s->session_title}}</td>
                                    <td>{{@$s->session_number}}</td>
                                    
                                    <td>
                                        <label class="switch">
                                           <input class="status" value="{{@$s->status}}" 
                                            data-id = "{{@$s->id}}" type="checkbox" data-toggle="toggle" data-size="xs" data-onstyle="success" data-offstyle="outline-danger" data-on="Active" data-off="Deactive" @if($s->status == 1) checked @endif />
                                        </label>
                                    </td>
                                    <td>

                                        <a class="btn btn-success" href="{{route('session.edit',@$s->id)}}" data-toggle="tooltip" data-original-title="{{'Edit'}}">
                                        <svg class="c-icon">
                                            <use xlink:href="{{ asset('icons/free.svg#cil-pencil') }}"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info view" href="javascript:void(0);" id="{{$s->id}}" data-toggle="tooltip" data-original-title="{{'View'}}">
                                        <svg class="c-icon">
                                            <use xlink:href="{{ asset('icons/free.svg#cil-description') }}"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger delete" href="javascript:void(0);" id="{{$s->id}}" data-toggle="tooltip" data-original-title="{{'Delete'}}">
                                        <svg class="c-icon">
                                            <use xlink:href="{{ asset('icons/free.svg#cil-trash') }}"></use>
                                        </svg>
                                    </a>

                                        
                                    </td>
                                </tr>
                                @php
                                    $i=$i+1;
                                @endphp
                            @endforeach
                        @endif  
                        </tbody>
                    </table>
                    <div class="modal" tabindex="-1" role="dialog" id="myModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-titlepage-title">{{'Session'}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="padding-top: 5px; padding-bottom: 1px;" id="modal-body">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">{{'Back'}}</button>
          </div>
        </div>
      </div>
    </div>
<meta name="csrf-token" content="{{ csrf_token() }}" />
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_css')
    
@endsection
@section('page_level_js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  
    @include('admin.session.sessionJs')
@endsection
