@extends('admin.layouts.app')

@section('title')
    {{'Tutor/Native Speaker'}}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="accordion mb-3" id="accordionExample">
                <div class="card rounded">
                    <div class="card-header collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo">
                        <h4 class="mb-0"><b>{{'Search'}}</b><i class="fa fa-angle-down" style="float:right;"></i></h4>
                    </div>

                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <form method="post" action="{{route('speaker')}}">
                         @csrf
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">{{'Name'}}</label>
                                            <select name="searchName" id="searchName"
                                                class="form-control select2Drp">
                                                <option value="">Select</option>
                                                @foreach($searachSpeakeres as $value)
                                                    @if($searchNameVal == $value->name)
                                                        <option value="{{$value->name}}" selected="selected">{{$value->name}}</option>
                                                    @else
                                                        <option value="{{$value->name}}">
                                                            {{$value->name}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">{{'Email'}}</label>
                                            <select name="searchEmail" id="searchEmail"
                                                class="form-control select2Drp">
                                                <option value="">Select</option>
                                                @foreach($searachSpeakeres as $value)
                                                    @if($searchEmailVal == $value->email)
                                                        <option value="{{$value->email}}" selected="selected">
                                                            {{$value->email}}
                                                        </option>
                                                    @else
                                                        <option value="{{$value->email}}">
                                                            {{$value->email}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">{{'Nationality'}}</label>
                                            <select name="searchNationality" id="searchNationality"
                                                class="form-control select2Drp">
                                               <option value="">Select</option>
                                                @foreach($searchCountries as $value)
                                                    @if($searchNationalityVal == $value->id)
                                                        <option value="{{$value->id}}" selected="selected">{{$value->name}}</option>
                                                    @else
                                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">{{'Status'}}</label>
                                            <select name="searchStatus" id="searchStatus" class="form-control select2Drp">
                                              <option value="">Select</option>
                                                @foreach(config('constant.searchStatus') as $key => $value)
                                                    @if($searchStatusVal == $key)
                                                        <option value="{{$key}}" selected="selected">{{$value}}</option>
                                                    @else
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <div class="text-right">
                                <button type="submit" name="search_submit" value="1"
                                class="btn btn-primary">{{'Search'}}</button>
                            <a href="{{route('speaker')}}"
                                class="btn btn-dark">{{'Reset'}}</a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="card-header-actions">
                        <a href="{{ route('createSpeaker') }}" class="btn btn-primary student_btn">
                            <b><i class="fa fa-plus-circle" aria-hidden="true"></i>{{' Add'}}</b>
                        </a>

                        <a href="{{ route('speaker.importView') }}" class="btn btn-primary student_btn">    <b><i class="fa fa-upload" aria-hidden="true"></i>{{' Import'}}</b>
                        </a>

                        <a href="{{ route('speaker.downloadFormat') }}" class="btn btn-primary student_btn">
                            <b><i class="fa fa-download" aria-hidden="true"></i>{{' Download Excel File'}}</b>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show m-0 mb-1" role="alert">
                            {{ session()->get('success') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible fade show m-0 mb-1" role="alert">
                            {{ session()->get('error') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('warning'))
                        <div class="alert alert-warning alert-dismissible fade show m-0 mb-1" role="alert">
                            {{ session()->get('warning') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    <table class="table table-striped table-bordered" id="listing">
                        <thead>
                            <tr>
                                <th>{{'No'}}</th>
                                <th>{{'Name'}}</th>
                                <th>{{'Email'}}</th>
                                <th>{{'Nationality'}}</th>
                                <th>{{'Status'}}</th>
                                <th>{{'Action'}}</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(!empty($speakeres))
                            @foreach($speakeres as $index => $speakere)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td>{{$speakere->name}}</td>
                                    <td>{{$speakere->email}}</td>
                                    <td>{{$speakere->cname}}</td>
                                    <td>
                                        <input type="checkbox" class="speakerStatus" data-modal="User" data-module="speaker" id="{{$speakere->id}}" data-toggle="toggle" data-size="xs" data-onstyle="success" data-offstyle="outline-danger" data-on="Active" data-off="Deactive" @if($speakere->status == 1) checked @endif />
                                    </td>
                                    <td>
                                        <a class="btn btn-success" href="{{route('editSpeaker',$speakere->id)}}" data-toggle="tooltip" data-original-title="{{'Edit'}}">
                                            <svg class="c-icon">
                                                <use xlink:href="{{ asset('icons/free.svg#cil-pencil') }}"></use>
                                            </svg>
                                        </a>

                                        <a class="btn btn-info view" href="javascript:void(0);" id="{{$speakere->id}}" data-toggle="tooltip" data-original-title="{{'View'}}">
                                            <svg class="c-icon">
                                                <use xlink:href="{{ asset('icons/free.svg#cil-description') }}"></use>
                                            </svg>
                                        </a>

                                        <a class="btn btn-danger delete" href="javascript:void(0);" id="{{$speakere->id}}" data-toggle="tooltip" data-original-title="{{'Delete'}}">
                                            <svg class="c-icon">
                                                <use xlink:href="{{ asset('icons/free.svg#cil-trash') }}"></use>
                                            </svg>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif            
                        </tbody>
                    </table>

                    <div class="modal" tabindex="-1" role="dialog" id="myModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-titlepage-title"> {{'View Speaker'}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                
                                <div class="modal-body" style="padding-top: 5px; padding-bottom: 1px;" id="modal-body">
                                    <!--  -->
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning btn-dark" data-dismiss="modal">{{'Back'}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_level_js')    
     @include('admin.speaker.js')
@endsection
