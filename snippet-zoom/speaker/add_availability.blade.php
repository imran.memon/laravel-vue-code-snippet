@extends('admin.layouts.app')

@section('title')
    <title>{{'Add Availability'}}</title>
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Admin</a></li>
        <li class="breadcrumb-item active">
            <a href="javascript:void(0);">Availability</a>
        </li>
    </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> 
                       <b>{{'Availability'}}</b>
                    </div>

                    <form method="post" action="{{route('saveAvailability')}}" id="createFrm">
                    @csrf
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-md-12">

                                <?php /*
                                <div class="form-row col-md-12">
                                    <div class="form-group col-md-12">
                                        <a href="javascript:void(0);" id="addMoreBreak">
                                            <strong>{{__('Add More')}}</strong>
                                        </a>
                                    </div>
                                </div>

                                <div class="form-row col-md-12">
                                    <div class="form-group col-md-6">
                                        <label>{{__('Break Start Date')}}<span class="text text-danger">*</span></label>

                                        <div class="input-group">
                                            <input type="text" class="form-control break_start_date" name="break_start_date[]"  autocomplete="off" readonly="readonly" id="brkStartDate0" />
                                            
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="far fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>{{__('Break End Date')}}<span class="text text-danger">*</span></label>

                                        <div class="input-group">
                                            <input type="text" class="form-control break_end_date" name="break_end_date[]"  autocomplete="off" readonly="readonly" id="brkEndDate0" />

                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="far fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="moreBreak"></div>
                                
                                <div class="form-row col-md-12">
                                    <div class="form-group col-md-6">
                                        <label>{{__('Hoildays')}}</label>

                                        <div class="input-group">
                                            <input type="text" class="form-control" name="holiday" id="holiday" autocomplete="off" readonly="readonly" />

                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="far fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                */ ?>

                                <div class="form-row">
                                    <div class="col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label>{{'Time'}}</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control timeBlock" name="start_time_block[]" autocomplete="off" readonly="readonly" id="strTimeBlock0" />
                                                
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label>{{'Time'}}</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control timeBlock" name="end_time_block[]" autocomplete="off" readonly="readonly" id="endTimeBlock0" />

                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label>
                                                <a href="javascript:void(0);" id="addMoreBooking">
                                                    <strong>{{'Add More'}}</strong>
                                                </a>
                                            </label>
                                            
                                            <div class="weekDays-selector d-flex">
                                                @foreach(config('constant.Days') as $key => $weekDays)
                                                    <input type="checkbox" name="timedays[0][]" id="{{$key}}" value="{{$key}}" />
                                                    <label for="{{$key}}">{{$weekDays}}</label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="moreBooking"></div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <input type="submit" class="btn btn-success" value="Save" id="save" />
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_level_css')
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.multidatespicker.css') }}">
    <link href="{{ asset('css/mdtimepicker.css') }}" rel="stylesheet">
    <style type="text/css">
        .weekDays-selector input {
            display: none!important;
        }

        .weekDays-selector input[type=checkbox] + label {
            display: inline-block;
            border-radius: 6px;
            background: #dddddd;
            width: auto;
            margin-right: 3px;
            line-height: 1;
            text-align: center;
            cursor: pointer;
            padding:10px 15px;
        }

        .weekDays-selector input[type=checkbox]:checked + label {
            background: #D83A3A;
            color: #ffffff;
        }
    </style>
@endsection

@section('page_level_js')
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-ui.multidatespicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/mdtimepicker.js') }}"></script>
    @include('admin.speaker.js_add_availability')
@endsection

