<script type="text/javascript">
$(document).ready(function(){
	$("#listing").DataTable({
        responsive: true,
        autoWidth: false,
        order: [],
        columnDefs: [{ 
            'orderable': false, 'targets': [4,5]
        }],
        oLanguage: {
            sEmptyTable: "{{__('No data available in table')}}",
            sZeroRecords: "{{__('No records')}}",
            sSearch: '<em class="fas fa-search"></em>',
            sLengthMenu: '_MENU_ {{__("records per page")}}',
            sInfo: '{{_("Showing")}} _START_ {{_("to")}} _END_ {{_("of")}} _TOTAL_ {{_("entries")}}',
            zeroRecords: "{{__('No records')}}",
            infoEmpty: "{{__('No records')}}",
            infoFiltered: '(filtered from MAX total records)',
            oPaginate: {
                sNext: '<em class="fa fa-caret-right"></em>',
                sPrevious: '<em class="fa fa-caret-left"></em>'
            }
        }  
    });
    
	jQuery.validator.addMethod("valid_email", function (value, element) {
		return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
	}, 'Please enter valid email');

	$('#createFrm').validate({
	    rules: {
	    	name: {
	        	required: true
	      	},
	      	email: {
	        	required: true,
	        	valid_email:true
	      	},
	      	country: {
	        	required: true,
	      	},
            zone: {
                required: true,
            },
	      	languages: {
	        	required: true
	      	},
	      	showInFront: {
	        	required: true
	      	},
	      	status: {
	        	required: true
	      	}
	    },messages: {
	    	name: {
	        	required: "Please enter name"
	      	},
	      	email: {
	        	required: "Please enter email"
	      	},
	      	country: {
	        	required: "Please select country"
	      	},
            zone: {
                required: "Please select zone"
            },
	      	languages: {
	      		required: "Please select language"
	      	},
	      	showInFront: {
	      		required: "Please select show in front"
	      	},
	      	status: {
	      		required: "Please select status"
	      	}
	    }
  	}); 

  	$(document).on("click", ".delete", function(){
        var id = $(this).attr('id');

        Swal.fire({
          title: 'Are you sure you want to delete this Tutor/Native Speaker?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
            	$.ajax({
                    type:'GET',
                    url: getsiteurl()+'/admin/delete/speaker',
                    data: {id:id},
                    success: (response) => {
                        Swal.fire({
                            type: response.status,
                            title: response.message
                        });

                        if(response.status == 'success'){
                            location.reload();
                        }
                    }
                });
            }
        });
    });

    $(document).on("change", ".speakerStatus", function() {
        var id = $(this).attr('id');

        $.ajax({
            type:'GET',
            url: getsiteurl()+'/admin/change/speaker_status',
            data: {id:id},
            success: (response) => {
                Swal.fire({
                    type: response.status,
                    title: response.message
                });

                if(response.status == 'warning'){
		    		$(this).parent().removeClass("toggle btn btn-xs btn-outline-danger off");
		    		$(this).parent().addClass("toggle btn btn-xs btn-success");
                }
            }
        });
    });

    $(document).on("click", ".view", function(){
      var id = $(this).attr('id');
      $.ajax({
            url: 'view/speaker/'+id,
            type: 'GET',
            success: function(oData){
            $('#modal-body').html(oData.html);
            $("#myModal").modal('show');

           },
        });
   });

    $('.select2Drp').select2({
        theme : "bootstrap"
    });
});
</script>