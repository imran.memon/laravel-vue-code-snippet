@extends('admin.layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{'Home'}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('speaker')}}">{{'Tutor/Native Speaker'}}</a></li>
    <li class="breadcrumb-item active"><a href="#">{{'Import Tutor/Native Speaker'}}</a></li>
  </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"> 
                      <b>{{'Import Tutor/Native Speaker'}}</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" id="createFrm" name="createFrm"
                                action="{{ route('speaker.import') }}" enctype="multipart/form-data">
                                    @csrf
                                    

                                    <div class="form-group">
                                        <label class="col-form-label" for="Name">{{'Tutor/Native Speaker Import File'}}</label>
                                        <div class="custom-file" id="bannerDiv">
                                            <input type="file" class="custom-file-input" name="speaker_import_file" id="speaker_import_file"/>
                                            <label class="custom-file-label">Choose file...</label>
                                        </div>
                                            @if ($errors->has('speaker_import_file'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('speaker_import_file') }}</strong>
                                                </span>
                                            @endif

                                            @if($failures)
                                                <ul>
                                                    @foreach ($failures as $failure)
                                                        @foreach ($failure->errors() as $error)
                                                            <li style="color:red;">{{ $error }}</li>
                                                        @endforeach
                                                    @endforeach
                                                </ul>
                                            @endif

                                    </div>
                                   
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary" type="submit" value="Save">Save</button>
                                        <a href="{{ route('speaker') }}" class="btn btn-warning btn-dark">{{'Back'}}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('page_level_css') 
@endsection
@section('page_level_js')
<script type="text/javascript">
    $('#speaker_import_file').change(function() {
          var i = $(this).prev('label').clone();
          var file = $('#speaker_import_file')[0].files[0].name;
          $('.custom-file-label').text(file);
        });
</script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
@endsection
