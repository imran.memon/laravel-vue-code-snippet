<div class="form-row " id="{{$randomNum}}">
    <div class="col-md-4 col-lg-3">
        <div class="form-group">
            <div class="input-group">
                <input type="text" class="form-control timeBlock" name="start_time_block[]" autocomplete="off" id="{{'strTimeBlock'.$bookingLength}}" readonly="readonly" />
                
                <div class="input-group-append">
                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-lg-3">
        <div class="form-group">
            <div class="input-group">
                <input type="text" class="form-control timeBlock" name="end_time_block[]" autocomplete="off" id="{{'endTimeBlock'.$bookingLength}}" readonly="readonly" />

                <div class="input-group-append">
                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-lg-3">
        <div class="form-group">
           <div class="weekDays-selector d-flex">
                @foreach(config('constant.Days') as $key => $weekDays)
                    <input type="checkbox" id="{{$key}}{{$bookingLength}}" name="{{'timedays['.$bookingLength.'][]'}}" value="{{$key}}" /> 
                    <label for="{{$key}}{{$bookingLength}}">{{$weekDays}}</label>
                   
                @endforeach
    
                <a class="btn btn-danger ml-2 removeBooking" id="{{$randomNum}}">
                    <i class="fas fa-times"></i>
                </a>
        </div>
    </div>
</div>