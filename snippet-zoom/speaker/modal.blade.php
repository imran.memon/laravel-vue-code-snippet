<div class="page-header row no-gutters">
    <div class="col-12">
        <div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item p-3">
                    <div class="row">
                        <div class="col">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>{{'Name'}}</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" autocomplete="off" value="{{$speaker->name}}" disabled="disabled" />
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>{{'Email'}}</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" value="{{$speaker->email}}" disabled="disabled" />
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>{{'Country'}}</label>
                                    <select name="country" id="country" class="form-control" disabled="disabled">
                                        <option value="">Select</option>
                                        @foreach($countries as $countrie)
                                            @if(isset($speaker) && $speaker->country_id == $countrie->id)
                                                <option selected="selected" value="{{$countrie->id}}">
                                                    {{$countrie->name}}
                                                </option>
                                            @else
                                                <option value="{{$countrie->id}}">
                                                    {{$countrie->name}}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>{{'Languages'}}</label>
                                    <select name="languages" id="languages" class="form-control" disabled="disabled">
                                        <option value="">Select</option>
                                        @foreach(config('constant.languages') as $index => $language)
                                            @if(isset($speaker) && $speaker->language_id == $index)
                                                <option selected="selected" value="{{$index}}">
                                                    {{$language}}
                                                </option>
                                            @else
                                                <option value="{{$index}}">
                                                    {{$language}}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>{{'Descrription'}}</label>
                                    <textarea class="form-control" name="desc" id="desc" rows="5" cols="5" disabled="disabled">{{isset($speaker)?$speaker->description:''}}</textarea>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>{{'Show In Front'}}</label>
                                    <select name="showInFront" id="showInFront" class="form-control" disabled="disabled">
                                        <option value="">Select</option>
                                        @if(isset($speaker))
                                            @foreach(config('constant.showInFront') as $index => $value)
                                                @if(isset($speaker) && $speaker->show_in_front == $index)
                                                    <option value="{{$index}}" selected="selected">{{$value}}</option>
                                                @else
                                                    <option value="{{$index}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>{{'Status'}}</label>
                                    <select name="status" id="status" class="form-control" disabled="disabled">
                                        <option value="">Select</option>
                                        @foreach(config('constant.status') as $key => $value)
                                            @if(isset($speaker) && $speaker->status == $key)
                                                <option value="{{$key}}" selected="selected">{{$value}}</option>
                                            @else
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>



