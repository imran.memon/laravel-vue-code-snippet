@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="accordion mb-3" id="accordionExample">
                <div class="card rounded">
                    <div class="card-header collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo">
                        <h4 class="mb-0"><b>{{'Search'}}</b><i class="fa fa-angle-down" style="float:right;"></i></h4>
                    </div>
                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                        data-parent="#accordionExample">
                        <form method="post"
                        action="{{route('student.index')}}">
                         @csrf
                        <div class="card-body">
                           
                               @csrf
                                <div class="form-body">
                                    <div class="row">

                                        @if(config('constant.adminRoleId') == Auth::user()->role_id)
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">{{'Institution Name'}}</label>
                                                    <select name="searchInstituteName" id="searchInstituteName" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach(@$instituteData as  $value)
                                                            @if(@$searchInstituteName == $value->id)
                                                                <option value="{{@$value->id}}" selected>{{@$value->name}}</option>
                                                            @else
                                                                <option value="{{@$value->id}}">{{@$value->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endif

                                        @if(config('constant.adminRoleId') == Auth::user()->role_id || config('constant.insAdminRoleId') == Auth::user()->role_id )
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">{{'Institution Coordinator'}}</label>
                                                    <select name="searchInstituteCoord" id="searchInstituteCoord" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach(@$instituteCoOrdData as  $value)
                                                            @if(@$searchInstituteCoord == $value->id)
                                                                <option value="{{@$value->id}}" selected>{{@$value->name}}</option>
                                                            @else
                                                                <option value="{{@$value->id}}">{{@$value->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endif

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Student Name'}}:</label>
                                                <select name="searchStudentName" id="searchStudentName"
                                                    class="form-control">
                                                   <option value="">Select</option>
                                                    @foreach(@$StudentData as $value)
                                                        @if(@$searchStudentName == $value->name)
                                                            <option value="{{@$value->name}}" selected>{{@$value->name}}</option>
                                                        @else
                                                            <option value="{{@$value->name}}">{{@$value->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Student Email'}}:</label>
                                                <select name="searchStudentEmail" id="searchStudentEmail"
                                                    class="form-control">
                                                    <option value="">Select</option>
                                                    @foreach(@$StudentData as $value)
                                                        @if(@$searchStudentEmail == $value->name)
                                                            <option value="{{@$value->name}}" selected>{{@$value->name}}</option>
                                                        @else
                                                            <option value="{{@$value->name}}">{{@$value->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Status'}}</label>
                                                <select name="searchStatus" id="searchStatus" class="form-control">
                                                   <option value="">Select</option>
                                                @foreach(config('constant.searchStatus') as $key => $value)
                                                    @if($searchStatus == $key)
                                                        <option value="{{$key}}" selected="selected">{{$value}}</option>
                                                    @else
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endif                                                   
                                                @endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <button type="submit" name="search_submit" value="1"
                                class="btn btn-primary">{{'Search'}}</button>
                            <a href="{{route('student.index')}}"
                                class="btn btn-dark">{{'Reset'}}</a>
                            </div>
                        </div>
                        
                    </form>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="card-header-actions">

                        <a href="{{ route('student.create') }}"
                            class="btn btn-primary student_btn">
                            <b><i class="fa fa-plus-circle" aria-hidden="true"></i> {{'Add'}}</b>
                        </a>
                        <a href="{{ route('student.importView') }}"
                            class="btn btn-primary student_btn"><b><i class="fa fa-upload" aria-hidden="true"></i>
                                {{'Import'}}</b></a>

                        <a href="{{ route('student.downloadFormat') }}"
                            class="btn btn-primary student_btn"><b><i class="fa fa-download" aria-hidden="true"></i>
                                {{'Download Excel File'}}</b></a>
                    </div>
                </div>
                <div class="card-body">
                     @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
                            {{ session()->get('success') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
                            {{ session()->get('error') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        
                    @endif
                    <table class="table table-striped table-bordered" id="listing">
                        <thead>
                            <tr>
                                <th>{{'Id'}}</th>
                                <th>{{'Institute Name'}}</th>
                                <th>{{'Institute Co-Ordinator Name'}}</th>
                                <th>{{'Student Name'}}</th>
                                <th>{{'Student Email'}}</th>
                                <th>{{'Status'}}</th>
                                <th>{{'Actions'}}</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if (!empty($students))
                @php
                    $i=1;
                @endphp
                @foreach ($students as $student)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{@$student->ins_name}}</td>
                        <td>{{@$student->ins_coord_name}}</td>
                        <td>{{@$student->name}}</td>
                        <td>{{@$student->email}}</td>
                        <td>

                            <label class="switch">
                            <input class="status" value="{{@$student->status}}" 
                                data-id = "{{@$student->id}}" type="checkbox" data-toggle="toggle" data-size="xs" data-onstyle="success" data-offstyle="outline-danger" data-on="Active" data-off="Deactive" @if($student->status == 1) checked @endif />
                            </label>

                            
                        </td>
                        <td>

                            <a class="btn btn-success" href="{{route('student.edit',@$student->id)}}" data-toggle="tooltip" data-original-title="{{'Edit'}}">
                                <svg class="c-icon">
                                   <use xlink:href="{{ asset('icons/free.svg#cil-pencil') }}"></use>
                                </svg>
                            </a>
                            <a class="btn btn-info view" href="javascript:void(0);" id="{{$student->id}}" data-toggle="tooltip" data-original-title="{{'View'}}">
                                <svg class="c-icon">
                                    <use xlink:href="{{ asset('icons/free.svg#cil-description') }}"></use>
                                </svg>
                            </a>
                            <a class="btn btn-danger delete" href="javascript:void(0);" id="{{$student->id}}" data-toggle="tooltip" data-original-title="{{'Delete'}}">
                                <svg class="c-icon">
                                    <use xlink:href="{{ asset('icons/free.svg#cil-trash') }}"></use>
                                </svg>
                            </a>

                            
                        </td>
                    </tr>
                    @php
                        $i=$i+1;
                    @endphp
                @endforeach
            @endif
                            
                        </tbody>
                    </table>
                    <div class="modal" tabindex="-1" role="dialog" id="myModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-titlepage-title">{{'Student Details'}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="padding-top: 5px; padding-bottom: 1px;" id="modal-body-student">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning btn-dark" data-dismiss="modal">{{'Back'}}</button>
          </div>
        </div>
      </div>
    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_css')
    
@endsection
@section('page_level_js')
    
     @include('admin.student.studentJs')
@endsection
