@extends('admin.layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{'Home'}}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('student.index') }}">{{'Student'}}</a></li>
    <li class="breadcrumb-item active"><a href="#">{{'Edit Student'}}</a></li>
  </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"> 
                      <b>{{'Edit Student'}}</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" id="createFrm" name="createFrm"
                                action="{{ route('student.update',@$student->id)}}" enctype="multipart/form-data">
                                    @csrf
                                    

                                    <div class="form-group">
                                        <label class="col-form-label" for="insName">{{'Institution Name'}}<span class="text text-danger">*</span></label>
                                        <select name="insName" id="insName" class="form-control">
                                                <option value="">Select</option>
                                                @foreach($institutions as $institution)
                                                    @if(isset($student) && $student->institute_id == $institution->id)
                                                        <option value="{{$institution->id}}" selected="selected">
                                                            {{$institution->name}}
                                                        </option>
                                                    @else
                                                        <option value="{{$institution->id}}">
                                                            {{$institution->name}}
                                                        </option> 
                                                    @endif
                                                @endforeach
                                            </select>

                                            @if ($errors->has('insName'))
                                                <span class="text text-danger">
                                                    <strong>{{'The institution name field is required.'}}</strong>
                                                </span>
                                            @endif

                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Institute Co-Ordinator Name'}}</label>
                                       <select name="insCoordName" id="insCoordName" class="form-control">
                                                <option value="">Select</option>
                                                @foreach($insCoordinations as $insCoordination)
                                                    @if(isset($student) && $student->institute_coordinator_id == $insCoordination->id)
                                                        <option value="{{$insCoordination->id}}" selected="selected">
                                                            {{$insCoordination->name}}
                                                        </option>
                                                    @else
                                                        <option value="{{$insCoordination->id}}">
                                                            {{$insCoordination->name}}
                                                        </option> 
                                                    @endif
                                                @endforeach
                                            </select>
                                            @if ($errors->has('insCoordName'))
                                                <span class="text text-danger">
                                                    <strong>{{'The institution Co ordinator name field is required.'}}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Student Name'}}</label>
                                     <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{$student->name}}">
                                        @if ($errors->has('name'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                    
                                        <div class="form-group">
                                            <label for="Email">{{'Student Email'}}</label>
                                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{$student->email}}">
                                            @if ($errors->has('email'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    

                                    <div class="form-group">
                                        <label class="col-form-label">{{'Description'}}</label>
                                         <textarea name="description" class="form-control" id="description" placeholder="{{__('Description')}}">{{!empty($student->description) ? $student->description:''}}</textarea>
                                        
                                    </div>


                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Status'}}</label>
                                         <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                                @foreach(config('constant.status') as $key => $value)
                                                    @if(isset($student) && $student->status == $key)
                                                    <option value="{{$key}}" selected="selected">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                            @if ($errors->has('status'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary" type="submit" value="Save">Save</button>
                                        <a href="{{ route('student.index') }}" class="btn btn-warning btn-dark">{{'Back'}}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('page_level_css') 
@endsection
@section('page_level_js')

@include('admin.student.studentJs')
@endsection
