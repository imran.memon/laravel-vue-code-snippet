@extends('admin.layouts.app')

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{'Home'}}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('student.index') }}">{{'Student'}} </a></li>
    <li class="breadcrumb-item active"><a href="#">{{'Add Stundent'}}</a></li>
  </ol>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"> 
                      <b>{{'Add Stundent'}}</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" id="createFrm" name="createFrm"
                                action="{{ route('student.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    

                                    <div class="form-group">
                                        <label class="col-form-label" for="institute_logo">{{'Institution Name'}}<span class="text text-danger">*</span></label>
                                        <select name="insName" id="insName" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($institutions as $institution)
                                            @if(Auth::user()->role_id == config('constant.insCoorRoleId') && Auth::user()->institute_id == $institution->id)
                                                <option value="{{$institution->id}}" selected="selected">
                                                    {{$institution->name}}
                                                </option>
                                            @else
                                                <option value="{{$institution->id}}">
                                                    {{$institution->name}}
                                                </option> 
                                            @endif
                                        @endforeach
                                    </select>
                                        @if ($errors->has('insName'))
                                            <span class="helpBlock alert">
                                                <strong>{{'The institution name field is required.'}}</strong>
                                            </span>
                                        @endif

                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Institute Co-Ordinator Name'}}<span class="text text-danger">*</span></label>

                                        <select name="insCoordName" id="insCoordName" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($insCoordinations as $insCoordination)
                                                <option value="{{$insCoordination->id}}">
                                                    {{$insCoordination->name}}
                                                </option> 
                                               
                                            @endforeach
                                        </select>
                                        @if ($errors->has('insCoordName'))
                                            <span class="helpBlock alert">
                                                <strong>{{'The institution Co ordinator name field is required.'}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Student Name'}}<span class="text text-danger">*</span></label>
                                            
                                       <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{old('name')}}">
                                        @if ($errors->has('name'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Student Email'}}<span class="text text-danger">*</span></label>
                                            
                                       <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{old('email')}}">
                                        @if ($errors->has('email'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                   
                                    <div class="form-group">
                                        <label class="col-form-label">{{'Description'}}</label>
                                          <textarea name="description" class="form-control" id="description"
                                        placeholder="{{__('Description')}}"></textarea>
                                        @if ($errors->has('description'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>


                                    <div class="form-group">
                                        <label class="col-form-label" >{{'Status'}}</label>
                                          <select name="status" id="status" class="form-control">
                                        <option value="">Select</option>
                                        @foreach(config('constant.status') as $key => $value)
                                               <option value="{{$key}}">{{$value}}</option>
                                           
                                            @endforeach
                                        </select>
                                        @if ($errors->has('status'))
                                            <span class="helpBlock alert">
                                                <strong>{{ $errors->first('status') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary" type="submit" value="Save">Save</button>
                                        <a href="{{ route('student.index') }}" class="btn btn-warning btn-dark">{{'Back'}}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('page_level_css') 
@endsection
@section('page_level_js')
     @include('admin.student.studentJs')
@endsection
