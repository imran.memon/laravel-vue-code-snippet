<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
   jQuery.validator.addMethod("valid_email", function (value, element) {
      return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
   }, 'Please enter valid email');
   
   $('#searchInstituteName').select2({
      theme : "bootstrap"
   });
   $('#searchInstituteCoord').select2({
      theme : "bootstrap"
   });
   $('#searchStudentName').select2({
      theme : "bootstrap"
   });
   $('#searchStudentEmail').select2({
      theme : "bootstrap"
   }); 
   $('#searchStatus').select2({
      theme : "bootstrap"
   }); 

   (function($) {
      $(document).ready(function() {
   		ckeditor();
   	});
   })(jQuery);

   $("#listing").DataTable({
      responsive: true,
      autoWidth: false,
      order: [],
      columnDefs: [{ 
         'orderable': false, 'targets': [4,5]
      }],
      oLanguage: {
         sEmptyTable: "{{__('No data available in table')}}",
         sZeroRecords: "{{__('No records')}}",
         sSearch: '<em class="fas fa-search"></em>',
         sLengthMenu: '_MENU_ {{__("records per page")}}',
         sInfo: '{{_("Showing")}} _START_ {{_("to")}} _END_ {{_("of")}} _TOTAL_ {{_("entries")}}',
         zeroRecords: "{{__('No records')}}",
         infoEmpty: "{{__('No records')}}",
         infoFiltered: '(filtered from MAX total records)',
         oPaginate: {
            sNext: '<em class="fa fa-caret-right"></em>',
            sPrevious: '<em class="fa fa-caret-left"></em>'
         }
      }
   });

   $(document).ready(function(){
   	$('#createFrm').validate({
   	   rules: {
   	    	insName: {
   	        	required: true
   	      },
            insCoordName: {
               required: true
            },
   	      name: {
   	        	required: true
   	      },
   	      email: {
   	        	required: true,
   	        	valid_email:true
   	      },
   	      description: {
   	        	required: true
   	      },
   	      status: {
   	        	required: true
   	      }
   	   },messages: {
   	    	insName: {
   	        	required: "Please select institution"
   	      },
            insCoordName: {
               required: "Please select institute Coordinator"
            },
   	      name: {
   	        	required: "Please enter name"
   	      },
   	      email: {
   	        	required: "Please enter email"
   	      },
   	      description: {
   	        	required: "Please enter description"
   	      },
   	      status: {
   	      	required: "Please select status"
   	      }
   	    }
     	});   

     	$(document).on("click", ".delete", function(){
         var id = $(this).attr('id');
         Swal.fire({
            title: 'Are you sure you want to delete this Student?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
         }).then((result) => {
            if (result.value) {
            	window.location.replace(getsiteurl()+'/admin/student/delete/'+id);
            }
         });
      });

      $(document).on("change", ".status", function(){
         var id = $(this).data('id');
         var _token   = $('meta[name="csrf-token"]').attr('content');
         var statusValue = $(this).prop('checked');
         Swal.fire({
            title: 'Are you sure you want to change status of this Student?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
         }).then((result) => {
            if (result.value) {
               $.ajax({
                  url: "{{route('updateStudentStatus')}}",
                  type: 'POST',
                  data: { id: id , _token: _token, statusValue: statusValue}, 
                  dataType: "json",
                  success: function(data){
                     console.log(data.flag);
                     if(data.flag == 0){
                        Swal.fire({
                           title: 'You can not deactivate this Student. Dependency Found!',
                           type: 'warning',
                        }).then((result) => {
                           window.location.replace('{{route("student.index")}}');
                        });
                     }else{
                        Swal.fire({
                           title: 'Student Status successfully Changed!',
                           type: 'Success',
                        }).then((result) => {
                           window.location.replace('{{route("student.index")}}');
                        });
                     }
                  }
               });
            }else{
               window.location.replace('{{route("student.index")}}');
            }
         });
      });
   });

   $("#insName").change(function(){
      var insId = $(this).val();
      $.ajax({
         url: "{{route('getInstituteCoordinators')}}",
         type: 'GET',
         data: { insId: insId}, 
         dataType: "json",
         success: function(data){
            $('#insCoordName').empty();
            $('#insCoordName').append(`<option value="">Select</option>`);
            $.each(data, function(key, value) {
               $('#insCoordName').append(`<option value="`+value.id+`">`+value.name+`</option>`); 
            });
         }
      });
   });
   $(document).on("click", ".view", function(){
      var id = $(this).attr('id');
      $.ajax({

                url: getsiteurl()+'/admin/student/show/'+id,
                type: 'GET',
                success: function(oData){
                $('#modal-body-student').html(oData.html);
                ckeditor()
                $("#myModal").modal('show');

               },
            });
   });
   function ckeditor(){
      CKEDITOR.editorConfig = function(config) {
               config.language = 'es';
               config.uiColor = '#F7B42C';
               config.height = 300;
               config.toolbarCanCollapse = true;

           };
           CKEDITOR.replace('description');
           CKEDITOR.replace('spaDescription');
   }
</script>