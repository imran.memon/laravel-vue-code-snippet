<div class="page-header row no-gutters">
     <div class="col-12 mb-4">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                        <div class="row">
                            <div class="col">
                                
                                   
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="Name">{{'Institute Name'}}</label>
                                            <input type="text" name="insName" class="form-control" id="insName" placeholder="insName" value="{{$student->instituteName}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="Name">{{'Institute Co-Ordinator Name'}}</label>
                                            <input type="text" name="insCoordName" class="form-control" id="insCoordName" placeholder="insCoordName" value="{{$student->instituteCoordName}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="Email">{{'Student Name'}}</label>
                                            <input type="name" name="name" class="form-control" id="name" placeholder="Name" value="{{$student->name}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="code">{{'Student Email'}}</label>
                                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{$student->email}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="description">{{'Description'}}</label>
                                            @if(Cookie::get('locale'.Auth::user()->id)=="english")
                                                <textarea name="description" class="form-control" id="description"
                                                placeholder="{{'Description'}}" readonly>{{!empty($student->description) ? $student->description:''}}</textarea> 
                                            @else
                                                <textarea name="description" class="form-control" id="description"
                                                placeholder="{{'Description'}}" readonly>{{!empty($student->userEs->description) ? $student->userEs->description:''}}</textarea>
                                            @endif
                                            @if ($errors->has('description'))
                                                <span class="helpBlock">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="status">{{'Status'}}</label>
                                            <select name="status" id="status" class="form-control" readonly>
                                                <option value="">Select</option>
                                                @foreach(config('constant.status') as $key => $value)
                                                    @if(isset($student) && $student->status == $key)
                                                    <option value="{{$key}}" selected="selected">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                  
                            </div>
                        </div>
                    </li>
                </ul>
            
        </div>



    </div>
    <!-- End Page Header -->
    <!-- Transaction History Table -->

@section('page_level_css') 
@endsection
@section('page_level_js')
    @include('admin.student.studentJs')
@endsection
