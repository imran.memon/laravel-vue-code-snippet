@extends('admin.layouts.app')
@section('title')
    <title>{{'Course Booking'}}</title>
@endsection

@section('breadcrumbs')
  <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
    <li class="breadcrumb-item active"><a href="#">Course Booking</a></li>
  </ol>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header"> 
                           <b>{{'Course Booking'}}</b> 
                        </div>
                        <div class="card-body">
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
                                    {{ session()->get('success') }} 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
                                    {{ session()->get('error') }} 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif

                            <table class="table table-striped table-bordered datatable">
                                <thead>
                                    <tr>
                                        <th>{{'No'}}</th>
                                        <th>{{'Course Name'}}</th>
                                        <th>{{'Session Name'}}</th>
                                        <th>{{'Session Date'}}</th>
                                        <th>{{'Time Slot'}}</th>
                                        <th>{{'Action'}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($courses))
                                        @foreach ($courses as $index => $data)
                                            <tr>
                                                <td>{{$index+1}}</td>
                                                <td>{{$data->courseTitle}}</td>
                                                <td>{{$data->sessionTitle}}</td>
                                                <td>{{$data->date}}</td>
                                                <td>{{$data->slotTimeFrom}} - {{$data->SlotTimeTo}}</td>
                                                <td>
                                                    @if ($data->paymentStatus == 'succeeded')
                                                        <span class="badge badge-success">Booked</span>
                                                    @else
                                                    {{-- <a href="/admin/course/checkout/{!! $data->courseId !!}" class="btn btn-info"> --}}
                                                    <a href="{{url("admin/course/checkout/$data->courseId")}}" class="btn btn-info">
                                                        Book Now
                                                    </a>
                                                    @endif
                                                </td>  
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{-- 
@section('main_content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/instituteAdmin.css') }}">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
            <h3 class="page-title">{{'Course Booking'}}</h3>
        </div>
    </div>

    
    

@endsection --}}
@section('page_level_css')
@endsection
@section('page_level_js')
    <script type="text/javascript">
        // $(document).ready(function(){
        //     $("#listing1").DataTable({
        //         responsive: true,
        //         autoWidth: false,
        //         order: [],
        //         columnDefs: [{ 
        //             'orderable': false, 'targets': [2]
        //         }],
        //         oLanguage: {
        //             sEmptyTable: "{{__('No data available in table')}}",
        //             sZeroRecords: "{{__('No records')}}",
        //             sSearch: '<em class="fas fa-search"></em>',
        //             sLengthMenu: '_MENU_ {{__("records per page")}}',
        //             sInfo: '{{_("Showing")}} _START_ {{_("to")}} _END_ {{_("of")}} _TOTAL_ {{_("entries")}}',
        //             zeroRecords: "{{__('No records')}}",
        //             infoEmpty: "{{__('No records')}}",
        //             infoFiltered: '(filtered from MAX total records)',
        //             oPaginate: {
        //                 sNext: '<em class="fa fa-caret-right"></em>',
        //                 sPrevious: '<em class="fa fa-caret-left"></em>'
        //             }
        //         }
        //     });
        // });
    </script>
@endsection
