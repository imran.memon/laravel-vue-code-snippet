@extends('admin.final')

@section('main_content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/instituteAdmin.css') }}">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
            <h3 class="page-title">{{'Course Booking'}}</h3>
        </div>
    </div>

    
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
            {{ session()->get('success') }} 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
            {{ session()->get('error') }} 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif

    <table class="transaction-history d-none" id="listing1">
        <thead>
            <tr>
                <th>{{'Id'}}</th>
                <th>{{'Course Name'}}</th>
                <th>{{'Session Name'}}</th>
                <th>{{'Session Date'}}</th>
                <th>{{'Time Slot'}}</th>
                <th>{{'Action'}}</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($courseBooking))
                @foreach ($courseBooking as $index => $data)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$data->courseTitle}}</td>
                        <td>{{$data->sessionTitle}}</td>
                        <td>{{$data->date}}</td>
                        <td>{{$data->slotTimeFrom}} - {{$data->SlotTimeTo}}</td>
                        <td>
                            <a href="#" class="btn btn-info">
                                Book
                            </a>
                        </td>  
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>

@endsection
@section('page_level_css')
    
@endsection
@section('page_level_js')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#listing1").DataTable({
                responsive: true,
                autoWidth: false,
                order: [],
                columnDefs: [{ 
                    'orderable': false, 'targets': [2]
                }],
                oLanguage: {
                    sEmptyTable: "{{__('No data available in table')}}",
                    sZeroRecords: "{{__('No records')}}",
                    sSearch: '<em class="fas fa-search"></em>',
                    sLengthMenu: '_MENU_ {{__("records per page")}}',
                    sInfo: '{{_("Showing")}} _START_ {{_("to")}} _END_ {{_("of")}} _TOTAL_ {{_("entries")}}',
                    zeroRecords: "{{__('No records')}}",
                    infoEmpty: "{{__('No records')}}",
                    infoFiltered: '(filtered from MAX total records)',
                    oPaginate: {
                        sNext: '<em class="fa fa-caret-right"></em>',
                        sPrevious: '<em class="fa fa-caret-left"></em>'
                    }
                }
            });
        });
    </script>
@endsection
