@extends('admin.final')

@section('main_content')
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">{{'Dashboard'}}</span>
            <h3 class="page-title">
               {{'Student'}} 
            </h3>
        </div>

        <div class="col-12 mb-4 mt-3">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <h6 class="m-0">{{'Edit Student'}}</h6>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                        <div class="row">
                            <div class="col">
                                <form method="post" 
                                    action="{{ route('student.update',@$student->id)}}" id="createFrmas" name="createFrm" >
                                    @csrf
                                    <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>{{'Institution Name'}}<span class="text text-danger">*</span></label>
                                        <select name="insName" id="insName" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($institutions as $institution)
                                                @if(isset($student) && $student->institute_id == $institution->id)
                                                    <option value="{{$institution->id}}" selected="selected">
                                                        {{$institution->name}}
                                                    </option>
                                                @else
                                                    <option value="{{$institution->id}}">
                                                        {{$institution->name}}
                                                    </option> 
                                                @endif
                                            @endforeach
                                        </select>

                                        @if ($errors->has('insName'))
                                            <span class="text text-danger">
                                                <strong>{{'The institution name field is required.'}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="Name">{{'Student Name'}}</label>
                                            <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{$student->name}}">
                                            @if ($errors->has('name'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="Email">{{'Student Email'}}</label>
                                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{$student->email}}">
                                            @if ($errors->has('email'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="description">{{'Description'}}</label>
                                            <textarea name="description" class="form-control" id="description"
                                                placeholder="{{'Description'}}">{{!empty($student->description) ? $student->description:''}}</textarea>
                                            @if ($errors->has('description'))
                                                <span class="helpBlock">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="status">{{'Status'}}</label>
                                            <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                                @foreach(config('constant.status') as $key => $value)
                                                    @if(isset($student) && $student->status == $key)
                                                    <option value="{{$key}}" selected="selected">{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                            @if ($errors->has('status'))
                                                <span class="helpBlock alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">{{'Save'}}</button>
                                    <a href="{{ route('student.index') }}" class="btn btn-warning">{{'Back'}}</a>
                                </form>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Transaction History Table -->


@endsection
@section('page_level_css') 
@endsection
@section('page_level_js')

@include('admin.student.studentJs')
@endsection
