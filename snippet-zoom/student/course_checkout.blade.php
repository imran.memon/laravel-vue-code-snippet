@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header"><strong>Checkout</strong></div>
                        <div class="card-body">
                            @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
                                    {{ session()->get('success') }} 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
                                    {{ session()->get('error') }} 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif
                            <form action="/admin/charge" method="post" id="payment-form">
                                @csrf
                                <input type="hidden" name="course_id" value="{!! $course->courseId !!}">
                                <input type="hidden" name="amount" value="5">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="name">Name on Card</label>
                                            <input class="form-control" name="name" id="name" type="text" placeholder="Name on Card">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input name="email" id="email" type="email" class="form-control" placeholder="Email" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="email">Phone</label>
                                            <input name="phone" id="phone" type="text" class="form-control" placeholder="Phone" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <input name="address" id="address" class="form-control" placeholder="Address">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="address">City</label>
                                            <input name="city" id="city" class="form-control" placeholder="City">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="address">State</label>
                                            <input name="state" id="state" class="form-control" placeholder="State">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="address">Country</label>
                                            {{-- <input name="country" id="country" class="form-control" placeholder="Country"> --}}
                                            <select name="country" id="country" class="form-control">
                                                <option value="">Select Country</option>
                                                @foreach ($countries as $country)
                                                    <option value="{!! $country->code !!}">{!! $country->name !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <p>Enter Your Card Details Below:</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="card-element" class="form-control"></div>
                                        <div id="card-errors" role="alert"></div>
                                    </div>
                                </div>
                                <div class="row align-items-center mt-5">
                                    <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                                        <button class="btn btn-block btn-primary" type="submits">Make Payment</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-header"><strong>Order Detail</strong></div>
                        <div class="card-body">
                            {{-- <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span class="text-muted">Order Detail</span>
                                <span class="badge badge-secondary badge-pill">1</span>
                            </h4> --}}
                            <ul class="list-group mb-3">
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">Institute Name</h6>
                                        <small class="text-muted">{!! $course->instituteName !!}</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">Institute Co-ordinator Name</h6>
                                        <small class="text-muted">{!! $course->coordinatorName !!}</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0">Course name</h6>
                                    <small class="text-muted">{!! $course->courseTitle !!}</small>
                                </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0">Session Name</h6>
                                    <small class="text-muted">{!! $course->sessionTitle !!}</small>
                                </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0">Session Date</h6>
                                    <small class="text-muted">{!! $course->date !!}</small>
                                </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">Time Slot</h6>
                                        <small class="text-muted">{{$course->slotTimeFrom}} - {{$course->SlotTimeTo}}</small>
                                    </div>
                                </li>
                                {{-- <li class="list-group-item d-flex justify-content-between bg-light">
                                <div class="text-success">
                                    <h6 class="my-0">Promo code</h6>
                                    <small>EXAMPLECODE</small>
                                </div>
                                <span class="text-success">-$5</span>
                                </li> --}}
                                <li class="list-group-item d-flex justify-content-between">
                                <span>Total (AUD)</span>
                                <strong>$20</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_js')
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script>
    $('#payment-form').validate({
        rules: {
            name: {
                required: true
            },
            course_id: {
                required: true
            },
            email: {
                required: true
            },
            phone: {
                required: true
            },
            address: {
                required: true
            },
            city: {
                required: true
            },
            state: {
                required: true
            }
        },messages: {
            course_id: {
                required: "Invalid Course"
            },
            status: {
                // required: "Please select Status"
            }
        }
    }); 
    // Create a Stripe client.
    var stripe = Stripe('pk_test_51HaXUoHdglcuJnklrQqj3dtwuvMRtTh8q2RWZYh37rOq5GZKFfFr5BEU1bYHWWIEEPcoRM5oQFMt1PonFb9EffCd00qRm3EAcz');

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
    base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
        color: '#aab7c4'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.on('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
        displayError.textContent = event.error.message;
    } else {
        displayError.textContent = '';
    }
    });

    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
    event.preventDefault();

    stripe.createToken(card).then(function(result) {
        if (result.error) {
        // Inform the user if there was an error.
        var errorElement = document.getElementById('card-errors');
        errorElement.textContent = result.error.message;
        } else {
        // Send the token to your server.
        stripeTokenHandler(result.token);
        }
    });
    });

    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
    }

</script>
@endsection
