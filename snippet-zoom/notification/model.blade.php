<div class="page-header row no-gutters">
     <div class="col-12 mb-4">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                        <div class="row">
                            <div class="col">
                               
                                   	<div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label>{{'Institutions'}}<span class="text text-danger">*</span></label>
                                            <select name="insName" id="insName" class="form-control" readonly>
                                                <option value="">Select</option>
                                                @foreach($institutions as $institute)
                                                    @if($notification->institute_id == $institute->id)
                                                        <option value="{{$institute->id}}" selected="selected">
                                                            {{$institute->name}}
                                                        </option>
                                                    @else
                                                        <option value="{{$institute->id}}">
                                                            {{$institute->name}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label>{{'Course'}}<span class="text text-danger">*</span></label>
                                            <select name="courseTitle" id="courseTitle" class="form-control" readonly>
                                                <option value="">Select</option>
                                                @foreach($courses as $course)
                                                    @if($notification->course_id == $course->id)
                                                        <option value="{{$course->id}}" selected="selected">
                                                            {{$course->title}}
                                                        </option>
                                                    @else
                                                        <option value="{{$course->id}}">
                                                            {{$course->title}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label>{{'Template'}}<span class="text text-danger">*</span></label>
                                            <select name="templateName" id="templateName" class="form-control" readonly>
                                                <option value="">Select</option>
                                                @foreach($templates as $template)
                                                    @if($notification->template_id == $template->id)
                                                        <option value="{{$template->id}}" selected="selected">
                                                            {{$template->template_title}}
                                                        </option>
                                                    @else
                                                        <option value="{{$template->id}}">
                                                            {{$template->template_title}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                </div>
                        </div>
                    </li>
                </ul>
            
        </div>



    </div>
    <!-- End Page Header -->
    <!-- Transaction History Table -->

@section('page_level_css') 
@endsection
@section('page_level_js')
    @include('admin.notification.notificationJs')
@endsection
