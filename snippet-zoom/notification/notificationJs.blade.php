<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
$(document).ready(function(){
	$('#createFrm').validate({
	   rules: {
	    	insName: {
	        	required: true
	      },
	      courseTitle: {
	        	required: true
	      },
	      templateName: {
	        	required: true,
	      }
	   },messages: {
	    	insName: {
	        	required: "Please select Institution."
	      },
	      courseTitle: {
	        	required: "Please select Course."
	      },
	      templateName: {
	        	required: "Please select Template."
	      }
	    }
  	});

   $('#searchCourseTitle').select2({
      theme : "bootstrap"
   });
   $('#searchCourseCode').select2({
      theme : "bootstrap"
   });
   $('#searchInstituteName').select2({
      theme : "bootstrap"
   });
   $('#searchEmailTemplate').select2({
      theme : "bootstrap"
   });
   $('#searchInstituteCoord').select2({
      theme : "bootstrap"
   });  
   
   $("#listing").DataTable({
      responsive: true,
      autoWidth: false,
      order: [],
      columnDefs: [{ 
         'orderable': false, 'targets': [4]
      }],
      oLanguage: {
         sEmptyTable: "{{__('No data available in table')}}",
         sZeroRecords: "{{__('No records')}}",
         sSearch: '<em class="fas fa-search"></em>',
         sLengthMenu: '_MENU_ {{__("records per page")}}',
         sInfo: '{{_("Showing")}} _START_ {{_("to")}} _END_ {{_("of")}} _TOTAL_ {{_("entries")}}',
         zeroRecords: "{{__('No records')}}",
         infoEmpty: "{{__('No records')}}",
         infoFiltered: '(filtered from MAX total records)',
         oPaginate: {
            sNext: '<em class="fa fa-caret-right"></em>',
            sPrevious: '<em class="fa fa-caret-left"></em>'
         }
      }
   });

   $("#insName").change(function(){
      var insId = $(this).val();
      $.ajax({
         url: "{{route('getInstituteCourse')}}",
         type: 'GET',
         data: { insId: insId}, 
         dataType: "json",
         success: function(data){
            $('#courseTitle').empty();
            $('#courseTitle').append(`<option value="">Select</option>`);
            $.each(data, function(key, value) {
               $('#courseTitle').append(`<option value="`+value.id+`">`+value.title+`</option>`); 
            });
         }
      });
   });

   $(document).on("click", ".delete", function(){
      var id = $(this).attr('id');
     // alert(id);
      Swal.fire({
         title: 'Are you sure you want to delete this notification?',
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes'
      }).then((result) => {
         if (result.value) {
         	window.location.replace(getsiteurl()+'/admin/delete/notification/'+id);
         }
      });
   });

   $("#insName").change(function(){
      var insId = $(this).val();
      $.ajax({
         url: "{{route('getInstituteCoordinators')}}",
         type: 'GET',
         data: { insId: insId}, 
         dataType: "json",
         success: function(data){
            $('#insCoordName').empty();
            $('#insCoordName').append(`<option value="">Select</option>`);
            $.each(data, function(key, value) {
               console.log("here" + value.id);
               $('#insCoordName').append(`<option value="`+value.id+`">`+value.name+`</option>`); 
            });
         }
      });
   });

   $(document).on("click", ".view", function(){
      var id = $(this).attr('id');
      $.ajax({

                url: getsiteurl()+'/admin/show/notification/'+id,
                type: 'GET',
                success: function(oData){
                $('#modal-body-notification').html(oData.html);
                $("#myModal").modal('show');

               },
            });
   });

});

</script>