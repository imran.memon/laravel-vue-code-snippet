@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="accordion mb-3" id="accordionExample">
                <div class="card rounded">
                    <div class="card-header collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo">
                        <h4 class="mb-0"><b>{{'Search'}}</b><i class="fa fa-angle-down" style="float:right;"></i></h4>
                    </div>
                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                        data-parent="#accordionExample">
                        <form method="post"
                        action="{{route('notification')}}">
                         @csrf
                        <div class="card-body">
                           
                               @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Course Title'}}:</label>
                                                <select name="searchCourseTitle" id="searchCourseTitle" class="form-control">
                                                    <option value="">Select</option>
                                                    @foreach($courseData as $course)
                                                        @if(@$searchCourseTitle == @$course->title)
                                                            <option value="{{@$course->title}}" selected>{{@$course->title}}</option>
                                                        @else
                                                            <option value="{{@$course->title}}">{{@$course->title}}</option>
                                                        @endif
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Course Code'}}:</label>
                                                <select name="searchCourseCode" id="searchCourseCode" class="form-control">
                                                    <option value="">Select</option>
                                                    @foreach($courseData as $course)
                                                        @if(@$searchCourseCode == @$course->code)
                                                            <option value="{{@$course->code}}" selected>{{@$course->code}}</option>
                                                        @else
                                                            <option value="{{@$course->code}}">{{@$course->code}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Institute Name'}}</label>
                                                 <select name="searchInstituteName" id="searchInstituteName" class="form-control">
                                                    <option value="">Select</option>
                                                    @foreach($instituteData as $value)
                                                        @if(@$searchInstituteName == @$value->name)
                                                            <option value="{{@$value->name}}" selected>{{@$value->name}}</option>
                                                        @else
                                                            <option value="{{@$value->name}}">{{@$value->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Institution Coordinator'}}</label>
                                                 <select name="searchInstituteCoord" id="searchInstituteCoord" class="form-control">
                                                    <option value="">Select</option>
                                                    @foreach(@$instituteCoOrdData as  $value)
                                                        @if(@$searchInstituteCoord == $value->id)
                                                            <option value="{{@$value->id}}" selected>{{@$value->name}}</option>
                                                        @else
                                                            <option value="{{@$value->id}}">{{@$value->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">{{'Search Email Template'}}</label>
                                                 <select name="searchEmailTemplate" id="searchEmailTemplate" class="form-control">
                                                    <option value="">Select</option>
                                                    @foreach($templateData as $value)
                                                        <option value="{{@$value->template_title}}">{{@$value->template_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <button type="submit" name="search_submit" value="1"
                                class="btn btn-primary">{{'Search'}}</button>
                            <a href="{{route('notification')}}"
                                class="btn btn-dark">{{'Reset'}}</a>
                            </div>
                        </div>
                        
                    </form>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="card-header-actions">

                        <a href="{{ route('createNotification') }}"
                            class="btn btn-primary student_btn">
                            <b><i class="fa fa-plus-circle" aria-hidden="true"></i> {{'Add'}}</b>
                        </a>
                        
                    </div>
                </div>
                <div class="card-body">
                     @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show m-0" role="alert">
                            {{ session()->get('success') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
                            {{ session()->get('error') }} 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        
                    @endif
                    <table class="table table-striped table-bordered" id="listing">
                        <thead>
                            <tr>
                                <th>{{'Id'}}</th>
                                <th>{{'Institute Name'}}</th>
                                <th>{{'Institute Co-Ordinator Name'}}</th>
                                <th>{{'Course Name'}}</th>
                                <th>{{'Email Template'}}</th>
                                <th>{{'Actions'}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($notifications))
                            @php
                                $i=1;
                            @endphp
                            @foreach ($notifications as $notification)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{@$notification->InsName}}</td>
                                    <td>{{@$notification->coordName}}</td>
                                    <td>{{@$notification->courseTitle}}</td>
                                    <td>{{@$notification->tempTitle}}</td>
                                    <td>
                                        <a class="btn btn-success" href="{{route('editNotification',@$notification->notificationId)}}" data-toggle="tooltip" data-original-title="{{'Edit'}}">
                                        <svg class="c-icon">
                                            <use xlink:href="{{ asset('icons/free.svg#cil-pencil') }}"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info view" href="javascript:void(0);" id="{{$notification->notificationId}}" data-toggle="tooltip" data-original-title="{{'View'}}">
                                        <svg class="c-icon">
                                            <use xlink:href="{{ asset('icons/free.svg#cil-description') }}"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger delete" href="javascript:void(0);" id="{{$notification->notificationId}}" data-toggle="tooltip" data-original-title="{{'Delete'}}">
                                        <svg class="c-icon">
                                            <use xlink:href="{{ asset('icons/free.svg#cil-trash') }}"></use>
                                        </svg>
                                    </a>

                                        
                                    </td>
                                </tr>
                                @php
                                    $i=$i+1;
                                @endphp
                            @endforeach
                        @endif
                            
                        </tbody>
                    </table>
                <div class="modal" tabindex="-1" role="dialog" id="myModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-titlepage-title">{{'Notification'}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="padding-top: 5px; padding-bottom: 1px;" id="modal-body-notification">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">{{'Back'}}</button>
          </div>
        </div>
      </div>
    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_css')
    
@endsection
@section('page_level_js')
    
    @include('admin.notification.notificationJs')
@endsection
