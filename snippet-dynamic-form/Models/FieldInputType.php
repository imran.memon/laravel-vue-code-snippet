<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FieldInputType extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'field_input_type_id';
    protected $table = 'field_input_type';

    protected $fillable = [
        'field_input_type_name', 'field_input_type', 'field_input_type_status'
    ];
}
