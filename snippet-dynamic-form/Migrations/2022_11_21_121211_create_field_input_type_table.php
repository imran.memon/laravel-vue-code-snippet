<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_input_type', function (Blueprint $table) {
            $table->id('field_input_type_id');
            $table->string('field_input_type_name', 30);
            $table->string('field_input_type', 30);
            $table->enum('field_input_type_status', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_input_type');
    }
};
